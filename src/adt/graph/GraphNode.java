package adt.graph;

import java.util.*;

public class GraphNode {
	public int index;
	public ArrayList<GraphNode> neighbors;
	
	public GraphNode(int x) {
		index = x;
		neighbors = new ArrayList<GraphNode>();
	}
	
	public void addNeighbors(GraphNode[] nodes) {
		for (int i = 0; i < nodes.length; i++) {
			neighbors.add(nodes[i]);
		}
	}
	
	public void printDFS() {
		Set<GraphNode> set = new HashSet<GraphNode>();
		dfs(this, set);
		System.out.println();
	}
	
	private void dfs(GraphNode node, Set<GraphNode> set) {
		if (node == null) {
			return;
		}
		
		if (set.contains(node)) {
			return;
		}
		
		System.out.format("%d ", node.index);
		set.add(node);
		
		for (int i = 0; i < node.neighbors.size(); i++) {
			GraphNode neighbor = node.neighbors.get(i);
			dfs(neighbor, set);
		}
	}
}
