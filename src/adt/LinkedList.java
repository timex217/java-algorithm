package adt;

import java.util.*;

import adt.ListNode;

/**
 * LinkedList
 */
public class LinkedList {
	
	public ListNode head;
	public ListNode tail;
	
	public LinkedList() {
		head = null;
		tail = null;
	}
	
	public LinkedList(int n) {
		head = null;
		tail = null;
		
		// initialize
		for (int i = 1; i <= n; i++) {
			append(i);
		}
		
		print();
	}
	
	public LinkedList(int[] arr) {
		head = null;
		tail = null;
		
		append(arr);
		
		print();
	}
	
	/**
	 * Insert new node to the end
	 */
	public ListNode append(int x) {
		ListNode node = new ListNode(x);
		
		if (head == null) {
			head = node;
			tail = node;
		} else {
			tail.next = node;
			tail = node;
		}
		
		return node;
	}
	
	/**
	 * 
	 */
	public void append(int[] arr) {
		if (arr == null) {
			return;
		}
		
		for (int i = 0; i < arr.length; i++) {
			append(arr[i]);
		}
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		ListNode p = head;
		while (p != null) {
			sb.append(p.val);
			p = p.next;
			
			if (p != null) {
				sb.append(" -> ");
			}
		}
		
		return sb.toString();
	}
	
	public void print() {
		System.out.println(this.toString());
	}
	
	// ---------------------------
	//  Merge Sort
	// ---------------------------
	
	public void mergeSort(boolean ascending) {
		ListNode h = head;
		head = mergeSort(h, ascending);
		print();
	}
	
	private ListNode mergeSort(ListNode h, boolean ascending) {
		if (h == null || h.next == null) {
			return h;
		}
		
		// find the middle node
		ListNode prev = null;
		ListNode slow = h;
		ListNode fast = h;
		
		while (fast != null && fast.next != null) {
			prev = slow;
			slow = slow.next;
			fast = fast.next.next;
		}
		
		prev.next = null;
		
		ListNode left = mergeSort(h, ascending);
		ListNode right = mergeSort(slow, ascending);
		
		return mergeLists(left, right, ascending);
	}
	
	private ListNode mergeLists(ListNode l1, ListNode l2, boolean ascending) {
		ListNode fakeHead = new ListNode();
		ListNode p = fakeHead;
		
		ListNode a = l1, b = l2;
		
		if (ascending) {
			while (a != null && b != null) {
				if (a.val < b.val) {
					p.next = a;
					p = a;
					a = a.next;
				} else {
					p.next = b;
					p = b;
					b = b.next;
				}
			}
		} else {
			while (a != null && b != null) {
				if (a.val > b.val) {
					p.next = a;
					p = a;
					a = a.next;
				} else {
					p.next = b;
					p = b;
					b = b.next;
				}
			}
		}
		
		while (a != null) {
			p.next = a;
			p = a;
			a = a.next;
		}
		
		while (b != null) {
			p.next = b;
			p = b;
			b = b.next;
		}
		
		p.next = null;
		return fakeHead.next;
	}
	
}
