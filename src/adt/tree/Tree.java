package adt.tree;

import adt.tree.TreeNode;

public class Tree {
	
	/**
	 * Tree 1:
	 *               1
	 *             /   \
	 *           2      3
	 *          / \    / \
	 *         4   5  6   7
	 */
	public static TreeNode tree1() {
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		
		root.left.left = new TreeNode(4);
		root.left.right = new TreeNode(5);
		
		root.right = new TreeNode(3);
		root.right.left = new TreeNode(6);
		root.right.right = new TreeNode(7);
		
		return root;
	}
	
	/**
	 * Tree 2:
	 *              10
	 *            /    \
	 *           8      2
	 *          / \    / 
	 *         3   5  2
	 */
	public static TreeNode tree2() {
		TreeNode root = new TreeNode(10);
		root.left = new TreeNode(8);
		
		root.left.left = new TreeNode(3);
		root.left.right = new TreeNode(5);
		
		root.right = new TreeNode(2);
		root.right.left = new TreeNode(2);
		
		return root;
	}
	
	/**
	 * Tree 3:
	 *              50
	 *            /    \
	 *           7      2
	 *          / \    / \ 
	 *         3   5  1  30
	 */
	public static TreeNode tree3() {
		TreeNode root = new TreeNode(50);
		
		root.left = new TreeNode(7);
		root.left.left = new TreeNode(3);
		root.left.right = new TreeNode(5);
		
		root.right = new TreeNode(2);
		root.right.left = new TreeNode(1);
		root.right.right = new TreeNode(30);
		
		return root;
	}
	
	/**
	 * Tree 4:
	 *              7
	 *             / \
	 *            3   5
	 */
	public static TreeNode tree4() {
		TreeNode root = new TreeNode(7);
		
		root.left = new TreeNode(3);
		root.right = new TreeNode(5);
		
		return root;
	}
	
	/**
	 * BST 1:
	 *               10
	 *             /    \
	 *           5       15
	 *          / \     /  \
	 *         1   8   12  17
	 */
	public static TreeNode bst1() {
		TreeNode root = new TreeNode(10);
		root.left = new TreeNode(5);
		root.left.left = new TreeNode(1);
		root.left.right = new TreeNode(8);
		
		root.right = new TreeNode(15);
		root.right.left = new TreeNode(12);
		root.right.right = new TreeNode(17);
		
		return root;
	}
	
	/**
	 * BST 2:
	 *              20
	 *             /  \
	 *            8    22
	 *           / \    \
	 *          4  12    25
	 *             / \
	 *           10   14
	 */
	public static TreeNode bst2() {
		TreeNode root = new TreeNode(20);
		
		root.left = new TreeNode(8);
		root.left.left = new TreeNode(4);
		root.left.right = new TreeNode(12);
		
		root.right = new TreeNode(22);
		root.left.right.left = new TreeNode(10);
		root.left.right.right = new TreeNode(14);
		
		root.right.right = new TreeNode(25);
		
		return root;
	}
	
	/**
	 * SumTree:
	 * 
	 *           26
	 *         /   \
	 *       10     3
	 *     /    \     \
	 *   4      6      3
	 */
	public static TreeNode sumtree() {
		TreeNode root = new TreeNode(26);
		
		root.left = new TreeNode(10);
		root.left.left = new TreeNode(4);
		root.left.right = new TreeNode(6);
		
		root.right = new TreeNode(3);
		root.right.right = new TreeNode(3);
		
		return root;
	}
	
	/**
	 * Tree 5:
	 * 
	 *           10
     *         /    \
     *       -2      6
     *      /  \    / \ 
     *     8   -4  7   5
     */
	public static TreeNode tree5() {
		TreeNode root = new TreeNode(10);
		
		root.left = new TreeNode(-2);
		root.left.left = new TreeNode(8);
		root.left.right = new TreeNode(-4);
		
		root.right = new TreeNode(6);
		root.right.left = new TreeNode(7);
		root.right.right = new TreeNode(5);
		
		return root;
	}
	
	/**
	 * Tree 6:
	 * 
	 *        50
	 *      /    \
	 *   30       60
	 *  /  \     /  \ 
	 * 5   20   45    70
	 *               /  \
	 *             65    80
	 *            / \
	 *           90  17 
	 */
	public static TreeNode tree6() {
		TreeNode root = new TreeNode(50);
		
		root.left = new TreeNode(30);
		root.left.left = new TreeNode(5);
		root.left.right = new TreeNode(20);
		
		root.right = new TreeNode(60);
		root.right.left = new TreeNode(45);
		root.right.right = new TreeNode(70);
		root.right.right.left = new TreeNode(65);
		root.right.right.right = new TreeNode(80);
		root.right.right.left.left = new TreeNode(90);
		root.right.right.left.right = new TreeNode(17);
		
		return root;
	}
	
	/**
	 * Tree 7:
	 * 
	 *          10
	 *         /  \
	 *        5    8
	 *       / \
	 *      2   20
	 */
	public static TreeNode tree7() {
		TreeNode root = new TreeNode(10);
		root.left = new TreeNode(5);
		root.left.left = new TreeNode(2);
		root.left.right = new TreeNode(20);
		root.right = new TreeNode(8);
		return root;
	}
	
	/**
	 * Tree 8:
	 * 
	 *                    10
	 *                   /  \
	 *                 20    30
	 *                 / \    \
	 *               40   50   60
	 *                    / \
	 *                  70   80 
	 */
	public static TreeNode tree8() {
		TreeNode root = new TreeNode(10);
		root.left = new TreeNode(20);
		root.right = new TreeNode(30);
		root.right.right = new TreeNode(60);
		root.left.left = new TreeNode(40);
		root.left.right = new TreeNode(50);
		root.left.right.left = new TreeNode(70);
		root.left.right.right = new TreeNode(80);
		return root;
	}
	
	/**
	 * Tree 9:
	 * 
	 *                   6
	 *                 /   \
	 *              -13    14
	 *                \    / \
	 *                -8  13  15
	 *                    /
	 *                   7
	 */
	public static TreeNode tree9() {
		TreeNode root = new TreeNode(6);
		root.left = new TreeNode(-13);
		root.right = new TreeNode(14);
		root.left.right = new TreeNode(-8);
		root.right.left = new TreeNode(13);
		root.right.right = new TreeNode(15);
		root.right.left.left = new TreeNode(7);
		return root;
	}
	
	/**
	 * Tree 10: 
	 * 
	 *           1 
	 *       /      \
	 *      2        3
	 *    /   \     /  \
	 *   4     5   6    7
	 *  / \    /       /
	 * 8   9  12      10
	 *    / \           \
	 *   13  14         11
	 *       / 
	 *      15
	 */
	public static TreeNode tree10() {
		TreeNode root = new TreeNode(1);
		
		root.left = new TreeNode(2);
		root.left.left = new TreeNode(4);
		root.left.left.left = new TreeNode(8);
		root.left.left.right = new TreeNode(9);
		root.left.left.right.left = new TreeNode(13);
		root.left.left.right.right = new TreeNode(14);
		root.left.left.right.right.left = new TreeNode(15);
		root.left.right = new TreeNode(5);
		root.left.right.left = new TreeNode(12);
		
		root.right = new TreeNode(3);
		root.right.left = new TreeNode(6);
		root.right.right = new TreeNode(7);
		root.right.right.left = new TreeNode(10);
		root.right.right.left.right = new TreeNode(11);
		
		return root;
	}
	
	/**
	 * Tree 11:
	 * 
	 *         1
	 *      /     \
	 *     2       3
	 *    / \       \
	 *   4   5       6
	 *  / \         / \
	 * 7   8       9   10
	 */
	public static TreeNode tree11() {
		TreeNode root = new TreeNode(1);
		
		root.left = new TreeNode(2);
		root.left.left = new TreeNode(4);
		root.left.left.left = new TreeNode(7);
		root.left.left.right = new TreeNode(8);
		root.left.right = new TreeNode(5);
		
		root.right = new TreeNode(3);
		root.right.right = new TreeNode(6);
		root.right.right.left = new TreeNode(9);
		root.right.right.right = new TreeNode(10);
		
		return root;
	}
	
	/**
	 * Tree 12:
	 * 
	 *        1
	 *      /   \
	 *     2     3
	 *   /      /  \  
	 *  4      5    6
	 *         \     \
	 *          7     8
	 *         /       \
	 *        9         10
	 */
	public static TreeNode tree12() {
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.left.left = new TreeNode(4);
		
		root.right = new TreeNode(3);
		root.right.left = new TreeNode(5);
		root.right.left.right = new TreeNode(7);
		root.right.left.right.left = new TreeNode(9);
		root.right.right = new TreeNode(6);
		root.right.right.right = new TreeNode(8);
		root.right.right.right.right = new TreeNode(10);
		
		return root;
	}
	
	/**
	 * Tree 13:
	 * 
	 *              6
	 *            /   \
	 *          3      5
	 *         / \      \
	 *        2   5      4  
	 *           / \
	 *          7   4
	 */
	public static TreeNode tree13() {
		TreeNode root = new TreeNode(6);
		
		root.left = new TreeNode(3);
		root.left.left = new TreeNode(2);
		root.left.right = new TreeNode(5);
		root.left.right.left = new TreeNode(7);
		root.left.right.right = new TreeNode(4);
		
		root.right = new TreeNode(5);
		root.right.right = new TreeNode(4);
		
		return root;
	}
	
	/**
	 * Tree 14:
	 *                1
	 *            /      \
	 *           2        3
	 *            \      /
	 *             4    5
	 *                 /
	 *                6
	 */
	public static TreeNode tree14() {
		TreeNode root = new TreeNode(1);
		
		root.left = new TreeNode(2);
		root.left.right = new TreeNode(4);
		
		root.right = new TreeNode(3);
		root.right.left = new TreeNode(5);
		root.right.left.left = new TreeNode(6);
		
		return root;
	}
	
	/**
	 * Tree 15:
	 * 
	 *              1
	 *           /     \
	 *          2       3
	 *         / \     / \
	 *        4   5   6   7
	 *                 \
	 *                  8
	 */
	public static TreeNode tree15() {
		TreeNode root = new TreeNode(1);
		
		root.left = new TreeNode(2);
		root.left.left = new TreeNode(4);
		root.left.right = new TreeNode(5);
		
		root.right = new TreeNode(3);
		root.right.left = new TreeNode(6);
		root.right.right = new TreeNode(7);
		root.right.left.right = new TreeNode(8);
		
		return root;
	}
	
	/**
	 * Tree 16:
	 * 
	 *                     1
	 *               /           \
	 *              2             3
	 *            /   \         /   \
	 *           4     5       6     7
	 *          / \   / \     / \   / \
	 *         8   9 10 11   12 13 14 15
	 */
	public static TreeNode tree16() {
		TreeNode root = new TreeNode(1);
		
		root.left = new TreeNode(2);
		root.left.left = new TreeNode(4);
		root.left.left.left = new TreeNode(8);
		root.left.left.right = new TreeNode(9);
		
		root.left.right = new TreeNode(5);
		root.left.right.left = new TreeNode(10);
		root.left.right.right = new TreeNode(11);
		
		root.right = new TreeNode(3);
		root.right.left = new TreeNode(6);
		root.right.left.left = new TreeNode(12);
		root.right.left.right = new TreeNode(13);
		
		root.right.right = new TreeNode(7);
		root.right.right.left = new TreeNode(14);
		root.right.right.right = new TreeNode(15);
		
		return root;
	}
	
	/**
	 * Tree 17:
	 * 
	 *                     -15
	 *                    /    \
	 *                   5      6
	 *                  / \    / \
	 *                -8   1  3   9
	 *                / \          \
	 *               2   6          0
	 *                             / \
	 *                            4  -1
	 *                               /
	 *                              10
	 */
	public static TreeNode tree17() {
		TreeNode root = new TreeNode(-15);
		
		root.left = new TreeNode(5);
		root.left.left = new TreeNode(-8);
		root.left.left.left = new TreeNode(2);
		root.left.left.right = new TreeNode(6);
		root.left.right = new TreeNode(1);
		
		root.right = new TreeNode(6);
		root.right.left = new TreeNode(3);
		root.right.right = new TreeNode(9);
		root.right.right.right = new TreeNode(0);
		root.right.right.right.left = new TreeNode(4);
		root.right.right.right.right = new TreeNode(-1);
		root.right.right.right.right.left = new TreeNode(10);
		
		return root;
	}
	
	// ----------------------------
	//  Helper
	// ----------------------------
	
	public static TreeNode getTreeNode(TreeNode root, int val) {
		if (root == null) {
			return null;
		}
		
		if (root.val == val) {
			return root;
		}
		
		TreeNode left = getTreeNode(root.left, val);
		TreeNode right = getTreeNode(root.right, val);
		
		return (left != null) ? left : right;
	}
}
