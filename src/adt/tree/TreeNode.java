package adt.tree;

import java.util.*;

public class TreeNode {
	public int val;
	public TreeNode left;
	public TreeNode right;
	
	public TreeNode(int x) {
		this.val = x;
		this.left = null;
		this.right = null;
	}
	
	public void print() {
		levelPrint(this);
	}
	
	private void levelPrint(TreeNode root) {
		if (root == null) {
			System.out.println("Empty Tree");
			return;
		}
		
		Queue<TreeNode> q = new LinkedList<TreeNode>();
		q.add(root);
		q.add(null);
		
		while (!q.isEmpty()) {
			TreeNode node = q.poll();
			
			if (node != null) {
				System.out.format("%d ", node.val);
				
				if (node.left != null) {
					q.add(node.left);
				}
				
				if (node.right != null) {
					q.add(node.right);
				}
			} else {
				System.out.println();
				
				if (!q.isEmpty()) {
					q.add(null);
				}
			}
		}
	}
	
	public int getHeight() {
		TreeNode root = this;
		return getHeightHelper(root);
	}
	
	private int getHeightHelper(TreeNode root) {
		if (root == null) {
			return 0;
		}
		
		return 1 + Math.max(getHeightHelper(root.left), getHeightHelper(root.right));
	}
}
