package adt;

/**
 * ListNode
 */

import java.util.*;

public class ListNode {
	public int val;
	public ListNode next;
	
	public ListNode() {
		this.val = 0;
		this.next = null;
	}
	
	public ListNode(int x) {
		this.val = x;
		this.next = null;
	}
	
	public void print() {
		StringBuilder sb = new StringBuilder();
		
		ListNode p = this;
		while (p != null) {
			sb.append(p.val);
			p = p.next;
			
			if (p != null) {
				sb.append(" -> ");
			}
		}
		
		System.out.println(sb.toString());
	}
}
