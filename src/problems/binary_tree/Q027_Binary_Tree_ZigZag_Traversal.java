package problems.binary_tree;

import java.util.*;

public class Q027_Binary_Tree_ZigZag_Traversal {

	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) { val = x; }
	}
	
	public static void main(String[] args) {
		
	}
	
	// Non-recursion
	public static List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List result = new ArrayList();
        
        if (root == null) return result;
        
        List<Integer> list = new ArrayList<Integer>();
        
        Stack<TreeNode> curr = new Stack<TreeNode>();
        Stack<TreeNode> next = new Stack<TreeNode>();
        
        boolean leftToRight = true;
        
        curr.push(root);
        
        while (!curr.isEmpty()) {
            TreeNode node = curr.pop();
            list.add(node.val);
            
            if (leftToRight) {
                if (node.left  != null) next.push(node.left);
                if (node.right != null) next.push(node.right);
            } else {
                if (node.right != null) next.push(node.right);
                if (node.left  != null) next.push(node.left);
            }
            
            if (curr.isEmpty()) {
                leftToRight = !leftToRight;
                
                // swap curr and next
                Stack<TreeNode> tmp = curr;
                curr = next;
                next = tmp;
                
                result.add(list);
                list = new ArrayList<Integer>();
            }
        }
        
        return result;
    }
	
	public static void visit(TreeNode node) {
		System.out.println(node);
	}

}
