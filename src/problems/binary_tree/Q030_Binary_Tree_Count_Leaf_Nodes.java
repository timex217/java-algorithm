package problems.binary_tree;

import java.util.*;

public class Q030_Binary_Tree_Count_Leaf_Nodes {
	
	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) { val = x; }
	}
	
	public static void main(String[] args) {
		
	}
	
	public static int countLeafNodes(TreeNode root) {
		if (root == null) {
			return 0;
		}
		
		if (root.left == null && root.right == null) {
			return 1;
		}
		
		int num_left = countLeafNodes(root.left);
		int num_right = countLeafNodes(root.right);
		
		return num_left + num_right;
	}

}
