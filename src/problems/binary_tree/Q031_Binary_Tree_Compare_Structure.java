package problems.binary_tree;

import java.util.*;

public class Q031_Binary_Tree_Compare_Structure {

	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) { val = x; }
	}
	
	public static void main(String[] args) {
		
	}
	
	public static boolean identical(TreeNode root1, TreeNode root2) {
		if (root1 == null && root2 == null) {
			return true;
		}
		
		if (root1 == null || root2 == null) {
			return false;
		}
		
		if (root1.val != root2.val) {
			return false;
		}
		
		return (identical(root1.left, root2.left) && identical(root1.right, root2.right)) ||
			   (identical(root1.right, root2.left) && identical(root1.left, root2.right));
	}

}
