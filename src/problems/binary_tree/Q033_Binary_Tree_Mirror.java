package problems.binary_tree;

import java.util.*;

public class Q033_Binary_Tree_Mirror {

	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) { val = x; }
	}
	
	public static void main(String[] args) {
		
	}
	
	public static TreeNode getMirror(TreeNode root) {
		if (root == null) {
			return null;
		}
		
		TreeNode left = getMirror(root.left);
		TreeNode right = getMirror(root.right);
		
		root.left = right;
		root.right = left;
		
		return root;
	}

}
