package problems.binary_tree;

import java.util.*;

public class Q034_Binary_Tree_Lowest_Common_Ancestor {

	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) { val = x; }
	}

	public static TreeNode lowestCommonAncestor(TreeNode root, TreeNode a, TreeNode b) {
		if (root == null) {
			return null;
		}
		
		if (root == a || root == b) {
			return root;
		}
		
		TreeNode left = lowestCommonAncestor(root.left, a, b);
		TreeNode right = lowestCommonAncestor(root.right, a, b);
		
		if (left != null && right != null) {
			return root;
		}
		else {
			return left != null ? left : right;
		}
	}
	
	public static void main(String[] args) {
		
	}

}
