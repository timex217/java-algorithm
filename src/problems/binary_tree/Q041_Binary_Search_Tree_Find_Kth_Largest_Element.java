package problems.binary_tree;

import java.util.*;

public class Q041_Binary_Search_Tree_Find_Kth_Largest_Element {
	
	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) { val = x; }
	}
	
	// ---------------------------------
    //  Recursion
	//  Use reverse inorder traversal
    // ---------------------------------
	static TreeNode findKthLargestElementR(TreeNode root, int k) {
	    HashMap<String, Integer> map = new HashMap<String, Integer>();
	    map.put("count", 0);
	    
	    return helper(root, k, map);
	}
	
	static TreeNode helper(TreeNode root, int k, HashMap<String, Integer> map) {
	    if (root == null) {
	        return null;
	    }
	    
	    helper(root.right, k, map);
	    
	    int count = map.get("count");
	    count++;
	    
	    if (count == k) {
	        return root;
	    }
	    
	    map.put("count", count);
	    
	    return helper(root.left, k, map);
	}
	
	// ------------------
	//  Loop
	// ------------------
	public TreeNode findKthLargestElement(TreeNode root, int k) {
		if (root == null || k <= 0) return null;
		
		// borrow a stack for help
		Stack<TreeNode> stack = new Stack<TreeNode>();
		TreeNode current = root;
		
		while (!stack.isEmpty() || current != null) {
			if (current != null) {
				stack.push(current);
				current = current.right;
			}
			else {
				current = stack.pop();
				if (--k == 0) {
					return current;
				}
				current = current.left;
			}
		}
		
		return null;
	}
	
	public static void main(String[] args) {

	}

}
