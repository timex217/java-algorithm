package problems.binary_tree;

import java.util.*;

public class Q040_Binary_Tree_K_Nodes_Lowest_Common_Ancestor {
	
	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) { val = x; }
	}

	public static TreeNode lowestCommonAncestor(TreeNode root, TreeNode a, TreeNode b) {
		if (root == null) {
			return null;
		}
		
		if (root == a || root == b) {
			return root;
		}
		
		TreeNode left = lowestCommonAncestor(root.left, a, b);
		TreeNode right = lowestCommonAncestor(root.right, a, b);
		
		if (left != null && right != null) {
			return root;
		}
		else {
			return left != null ? left : right;
		}
	}
	
	public static TreeNode kLowestCommonAncestor(TreeNode root, List<TreeNode> nodes, int lo, int hi) {
		if (lo < hi) {
			int mid = lo + (hi - lo) / 2;
			
			TreeNode left = kLowestCommonAncestor(root, nodes, lo, mid);
			TreeNode right = kLowestCommonAncestor(root, nodes, mid + 1, hi);
			
			return lowestCommonAncestor(root, left, right);
		}
		else {
			return nodes.get(lo);
		}
	}
	
	public static TreeNode kLowestCommonAncestor(TreeNode root, List<TreeNode> nodes) {
		if (nodes == null || nodes.size() == 0) {
			return null;
		}
		
		return kLowestCommonAncestor(root, nodes, 0, nodes.size() - 1);
	}
	
	public static void main(String[] args) {
		
	}

}
