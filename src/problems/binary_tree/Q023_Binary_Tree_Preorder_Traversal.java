package problems.binary_tree;

import java.util.*;

public class Q023_Binary_Tree_Preorder_Traversal {
	
	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) { val = x; }
	}
	
	public static void main(String[] args) {
		
	}
	
	// Recursion
	public static void preorder(TreeNode root) {
		// base case
		if (root == null) {
			return;
		}
		
		visit(root);
		preorder(root.left);
		preorder(root.right);
	}
	
	// Loop
	public static void preorder_loop(TreeNode root) {
		if (root == null) {
			return;
		}
		
		Stack<TreeNode> stack = new Stack<TreeNode>();
		stack.push(root);
		
		while (!stack.isEmpty()) {
			TreeNode node = stack.pop();
			
			visit(node);
			
			if (node.right != null) {
				stack.push(node.right);
			}
			
			if (node.left != null) {
				stack.push(node.left);
			}
		}
				
	}
	
	public static void visit(TreeNode node) {
		System.out.println(node);
	}

}
