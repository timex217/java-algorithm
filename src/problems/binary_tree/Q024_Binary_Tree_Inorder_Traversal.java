package problems.binary_tree;

import java.util.*;

public class Q024_Binary_Tree_Inorder_Traversal {
	
	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) { val = x; }
	}
	
	public static void main(String[] args) {
		
	}
	
	// Recursion
	public static void inorder(TreeNode root) {
		if (root == null) {
			return;
		}
		
		inorder(root.left);
		visit(root);
		inorder(root.right);
	}
	
	// Non-recursion
	public static void inorder_loop(TreeNode root) {
		if (root == null) {
			return;
		}
		
		Stack<TreeNode> stack = new Stack<TreeNode>();
		TreeNode current = root;
		
		while (!stack.isEmpty() || current != null) {
			if (current != null) {
				stack.push(current);
				current = current.left;
			}
			else {
				current = stack.pop();
				visit(current);
				current = current.right;
			}
		}
	}
	
	public static void visit(TreeNode node) {
		System.out.println(node);
	}

}
