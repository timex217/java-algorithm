package problems.binary_tree;

import java.util.*;

public class Q039_Binary_Tree_Is_BST {

	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) { val = x; }
	}
	
	public static boolean isBST(TreeNode root) {
		return isBST(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
	}
	
	public static boolean isBST(TreeNode root, int min, int max) {
		if (root == null) {
			return true;
		}
		
		if (root.val <= min || root.val >= max) {
			return false;
		}
		
		return isBST(root.left, min, root.val) && isBST(root.right, root.val, max);
	}
	
	public static void main(String[] args) {
		
	}

}
