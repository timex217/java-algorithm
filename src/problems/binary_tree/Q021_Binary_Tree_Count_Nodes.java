package problems.binary_tree;

public class Q021_Binary_Tree_Count_Nodes {
	
	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) { val = x; }
	}
	
	public static void main(String[] args) {
		
	}
	
	// Recursion
	public static int countNodes(TreeNode root) {
		// base case
		if (root == null) {
			return 0;
		}
		
		return countNodes(root.left) + countNodes(root.right) + 1;
	}

}
