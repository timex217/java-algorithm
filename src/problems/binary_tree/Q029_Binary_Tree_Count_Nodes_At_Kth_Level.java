package problems.binary_tree;

import java.util.*;

public class Q029_Binary_Tree_Count_Nodes_At_Kth_Level {
	
	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) { val = x; }
	}
	
	public static void main(String[] args) {
		
	}
	
	// Recursion
	public static int countNodes(TreeNode root, int k) {
		if (root == null || k < 1) {
			return 0;
		}
		
		if (k == 1) {
			return 1;
		}
		
		int num_left = countNodes(root.left, k - 1);
		int num_right = countNodes(root.right, k - 1);
		
		return num_left + num_right;
	}
}
