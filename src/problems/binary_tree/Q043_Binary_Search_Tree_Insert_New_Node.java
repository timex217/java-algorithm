package problems.binary_tree;

import java.util.*;

public class Q043_Binary_Search_Tree_Insert_New_Node {
	
	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) { val = x; }
	}

	public TreeNode insertNode(TreeNode root, int target) {
		TreeNode new_node = new TreeNode(target);
		TreeNode node = root;
		
		while (node != null) {
			if (target == node.val) {
				return null; // no need to insert
			}

			if (target < node.val) {
				if (node.left == null) {
					node.left = new_node;
					break;
				}
				else {
					node = node.left;
				}
			}
			
			else if (target > node.val) {
				if (node.right == null) {
					node.right = new_node;
					break;
				}
				else {
					node = node.right;
				}
			}
		}

		return null;
	}

}
