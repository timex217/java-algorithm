package problems.binary_tree;

import java.util.*;

public class Q034_K_Tree_Lowest_Common_Ancestor {
	
	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode[] nodes;
		TreeNode(int x) { val = x; }
	}
	
	public static HashMap<TreeNode, TreeNode> map = new HashMap<TreeNode, TreeNode>();

	public static TreeNode LCA(TreeNode root, TreeNode a, TreeNode b) {
		// get level and upate map
		int aLevel = getLevel(root, a);
		int bLevel = getLevel(root, b);

		int diff = Math.abs(aLevel - bLevel);

		TreeNode n1 = a;
		TreeNode n2 = b;

		if (aLevel > bLevel) {
			TreeNode tmp = n1;
			n1 = n2;
			n2 = tmp;
		}

		while (diff > 0) {
			n2 = map.get(n2);
			diff--;
		}

		while (n1 != null && n2 != null) {
			if (n1 == n2) {
				return n1;
			} else {
				n1 = map.get(n1);
				n2 = map.get(n2);
			}
		}

		return null;
	}

	// use level order traversal to get the level and build the hash map
	public static int getLevel(TreeNode root, TreeNode node) {
		int level = 1;

		Queue<TreeNode> Q = new LinkedList<TreeNode>();
		Q.add(root);
		Q.add(null);

		TreeNode parent = null;

		while (!Q.isEmpty()) {
			TreeNode p = Q.poll();
			
			if (p != null) {
				for (int i = 0; i < p.nodes.length; i++) {
					TreeNode child = p.nodes[i];

					if (!map.containsKey(child)) {
						map.put(child, p);
					}

					if (child == node) {
						return level;
					}

					Q.add(child);
				}
			}
			else {
				level++;

				if (!Q.isEmpty()) {
					Q.add(null);
				}
			}
		}

		return -1;
	}

}
