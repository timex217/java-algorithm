package problems.binary_tree;

import java.util.*;

public class Q042_Binary_Search_Tree_Delete_Node {
	
	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) { val = x; }
	}

	public void deleteNode(TreeNode root, TreeNode node) {
		if (node == null) {
			return;
		}

		// get parent
		TreeNode parent = getParent(root, node);
		if (parent == null) {
			return;
		}

		// case 1: node is a leaf
		if (node.left == null && node.right == null) {
			if (parent.left == node) {
				parent.left = null;
			} 
			else {
				parent.right = null;
			}

			node = null;
		}

		// case 2: node has only one left or right subtree
		else if (node.left == null || node.right == null) {
			TreeNode child = node.left != null ? node.left : node.right;

			if (parent.left == node) {
				parent.left = child;
			}
			else {
				parent.right = child;
			}

			node = null;
		}

		// case 3: node has both left and right subtree
		else {
			TreeNode child = getRightMostNode(node.left);
			TreeNode childParent = getParent(root, child);

			node.val = child.val;
			if (childParent.left == child) {
				childParent.left = null;
			}
			else {
				childParent.right = null;
			}

			child = null;
		}
	}

	/**
	 * Get node's parent node in tree root
	 */
	public static TreeNode getParent(TreeNode root, TreeNode node) {
		if (root == null || node == null) {
			return null;
		}

		if (root.left == node || root.right == node) {
			return root;
		}

		TreeNode left = getParent(root.left, node);
		TreeNode right = getParent(root.right, node);

		return left != null ? left : right;
	}

	/**
	 * Get right most child from root
	 */
	public static TreeNode getRightMostNode(TreeNode root) {
		if (root == null) {
			return null;
		}

		if (root.left == null && root.right == null) {
			return root;
		}

		return getRightMostNode(root.right);
	}

}
