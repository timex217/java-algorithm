package problems.binary_tree;

import java.util.*;

public class Q025_Binary_Tree_Postorder_Traversal {
	
	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) { val = x; }
	}
	
	public static void main(String[] args) {
		
	}
	
	// Recursion
	public static void postorder(TreeNode root) {
		if (root == null) {
			return;
		}
		
		postorder(root.left);
		postorder(root.right);
		visit(root);
	}
	
	// Loop
	public static void postorder_loop(TreeNode root) {
		if (root == null) {
			return;
		}
		
		Stack<TreeNode> stack = new Stack<TreeNode>();
		Stack<TreeNode> output = new Stack<TreeNode>();
		stack.push(root);
		
		while (!stack.isEmpty()) {
			TreeNode node = stack.pop();
			output.push(node);
			
			if (node.left != null) {
				stack.push(node.left);
			}
			
			if (node.right != null) {
				stack.push(node.right);
			}
		}
		
		while (!output.isEmpty()) {
			TreeNode node = output.pop();
			visit(node);
		}
		
	}
	
	public static void visit(TreeNode node) {
		System.out.println(node);
	}

}
