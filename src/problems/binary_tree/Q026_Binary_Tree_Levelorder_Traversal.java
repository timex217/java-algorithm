package problems.binary_tree;

import java.util.*;

public class Q026_Binary_Tree_Levelorder_Traversal {
	
	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) { val = x; }
	}
	
	public static void main(String[] args) {
		
	}
	
	// Non-recursion 1
	public static void levelorder(TreeNode root) {
		if (root == null) {
			return;
		}
		
		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		queue.add(root);
		queue.add(null);
		
		while (!queue.isEmpty()) {
			TreeNode node = queue.poll();
			
			if (node != null) {
				visit(node);
				
				if (node.left != null) {
					queue.add(node.left);
				}
				
				if (node.right != null) {
					queue.add(node.right);
				}
			}
			else {
				// level is ended
				if (!queue.isEmpty()) {
					queue.add(null);
				}
			}
		}
	}
	
	// Non-recursion 2
	public static void levelorder_loop(TreeNode root) {
		if (root == null) {
			return;
		}
		
		Stack<TreeNode> curr = new Stack<TreeNode>();
		Stack<TreeNode> next = new Stack<TreeNode>();
		
		curr.push(root);
		
		while (!curr.isEmpty()) {
			TreeNode node = curr.pop();
			
			visit(node);
			
			if (node.right != null) {
				next.push(node.right);
			}
			
			if (node.left != null) {
				next.push(node.left);
			}
			
			if (curr.isEmpty()) {
				Stack<TreeNode> tmp = curr;
				curr = next;
				next = tmp;
			}
		}
	}
	
	public static List<List<Integer>> levelOrder(TreeNode root) {
        ArrayList result = new ArrayList();
        ArrayList current = new ArrayList();
        
        if (root == null) {
        	return result;
        }
        
        // use queue to help traverse
        Queue<TreeNode> q = new LinkedList<TreeNode>();
        q.add(root);
        q.add(null);
        
        while (!q.isEmpty()) {
            TreeNode node = q.poll();
            
            // not null, add the list
            if (node != null) {
                current.add(node.val);
                
                if (node.left != null) {
                	q.add(node.left);
                }
                
                if (node.right != null) {
                	q.add(node.right);
                }
            } 
            else {
                // null, reset current list
                result.add(current);
                current = new ArrayList();
                
                if (q.isEmpty()) {
                    break;
                } 
                else {
                    q.add(null);
                }
            }
        }
        
        return result;
    }
	
	public static void visit(TreeNode node) {
		System.out.println(node);
	}

}
