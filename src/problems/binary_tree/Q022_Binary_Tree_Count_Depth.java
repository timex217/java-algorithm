package problems.binary_tree;

public class Q022_Binary_Tree_Count_Depth {
	
	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) { val = x; }
	}
	
	public static void main(String[] args) {
		
	}
	
	private static int countDepth(TreeNode root) {
		if (root == null) {
			return 0;
		}
		
		int left_depth = countDepth(root.left);
		int right_depth = countDepth(root.right);
		
		return Math.max(left_depth, right_depth) + 1;
	}

}
