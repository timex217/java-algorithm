package problems.binary_tree;

import java.util.*;

public class Q034_Binary_Tree_Lowest_Common_Ancestor_With_Parent_Pointer {
	
	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode parent;
		TreeNode(int x) { val = x; }
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public static TreeNode LCA(TreeNode root, TreeNode a, TreeNode b) {
		int aLevel = getLevel(a);
		int bLevel = getLevel(b);

		int diff = Math.abs(aLevel - bLevel);

		TreeNode n1 = a;
		TreeNode n2 = b;

		if (aLevel > bLevel) {
			TreeNode tmp = n1;
			n1 = n2;
			n2 = tmp;
		}

		while (diff > 0) {
			n2 = n2.parent;
			diff--;
		}

		while (n1 != null && n2 != null) {
			if (n1 == n2) {
				return n1;
			} else {
				n1 = n1.parent;
				n2 = n2.parent;
			}
		}

		return null;
	}

	public static int getLevel(TreeNode node) {
		int level = 0;
		TreeNode p = node;

		while (p != null) {
			p = p.parent;
			level++;
		}

		return level;
	}

}
