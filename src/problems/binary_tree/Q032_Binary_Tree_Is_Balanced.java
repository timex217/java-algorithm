package problems.binary_tree;

import java.util.*;

public class Q032_Binary_Tree_Is_Balanced {

	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) { val = x; }
	}
	
	public static void main(String[] args) {
		
	}
	
	public static boolean isBalanced(TreeNode root) {
		return getHeight(root) != -1;
	}
	
	public static int getHeight(TreeNode root) {
		if (root == null) {
			return 0;
		}
		
		int left_height = getHeight(root.left);
		int right_height = getHeight(root.right);
		
		if (left_height == -1 || right_height == -1) {
			return -1;
		}
		
		if (Math.abs(left_height - right_height) > 1) {
	        return -1;
	    }
		
		return Math.max(left_height, right_height) + 1;
	}

}
