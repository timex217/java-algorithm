package problems.binary_tree;

import java.util.*;

public class Q036_Binary_Tree_Is_Complete {

	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) { val = x; }
	}
	
	public static boolean isComplete(TreeNode root) {
		if (root == null) {
			return false;
		}
		
		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		queue.add(root);
		
		boolean must_have_no_child = false;
		boolean result = false;
		
		while (!queue.isEmpty()) {
			TreeNode node = queue.poll();
			
			if (must_have_no_child) {
				if (node.left != null || node.right != null) {
					result = false;
					break;
				}
			}
			else {
				if (node.left != null && node.right != null) {
					queue.add(node.left);
					queue.add(node.right);
				}
				else if (node.left != null && node.right == null) {
					queue.add(node.left);
					must_have_no_child = true;
				}
				else if (node.left == null && node.right != null) {
					result = false;
					break;
				}
				else {
					must_have_no_child = true;
				}
			}
			
		}
		
		return result;
	}
	
	public static void main(String[] args) {
		
	}

}
