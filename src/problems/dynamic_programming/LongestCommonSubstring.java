package problems.dynamic_programming;

import java.util.*;

public class LongestCommonSubstring {
	
	public static void main(String[] args) {
		String s1 = "student";
		String s2 = "sweden";
		
		String s = lcs(s1, s2);
		System.out.format("longest common substring is: %s", s);
	}
	
	public static String lcs(String s1, String s2) {
		if (s1 == null || s2 == null) {
			return null;
		}
		
		int n1 = s1.length();
		int n2 = s2.length();
		
		if (n1 == 0 || n2 == 0) {
			return "";
		}
		
		// m[i][j] represents the length of the longest common substring of A and B 
		// that are ended at A[i] and B[j], respectively.
		int i = 0, j = 0;
		int max = 0, max_index = -1;
		int m = n1 + 1, n = n2 + 1;
		int[][] M = new int[m][n];
		
		for (i = 0; i < m; i++) {
			for (j = 0; j < n; j++) {
				if (i == 0 || j == 0) {
					M[i][j] = 0;
				} 
				else if (s1.charAt(i - 1) == s2.charAt(j - 1)) {
					M[i][j] = 1 + M[i - 1][j - 1];
					
					if (M[i][j] > max) {
						max = M[i][j];
						max_index = i - max;
					}
				} 
				else {
					M[i][j] = 0;
				}
			}
		}
		
		if (max == 0) {
			return "";
		}
		
		// print out the matrix
		for (i = 0; i < m; i++) {
			String row = "";
			for (j = 0; j < n; j++) {
				row += String.valueOf(M[i][j]) + "\t";
			}
			System.out.println(row);
		}
		
		return s1.substring(max_index, max_index + max);
	}
	
}
