/**
 * What is the edge length of the largest square of 1�s in a given binary matrix. 
 * In this case, your solution should return 3 (3x3 square).
 * 
 * 0 0 0 0 0
 * 1 1 1 1 0
 * 1 1 1 1 0
 * 1 1 1 0 0
 * 1 1 1 0 0
 */
package problems.dynamic_programming;

public class LargestSquareOfOnesInBinaryMatrix {
	
	public static void main(String[] args) {
		int[][] M = {
			{0, 0, 0, 0, 0},
			{1, 1, 1, 1, 0},
			{1, 1, 1, 0, 0},
			{1, 1, 1, 0, 0},
			{1, 1, 1, 1, 0}
		};
		
		int num = getEdge(M);
		System.out.println(num);
	}
	
	public static int getEdge(int[][] M) {
		// e(i, j) represents the length of the edge of the ones square with (i, j) as its lowest right corner.
		// e(i, j) = min{ e(i-1, j-1), e(i-1, j), e(i, j-1) } + 1, M[i][j] = 1
		
		int m = M.length;
		int n = M[0].length;
		int max = 0;
		
		int[][] dp = new int[m + 1][n + 1];
		
		for (int i = 1; i <= m; i++) {
			for (int j = 1; j <= n; j++) {
				if (M[i-1][j-1] == 1) {
					int min = Math.min(dp[i-1][j-1], dp[i][j-1]);
					dp[i][j] = Math.min(min, dp[i-1][j]) + 1;
					max = Math.max(dp[i][j], max);
				}
			}
		}
		
		return max;
	}

}
