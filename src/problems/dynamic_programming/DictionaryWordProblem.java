/**
 * Given a word, can it be composed by concatenating words from a given dictionary? 
 * 
 * Example: Dictionary:
 * bob
 * cat
 * rob
 * Word: bcoabt
 * 
 * Solution: False
 */
package problems.dynamic_programming;

import java.util.*;

public class DictionaryWordProblem {
	
	public static void main(String[] args) {
		HashSet<String> dict = new HashSet<String>(Arrays.asList("bob", "cat", "rob"));
		boolean can = canMake("bobcatrob", dict);
		System.out.println(can ? "Yes" : "No");
	}
	
	public static boolean canMake(String s, HashSet<String> dict) {
		if (s == null || s.length() == 0) {
			return false;
		}
		
		int n = s.length();
		
		// dp[i] represents whether the s[0...i] can be made by the dictionary words
		boolean[] dp = new boolean[n];
		
		for (int i = 0; i < n; i++) {
		    // 首先，检查当前整个前i个字符构成的字符串是否是字典的词
		    if (dict.contains(s.substring(0, i+1))) {
		        dp[i] = true;
		        continue;
		    }
		    
		    // 其次，依次检查从1到i的各个位置上，s[j...i]是否就是字典的词以及前j-1的字符串是否能由字典组成
			for (int j = 1; j <= i; j++) {
				if (dp[j-1] && dict.contains(s.substring(j, i+1))) {
					dp[i] = true;
					break;
				}
			}
		}
		
		return dp[n-1];
	}

}
