/**
 * Q28-1. Given 1 cent, 5 cents, 10 cents and 25 cents coin, and a certain amount of money 99.
 * Find the minimum number of coins to change.
 *
 * C(P) represents the minimum no. of coins to make changes to amount of P.
 * {v1, v2, v3 ... vi ... vn}
 * {1, 5, 10, 25}
 * C(P) = min{ C(P - vi) } + 1
 * C(P) = min( C(P - v1), C(P - v2) ... C(P - vi) ) + 1
 */

package problems.dynamic_programming;

import java.util.*;

public class Coin_Minimum_Changes {
	
	public static void main(String[] args) {
		int num = minCoins(99);
		System.out.println(num);
	}
	
	public static int minCoins(int n) {
		int[] coins = {1, 5, 10, 25};

		int[] dp = new int[n + 1];
		dp[0] = 0;

		// 对每一分钱都找零，即保存子问题的解以备用，即填表
		for (int i = 1; i <= n; i++) {
			
			// 首先, 当用最小币值的硬币找零时，所需硬币数量最多
			int min = i / coins[0];

			// 然后, 遍历其他每一种面值的硬币，看是否可作为找零的其中之一
			for (int j = 1; j < coins.length; j++) {
				int val = coins[j];

				if (i >= val) {
				    min = Math.min(min, dp[i - val] + 1);
				}
			}

			dp[i] = min;
		}
		
		return dp[n];
	}

}
