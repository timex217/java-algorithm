/**
 * In an unsorted 0/1 array, find the longest continuous ones.
 * 
 * Example: A[n] = 0011001110111110010100111111111
 */
package problems.dynamic_programming;

public class LongestContinuousOnes {
	
	public static void main(String[] args) {
		int[] A = {0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1};
		int num = find(A);
		System.out.println(num);
	}
	
	public static int find(int[] A) {
		if (A == null || A.length == 0) {
			return 0;
		}
		
		int n = A.length;
		int max = 0;
		int start = -1;
		int finish = -1;
		int tmp_start = -1;
		
		// dp[i] represents the number of continuous ones ended at i-th place.
		int[] dp = new int[n + 1];
		
		for (int i = 1; i <= n; i++) {
			if (A[i-1] == 1) {
				dp[i] = dp[i-1] + 1;
				
				if (dp[i] == 1) {
					tmp_start = i-1;
				}
				
				if (dp[i] > max) {
					max = dp[i];
					start = tmp_start;
					finish = i-1;
				}
			}
		}
		
		System.out.format("start: %d, finish: %d\n", start, finish);
		
		return max;
	}

}
