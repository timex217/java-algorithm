/**
 * 
 */

package problems.dynamic_programming;

public class LargestSumOfSubArray {
	
	public static void main(String[] args) {
		int[] a = {0, 1, 0, 0, 2, 4, -1, -2, -1};
		int res = find(a);
		System.out.println(res);
	}
	
	static int find(int[] a) {
		if (a == null || a.length == 0) {
			return Integer.MIN_VALUE;
		}
		
		int n = a.length;
		int max;
		
		// s(i) represents the largest sum till a[i] (including a[i])
		// s(i) = max{ s(i-1) + a[i], a[i] }
		
		int[] s = new int[n];
		s[0] = a[0];
		max = a[0];
		
		for (int i = 1; i < n; i++) {
		    s[i] = Math.max(s[i-1] + a[i], a[i]);
		    max = Math.max(max, s[i]);
		}
		
		return max;
	}

}
