package problems.dynamic_programming;

public class Rod_Cutting {
	
	public static void main(String[] args) {
		int[] price = {1, 5, 8, 9, 10, 17, 17, 20};
		int max = cut(price, 8);
		
		System.out.println(max);
	}
	
	public static int cut(int[] price, int n) {
		// p(i) represents the maximum price for length i
	    // 
	    // |___|___|___|___|
	    // 0       j   i   n
	    //
		// p(i) = max{v(i), v(j) + p(i - j)} (0 < j < i)
	    
		int[] dp = new int[n + 1];
		
		// 从 1 米开始循环，一直到 n 米
		for (int i = 1; i <= n; i++) {
			int max = price[i - 1];
			
			for (int j = 1; j < i; j++) {
				max = Math.max(max, price[j - 1] + dp[i - j]);
			}
			
			dp[i] = max;
		}
		
		return dp[n];
	}

}
