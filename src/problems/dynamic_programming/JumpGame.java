package problems.dynamic_programming;

public class JumpGame {
	
	public static void main(String[] args) {
		int[] A= {2, 3, 1, 1, 4};
		//int[] A= {3, 2, 1, 0, 4};
		boolean can = canJump(A);
		int min = minJump(A);
		System.out.format("%s\n", can ? "Yes" : "No");
		System.out.format("%d\n", min);
	}
	
	public static boolean canJump(int[] A) {
		if (A == null || A.length == 0) {
			return true;
		}
		
		int n = A.length;
		
		// initialize dp
		// dp[i] represents whether can jump to the end from i-th place
		boolean[] dp = new boolean[n];
		dp[n - 1] = true;
		
		for (int i = n - 2; i >= 0; i--) {
			boolean can = false;
			for (int j = 1; j <= A[i] && i + j < n; j++) {
				if (dp[i + j]) {
					can = true;
					break;
				}
			}
			dp[i] = can;
		}
		
		return dp[0];
	}
	
	public static boolean canJumpFast(int[] A) {
		if (A == null) {
			return false;
		}
		
		int n = A.length;
		int reach = 0;
		
		for (int i = 0; i < n && i <= reach; i++) {
			reach = Math.max(i + A[i], reach);
			if ( i + A[i] >= n - 1) {
				return true;
			}
		}
		
		return false;
	}
	
	public static int minJump(int[] A) {
		if (A == null || A.length == 0) {
			return 0;
		}
		
		// dp[i] represents the minimum jumps from i-th place
		int n = A.length;
		int[] dp = new int[n];
		
		dp[n - 1] = 0;
		
		for (int i = n - 2; i >= 0; i--) {
			if (A[i] == 0) {
				dp[i] = Integer.MAX_VALUE;
			}
			else if (i + A[i] >= n - 1) {
				dp[i] = 1;
			}
			else {
				int min = Integer.MAX_VALUE;
				// 跳1步，跳2步 ... 跳A[i]步
				// 找最小的步数
				for (int j = 1; j <= A[i]; j++) {
					min = Math.min(min, dp[i + j]);
				}
				
				if (min != Integer.MAX_VALUE) {
					min = min + 1;
				}
				
				dp[i] = min;
			}
		}
		
		return dp[0];
	}

}
