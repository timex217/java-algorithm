/**
 * Given a Matrix that contains only 1s and 0s, how to find the largest cross
 * with the same arm lengths and the two arms join at the central point of each other.
 * 
 * 0100   
 * 1111   
 * 0100   
 * 0100
 * 
 * idea: 
 * Step 1: from each row of the matrix, from left to right, calculate the longest 1s ending at A[i][j]
 * Step 2, from each row of the matrix, from right to left, calculate the longest 1s ending at A[i][j]
 * Step 3: from each column of the matrix, from bottom up, calculate the longest 1s ending at A[i][j]
 * Step 4: from each column of the matrix, from up to bottom, calculate the longest 1s ending at A[i][j]
 * Step 5: iterate through A[N][N] ,take the min(step 1 2 3 4) for all four direction, then we get the arm length for A[i][j]
 */

package problems.dynamic_programming;

public class LargestCross {
	
	public static void main(String[] args) {
		int[][] A = {
			{0, 1, 0, 0},
			{1, 1, 1, 1},
			{0, 1, 0, 0},
			{0, 1, 0, 0}
		};
		
		int num = find(A);
		System.out.println(num);
	}
	
	public static int find(int[][] A) {
		int m = A.length;
		int n = A[0].length;
		int max = 0;
		
		// use four m x n matrix to count the ones
		int[][] up    = new int[m][n];
		int[][] down  = new int[m][n];
		int[][] left  = new int[m][n];
		int[][] right = new int[m][n];
		int[][] dp    = new int[m][n];
		
		int i, j;
		
		// Step 1.
		// from each row of the matrix, from left to right, calculate the longest 1s ending at A[i][j]
		for (i = 0; i < m; i++) {
			for (j = 0; j < n; j++) {
				if (A[i][j] == 1) {
					left[i][j] = (j == 0) ? 1 : left[i][j-1] + 1;
				}
			}
		}
		
		// Step 2
		// from each row of the matrix, from right to left, calculate the longest 1s ending at A[i][j]
		for (i = 0; i < m; i++) {
			for (j = n - 1; j >= 0; j--) {
				if (A[i][j] == 1) {
					right[i][j] = (j == n - 1) ? 1 : right[i][j+1] + 1;
				}
			}
		}
		
		// Step 3
		// from each column of the matrix, from bottom up, calculate the longest 1s ending at A[i][j]
		for (j = 0; j < n; j++) {
			for (i = 0; i < m; i++) {
				if (A[i][j] == 1) {
					up[i][j] = (i == 0) ? 1 : up[i-1][j] + 1;
				}
			}
		}
		
		// Step 4
		// from each column of the matrix, from up to bottom, calculate the longest 1s ending at A[i][j]
		for (j = 0; j < n; j++) {
			for (i = m - 1; i >= 0; i--) {
				if (A[i][j] == 1) {
					down[i][j] = (i == m - 1) ? 1 : down[i+1][j] + 1;
				}
			}
		}
		
		// Step 5
		// iterate through A[N][N] ,take the min(step 1 2 3 4) for all four direction, then we get the arm length for A[i][j]
		for (i = 0; i < m; i++) {
			for (j = 0; j < n; j++) {
				dp[i][j] = min(left[i][j], right[i][j], up[i][j], down[i][j]);
				max = Math.max(max, dp[i][j]);
			}
		}
		
		return max;
	}
	
	public static int min(int a, int b, int c, int d) {
		return Math.min(Math.min(a, b), Math.min(c, d));
	}

}
