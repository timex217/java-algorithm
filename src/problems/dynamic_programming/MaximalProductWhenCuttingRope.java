package problems.dynamic_programming;

public class MaximalProductWhenCuttingRope {

	public static void main(String[] args) {
		int max = getProduct(8);
		System.out.println(max);
	}
	
	public static int getProduct(int n) {
		if (n < 0) {
			return 0;
		}
		
		// dp[i] represents the max product of cutting wood with i meters length
		//
		// |___|___|___|___|
		// 0       j   i   n
		
		int[] dp = new int[n + 1];
		
		// base case
		// 切 0 米和 1 米时，由于0 - 1之间没有任何切割点，也就是不能切，故product为0
		// 从 1 米开始循环，一直到 n 米
		for (int i = 1; i <= n; i++) {
			int max = 0;
			
			// 从 i/2 米处开始切，每切一刀，当前最大的product为
			// max {j * (i - j), j * dp(i - j) }
			for (int j = 1; j < i; j++) {
				int curr = Math.max(j * (i - j), j * dp[i - j]);
				max = Math.max(max, curr);
			}
			
			dp[i] = max;
		}
		
		return dp[n];
	}

}
