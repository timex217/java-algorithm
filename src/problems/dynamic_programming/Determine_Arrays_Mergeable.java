/**
 * Q31. How to determine whether an array C[] can be merged by A[] and B[]?
 *
 * Example:
 * A = {'a', 'b', 'c', 'd'}
 * B = {'a', 'c', 'd', 'e'}
 * C = {'a', 'c', 'd', 'e', 'a', 'b', 'c', 'd'}
 *
 * boolean[][] M[i][j] represents we have tried to use first i letters of A[] and first j letters of B[] to form the
 * first i+j letter of C, that is C[1] to C[i+j]
 *
 * M[i][j] = M[i-1][j] and (A[i] == C[i+j]) || (同一列, B[] 不取新字母)
 *           M[i][j-1] and (B[j] == C[i+j])    (同一行, A[] 不取新字母)
 *         
 *            a c d e
 *          0 1 2 3 4
 *        ---------------- 
 *      0 |
 *    a 1 |
 *    b 2 |
 *    c 3 |       M(i, j)
 *    d 4 |
 *    
 * 难点在于，前i个字符的最后一个是A[i-1]，前j个字符的最后一个是B[j-1]，而前i+j个字符的最后一个是C[i+j-1]!
 */

package problems.dynamic_programming;

import java.util.*;

public class Determine_Arrays_Mergeable {
	
	public static char[] A = {'a', 'b', 'c', 'd'};
	public static char[] B = {'a', 'c', 'd', 'e'};
	public static char[] C = {'a', 'c', 'd', 'e', 'a', 'b', 'c', 'd'};
	
	public static void main(String[] args) {
		boolean result = isCombinableDP();
		System.out.println(result ? "Yes" : "No");
	}
	
	// Recursion
	public static boolean isCombinable(int indexA, int indexB, int indexC) {
		if (indexA == A.length && indexB == B.length) {
			return indexC == C.length;
		}
		
		if (indexA < A.length && A[indexA] == C[indexC] && indexB < B.length && B[indexB] == C[indexC]) {
			return isCombinable(indexA + 1, indexB, indexC + 1) || isCombinable(indexA, indexB + 1, indexC + 1);
		}
		else if (indexA < A.length && A[indexA] == C[indexC]) {
			return isCombinable(indexA + 1, indexB, indexC + 1);
		}
		else if (indexB < B.length && B[indexB] == C[indexC]) {
			return isCombinable(indexA, indexB + 1, indexC + 1);
		}
		else {
			return false;
		}
	}

	// Dynamic Programming
	public static boolean isCombinableDP() {
		int m = A.length, n = B.length;
		boolean[][] dp = new boolean[m + 1][n + 1];
		
		for (int i = 0; i <= m; i++) {
			for (int j = 0; j <= n; j++) {
				if (i == 0 && j == 0) {
					dp[i][j] = true;
				}
				// only B contributes
				else if (i == 0) {
					dp[i][j] = dp[i][j-1] && (B[j-1] == C[i+j-1]);
				} 
				// only A contributes
				else if (j == 0) {
					dp[i][j] = dp[i-1][j] && (A[i-1] == C[i+j-1]);
				} 
				// either A or B contributes
				else {
					dp[i][j] = (dp[i-1][j] && (A[i-1] == C[i+j-1])) || (dp[i][j-1] && (B[j-1] == C[i+j-1]));
				}
			}
		}
		
		return dp[m][n];
	}

}
