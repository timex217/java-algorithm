package problems.dynamic_programming;

import java.util.*;

public class LongestCommonSubsequence {
	
	public static void main(String[] args) {
		String s1 = "sweden";
		String s2 = "student";
		
		String s = lcs(s1, s2);
		System.out.format("the longest common sub sequence is: %s\n ", s);
	}
	
	public static String lcs(String s1, String s2) {
		if (s1 == null || s2 == null) {
			return null;
		}
		
		int n1 = s1.length();
		int n2 = s2.length();
		
		if (n1 == 0 || n2 == 0) {
			return "";
		}
		
		// m[i][j] represents the length of the longest common sub sequence
		// of A and B ending at A[i] and B[j], repectively.
		int i = 0, j = 0;
		int m = n1 + 1, n = n2 + 1;
		int[][] M = new int[m][n];
		
		for (i = 0; i < m; i++) {
			for (j = 0; j < n; j++) {
				if (i == 0 || j == 0) {
					M[i][j] = 0;
				} 
				else if (s1.charAt(i - 1) == s2.charAt(j - 1)) {
					M[i][j] = 1 + M[i - 1][j - 1];
				} 
				else {
					M[i][j] = Math.max(M[i][j - 1], M[i - 1][j]);
				}
			}
		}
		
		// print out the matrix
		for (i = 0; i < m; i++) {
			String row = "";
			for (j = 0; j < n; j++) {
				row += String.valueOf(M[i][j]) + "\t";
			}
			System.out.println(row);
		}
		
		// find the longest common sub sequence
		String s = "";
		
		// start from the right-most-bottom-most corner
		i = n1;
		j = n2;
		
		while (i > 0 && j > 0) {
			// if current character in s1 and s2 are same, then
		    // current character is part of LCS
			if (s1.charAt(i - 1) == s2.charAt(j - 1)) {
				s = s1.charAt(i - 1) + s;
				i--; 
				j--;
			} 
			// if not same, then find the larger of two and
		    // go in the direction of larger value
			else if (M[i - 1][j] > M[i][j - 1]) {
				i--;
			} else {
				j--;
			}
		}
		
		return s;
	}
	
}
