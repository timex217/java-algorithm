/**
 * Given an unsorted array, find the length of the longest subarray in which the numbers are in ascending order. 
 * 
 * For example: If the input array is {7, 2, 3, 1, 5, 8, 9, 6}, 
 * the subarray with the most numbers in ascending order is {1, 5, 8, 9} 
 * and the expected output is 4.
 */

package problems.dynamic_programming;

public class LongestAscendingSubarray {
	
	public static void main(String[] args) {
		int[] A = {7, 2, 3, 1, 5, 8, 9, 6};
		int max_len = find(A);
		System.out.println(max_len);
	}
	
	public static int find(int[] A) {
		if (A == null || A.length == 0) {
			return 0;
		}
		
		int n = A.length;
		int max_len = 1;
		int max_index = 0;
		
		// dp[i] represents the length of ascending numbers ending at A[i]
		int[] dp = new int[n];
		dp[0] = 1;
		
		for (int i = 1; i < n; i++) {
			if (A[i] >= A[i - 1]) {
				dp[i] = dp[i - 1] + 1;
			}
			else {
				dp[i] = 1;
			}
			
			if (dp[i] > max_len) {
				max_len = dp[i];
				max_index = i;
			}
		}
		
		return max_len;
	}

}
