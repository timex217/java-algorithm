/**
 * Given a Matrix of integers (positive & negative numbers & 0s), how to find the submatrix with the largest sum?
 */

package problems.dynamic_programming;

import java.util.*;

public class LargestSumSubMatrix {
    
	public static void main(String[] args) {
	    int[][] M = {
			{ 1,  2, -1, -4, -20},
			{-8, -3,  4,  2,   1},
			{ 3,  8, 10,  1,   3},
			{-4, -1, 1,   7,  -6}
		};
		
		int res = getMaxSubMatrix(M, 4, 5);
		System.out.println(res);
	}
	
	static int getMaxSubMatrix(int[][] M, int m, int n) {
	    int res = Integer.MIN_VALUE;
	    
	    // step 1. 计算出辅助矩阵s，方便得出把任意两行元素拍成一行得到的和
	    int[][] s = getSumMatrix(M, m, n);
	    
	    // step 2. 移动上下两行
        for (int top = 0; top < m; top++) {
            for (int bottom = top; bottom < m; bottom++) {
                
                // step 3. 将上下两行拍成一行
                int[] v = new int[n];
                
                for (int j = 0; j < n; j++) {
                    v[j] = s[bottom][j] - (top == 0 ? 0 : s[top - 1][j]);
                }
                
                // step 4. 找这一行的最大连续和
                int max = getMaxSubArray(v, n);
                res = Math.max(max, res);
            }
        }
        
        return res;
	}
	
	static int getMaxSubArray(int[] A, int n) {
	    int[] s = new int[n];
	    s[0] = A[0];
	    int max = A[0];
	    
	    for (int i = 1; i < n; i++) {
	        s[i] = Math.max(s[i - 1] + A[i], A[i]);
	        max = Math.max(max, s[i]);
	    }
	    
	    return max;
	}
	
	static int[][] getSumMatrix(int[][] M, int m, int n) {
	    int[][] s = new int[m][n];
	    
	    for (int j = 0; j < n; j++) {
	        for (int i = 0; i < m; i++) {
	            if (i == 0) {
	                s[i][j] = M[i][j];
	            }
	            else {
	                s[i][j] += M[i][j];
	            }
	        }
	    }
	    
	    return s;
	}

}
