/**
 * Fibonacci: 0 1 1 2 3 5 8 13 21 34
 */

package problems.dynamic_programming;

public class Fibonacci {
	
	public static void main(String[] args) {
		int val = fibonacci(8);
		System.out.println(val);
	}
	
	public static int fibonacci(int n) {
		if (n < 2) {
			return n;
		}
		
		int[] dp = new int[n + 1];
		dp[0] = 0;
		dp[1] = 1;
		
		for (int i = 2; i <= n; i++) {
			dp[i] = dp[i - 1] + dp[i - 2];
		}
		
		return dp[n];
	}

}
