/**
 * Given two strings of alphanumeric characters, determine the minimum number of Replace, Delete, and Insert 
 * operations needed to transform one string into the other.
 */

package problems.dynamic_programming;

public class EditDistance {
	
	public static void main(String[] args) {
		int distance = editDistance("Jing Jin", "Yong Su");
		System.out.format("The edit distance is: %d\n", distance);
	}
	
	public static int editDistance(String word1, String word2) {
		int n1 = word1.length();
		int n2 = word2.length();
		
		// dp[i][j] represents the minimum edit distance from words[0...i] to words[0...j]
		int[][] dp = new int[n1 + 1][n2 + 1];
		
		for (int i = 0; i <= n1; i++) {
			for (int j = 0; j <= n2; j++) {
			    // 由空字符串变成word2
				if (i == 0) {
					dp[i][j] = j;
				}
				// 由word1变成空字符串
				else if (j == 0) {
					dp[i][j] = i;
				}
				else {
					char c1 = word1.charAt(i - 1);
					char c2 = word2.charAt(j - 1);
					
					int insert  = dp[i][j - 1] + 1;
					int delete  = dp[i - 1][j] + 1;
					int replace = dp[i - 1][j - 1] + ((c1 == c2) ? 0 : 1);
					
					int min = Math.min(insert, delete);
					dp[i][j] = Math.min(min, replace);
				}
			}
		}
		
		return dp[n1][n2];
		
	}

}
