package problems.dynamic_programming;
import java.util.*;

public class LongestIncreasingSubsequence {
	
	public static void main(String[] args) {
		int[] A = {1, -1, 2, -3, 4, -5, 4, 6, -7};
		int[] B = lis(A);
		
		for (int i = 0; i < B.length; i++)
			System.out.format("%d ", B[i]);
	}
	
	public static int[] lis(int[] A) {
		int n = A.length;
		int len = 0;
		// lis[i] represents the length of the longest increasing sub sequence at i-th position
		// lis[i] = max{1, lis[j] + 1}, j < i && A[j] < A[i]
		int[] dp = new int[n];
		
		// construct dp
		for (int i = 0; i < n; i++) {
			int max = 1;
			for (int j = 0; j < i; j++) {
				if (A[j] <= A[i]) {
					max = Math.max(max, dp[j] + 1);
				}
			}
			dp[i] = max;
			len = Math.max(dp[i], len);
		}
		
		// print
		int[] B = new int[len];
		int v = 0, c = 0;
		for (int i = 0; i < n; i++) {
			if (dp[i] > v) {
				B[c++] = (A[i]);
				v = dp[i];
			}
		}
		
		return B;
	}
	
}
