package problems.recursion;

import java.util.*;

public class Valid_Multi_Parenthesis_Permutation {
	
	public static char[] left = {'(', '[', '{'};
	public static char[] right = {')', ']', '}'};
	
	public static void dfs(int[] n, int[] l, int[] r, Stack<Character> stack, String sol, List<String> res) {
		boolean ok = true;
		
		for (int i = 0; i < l.length; i++) {
			if (r[i] < n[i]) {
				ok = false;
				break;
			}
		}
		
		if (ok) {
			res.add(sol);
			return;
		}
		
		// 右括号少了，加右括号
		// 加右括号的时候必须是一对一对加
		// 所以要先检查前一个是不是对应的左括号
		// 一旦加了，把左括号弹出来，表示这一对结束了
		for (int i = 0; i < l.length; i++) {
            if (r[i] < l[i] && stack.peek() == left[i]) {
                // 处理当前
                stack.pop();
                r[i]++;
                
                // 递归
                dfs(n, l, r, stack, sol + right[i], res);
                
                // 恢复
                stack.push(left[i]);
                r[i]--;
            }
        }
		
		// 左括号少了，加左括号
		for (int i = 0; i < l.length; i++) {
			if (l[i] < n[i]) {
			    // 处理当前
				stack.push(left[i]);
				l[i]++;
				
				// 递归
				dfs(n, l, r, stack, sol + left[i], res);
				
				// 恢复
				stack.pop();
				l[i]--;
			}
		}
	}
	
	public static void main(String[] args) {
		int[] n = {1, 1, 1};
		int[] l = {0, 0, 0};
		int[] r = {0, 0, 0};
		
		List<String> res = new ArrayList<String>();
		Stack<Character> stack = new Stack<Character>();
		
		dfs(n, l, r, stack, "", res);
		
		for (int i = 0; i < res.size(); i++) {
			System.out.println(res.get(i));
		}
	}

}
