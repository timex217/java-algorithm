package problems.recursion;

public class Valid_Parenthesis_Permutation {

	public static void main(String[] args) {
		print(3, 0, 0, "");
	}

	static void print(int n, int open, int close, String sol) {
		if (close == n) {
			System.out.println(sol);
			return;
		}

		if (open > close) {
			print(n, open, close + 1, sol + ")");
		}

		if (open < n) {
			print(n, open + 1, close, sol + "(");
		}
	}

}
