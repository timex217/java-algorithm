package problems.recursion;

import java.util.*;

public class Coin_Changes {

	public static int[] coins = {25, 10, 5, 1};
	
	public static void main(String[] args) {
		int[] solution = new int[4];
		findCombination(99, 0, solution);
	}

	public static void findCombination(int money, int level, int[] sol) {
		// base case
		if (level == coins.length - 1) {
			sol[level] = money;
			System.out.println(Arrays.toString(sol));
			return;
		}

		for (int i = 0; i * coins[level] <= money; i++) {
			sol[level] = i;
			findCombination(money - i * coins[level], level + 1, sol);
		}
	}

}
