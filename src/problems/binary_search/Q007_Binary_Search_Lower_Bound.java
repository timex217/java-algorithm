package problems.binary_search;

public class Q007_Binary_Search_Lower_Bound {
	
	public static void main(String[] args) {
		int[] A = {-2, 0, 0, 1, 3, 3, 3, 3, 3, 3, 4, 5, 6, 7, 8, 9};
		//int result = lower_bound(A, 3);
		int result = lower_bound(A, 0, A.length - 1, 3);
		System.out.println(result == -1 ? "Not found" : result);
	}
	
	// Loop
	public static int lower_bound(int[] A, int target) {
		if (A == null || A.length == 0) {
			return -1;
		}
		
		int lo = 0, hi = A.length - 1;
		
		while (lo < hi) {
			int mid = lo + (hi - lo) / 2;
			
			if (target == A[mid]) {
			    hi = mid;
			}
			else if (A[mid] < target) {
				lo = mid + 1;
			}
			else {
				hi = mid - 1;
			}
		}
		
		return (A[lo] == target) ? lo : -1;
	}
	
	// Recursion
	public static int lower_bound(int[] A, int lo, int hi, int target) {
		// sanity check
		if (A == null || A.length == 0 || lo > hi) {
			return -1;
		}
		
		if (lo == hi) {
			return (A[lo] == target) ? lo : -1;
		}
		
		int mid = lo + (hi - lo) / 2;
		
		if (A[mid] == target) {
		    return lower_bound(A, lo, mid, target);
		}
		else if (A[mid] < target) {
			return lower_bound(A, mid + 1, hi, target);
		}
		else {
			return lower_bound(A, lo, mid - 1, target);
		}
	}

}
