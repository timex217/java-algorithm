package problems.binary_search;

public class Q008_Binary_Search_Upper_Bound {
	
	public static void main(String[] args) {
		int[] A = {-2, 0, 0, 1, 3, 3, 3, 3, 3, 3, 4, 5, 6, 7, 8, 9};
		//int result = upper_bound(A, 3);
		int result = upper_bound(A, 0, A.length - 1, 3);
		System.out.println(result == -1 ? "Not found" : result);
	}
	
	// Loop
	public static int upper_bound(int[] A, int target) {
		if (A == null || A.length == 0) {
			return -1;
		}
		
		int lo = 0, hi = A.length - 1;
		
		while (lo < hi) {
			int mid = lo + (hi - lo + 1) / 2;
			
			if (A[mid] > target) {
				hi = mid - 1;
			}
			else {
				lo = mid;
			}
		}
		
		return (A[hi] == target) ? hi : -1;
	}
	
	// Recursion
	public static int upper_bound(int[] A, int lo, int hi, int target) {
		// sanity check
		if (A == null || A.length == 0 || lo > hi) {
			return -1;
		}
		
		if (lo == hi) {
			return A[hi] == target ? hi : -1;
		}
		
		int mid = lo + (hi - lo + 1) / 2;
		
		if (A[mid] > target) {
			return upper_bound(A, lo, mid - 1, target);
		}
		else {
			return upper_bound(A, mid, hi, target);
		}
	}

}
