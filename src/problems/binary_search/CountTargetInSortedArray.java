package problems.binary_search;
import java.util.*;

public class CountTargetInSortedArray {
	
	public static void main(String[] args) {
		int[] A = {1, 2, 3, 4, 5, 6, 6, 6, 6, 6, 7, 8, 9};
		int count = countTarget(A, 6);
		System.out.println(count);
	}
	
	public static int countTarget(int[] A, int target) {
		if (A == null || A.length == 0) {
			return 0;
		}
		
		int lower = getLowerBound(A, target);
		int upper = getUpperBound(A, target);
		
		System.out.format("[%d, %d]\n", lower, upper);
		
		if (lower != -1) {
			return upper - lower + 1;
		}
		
		return 0;
	}
	
	public static int getLowerBound(int[] A, int target) {
		if (A == null || A.length == 0) {
			return -1;
		}
		
		int lo = 0;
		int hi = A.length - 1;
		
		while (lo < hi) {
			int mid = lo + (hi - lo) / 2;
			if (A[mid] >= target) {
				hi = mid;
			} else {
				lo = mid + 1;
			}
		}
		
		return A[lo] == target ? lo : -1;
	}
	
	public static int getUpperBound(int[] A, int target) {
		if (A == null || A.length == 0) {
			return -1;
		}
		
		int lo = 0;
		int hi = A.length - 1;
		
		while (lo < hi) {
			int mid = lo + (hi - lo + 1) / 2;
			if (A[mid] <= target) {
				lo = mid;
			} else {
				hi = mid - 1;
			}
		}
		
		return A[hi] == target ? hi : -1;
	}
}
