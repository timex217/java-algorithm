package problems.binary_search;

public class Q010_Binary_Search_First_Greater_Than {
	
	public static void main(String[] args) {
		int[] a = {-2, 0, 1, 3, 4, 5, 6, 7, 8, 9};
		//int result = first_greater_than(A, 3);
		//System.out.println(result == -1 ? "Not found" : result);
		
		int res = searchR(a, 0, a.length - 1, 9);
		if (res != -1) {
			System.out.println(a[res]);
		} else {
			System.out.println("not found.");
		}
	}
	
	// 找最小的一个数使它 > x
	static int searchR(int[] a, int lo, int hi, int x) {
		if (a[lo] > x) {
			return lo;
		}
		
		if (a[hi] <= x) {
			return -1;
		}
		
		int mid = lo + (hi - lo) / 2;
		
		if (a[mid] <= x) {
			if (mid + 1 <= hi && x < a[mid + 1]) {
				return mid + 1;
			} else {
				return searchR(a, mid + 1, hi, x);
			}
		} else {
			if (mid - 1 >= lo && a[mid - 1] <= x) {
				return mid;
			} else {
				return searchR(a, lo, mid - 1, x);
			}
		}
	}
	
	public static int first_greater_than(int[] A, int target) {
		if (A == null || A.length == 0) {
			return -1;
		}
		
		int lo = 0, hi = A.length - 1;
		
		while (lo < hi) {
			int mid = lo + (hi - lo) / 2;
			
			if (A[mid] > target) {
				hi = mid;
			}
			else {
				lo = mid + 1;
			}
		}
		
		return (A[lo] > target) ? lo : -1;
	}

}
