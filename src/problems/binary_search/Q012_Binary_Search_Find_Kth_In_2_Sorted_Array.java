/**
 * Refer: http://blog.csdn.net/luckyxiaoqiang/article/details/8937978
 */

package problems.binary_search;

public class Q012_Binary_Search_Find_Kth_In_2_Sorted_Array {
	
	public static void main(String[] args) {
		int[] A = {0, 2, 4, 6, 8, 10};
		int[] B = {1, 3, 5, 7};
		
		int result = find(A, B, 5);
		System.out.println(result == -1 ? "Not found" : result);
	}
	
	public static int find(int[] A, int[] B, int k) {
		return -1;
	}

}
