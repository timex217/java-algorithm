package problems.binary_search;

public class Q009_Binary_Search_Last_Less_Than {
	
	public static void main(String[] args) {
		int[] a = {-2, 0, 1, 3, 4, 5, 6, 7, 8, 9};
		//int result = last_less_than(A, 3);
		//System.out.println(result == -1 ? "Not found" : result);
		
		int res = searchR(a, 0, a.length - 1, 3);
		if (res != -1) {
			System.out.println(a[res]);
		} else {
			System.out.println("not found.");
		}
	}
	
	// 找最大的一个数使它 < x
	static int searchR(int[] a, int lo, int hi, int x) {
		if (a[hi] < x) {
			return hi;
		}
		
		if (a[lo] >= x) {
			return -1;
		}
		
		int mid = lo + (hi - lo) / 2;
		
		if (a[mid] >= x) {
			if (mid - 1 >= lo && a[mid - 1] < x) {
				return mid - 1;
			} else {
				return searchR(a, lo, mid - 1, x);
			}
		} else {
			if (mid + 1 <= hi && a[mid + 1] >= x) {
				return mid;
			} else {
				return searchR(a, mid + 1, hi, x);
			}
		}
	}
	
	public static int last_less_than(int[] A, int target) {
		if (A == null || A.length == 0) {
			return -1;
		}
		
		int lo = 0, hi = A.length - 1;
		
		while (lo < hi) {
			int mid = lo + (hi - lo + 1) / 2;
			
			if (A[mid] < target) {
				lo = mid;
			}
			else {
				hi = mid - 1;
			}
		}
		
		return (A[hi] < target) ? hi : -1;
	}

}
