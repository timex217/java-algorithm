package problems.binary_search;

public class Q014_Binary_Search_Min_Max_In_Rotate_Array {
	
	public static void main(String[] args) {
		int[] A = {7, 8, 9, 0, 1, 2, 3, 4, 5, 6};
		
		int min_index = search_min(A);
		int max_index = search_max(A);
		int kth_index = search_min(A, 8);
		
		System.out.println(min_index == -1 ? "Min not found" : A[min_index]);
		System.out.println(max_index == -1 ? "Max not found" : A[max_index]);
		System.out.println(kth_index == -1 ? "kth not found" : A[kth_index]);
	}
	
	public static int search_min(int[] A) {
		if (A == null || A.length == 0) {
			return -1;
		}
		
		int lo = 0, hi = A.length - 1;
		
		// make sure mid != lo && mid != hi
		while (lo < hi - 1) {
			int mid = lo + (hi - lo) / 2;
			
			if (A[lo] <= A[mid]) {
				lo = mid;
			}
			else {
				hi = mid;
			}
		}
		
		return (A[lo] < A[hi]) ? lo : hi;
	}
	
	public static int search_max(int[] A) {
		if (A == null || A.length == 0) {
			return -1;
		}
		
		int lo = 0, hi = A.length - 1;
		
		while (lo < hi - 1) {
			int mid = lo + (hi - lo) / 2;
			
			if (A[lo] <= A[mid]) {
				lo = mid;
			}
			else {
				hi = mid;
			}
		}
		
		return (A[lo] > A[hi]) ? lo : hi;
	}
	
	public static int search_min(int[] A, int k) {
		if (A == null || A.length == 0) {
			return -1;
		}
		
		int lo = 0, hi = A.length - 1;
		
		// make sure mid != lo && mid != hi
		while (lo < hi - 1) {
			int mid = lo + (hi - lo) / 2;
			
			if (A[lo] <= A[mid]) {
				lo = mid;
			}
			else {
				hi = mid;
			}
		}
		
		int min_index = (A[lo] < A[hi]) ? lo : hi;
		
		return (min_index + k - 1) % A.length;
	}

}
