package problems.binary_search;

public class Q011_Binary_Search_Min_Abs {
	
	public static void main(String[] args) {
		int[] A = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
		int result = search_min_abs(A);
		System.out.println(result == -1 ? "Not found" : A[result]);
	}
	
	public static int search_min_abs(int[] A) {
		if (A == null || A.length == 0) {
			return -1;
		}
		
		int lo = 0, hi = A.length - 1;
		
		while (lo < hi) {
			int mid = lo + (hi - lo) / 2;
			
			if (A[mid] >= 0) {
				hi = mid;
			}
			else {
				lo = mid + 1;
			}
		}
		
		if (lo > 0 && Math.abs(A[lo - 1]) < Math.abs(A[lo])) {
			return lo - 1;
		}
		else {
			return lo;
		}
	}

}
