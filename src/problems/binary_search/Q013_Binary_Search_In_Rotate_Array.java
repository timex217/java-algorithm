package problems.binary_search;

public class Q013_Binary_Search_In_Rotate_Array {
	
	public static void main(String[] args) {
		int[] A = {7, 8, 9, 0, 1, 2, 3, 4, 5, 6};
		int result = search(A, 3);
		System.out.println(result == -1 ? "Not found" : result);
	}
	
	public static int search(int[] A, int target) {
		if (A == null || A.length == 0) {
			return -1;
		}
		
		int lo = 0, hi = A.length - 1;
		
		while (lo <= hi) {
			int mid = lo + (hi - lo) / 2;
			
			if (A[mid] == target) {
				return mid;
			}
			
			
			if (A[lo] <= A[mid]) {
				if (target >= A[lo] && target < A[mid]) {
					hi = mid - 1;
				}
				else {
					lo = mid + 1;
				}
			}
			else {
				if (target > A[mid] && target <= A[hi]) {
					lo = mid + 1;
				}
				else {
					hi = mid - 1;
				}
			}
		}
		
		return -1;
	}

}
