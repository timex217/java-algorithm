/**
 * Given a sorted dictionary with unknown size, how to determine whether a word is in this dictionary or not.
 * 
 * Idea: first we need to find the upper bound, then perform binary search.
 */

package problems.binary_search;

import java.util.*;
import java.lang.*;

public class Q015_Binary_Search_Word_In_Dictionary {
	
	public static void main(String[] args) {
		int[] A = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17};
		
		int result = search(A, 11);
		
		System.out.println(result == -1 ? "Not found" : result);
	}
	
	public static int search(int[] A, int target) {
		if (A == null) {
			return -1;
		}
		
		// step 1. get upper bound
		int upper_bound = find_upper_bound(A);
		if (upper_bound == -1) {
			return -1;
		}
		
		// step 2. binary search
		return binary_search(A, 0, upper_bound, target);
	}
	
	public static int binary_search(int[] A, int lo, int hi, int target) {
		while (lo <= hi) {
			int mid = lo + (hi - lo) / 2;
			
			if (A[mid] == target) {
				return mid;
			}
			else if (A[mid] < target) {
				lo = mid + 1;
			}
			else {
				hi = mid - 1;
			}
		}
		
		return -1;
	}
	
	public static int find_upper_bound(int[] A) {
		int size = 10;
		boolean done = false;
		
		//1. first try the size by 10, 20 until we found null
		while (!done) {
			try {
				int a = A[size];
				size = size * 2;
			} 
			catch (ArrayIndexOutOfBoundsException e) {
				done = true;
			}
		}
		
		// 2. find the right border (lower bound of null)
		int lo = 0, hi = size;
		
		while (lo < hi) {
			int mid = lo + (hi - lo) / 2;
			
			try {
				int a = A[mid];
				lo = mid + 1;
			} 
			catch (ArrayIndexOutOfBoundsException e) {
				hi = mid - 1;
			}
		}
		
		return hi;
	}

}
