/**
 * Sort a stack using recursion. 
 */

package problems.stack;

import java.util.*;

public class Q041_Sort_Stack_Recursion {
	
	public static void main(String[] args) {
		Stack<Integer> stack = new Stack<Integer>();
		stack.push(3);
		stack.push(1);
		stack.push(5);
		stack.push(4);
		stack.push(3);
		stack.push(2);
		stack.push(7);
		stack.push(8);
		stack.push(9);
		stack.push(0);
		stack.push(6);
		print(stack);
		System.out.println("-----");
		sortStack(stack);
		print(stack);
		System.out.println("-----");
	}
	
	public static void sortStack(Stack<Integer> stack) {
		if (stack != null && !stack.isEmpty()) {
			int top = stack.pop();
			sortStack(stack);
			insert(stack, top);
		}
	}
	
	public static void insert(Stack<Integer> stack, int t) {
		if (stack.isEmpty()) {
			stack.push(t);
		}
		else if (t <= stack.peek()) {
			stack.push(t);
		} 
		else {
			int top = stack.pop();
			insert(stack, t);
			stack.push(top);
		}
	}
	
	public static void print(Stack<Integer> stack) {
		if (stack != null && !stack.isEmpty()) {
			int top = stack.pop();
			System.out.println(top);
			print(stack);
			stack.push(top);
		}
	}

}
