package problems.stack;

public class Q016_Stack_Array_Implementation {
	
	private static class Stack {
		int[] arr;
		int total;
		
		public Stack() {
			total = 0;
			arr = new int[1];
		}
		
		public void push(int x) {
			if (arr.length == total) {
				resize(arr.length * 2);
			}
			
			arr[++total] = x;
		}
		
		public int pop() {
			if (total > 0) {
				int x = arr[--total];
				
				if (total == arr.length / 4) {
					resize(arr.length / 2);
				}
				
				return x;
			} else {
				return -1;
			}
		}
		
		private void resize(int cap) {
			int[] tmp = new int[cap];
			for (int i = 0; i < total; i++) {
				tmp[i] = arr[i];
			}
			arr = tmp;
		}
	}
	
	public static void main(String[] args) {
		
	}

}
