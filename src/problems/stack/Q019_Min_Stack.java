package problems.stack;

import java.util.*;

public class Q019_Min_Stack {
	
	private static class MinStack {
		private Stack<Integer> dataStack = new Stack<Integer>();
	    private Stack<Integer> minStack = new Stack<Integer>();
	    private int total = 0;
	    
	    public void push(int x) {
	        dataStack.push(x);
	        if (total == 0 || x <= minStack.peek().intValue()) {
	            minStack.push(x);
	        }
	        total++;
	    }

	    public void pop() {
	        if (total > 0) {
	            if (dataStack.peek().intValue() == minStack.peek().intValue()) {
	                minStack.pop();
	            }
	            dataStack.pop();
	            total--;
	        }
	    }

	    public int top() {
	        if (total > 0) {
	            return dataStack.peek();
	        } else {
	            return 0;
	        }
	    }

	    public int getMin() {
	        if (total > 0) {
	            return minStack.peek();
	        } else {
	            return 0;
	        }
	    }
	}
	
	public static void main(String[] args) {
		MinStack min_stack = new MinStack();
		
		min_stack.push(512);
		min_stack.push(-1024);
		min_stack.push(-1024);
		min_stack.push(512);
		
		min_stack.pop();
		System.out.println(min_stack.getMin());
		min_stack.pop();
		System.out.println(min_stack.getMin());
		min_stack.pop();
		System.out.println(min_stack.getMin());
	}

}
