/**
 * You are not allowed to use loop constructs like while, for..etc, and you can only use the following ADT functions on Stack S:
 * isEmpty(S)
 * push(S)
 * pop(S)
 */

package problems.stack;

import java.util.*;

public class Q040_Reverse_Stack_Recursion<T> {
	
	public static void main(String[] args) {
		Stack<Integer> stack = new Stack<Integer>();
		stack.push(3);
		stack.push(1);
		stack.push(5);
		stack.push(4);
		stack.push(3);
		stack.push(2);
		stack.push(7);
		stack.push(8);
		stack.push(9);
		stack.push(0);
		stack.push(6);
		print(stack);
		System.out.println("-----");
		reverseStack(stack);
		print(stack);
		System.out.println("-----");
	}
	
	public static void reverseStack(Stack<Integer> stack) {
		if (stack != null && !stack.isEmpty()) {
			int top = stack.pop();
			reverseStack(stack);
			addToBottom(stack, top);
		}
	}
	
	public static void addToBottom(Stack<Integer> stack, int t) {
		if (stack.isEmpty()) {
			stack.push(t);
		}
		else {
			int top = stack.pop();
			addToBottom(stack, t);
			stack.push(top);
		}
	}
	
	public static void print(Stack<Integer> stack) {
		if (stack != null && !stack.isEmpty()) {
			int top = stack.pop();
			System.out.println(top);
			print(stack);
			stack.push(top);
		}
	}

}
