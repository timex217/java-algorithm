package problems.stack;

import java.util.*;

public class Q018_Queue_Two_Stacks {
	
	private static class Queue<T> {
		Stack<T> s1; // used to add
		Stack<T> s2; // used to poll
		
		Queue() {
			s1 = new Stack<T>();
			s2 = new Stack<T>();
		}
		
		public void add(T val) {
			s1.push(val);
		}
		
		public T peek() {
			if (size() == 0) {
				return null;
			}
			
			while (!s1.isEmpty()) {
				s2.push(s1.pop());
			}
			
			T val = s2.peek();
			
			while (!s2.isEmpty()) {
				s1.push(s2.pop());
			}
			
			return val;
		}
		
		public T poll() {
			if (size() == 0) {
				return null;
			}
			
			while (!s1.isEmpty()) {
				s2.push(s1.pop());
			}
			
			T val = s2.pop();
			
			while (!s2.isEmpty()) {
				s1.push(s2.pop());
			}
			
			return val;
		}
		
		public boolean isEmpty() {
			return s1.isEmpty();
		}
		
		public int size() {
			return s1.size();
		}
	}
	
	public static void main(String[] args) {
		Queue<Integer> q = new Queue<Integer>();
		q.add(0);
		int val = q.peek();
		System.out.println(val);
	}

}
