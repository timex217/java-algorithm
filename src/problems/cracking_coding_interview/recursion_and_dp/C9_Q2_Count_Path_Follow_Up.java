package problems.cracking_coding_interview.recursion_and_dp;

import java.util.*;

public class C9_Q2_Count_Path_Follow_Up {
    
    static class Point {
        int x;
        int y;
        Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
    
    static int[][] M = {
        {1, 1, 1, 1, 0},
        {1, 1, 1, 0, 1},
        {1, 0, 1, 1, 1},
        {1, 0, 1, 1, 1}
    };
    
    // --------------------
    //  Recursion
    // --------------------
    static boolean getPath(int x, int y, ArrayList<Point> path) {
        Point p = new Point(x, y);
        path.add(p);
        
        // 如果已经走到 (0, 0) 了，结束查找
        if (x == 0 && y == 0) {
            return true;
        }
        
        boolean success = false;
        
        // 试着往上走
        if (x > 0 && isFree(x - 1, y)) {
            success = getPath(x - 1, y, path);
        }
        
        // 试着往左走
        if (!success && y > 0 && isFree(x, y - 1)) {
            success = getPath(x, y - 1, path);
        }
        
        // 若从这个点出发都无法达到 (0, 0)，将其从path中删除
        if (!success) {
            path.remove(p);
        }
        
        return success;
    }
    
    // Check whether the point is accessible
    static boolean isFree(int x, int y) {
        return M[x][y] == 1;
    }
    
    // ----------------------
    //  DP
    // ----------------------
    static boolean getPath(int x, int y, ArrayList<Point> path, Hashtable<Point, Boolean> cache) {
        if (x == 0 && y == 0) {
            return true;
        }
        
        Point p = new Point(x, y);
        
        // 首先检查下这个点我们是否已经试过
        if (cache.containsKey(p)) {
            return cache.get(p);
        }
        
        // 处理当前点
        path.add(p);
        
        // 递归
        boolean success = false;
        
        if (x > 0 && isFree(x - 1, y)) {
            success = getPath(x - 1, y, path, cache);
        }
        
        if (!success && y > 0 && isFree(x, y - 1)) {
            success = getPath(x, y - 1, path, cache);
        }
        
        // 恢复当前点
        if (!success) {
            path.remove(p);
        }
        
        cache.put(p, success);
        
        return success;
    }
    
    public static void main(String[] args) {
        
    }

}
