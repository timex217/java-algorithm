package problems.cracking_coding_interview.recursion_and_dp;

/**
 * Imagine a robot sitting on the upper left corner of an X by Y gird. The robot can only move in two directions: right and down.
 * How many possible paths are there for the robot to go from (0, 0) to (X, Y) ?
 * 
 * FOLLOW UP
 * 
 * Imagine certain spots are "off limits", such that the robot cannot step on them.
 * Design an algorithm to find a path for the robot from the top left to the bottom right.
 */

public class C9_Q2_Count_Path {
    
    public static void main(String[] args) {
        int res = countPathDP(2, 2);
        System.out.println(res);
    }
    
    // -----------------------------
    //  Recursion
    // -----------------------------
    static int countPath(int x, int y) {
        if (x < 0 || y < 0) {
            return 0;
        }
        
        if (x == 0 && y == 0) {
            return 1;
        }
        
        return countPath(x - 1, y) + countPath(x, y - 1);
    }
    
    // -----------------------------
    //  DP
    // -----------------------------
    static int countPathDP(int x, int y) {
        if (x < 0 || y < 0) {
            return 0;
        }
        
        // dp(i, j) represents the ways of path to go from (0, 0) to (i, j)
        // dp(i, j) = 1, i = 0 && j = 0
        // dp(i, j) = dp(i - 1, j) + dp(i, j - 1)
        
        int[][] dp = new int[x + 1][y + 1];
        
        for (int i = 0; i <= x; i++) {
            for (int j = 0; j <= y; j++) {
                if (i == 0 && j == 0) {
                    dp[i][j] = 1;
                } else if (i == 0) {
                    dp[i][j] = dp[i][j - 1];  
                } else if (j == 0) {
                    dp [i][j] = dp[i - 1][j];
                } else { 
                    dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
                }
            }
        }
        
        return dp[x][y];
    }

}
