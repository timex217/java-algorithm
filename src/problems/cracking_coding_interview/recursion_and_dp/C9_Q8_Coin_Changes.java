package problems.cracking_coding_interview.recursion_and_dp;

/**
 * Given an infinite number of quarters(25 cents), dimes(10 cents), nickels(5 cents) and pennies(1 cent),
 * write code to calculate the number of ways of representing n cents
 */

import java.util.*;

public class C9_Q8_Coin_Changes {
    
    static int[] coins = {25, 10, 5, 1};
    
    public static void main(String[] args) {
        int n = 99;
        int res = countWays(n);
        
        System.out.println(res);
    }
    
    static int countWays(int n) {
        if (n <= 0) {
            return 0;
        }
        
        ArrayList<Integer> sol = new ArrayList<Integer>(Arrays.asList(0, 0, 0, 0));
        ArrayList<ArrayList<Integer>> res = new ArrayList<ArrayList<Integer>>();
        
        helper(n, 0, sol, res);
        
        System.out.println(res.toString());
        return res.size();
    }
    
    static void helper(int n, int index, ArrayList<Integer> sol, ArrayList<ArrayList<Integer>> res) {
        if (index == coins.length - 1) {
            sol.set(index, n);
            res.add(new ArrayList<Integer>(sol));
            return;
        }
        
        // 处理当前层
        for (int i = 0; i * coins[index] <= n; i++) {
            sol.set(index, i);
            // 递归
            helper(n - coins[index] * i, index + 1, sol, res);
        }
    }

}
