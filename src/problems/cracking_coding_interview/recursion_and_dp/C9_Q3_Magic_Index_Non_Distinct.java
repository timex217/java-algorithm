package problems.cracking_coding_interview.recursion_and_dp;

public class C9_Q3_Magic_Index_Non_Distinct {
    
    public static void main(String[] args) {
        int[] A = {-10, -5, 2, 2, 2, 3, 4, 7, 9, 12, 13};
        //           0,  1, 2, 3, 4, 5, 6, 7, 8,  9, 10
        int index = getMagicIndex(A);
        System.out.println(index);
    }
    
    static int getMagicIndex(int[] A) {
        if (A == null || A.length == 0) {
            return -1;
        }
        
        return helper(A, 0, A.length - 1);
    }
    
    static int helper(int[] A, int lo, int hi) {
        if (lo > hi) {
            return -1;
        }
        
        if (lo == hi) {
            return (A[lo] == lo) ? lo : -1;
        }
        
        int mid = lo + (hi - lo) / 2;
        
        if (A[mid] == mid) {
            return mid;
        }
        
        // search left
        int left_index = Math.min(mid - 1, A[mid]);
        int left = helper(A, lo, left_index);
        if (left >= 0) {
            return left;
        }
        
        // search right
        int right_index = Math.max(mid + 1, A[mid]);
        int right = helper(A, right_index, hi);
        
        return right;
    }

}
