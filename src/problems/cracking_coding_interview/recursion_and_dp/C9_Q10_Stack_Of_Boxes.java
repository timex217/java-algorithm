package problems.cracking_coding_interview.recursion_and_dp;

/**
 * You have a stack of n boxes, with widths w[1...n], heights h[1...n], and depths d[1...n].
 * The boxes cannot be rotated and can only be stacked on top of one another if each box in the stack is strickly larger than the box above it in width, height and depth.
 * Implement a method to build the tallest stack possible, where the height of a stack is the sum of the heights of each box.
 */

import java.util.*;

public class C9_Q10_Stack_Of_Boxes {
    
    static class Box {
        int width;
        int height;
        int depth;
        
        Box(int w, int h, int d) {
            this.width = w;
            this.height = h;
            this.depth = d;
        }
        
        boolean canBeAbove(Box box) {
            if (box == null) {
                return true;
            }
            
            if (this.width >= box.width || 
                this.height >= box.height ||
                this.depth >= box.depth) {
                return false;
            }
            
            return true;
        }
    }
    
    // -----------------------
    //  Recursion
    // -----------------------
    static ArrayList<Box> createStack(Box[] boxes, Box bottom) {
        int max_height = 0;
        ArrayList<Box> max_stack = new ArrayList<Box>();
        
        // 遍历所有的 box
        for (int i = 0; i < boxes.length; i++) {
            // 如果某个 box 能放在 bottom box 的上方
            if (boxes[i].canBeAbove(bottom)) {
                // 就递归获取以该 box 为底的新叠层
                ArrayList<Box> new_stack = createStack(boxes, boxes[i]);
                
                // 更新 max_height 和 max_stack
                int new_height = stackHeight(new_stack);
                if (new_height > max_height) {
                    max_stack = new_stack;
                    max_height = new_height;
                }
            }
        }
        
        if (bottom != null) {
            max_stack.add(0, bottom); // insert in bottom of stack
        }
        
        return max_stack;
    }
    
    static int stackHeight(ArrayList<Box> stack) {
        int height = 0;
        for (Box box : stack) {
            height += box.height;
        }
        return height;
    }
    
    public static void main(String[] args) {
        
    }

}
