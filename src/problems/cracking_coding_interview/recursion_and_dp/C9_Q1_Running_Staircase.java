package problems.cracking_coding_interview.recursion_and_dp;

/**
 * A child is running up a staircase with n steps, and can hop either 1 step, 2 steps, or 3 steps at a time. 
 * Implement a method to count how many possible ways the child can run up the stairs.
 * 
 *    n: 0, 1, 2, 3 ... 8
 * ways: 1, 1, 2, 4 ... 44
 */

public class C9_Q1_Running_Staircase {
    
    public static void main(String[] args) {
        int n = 3;
        int ways = countWaysDP(n);
        
        System.out.println(ways);
    }
    
    // -------------------------
    //  Recursion
    // -------------------------
    static int countWays(int n) {
        if (n < 0) {
            return 0;
        }
        
        if (n == 0) {
            return 1;
        }
        
        return countWays(n - 1) + countWays(n - 2) + countWays(n - 3);
    }
    
    // --------------------------
    //  DP
    // --------------------------
    static int countWaysDP(int n) {
        if (n < 0) {
            return 0;
        }
        
        // dp(i) represents the ways to hop to step i
        // dp(i) = 1, i = 0
        // dp(i) = dp(i-1) + dp(i-2) + dp(i-3)
        
        int[] dp = new int[n + 1];
        dp[0] = 1;
        
        for (int i = 1; i <= n; i++) {
            int d1 = i < 1 ? 0 : dp[i-1];
            int d2 = i < 2 ? 0 : dp[i-2];
            int d3 = i < 3 ? 0 : dp[i-3];
            
            dp[i] = d1 + d2 + d3;
        }
        
        return dp[n];
    }

}
