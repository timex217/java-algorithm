package problems.cracking_coding_interview.recursion_and_dp;

/**
 * Implement the "paint fill" function that one might see on many image editing programs. 
 * That is, given a screen (represented by a two-dimensional array of colors), a point, and a new color,
 * fill in the surrounding area until the color changes from the original color.
 */

import java.util.*;

public class C9_Q7_Paint_Fill {
    
    static class Point {
        int x;
        int y;
        Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
    
    static enum Color {
        Black, White, Red, Yellow, Green
    }
    
    // ------------------------------
    //  Recursion
    // ------------------------------
    boolean paintFill(Color[][] screen, int x, int y, Color oldColor, Color newColor) {
        // 超出屏幕边界, 返回 false
        if (x < 0 || x >= screen[0].length ||
            y < 0 || y >= screen.length) {
            return false;
        }
        
        // 若当前点是旧颜色，将其替换为新颜色，并dfs
        if (screen[y][x] == oldColor) {
            
            screen[y][x] = newColor;
            
            paintFill(screen, x - 1, y, oldColor, newColor); // left
            paintFill(screen, x + 1, y, oldColor, newColor); // right
            paintFill(screen, x, y - 1, oldColor, newColor); // up
            paintFill(screen, x, y + 1, oldColor, newColor); // down
        }
        
        return true;
    }
    
    // ------------------------------
    //  DFS
    // ------------------------------
    static void paintFillDFS(Color[][] screen, int x, int y, Color oldColor, Color newColor) {
        Stack<Point> stack = new Stack<Point>();
        stack.push(new Point(x, y));
        
        while (!stack.isEmpty()) {
            Point p = stack.pop();
            
            // 超出屏幕边界, 继续下一个
            if (p.x < 0 || p.x >= screen[0].length ||
                p.y < 0 || p.y >= screen.length) {
                continue;
            }
            
            // 若当前点的颜色不是旧色，继续下一个
            if (screen[p.y][p.x] != oldColor) {
                continue;
            }
            
            // 处理当前节点
            screen[p.y][p.x] = newColor;
            
            stack.push(new Point(p.x - 1, p.y));
            stack.push(new Point(p.x + 1, p.y));
            stack.push(new Point(p.x, p.y - 1));
            stack.push(new Point(p.x, p.y + 1));
        }
    }
    
    // ------------------------------
    //  BFS
    // ------------------------------
    static void paintFillBFS(Color[][] screen, int x, int y, Color oldColor, Color newColor) {
        Queue<Point> queue = new LinkedList<Point>();
        queue.add(new Point(x, y));
        
        while (!queue.isEmpty()) {
            Point p = queue.poll();
            
            // 超出屏幕边界, 继续下一个
            if (p.x < 0 || p.x >= screen[0].length ||
                p.y < 0 || p.y >= screen.length) {
                continue;
            }
            
            // 若当前点的颜色不是旧色，继续下一个
            if (screen[p.y][p.x] != oldColor) {
                continue;
            }
            
            // 处理当前节点
            screen[p.y][p.x] = newColor;
            
            queue.add(new Point(p.x - 1, p.y));
            queue.add(new Point(p.x + 1, p.y));
            queue.add(new Point(p.x, p.y - 1));
            queue.add(new Point(p.x, p.y + 1));
        }
    }
    
    public static void main(String[] args) {
        
    }

}
