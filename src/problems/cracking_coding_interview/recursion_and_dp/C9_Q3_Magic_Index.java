package problems.cracking_coding_interview.recursion_and_dp;

/**
 * A magic index in an array A[1...n-1] is defined to be an index such that A[i] = i.
 * Given a sorted array of distinct integers, write a method to find a magic index, if one exists, in array A.
 * 
 * Follow Up
 * What if the values are not distinct
 */

public class C9_Q3_Magic_Index {
    
    public static void main(String[] args) {
        int[] A = {-40, -20, -1, 1, 2, 3, 5, 7, 9, 12, 13};
        //           0,   1,  2, 3, 4, 5, 6, 7, 8,  9, 10
        int index = getMagicIndex(A);
        System.out.println(index);
    }
    
    static int getMagicIndex(int[] A) {
        if (A == null || A.length == 0) {
            return -1;
        }
        
        return helper(A, 0, A.length - 1);
    }
    
    static int helper(int[] A, int lo, int hi) {
        if (lo > hi) {
            return -1;
        }
        
        if (lo == hi) {
            return (A[lo] == lo) ? lo : -1;
        }
        
        int mid = lo + (hi - lo) / 2;
        
        if (A[mid] == mid) {
            return mid;
        }
        else if (A[mid] < mid) {
            return helper(A, mid + 1, hi);
        }
        else {
            return helper(A, lo, mid - 1);
        }
    }

}
