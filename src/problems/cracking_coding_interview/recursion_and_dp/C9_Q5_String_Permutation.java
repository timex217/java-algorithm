package problems.cracking_coding_interview.recursion_and_dp;

/**
 * Write a method to compute all permutations of a string.
 */

import java.util.*;

public class C9_Q5_String_Permutation {
    
    public static void main(String[] args) {
        String str = "Jing";
        ArrayList<String> perms = getPermutations(str);
        
        System.out.println(perms.toString());
    }
    
    static ArrayList<String> getPermutations(String str) {
        if (str == null) {
            return null;
        }
        
        ArrayList<String> res = new ArrayList<String>();
        
        // case 1: 字符串为空
        if (str.length() == 0) {
            res.add("");
            return res;
        }
        
        // get the first character
        char first = str.charAt(0);
        // the rest of the string
        String remainder = str.substring(1);
        
        ArrayList<String> words = getPermutations(remainder);
        
        for (String word: words) {
            for (int i = 0; i <= word.length(); i++) {
                String s = insertCharAt(first, word, i);
                res.add(s);
            }
        }
        
        return res;
    }
    
    static String insertCharAt(char c, String word, int i) {
        String start = word.substring(0, i);
        String end = word.substring(i);
        return start + c + end;
    }

}
