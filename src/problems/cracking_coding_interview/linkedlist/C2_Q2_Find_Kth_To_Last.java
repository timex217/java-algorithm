package problems.cracking_coding_interview.linkedlist;

/**
 * Implement an algorithm to find the kth to last element of a singly linked list
 */

import java.util.*;

public class C2_Q2_Find_Kth_To_Last {
    
    static class ListNode {
        int val;
        ListNode next;
        ListNode(int x) {
            this.val = x;
        }
    }
    
    // Non-recursion
    static ListNode kToLast(ListNode head, int k) {
        if (k <= 0) {
            return null;
        }
        
        ListNode slow = head;
        ListNode fast = head;
        
        while (k > 0) {
            if (fast == null) {
                return null;
            }
            
            fast = fast.next;
            k--;
        }
        
        while (fast != null) {
            slow = slow.next;
            fast = fast.next;
        }
        
        return slow;
    }
    
    // Recursion
    static class IntWrapper {
        int value = 0;
    }
    
    static ListNode kToLastR(ListNode head, int k, IntWrapper i) {
        // base case
        if (head == null) {
            return null;
        }
        
        ListNode node = kToLastR(head.next, k, i);
        i.value = i.value + 1;
        
        if (i.value == k) {
            return head;
        }
        
        return node;
    }
    
    public static void main(String[] args) {
        
    }

}
