package problems.cracking_coding_interview.linkedlist;

/**
 * Write code to partition a linked list around a value x, such that all nodes less than x come before
 * all nodes greater than or equal to x.
 */

import java.util.*;

public class C2_Q4_Partition_Linked_List {
    
    static class ListNode {
        int val;
        ListNode next;
        ListNode(int x) {
            this.val = x;
        }
    }
    
    static ListNode partition(ListNode head, int x) {
        if (head == null) {
            return null;
        }
        
        ListNode beforeHead = null;
        ListNode beforeTail = null;
        ListNode afterHead = null;
        
        // step 1. go through the list and create the before list and after list
        ListNode node = head;
        ListNode next = null;
        
        while (node != null) {
            next = node.next;
            
            // if node val is less than x
            // insert node to before list
            if (node.val < x) {
                node.next = beforeHead;
                beforeHead = node;
                
                if (beforeTail == null) {
                    beforeTail = node;
                }
            }
            // if node val is greater than x
            // insert node to after list
            else {
                node.next = afterHead;
                afterHead = node;
            }
        }
        
        // step 2. connect the before and after lists
        if (beforeHead == null) {
            return afterHead;
        }
        else {
            beforeTail.next = afterHead;
            return beforeHead;
        }
    }
    
    public static void main(String[] args) {
        
    }

}
