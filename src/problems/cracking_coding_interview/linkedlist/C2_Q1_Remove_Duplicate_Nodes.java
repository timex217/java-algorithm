package problems.cracking_coding_interview.linkedlist;

/**
 * Write code to remove duplicates from an unsorted linked list.
 * 
 * Follow Up
 * 
 * How would you solve this problem if a temporary buffer is not allowed?
 */

import java.util.*;

public class C2_Q1_Remove_Duplicate_Nodes {
    
    static class ListNode {
        int val;
        ListNode next;
        ListNode(int x) {
            this.val = x;
        }
    }
    
    // 这里的删除，如果重复，要保留一个
    static void removeDups(ListNode head) {
        ListNode node = head;
        ListNode prev = null;
        
        Set<ListNode> set = new HashSet<ListNode>();
        
        while (node != null) {
            if (set.contains(node.val)) {
                prev.next = node.next;
            }
            else {
                set.add(node);
                prev = node;
            }
            node = node.next;
        }
    }
    
    // Follow Up: No Buffer Allowed
    static void removeDupsWithoutBuffer(ListNode head) {
        if (head == null) {
            return;
        }
        
        ListNode current = head;
        
        while (current != null) {
            ListNode runner = current;
            
            while (runner.next != null) {
                if (runner.next.val == current.val) {
                    runner.next = runner.next.next;
                }
                else {
                    runner = runner.next;
                }
            }
            
            current = current.next;
        }
    }
    
    public static void main(String[] args) {
        
    }

}
