package problems.cracking_coding_interview.linkedlist;

/**
 * Implement a function to check if a linked list is a palindrome
 * 
 * 简单的方法是将链表翻转然后从头比较
 */

import java.util.*;

public class C2_Q7_Check_Linked_List_Palindrome {
    
    static class ListNode {
        int val;
        ListNode next;
        ListNode(int x) {
            this.val = x;
        }
    }
    
    /**
     * 利用一个stack，记录慢指针的数值, 当快指针到尽头时，慢指针和stack比较，直到慢指针走到尽头
     */
    static boolean isPalindrome(ListNode head) {
        ListNode slow = head;
        ListNode fast = head;
        
        Stack<Integer> stack = new Stack<Integer>();
        
        // step 1. fast pointer moves at 2x speed
        while (fast != null && fast.next != null) {
            stack.push(slow.val);
            slow = slow.next;
            fast = fast.next.next;
        }
        
        // step 2. skip the middle one
        if (fast != null) {
            slow = slow.next;
        }
        
        // step 3. compare with stack
        while (slow != null) {
            int val = stack.pop();
            if (val != slow.val) {
                return false;
            }
            slow = slow.next;
        }
        
        return true;
    }
    
    public static void main(String[] args) {
        
    }

}
