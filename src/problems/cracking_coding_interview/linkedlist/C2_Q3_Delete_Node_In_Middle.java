package problems.cracking_coding_interview.linkedlist;

/**
 * Implement an algorithm to delete a node in the middle of a singly linked list, given only access to that node.
 */

public class C2_Q3_Delete_Node_In_Middle {
    
    static class ListNode {
        int val;
        ListNode next;
        ListNode(int x) {
            this.val = x;
        }
    }
    
    static boolean deleteNode(ListNode node) {
        if (node == null || node.next == null) {
            return false;
        }
        
        node.val = node.next.val;
        node.next = node.next.next;
        
        return true;
    }
    
    public static void main(String[] args) {
        
    }

}
