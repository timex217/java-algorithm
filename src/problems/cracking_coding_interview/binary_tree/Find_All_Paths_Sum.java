package problems.cracking_coding_interview.binary_tree;

/**
 * You are given a binary tree in which each node contains a value.
 * Design an algorithm to print all paths which sum to a given value.
 * Note that a path can start or end anywhere in the tree.
 */

import java.util.*;

public class Find_All_Paths_Sum {
    
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) {
            this.val = x;
        }
    }
    
    static void helper(TreeNode root, int sum, ArrayList<TreeNode> sol, ArrayList<ArrayList<TreeNode>> res) {
        if (root == null) {
            return;
        }
        
        if (sum < 0) {
            
        }
    }
    
    public static void main(String[] args) {
        
    }

}
