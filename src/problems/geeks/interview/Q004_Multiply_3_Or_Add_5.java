package problems.geeks.interview;

/**
 * Intuit Interview | Set 5 (On-Campus for Internship)
 * 
 * A number starting from 1 can be got by either multiplying 3 or adding 5 to it. 
 * Given a number, find the sequence of operations to get it or say it�s not possible.
 * 
 * Eg: 13 = 1 * 3 + 5 + 5, 
 *     15: Not possible
 */

public class Q004_Multiply_3_Or_Add_5 {
	
	public static void main(String[] args) {
		int n = 26;
		boolean res = dfs(1, n, "1");
		if (!res) {
			System.out.println("Not possible");
		}
	}
	
	static boolean dfs(int s, int n, String sol) {
		if (s == n) {
			System.out.println(sol);
			return true;
		}
		
		if (s > n) {
			return false;
		}
		
		return dfs(s * 3, n, sol + " * 3") || dfs(s + 5, n, sol + " + 5");
	}
	
}
