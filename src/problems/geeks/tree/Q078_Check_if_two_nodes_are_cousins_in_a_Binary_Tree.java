package problems.geeks.tree;

/**
 * Check if two nodes are cousins in a Binary Tree
 * 
 * http://www.geeksforgeeks.org/check-two-nodes-cousins-binary-tree/
 * 
 * Given the binary Tree and the two nodes say �a� and �b�, determine whether the two nodes are cousins of each other or not.
 * 
 * Two nodes are cousins of each other if they are at same level and have different parents.
 * 
 * Example
 * 
 *              20
 *             /  \
 *            8    22
 *           / \    \
 *          4  12    25
 *             / \
 *           10   14
 * Say two node be 4 and 25, result is TRUE.
 * Say two nodes are 4 and 12, result is FALSE.
 * Say two nodes are 10 and 14, result is FALSE.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q078_Check_if_two_nodes_are_cousins_in_a_Binary_Tree {

	public static void main(String[] args) {
		TreeNode root = Tree.bst2();
		TreeNode a = Tree.getTreeNode(root, 4);
		TreeNode b = Tree.getTreeNode(root, 25);
		
		boolean res = areCousins(root, a, b);
		System.out.println(res);
	}
	
	static boolean areCousins(TreeNode root, TreeNode a, TreeNode b) {
		int la = getLevel(root, a, 1);
		int lb = getLevel(root, b, 1);
		
		// check if they are at the same level
		if (la != lb) {
			return false;
		}
		
		// check if they have same parent
		return !isSibling(root, a, b);
	}
	
	static int getLevel(TreeNode root, TreeNode node, int level) {
		if (root == null) {
			return -1;
		}
		
		if (root == node) {
			return level;
		}
		
		int left = getLevel(root.left, node, level + 1);
		int right = getLevel(root.right, node, level + 1);
		
		return (left != -1) ? left : right;
	}
	
	static boolean isSibling(TreeNode root, TreeNode a, TreeNode b) {
		if (root == null) {
			return false;
		}
		
		if ((root.left == a && root.right == b) || (root.left == b && root.right == a)) {
			return true;
		}
		
		return isSibling(root.left, a, b) || isSibling(root.right, a, b);
	}
	
}
