package problems.geeks.tree;

/**
 * Connect nodes at same level
 * 
 * http://www.geeksforgeeks.org/connect-nodes-at-same-level/
 * 
 * Write a function to connect all the adjacent nodes at the same level in a binary tree. Structure of the given Binary Tree node is like following.
 * 
 * Initially, all the nextRight pointers point to garbage values. Your function should set these pointers to point next right for each node.
 * 
 * Example
 * 
 * Input Tree
 *        A
 *       / \
 *      B   C
 *     / \   \
 *    D   E   F
 * 
 * 
 * Output Tree
 *        A--->NULL
 *       / \
 *      B-->C-->NULL
 *     / \   \
 *    D-->E-->F-->NULL
 */

import java.util.*;

public class Q033_Connect_nodes_at_same_level {

	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode nextRight;
	}
	
	static void connect(TreeNode root) {
		Queue<TreeNode> curr = new LinkedList<TreeNode>();
		Queue<TreeNode> next = new LinkedList<TreeNode>();
		
		curr.add(root);
		
		TreeNode prev = null;
		
		while (!curr.isEmpty()) {
			TreeNode node = curr.poll();
			node.nextRight = prev;
			prev = node;
			
			if (node.left != null) {
				next.add(node.left);
			}
			
			if (node.right != null) {
				next.add(node.right);
			}
			
			if (curr.isEmpty()) {
				curr = next;
				next = new LinkedList<TreeNode>();
				prev = null;
			}
		}
		
	}
	
	public static void main(String[] args) {
		
	}
	
}
