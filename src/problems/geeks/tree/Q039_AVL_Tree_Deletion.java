package problems.geeks.tree;

/**
 * AVL Tree | Set 2 (Deletion)
 * 
 * http://www.geeksforgeeks.org/avl-tree-set-2-deletion/
 * 
 * We have discussed AVL insertion in the previous post. In this post, we will follow a similar approach for deletion.
 * 
 * Steps to follow for deletion.
 * To make sure that the given tree remains AVL after every deletion, we must augment the standard BST delete operation to perform some re-balancing. Following are two basic operations that can be performed to re-balance a BST without violating the BST property (keys(left) < key(root) < keys(right)).
 * 1) Left Rotation
 * 2) Right Rotation
 * 
 * T1, T2 and T3 are subtrees of the tree rooted with y (on left side)
 * or x (on right side)
 *                 y                               x
 *                / \     Right Rotation          /  \
 *               x   T3   � � � � � � � >        T1   y
 *              / \       < - - - - - - -            / \
 *             T1  T2     Left Rotation            T2  T3
 * Keys in both of the above trees follow the following order
 *       keys(T1) < key(x) < keys(T2) < key(y) < keys(T3)
 * So BST property is not violated anywhere.
 * Let w be the node to be deleted
 * 1) Perform standard BST delete for w.
 * 2) Starting from w, travel up and find the first unbalanced node. Let z be the first unbalanced node, y be the larger height child of z, and x be the larger height child of y. Note that the definitions of x and y are different from insertion here.
 * 3) Re-balance the tree by performing appropriate rotations on the subtree rooted with z. There can be 4 possible cases that needs to be handled as x, y and z can be arranged in 4 ways. Following are the possible 4 arrangements:
 * a) y is left child of z and x is left child of y (Left Left Case)
 * b) y is left child of z and x is right child of y (Left Right Case)
 * c) y is right child of z and x is right child of y (Right Right Case)
 * d) y is right child of z and x is left child of y (Right Left Case)
 * 
 * Like insertion, following are the operations to be performed in above mentioned 4 cases. Note that, unlike insertion, fixing the node z won�t fix the complete AVL tree. After fixing z, we may have to fix ancestors of z as well (See this video lecture for proof)
 * 
 * a) Left Left Case
 * 
 * T1, T2, T3 and T4 are subtrees.
 *          z                                      y 
 *         / \                                   /   \
 *        y   T4      Right Rotate (z)          x      z
 *       / \          - - - - - - - - ->      /  \    /  \ 
 *      x   T3                               T1  T2  T3  T4
 *     / \
 *   T1   T2
 * b) Left Right Case
 * 
 *      z                               z                           x
 *     / \                            /   \                        /  \ 
 *    y   T4  Left Rotate (y)        x    T4  Right Rotate(z)    y      z
 *   / \      - - - - - - - - ->    /  \      - - - - - - - ->  / \    / \
 * T1   x                          y    T3                    T1  T2 T3  T4
 *     / \                        / \
 *   T2   T3                    T1   T2
 * c) Right Right Case
 * 
 *   z                                y
 *  /  \                            /   \ 
 * T1   y     Left Rotate(z)       z      x
 *     /  \   - - - - - - - ->    / \    / \
 *    T2   x                     T1  T2 T3  T4
 *        / \
 *      T3  T4
 * d) Right Left Case
 * 
 *    z                            z                            x
 *   / \                          / \                          /  \ 
 * T1   y   Right Rotate (y)    T1   x      Left Rotate(z)   z      x
 *     / \  - - - - - - - - ->     /  \   - - - - - - - ->  / \    / \
 *    x   T4                      T2   y                  T1  T2  T3  T4
 *   / \                              /  \
 * T2   T3                           T3   T4
 * Unlike insertion, in deletion, after we perform a rotation at z, we may have to perform a rotation at ancestors of z. Thus, we must continue to trace the path until we reach the root.
 * 
 * C implementation
 * Following is the C implementation for AVL Tree Deletion. The following C implementation uses the recursive BST delete as basis. In the recursive BST delete, after deletion, we get pointers to all ancestors one by one in bottom up manner. So we don�t need parent pointer to travel up. The recursive code itself travels up and visits all the ancestors of the deleted node.
 * 1) Perform the normal BST deletion.
 * 2) The current node must be one of the ancestors of the deleted node. Update the height of the current node.
 * 3) Get the balance factor (left subtree height � right subtree height) of the current node.
 * 4) If balance factor is greater than 1, then the current node is unbalanced and we are either in Left Left case or Left Right case. To check whether it is Left Left case or Left Right case, get the balance factor of left subtree. If balance factor of the left subtree is greater than or equal to 0, then it is Left Left case, else Left Right case.
 * 5) If balance factor is less than -1, then the current node is unbalanced and we are either in Right Right case or Right Left case. To check whether it is Right Right case or Right Left case, get the balance factor of right subtree. If the balance factor of the right subtree is smaller than or equal to 0, then it is Right Right case, else Right Left case.
 */

public class Q039_AVL_Tree_Deletion {

}
