package problems.geeks.tree;

/**
 * Inorder Tree Traversal without recursion and without stack!
 * 
 * http://www.geeksforgeeks.org/inorder-tree-traversal-without-recursion-and-without-stack/
 * 
 * Using Morris Traversal, we can traverse the tree without using stack and recursion. The idea of Morris Traversal is based on Threaded Binary Tree. In this traversal, we first create links to Inorder successor and print the data using these links, and finally revert the changes to restore original tree.
 * 
 * 1. Initialize current as root 
 * 2. While current is not NULL
 *    If current does not have left child
 *       a) Print current�s data
 *       b) Go to the right, i.e., current = current->right
 *    Else
 *       a) Make current as right child of the rightmost node in current's left subtree
 *       b) Go to this left child, i.e., current = current->left
 * Although the tree is modified through the traversal, it is reverted back to its original shape after the completion. Unlike Stack based traversal, no extra space is required for this traversal.
 */

public class Q052B_Morris_traversal_for_Inorder {

}
