package problems.geeks.tree;

/**
 * The Great Tree-List Recursion Problem
 * 
 * http://www.geeksforgeeks.org/the-great-tree-list-recursion-problem/
 * 
 * Question:
 * Write a recursive function treeToList(Node root) that takes an ordered binary tree and rearranges the internal pointers to make a circular doubly linked list 
 * out of the tree nodes. The�previous� pointers should be stored in the �small� field and the �next� pointers should be stored in the �large� field. 
 * The list should be arranged so that the nodes are in increasing order. Return the head pointer to the new list.
 * 
 * This is very well explained and implemented at http://cslibrary.stanford.edu/109/TreeListRecursion.html
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q009_The_Great_Tree_List_Recursion_Problem {
	
	public static void main(String[] args) {
		
	}
	
}
