package problems.geeks.tree;

/**
 * Iterative Method to find Height of Binary Tree
 * 
 * http://www.geeksforgeeks.org/iterative-method-to-find-height-of-binary-tree/
 * 
 * There are two conventions to define height of Binary Tree
 * 1) Number of nodes on longest path from root to the deepest node.
 * 2) Number of edges on longest path from root to the deepest node.
 * 
 * In this post, the first convention is followed. For example, height of the below tree is 3.
 * 
 * Recursive method to find height of Binary Tree is discussed here. How to find height without recursion? 
 * We can use level order traversal to find height without recursion. The idea is to traverse level by level. 
 * Whenever move down to a level, increment height by 1 (height is initialized as 0). Count number of nodes at each level, 
 * stop traversing when count of nodes at next level is 0.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q059_Iterative_Method_to_find_Height_of_Binary_Tree {

	public static void main(String[] args) {
		TreeNode root = Tree.tree1();
		int res = treeHeight(root);
		System.out.println(res);
	}
	
	static int treeHeight(TreeNode root) {
		if (root == null) {
			return 0;
		}
		
		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		queue.add(root);
		queue.add(null);
		
		int height = 0;
		
		while (!queue.isEmpty()) {
			TreeNode node = queue.poll();
			
			if (node != null) {
				if (node.left != null) {
					queue.add(node.left);
				}
				
				if (node.right != null) {
					queue.add(node.right);
				}
			} else {
				height++;
				if (!queue.isEmpty()) {
					queue.add(null);
				}
			}
		}
		
		return height;
	}
	
}
