package problems.geeks.tree;

/**
 * Diameter of a Binary Tree
 * 
 * http://www.geeksforgeeks.org/diameter-of-a-binary-tree/
 * 
 * The diameter of a tree (sometimes called the width) is the number of nodes on the longest path between two leaves in the tree. 
 * The diagram below shows two trees each with diameter nine, the leaves that form the ends of a longest path are shaded 
 * (note that there is more than one path in each tree of length nine, but no path longer than nine nodes).
 * 
 * The diameter of a tree T is the largest of the following quantities:
 * 
 * the diameter of T�s left subtree
 * the diameter of T�s right subtree
 * the longest path between leaves that goes through the root of T (this can be computed from the heights of the subtrees of T)
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q016_Diameter_of_a_Binary_Tree {

	public static void main(String[] args) {
		TreeNode root = Tree.bst2();
		int d = diameter(root);
		System.out.println(d);
	}
	
	// Time Complexity: O(n^2)
	static int diameter(TreeNode root) {
		/* base case where tree is empty */
		if (root == null) {
			return 0;
		}
		
		/* get the height of left and right sub-trees */
		int lh = height(root.left);
		int rh = height(root.right);
		
		/* get the diameter of left and right sub-trees */
		int ld = diameter(root.left);
		int rd = diameter(root.right);
		
		/* Return max of following three
		   1) Diameter of left subtree
		   2) Diameter of right subtree
		   3) Height of left subtree + height of right subtree + 1 */
		return Math.max(lh + rh + 1, Math.max(ld, rd));
	}
	
	static int height(TreeNode root) {
		if (root == null) {
			return 0;
		}
		
		int lh = height(root.left);
		int rh = height(root.right);
		
		return 1 + Math.max(lh, rh);
	}
	
}
