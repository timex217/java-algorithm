package problems.geeks.tree;

/**
 * Sorted Array to Balanced BST
 * 
 * http://www.geeksforgeeks.org/sorted-array-to-balanced-bst/
 * 
 * Given a sorted array. Write a function that creates a Balanced Binary Search Tree using array elements.
 * 
 * Examples:
 * 
 * Input:  Array {1, 2, 3}
 * Output: A Balanced BST
 *      2
 *    /  \
 *   1    3 
 * 
 * Input: Array {1, 2, 3, 4}
 * Output: A Balanced BST
 *       3
 *     /  \
 *    2    4
 *  /
 * 1
 * Algorithm
 * In the previous post, we discussed construction of BST from sorted Linked List. Constructing from sorted array in O(n) time is simpler as we can get the middle element in O(1) time. Following is a simple algorithm where we first find the middle node of list and make it root of the tree to be constructed.
 * 
 * 1) Get the Middle of the array and make it root.
 * 2) Recursively do same for left half and right half.
 *       a) Get the middle of left half and make it left child of the root
 *           created in step 1.
 *       b) Get the middle of right half and make it right child of the
 *           root created in step 1.
 */

import java.util.*;
import adt.tree.TreeNode;

public class Q034_Sorted_Array_to_Balanced_BST {

	public static void main(String[] args) {
		int[] arr = {1, 2, 3, 4};
		TreeNode tree = sortedArrayToBST(arr, 0, arr.length - 1);
		tree.print();
	}
	
	static TreeNode sortedArrayToBST(int[] arr, int lo, int hi) {
		if (lo > hi) {
			return null;
		}
		
		int mid = lo + (hi - lo) / 2;
		
		TreeNode root = new TreeNode(arr[mid]);
		
		root.left = sortedArrayToBST(arr, lo, mid - 1);
		root.right = sortedArrayToBST(arr, mid + 1, hi);
		
		return root;
	}
	
}
