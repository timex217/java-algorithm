package problems.geeks.tree;

/**
 * Vertical Sum in a given Binary Tree
 * 
 * http://www.geeksforgeeks.org/vertical-sum-in-a-given-binary-tree/
 * 
 * Given a Binary Tree, find vertical sum of the nodes that are in same vertical line. Print all sums through different vertical lines.
 * 
 * Examples:
 * 
 *       1
 *     /   \
 *   2      3
 *  / \    / \
 * 4   5  6   7
 * The tree has 5 vertical lines
 * 
 * Vertical-Line-1 has only one node 4 => vertical sum is 4
 * Vertical-Line-2: has only one node 2=> vertical sum is 2
 * Vertical-Line-3: has three nodes: 1,5,6 => vertical sum is 1+5+6 = 12
 * Vertical-Line-4: has only one node 3 => vertical sum is 3
 * Vertical-Line-5: has only one node 7 => vertical sum is 7
 * 
 * So expected output is 4, 2, 12, 3 and 7
 * 
 * Solution:
 * We need to check the Horizontal Distances from root for all nodes. If two nodes have the same Horizontal Distance (HD), then they are on same vertical line. The idea of HD is simple. HD for root is 0, a right edge (edge connecting to right subtree) is considered as +1 horizontal distance and a left edge is considered as -1 horizontal distance. For example, in the above tree, HD for Node 4 is at -2, HD for Node 2 is -1, HD for 5 and 6 is 0 and HD for node 7 is +2.
 * We can do inorder traversal of the given Binary Tree. While traversing the tree, we can recursively calculate HDs. We initially pass the horizontal distance as 0 for root. For left subtree, we pass the Horizontal Distance as Horizontal distance of root minus 1. For right subtree, we pass the Horizontal Distance as Horizontal Distance of root plus 1.
 * 
 * Following is Java implementation for the same. HashMap is used to store the vertical sums for different horizontal distances.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q040_Vertical_Sum_in_a_given_Binary_Tree {

	public static void main(String[] args) {
		TreeNode root = Tree.tree1();
		Map<Integer, ArrayList<TreeNode>> map = new TreeMap<Integer, ArrayList<TreeNode>>();
		
		verticalSum(root, 0, map);
		
		// print out the sums
		for (Map.Entry<Integer, ArrayList<TreeNode>> entry : map.entrySet()) {
			int sum = 0;
			ArrayList<TreeNode> list = entry.getValue();
			for (int i = 0; i < list.size(); i++) {
				TreeNode node = list.get(i);
				sum += node.val;
			}
			
			System.out.format("%d ", sum);
		}
	}
	
	static void verticalSum(TreeNode node, int level, Map<Integer, ArrayList<TreeNode>> map) {
		if (node == null) {
			return;
		}
		
		if (!map.containsKey(level)) {
			map.put(level, new ArrayList<TreeNode>());
		}
		
		ArrayList<TreeNode> list = map.get(level);
		list.add(node);
		map.put(level, list);
		
		verticalSum(node.left, level - 1, map);
		verticalSum(node.right, level + 1, map);
	}
	
}
