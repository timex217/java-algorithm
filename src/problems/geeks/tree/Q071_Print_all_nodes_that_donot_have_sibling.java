package problems.geeks.tree;

/**
 * Print all nodes that don’t have sibling
 * 
 * http://www.geeksforgeeks.org/print-nodes-dont-sibling-binary-tree/
 * 
 * Given a Binary Tree, print all nodes that don’t have a sibling (a sibling is a node that has same parent. 
 * In a Binary Tree, there can be at most one sibling). Root should not be printed as root cannot have a sibling.
 * 
 * For example, the output should be “4 5 6″ for the following tree.
 * 
 *                1
 *            /      \
 *           2        3
 *            \      /
 *             4    5
 *                 /
 *                6
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q071_Print_all_nodes_that_donot_have_sibling {

	public static void main(String[] args) {
		TreeNode root = Tree.tree14();
		printSibling(root);
	}
	
	static void printSibling(TreeNode root) {
		if (root == null) {
			return;
		}
		
		if (root.left == null && root.right != null) {
			System.out.format("%d ", root.right.val);
		}
		
		if (root.left != null && root.right == null) {
			System.out.format("%d ", root.left.val);
		}
		
		printSibling(root.left);
		printSibling(root.right);
	}
	
}
