package problems.geeks.tree;

/**
 * Maximum width of a binary tree
 * 
 * http://www.geeksforgeeks.org/maximum-width-of-a-binary-tree/
 * 
 * Given a binary tree, write a function to get the maximum width of the given tree. Width of a tree is maximum of widths of all levels.
 * 
 * Let us consider the below example tree.
 * 
 *          1
 *         /  \
 *        2    3
 *      /  \     \
 *     4    5     8 
 *               /  \
 *              6    7
 *              
 * For the above tree,
 * width of level 1 is 1,
 * width of level 2 is 2,
 * width of level 3 is 3
 * width of level 4 is 2.
 * 
 * So the maximum width of the tree is 3.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q022_Maximum_width_of_a_binary_tree {

	public static void main(String[] args) {
		TreeNode root = Tree.tree1();
		int width = getTreeWidth(root);
		System.out.println(width);
		
		int max = getMaxWidth(root);
		System.out.println(max);
	}
	
	// -------------------------------------
	//  Method 1 - Levelorder Traversal
	// -------------------------------------
	
	static int getTreeWidth(TreeNode root) {
		if (root == null) {
			return 0;
		}
		
		Queue<TreeNode> curr = new LinkedList<TreeNode>();
		Queue<TreeNode> next = new LinkedList<TreeNode>();
		
		curr.add(root);
		
		int count = 0;
		int width = 0;
		
		while (!curr.isEmpty()) {
			TreeNode node = curr.poll();
			count++;
			
			if (node.left != null) {
				next.add(node.left);
			}
			
			if (node.right != null) {
				next.add(node.right);
			}
			
			if (curr.isEmpty()) {
				width = Math.max(width, count);
				
				// 这是正确的交换两个 Queue 的方法，当调用 new 的时候，next 重新指向新的 Queue
				// 如果调用 next.clear(), 那么 curr 也会被 clear!
				curr = next;
				next = new LinkedList<TreeNode>();
				
				count = 0;
			}
		}
		
		return width;
	}
	
	// -------------------------------------
	//  Method 2 - Preorder Traversal
	//
	//  In this method we create a temporary array count[] of size equal to the height of tree. 
	//  We initialize all values in count as 0. 
	//  We traverse the tree using preorder traversal and fill the entries in count so that the 
	//  count array contains count of nodes at each level in Binary Tree.
	// -------------------------------------
	
	static int getMaxWidth(TreeNode root) {
		int depth = depth(root);
		int[] path = new int[depth];
		
		countNode(root, path, 0);
		
		int max = 0;
		for (int i = 0; i < depth; i++) {
			max = Math.max(max, path[i]);
		}
		
		return max;
	}
	
	static void countNode(TreeNode root, int[] path, int level) {
		// base case
		if (root == null) {
			return;
		}
		
		path[level]++;
		
		countNode(root.left, path, level + 1);
		countNode(root.right, path, level + 1);
	}
	
	static int depth(TreeNode root) {
		if (root == null) {
			return 0;
		}
		return 1 + Math.max(depth(root.left), depth(root.right));
	}
	
}
