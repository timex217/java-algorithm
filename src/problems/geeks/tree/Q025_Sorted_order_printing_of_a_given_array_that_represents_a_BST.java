package problems.geeks.tree;

/**
 * Sorted order printing of a given array that represents a BST
 * 
 * http://www.geeksforgeeks.org/sorted-order-printing-of-an-array-that-represents-a-bst/
 * 
 * Given an array that stores a complete Binary Search Tree, write a function that efficiently prints the given array in ascending order.
 * 
 * For example, given an array [4, 2, 5, 1, 3], the function should print 1, 2, 3, 4, 5
 * 
 *           4
 *          / \
 *         2   5
 *        / \
 *       1   3
 *       
 * Solution:
 * Inorder traversal of BST prints it in ascending order. The only trick is to modify recursion termination condition in standard Inorder Tree Traversal.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q025_Sorted_order_printing_of_a_given_array_that_represents_a_BST {

	public static void main(String[] args) {
		int[] arr = {4, 2, 5, 1, 3};
		printBST(arr, 0, arr.length - 1);
	}
	
	static void printBST(int[] arr, int lo, int hi) {
		if (lo > hi) {
			return;
		}
		
		printBST(arr, 2 * lo + 1, hi);
		
		System.out.format("%d ", arr[lo]);
		
		printBST(arr, 2 * lo + 2, hi);
	}
	
}
