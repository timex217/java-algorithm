package problems.geeks.tree;

/**
 * Check if each internal node of a BST has exactly one child
 * 
 * http://www.geeksforgeeks.org/check-if-each-internal-node-of-a-bst-has-exactly-one-child/
 * 
 * Given Preorder traversal of a BST, check if each non-leaf node has only one child. Assume that the BST contains unique entries.
 * 
 * Examples
 * 
 * Input: pre[] = {20, 10, 11, 13, 12}
 * Output: Yes
 * The give array represents following BST. In the following BST, every internal
 * node has exactly 1 child. Therefor, the output is true.
 *         20
 *        /
 *       10
 *        \
 *         11
 *           \
 *            13
 *            /
 *          12
 * In Preorder traversal, descendants (or Preorder successors) of every node appear after the node. 
 * In the above example, 20 is the first node in preorder and all descendants of 20 appear after it. 
 * All descendants of 20 are smaller than it. For 10, all descendants are greater than it. 
 * In general, we can say, if all internal nodes have only one child in a BST, then all the descendants of every node are either smaller or larger than the node. 
 * The reason is simple, since the tree is BST and every node has only one child, all descendants of a node will either be on left side or right side, 
 * means all descendants will either be smaller or greater.
 */

import java.util.*;

public class Q045_Check_if_each_internal_node_of_a_BST_has_exactly_one_child {

	public static void main(String[] args) {
		int[] pre = {20, 10, 11, 13, 12};
		boolean res = hasOnlyOneChild(pre);
		System.out.println(res);
	}
	
	static boolean hasOnlyOneChild(int[] pre) {
		if (pre == null) {
			return false;
		}
		
		int n = pre.length;
		
		int max = pre[n - 1];
		int min = pre[n - 1];
		
		for (int i = n - 2; i >= 0; i--) {
			if (pre[i] < min) {
				min = pre[i];
			} else if (pre[i] > max) {
				max = pre[i];
			} else {
				return false;
			}
		}
		
		return true;
	}
	
}
