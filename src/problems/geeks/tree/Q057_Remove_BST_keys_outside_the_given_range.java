package problems.geeks.tree;

/**
 * Remove BST keys outside the given range
 * 
 * http://www.geeksforgeeks.org/remove-bst-keys-outside-the-given-range/
 * 
 * Given a Binary Search Tree (BST) and a range [min, max], remove all keys which are outside the given range. 
 * The modified tree should also be BST. 
 * 
 * For example, consider the following BST and range [-10, 13]. 
 * 
 *                   6
 *                 /   \
 *              -13    14
 *                \    / \
 *                -8  13  15
 *                    /
 *                   7
 * 
 * The given tree should be changed to following. Note that all keys outside the range [-10, 13] are removed and modified tree is BST.
 *                   
 *                   6
 *                 /   \
 *                -8   13
 *                     /
 *                    7
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q057_Remove_BST_keys_outside_the_given_range {

	public static void main(String[] args) {
		TreeNode root = Tree.tree9();
		root = removeOutsideRange(root, -10, 13);
		root.print();
	}
	
	static TreeNode removeOutsideRange(TreeNode root, int min, int max) {
		// base case
		if (root == null) {
			return null;
		}
		
		TreeNode left = removeOutsideRange(root.left, min, max);
		TreeNode right = removeOutsideRange(root.right, min, max);
		
		if (root.val < min) {
			return right;
		}
		
		if (root.val > max) {
			return left;
		}
		
		root.left = left;
		root.right = right;
		
		return root;
	}
	
}
