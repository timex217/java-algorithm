package problems.geeks.tree;

/**
 * Floor and Ceil from a BST
 * 
 * http://www.geeksforgeeks.org/floor-and-ceil-from-a-bst/
 * 
 * There are numerous applications we need to find floor (ceil) value of a key in a binary search tree or sorted array. 
 * For example, consider designing memory management system in which free nodes are arranged in BST. Find best fit for the input request.
 * 
 * Ceil Value Node: Node with smallest data larger than or equal to key value.
 * 
 * Imagine we are moving down the tree, and assume we are root node. The comparison yields three possibilities,
 * 
 * A) Root data is equal to key. We are done, root data is ceil value.
 * 
 * B) Root data < key value, certainly the ceil value can�t be in left subtree. Proceed to search on right subtree as reduced problem instance.
 * 
 * C) Root data > key value, the ceil value may be in left subtree. We may find a node with is larger data than key value in left subtree, 
 * if not the root itself will be ceil node.
 * 
 *              20
 *             /  \
 *            8    22
 *           / \    \
 *          4  12    25
 *             / \
 *           10   14
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q050_Floor_and_Ceil_from_a_BST {

	public static void main(String[] args) {
		TreeNode root = Tree.bst2();
		
		int ceil = findCeil(root, 17);
		System.out.format("ceil: %d\n", ceil);
		
		int floor = findFloor(root, 17);
		System.out.format("floor: %d\n", floor);
	}
	
	// -------------------------
	//  Ceil
	// -------------------------
	static int findCeil(TreeNode root, int key) {
		// base case
		if (root == null) {
			return Integer.MIN_VALUE;
		}
		
		if (root.val == key) {
			return key;
		}
		
		if (root.val < key) {
			return findCeil(root.right, key);
		} else {
			int ceil = findCeil(root.left, key);
			return (ceil >= key) ? ceil : root.val;
		}
	}
	
	// -------------------------
	//  Floor
	// -------------------------
	static int findFloor(TreeNode root, int key) {
		// base case
		if (root == null) {
			return Integer.MAX_VALUE;
		}
		
		if (root.val == key) {
			return key;
		}
		
		if (root.val > key) {
			return findFloor(root.left, key);
		} else {
			int floor = findFloor(root.right, key);
			return (floor <= key) ? floor : root.val;
		}
	}
	
}
