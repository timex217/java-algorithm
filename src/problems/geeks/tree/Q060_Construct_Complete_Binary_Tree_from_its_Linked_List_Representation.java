package problems.geeks.tree;

/**
 * Construct Complete Binary Tree from its Linked List Representation
 * 
 * http://www.geeksforgeeks.org/given-linked-list-representation-of-complete-tree-convert-it-to-linked-representation/
 * 
 * Given Linked List Representation of Complete Binary Tree, construct the Binary tree. A complete binary tree can be represented in an array in the following approach.
 * 
 * If root node is stored at index i, its left, and right children are stored at indices 2*i+1, 2*i+2 respectively.
 * Suppose tree is represented by a linked list in same way, how do we convert this into normal linked representation of binary tree where every node has data, 
 * left and right pointers? In the linked list representation, we cannot directly access the children of the current node unless we traverse the list.
 * 
 *    10 -> 12 -> 15 -> 25 -> 30 -> 36
 *    
 *                10
 *               /  \
 *             12    15
 *            /  \   /
 *           25  30 36
 *           
 * We are mainly given level order traversal in sequential access form. We know head of linked list is always is root of the tree. 
 * We take the first node as root and we also know that the next two nodes are left and right children of root. So we know partial Binary Tree. 
 * The idea is to do Level order traversal of the partially built Binary Tree using queue and traverse the linked list at the same time. 
 * At every step, we take the parent node from queue, make next two nodes of linked list as children of the parent node, and enqueue the next two nodes to queue.
 * 
 * 1. Create an empty queue.
 * 2. Make the first node of the list as root, and enqueue it to the queue.
 * 3. Until we reach the end of the list, do the following.
 * ………a. Dequeue one node from the queue. This is the current parent.
 * ………b. Traverse two nodes in the list, add them as children of the current parent.
 * ………c. Enqueue the two nodes into the queue.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;
import adt.LinkedList;
import adt.ListNode;

public class Q060_Construct_Complete_Binary_Tree_from_its_Linked_List_Representation {

	public static void main(String[] args) {
		LinkedList list = new LinkedList(new int[]{10, 12, 15, 25, 30, 36});
		TreeNode root = constructTree(list.head);
		root.print();
	}
	
	static TreeNode constructTree(ListNode head) {
		if (head == null) {
			return null;
		}
		
		// 利用一个Queue
		Queue<TreeNode> queue = new java.util.LinkedList<TreeNode>();
		TreeNode root = new TreeNode(head.val);
		queue.add(root);
		
		head = head.next;
		
		while (head != null) {
			TreeNode node = queue.poll();
			
			// left
			TreeNode left = new TreeNode(head.val);
			queue.add(left);
			head = head.next;
			
			// right
			TreeNode right = null;
			if (head != null) {
				right = new TreeNode(head.val);
				queue.add(right);
				head = head.next;
			}
			
			node.left = left;
			node.right = right;
		}
		
		return root;
	}
	
}
