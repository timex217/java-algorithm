package problems.geeks.tree;

/**
 * Difference between sums of odd level and even level nodes of a Binary Tree
 * 
 * http://www.geeksforgeeks.org/difference-between-sums-of-odd-and-even-levels/
 * 
 * Given a a Binary Tree, find the difference between the sum of nodes at odd level and the sum of nodes at even level. 
 * Consider root as level 1, left and right children of root as level 2 and so on.
 * 
 * For example, in the following tree, sum of nodes at odd level is (5 + 1 + 4 + 8) which is 18. And sum of nodes at even level is (2 + 6 + 3 + 7 + 9) which is 27. 
 * The output for following tree should be 18 � 27 which is -9.
 * 
 *       5
 *     /   \
 *    2     6
 *  /  \     \  
 * 1    4     8
 *     /     / \ 
 *    3     7   9  
 *    
 * A straightforward method is to use level order traversal. In the traversal, check level of current node, if it is odd, increment odd sum by data of current node, 
 * otherwise increment even sum. Finally return difference between odd sum and even sum. See following for implementation of this approach.
 * 
 * C implementation of level order traversal based approach to find the difference.
 * 
 * The problem can also be solved using simple recursive traversal. We can recursively calculate the required difference as, value of root�s data subtracted by the 
 * difference for subtree under left child and the difference for subtree under right child. Following is C implementation of this approach.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q061_Difference_between_sums_of_odd_level_and_even_level_nodes_of_a_Binary_Tree {

	public static void main(String[] args) {
		TreeNode root = Tree.tree1();
		int diff = getLevelDiff(root);
		System.out.println(diff);
	}
	
	// ---------------------------
	//  Recursion
	// ---------------------------
	static int getLevelDiff(TreeNode root) {
		if (root == null) {
			return 0;
		}
		
		return root.val - (getLevelDiff(root.left) + getLevelDiff(root.right));
	}
	
	// ---------------------------
	//  Iterative
	// ---------------------------
	
	
}
