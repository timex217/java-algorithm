package problems.geeks.tree;

/**
 * Check if a binary tree is BST or not
 * 
 * http://www.geeksforgeeks.org/a-program-to-check-if-a-binary-tree-is-bst-or-not/
 * 
 * A binary search tree (BST) is a node based binary tree data structure which has the following properties.
 * • The left subtree of a node contains only nodes with keys less than the node’s key.
 * • The right subtree of a node contains only nodes with keys greater than the node’s key.
 * • Both the left and right subtrees must also be binary search trees.
 * 
 * From the above properties it naturally follows that:
 * • Each node (item in the tree) has a distinct key.
 */

import java.util.*;

import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q012_Check_if_a_binary_tree_is_BST_or_not {

	public static void main(String[] args) {
		TreeNode root = Tree.bst2();
		
		boolean res1 = isBSTPreorder(root);
		System.out.println(res1);
		
		boolean res2 = isBSTPostorder(root);
		System.out.println(res2);
	}

	// --------------------------------
	//  Preorder
	// --------------------------------
	static boolean isBSTPreorder(TreeNode root) {
		return preorder(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
	}
	
	static boolean preorder(TreeNode node, int min, int max) {
		// Empty tree is BST
		if (node == null) {
			return true;
		}
		
		if (node.val < min || node.val > max) {
			return false;
		}
		
		return preorder(node.left, min, node.val - 1) && preorder(node.right, node.val + 1, max);
	}
	
	// --------------------------------
	//  Postorder
	// --------------------------------
	static int max_ref;  // 左子树最大值
	static int min_ref;  // 右子树最小值
	
	static boolean isBSTPostorder(TreeNode root) {
		max_ref = Integer.MIN_VALUE;
		min_ref = Integer.MAX_VALUE;
		
		return postorder(root);
	}
	
	static boolean postorder(TreeNode node) {
		// Empty tree is BST
		if (node == null) {
			return true;
		}
		
		// ----------------------
		//  left subtree
		// ----------------------
		max_ref = Integer.MIN_VALUE;
		boolean is_left_bst = postorder(node.left);
		
		// 如果当前节点的值小于或等于左子树的最大值，不是BST
		if (!is_left_bst || node.val <= max_ref) {
			return false;
		}
		
		// 保存当前左子树的最小值! 因为在遍历右边的时候，min_ref会被更改!
		int min = min_ref;
		
		// -----------------------
		//  right subtree
		// -----------------------
		min_ref = Integer.MAX_VALUE;
		boolean is_right_bst = postorder(node.right);
		// 此时min_ref已经变为了右子树的最小值!
		
		// 如果当前节点的值大于或等于右子树的最小值，不是BST
		if (!is_right_bst || node.val >= min_ref) {
			return false;
		}
		
		// -----------------------
		//  current node
		// -----------------------
		// 为上一层进行更新
		// 此时min_ref表示从当前节点开始遍历的所有节点中的最小值
		// 而max_ref表示最大值
		//                 左子树最小值     当前值     右子树最小值
		min_ref = Math.min(min, Math.min(node.val, min_ref));
		max_ref = Math.max(node.val, max_ref);
		
		// 否则, 是BST
		return true;
	}
	
}
