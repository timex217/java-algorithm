package problems.geeks.tree;

/**
 * Merge Two Balanced Binary Search Trees
 * 
 * http://www.geeksforgeeks.org/merge-two-balanced-binary-search-trees/
 * 
 * You are given two balanced binary search trees e.g., AVL or Red Black Tree. Write a function that merges the two given balanced BSTs into a balanced binary search tree. Let there be m elements in first tree and n elements in the other tree. Your merge function should take O(m+n) time.
 * 
 * In the following solutions, it is assumed that sizes of trees are also given as input. If the size is not given, then we can get the size by traversing the tree (See this).
 * 
 * Method 1 (Insert elements of first tree to second) 
 * Take all elements of first BST one by one, and insert them into the second BST. Inserting an element to a self balancing BST takes Logn time (See this) where n is size of the BST. So time complexity of this method is Log(n) + Log(n+1) � Log(m+n-1). The value of this expression will be between mLogn and mLog(m+n-1). As an optimization, we can pick the smaller tree as first tree.
 * 
 * Method 2 (Merge Inorder Traversals) 
 * 1) Do inorder traversal of first tree and store the traversal in one temp array arr1[]. This step takes O(m) time.
 * 2) Do inorder traversal of second tree and store the traversal in another temp array arr2[]. This step takes O(n) time.
 * 3) The arrays created in step 1 and 2 are sorted arrays. Merge the two sorted arrays into one array of size m + n. This step takes O(m+n) time.
 * 4) Construct a balanced tree from the merged array using the technique discussed in this post. This step takes O(m+n) time.
 * 
 * Time complexity of this method is O(m+n) which is better than method 1. This method takes O(m+n) time even if the input BSTs are not balanced.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q041A_Merge_Two_Balanced_Binary_Search_Trees {

	public static void main(String[] args) {
		TreeNode r1 = Tree.bst1();
		TreeNode r2 = Tree.bst2();
		TreeNode root = mergeBST(r1, r2);
		root.print();
	}
	
	static TreeNode mergeBST(TreeNode r1, TreeNode r2) {
		ArrayList<Integer> l1 = new ArrayList<Integer>();
		ArrayList<Integer> l2 = new ArrayList<Integer>();
		
		inorder(r1, l1);
		inorder(r2, l2);
		
		ArrayList<Integer> l = mergeLists(l1, l2);
		TreeNode root = sortedArrayToBST(l, 0, l.size() - 1);
		
		return root;
	}
	
	static void inorder(TreeNode node, ArrayList<Integer> list) {
		if (node == null) {
			return;
		}
		
		inorder(node.left, list);
		list.add(node.val);
		inorder(node.right, list);
	}
	
	static ArrayList<Integer> mergeLists(ArrayList<Integer> l1, ArrayList<Integer> l2) {
		if (l1 == null) {
			return l2;
		}
		
		if (l2 == null) {
			return l1;
		}
		
		ArrayList<Integer> res = new ArrayList<Integer>();
		
		int i = 0, j = 0;
		while (i < l1.size() && j < l2.size()) {
			int v1 = l1.get(i), v2 = l2.get(j);
			
			if (v1 < v2) {
				res.add(v1);
				i++;
			} else {
				res.add(v2);
				j++;
			}
		}
		
		while (i < l1.size()) {
			res.add(l1.get(i++));
		}
		
		while (j < l2.size()) {
			res.add(l2.get(j++));
		}
		
		return res;
	}
	
	static TreeNode sortedArrayToBST(ArrayList<Integer> list, int lo, int hi) {
		if (lo > hi) {
			return null;
		}
		
		int mid = lo + (hi - lo) / 2;
		
		TreeNode root = new TreeNode(list.get(mid));
		
		root.left = sortedArrayToBST(list, lo, mid - 1);
		root.right = sortedArrayToBST(list, mid + 1, hi);
		
		return root;
	}
	
}
