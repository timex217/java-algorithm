package problems.geeks.tree;

/**
 * Add all greater values to every node in a given BST
 * 
 * http://www.geeksforgeeks.org/add-greater-values-every-node-given-bst/
 * 
 * Given a Binary Search Tree (BST), modify it so that all greater values in the given BST are added to every node. For example, consider the following BST.
 * 
 *               50
 *            /      \
 *          30        70
 *         /   \      /  \
 *       20    40    60   80 
 * 
 * The above tree should be modified to following 
 * 
 *               260
 *            /      \
 *          330        150
 *         /   \       /  \
 *       350   300    210   80
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q065_Add_all_greater_values_to_every_node_in_a_given_BST {

	public static void main(String[] args) {
		TreeNode root = Tree.bst1();
		modifyBST(root);
		root.print();
	}
	
	// ------------------------------
	//  Reverse inorder
	// ------------------------------
	
	static int sum = 0;
	
	static void modifyBST(TreeNode root) {
		if (root == null) {
			return;
		}
		
		modifyBST(root.right);
		
		sum += root.val;
		root.val = sum;
		
		modifyBST(root.left);
	}
	
}
