package problems.geeks.tree;

/**
 * Find if there is a triplet in a Balanced BST that adds to zero
 * 
 * http://www.geeksforgeeks.org/find-if-there-is-a-triplet-in-bst-that-adds-to-0/
 * 
 * Given a Balanced Binary Search Tree (BST), write a function isTripletPresent() that returns true if there is a triplet in given BST with sum equals to 0, 
 * otherwise returns false. Expected time complexity is O(n^2) and only O(Logn) extra space can be used. You can modify given Binary Search Tree. 
 * Note that height of a Balanced BST is always O(Logn)
 * For example, isTripletPresent() should return true for following BST because there is a triplet with sum 0, the triplet is {-13, 6, 7}.
 * 
 *                   6
 *                 /   \
 *              -13    14
 *                \    / \
 *                -8  13  15
 *                    /
 *                   7
 *                   
 * Time Complexity: Time taken to convert BST to DLL is O(n) and time taken to find triplet in DLL is O(n^2).
 * 
 * Auxiliary Space: The auxiliary space is needed only for function call stack in recursive function convertBSTtoDLL(). 
 * Since given tree is balanced (height is O(Logn)), the number of functions in call stack will never be more than O(Logn).
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q054_Find_if_there_is_a_triplet_in_a_Balanced_BST_that_adds_to_zero {

	public static void main(String[] args) {
		TreeNode root = Tree.tree9();
		boolean res = isTripletPresent(root);
		System.out.println(res);
	}
	
	// The main function that returns true if there is a 0 sum triplet in
	// BST otherwise returns false
	static boolean isTripletPresent(TreeNode root) {
		if (root == null) {
			return false;
		}
		
		// Convert given BST to doubly linked list.  head and tail store the
	    // pointers to first and last nodes in DLLL
		convertBSTtoDLL(root);
		
		// Now iterate through every node and find if there is a pair with sum
	    // equal to -1 * heaf->key where head is current node
		return find3Sum(head, tail, 0);
	}
	
	// -------------------------------------------------------------------------------
	// A function to convert given BST to Doubly Linked List. left pointer is used
	// as previous pointer and right pointer is used as next pointer. The function
	// sets *head to point to first and *tail to point to last node of converted DLL
	//
	// 记住，用 inorder 来实现 BST -> DLL
	// -------------------------------------------------------------------------------
	static TreeNode head = null;
	static TreeNode tail = null;
	
	static void convertBSTtoDLL(TreeNode root) {
		// base case
		if (root == null) {
			return;
		}
		
		convertBSTtoDLL(root.left);
		
		// 上面一个遍历到的节点由tail指着
		root.left = tail;
		// 上一个节点指向当前节点
		if (tail != null) {
			tail.right = root;
		} else {
			head = root;
		}
		// tail永远指向当前最新遍历到的节点
		tail = root;
		
		convertBSTtoDLL(root.right);
	}
	
	// -------------------------------------------------------------------------------
	//  3-Sum in doubly linked list
	// -------------------------------------------------------------------------------
	static boolean find3Sum(TreeNode head, TreeNode tail, int sum) {
		while (head.right != tail) {
			if (find2Sum(head.right, tail, sum - head.val)) {
				return true;
			}
			head = head.right;
		}
		return false;
	}
	
	// -------------------------------------------------------------------------------
	//  2-Sum in doubly linked list
	// -------------------------------------------------------------------------------
	static boolean find2Sum(TreeNode head, TreeNode tail, int sum) {
		while (head != tail) {
			int val = head.val + tail.val;
			if (val == sum) {
				return true;
			} else if (val < sum) {
				head = head.right;
			} else {
				tail = tail.left;
			}
		}
		return false;
	}
}
