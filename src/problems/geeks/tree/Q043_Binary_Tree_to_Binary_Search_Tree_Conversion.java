package problems.geeks.tree;

/**
 * Binary Tree to Binary Search Tree Conversion
 * 
 * http://www.geeksforgeeks.org/binary-tree-to-binary-search-tree-conversion/
 * 
 * Given a Binary Tree, convert it to a Binary Search Tree. The conversion must be done in such a way that keeps the original structure of Binary Tree.
 * 
 * Examples.
 * 
 * Example 1
 * Input:
 *           10
 *          /  \
 *         2    7
 *        / \
 *       8   4
 * Output:
 *           8
 *          /  \
 *         4    10
 *        / \
 *       2   7
 * 
 * 
 * Example 2
 * Input:
 *           10
 *          /  \
 *         30   15
 *        /      \
 *       20       5
 * Output:
 *           15
 *          /  \
 *        10    20
 *        /      \
 *       5        30
 *       
 * Solution
 * Following is a 3 step solution for converting Binary tree to Binary Search Tree.
 * 
 * 1) Create a temp array arr[] that stores inorder traversal of the tree. This step takes O(n) time.
 * 
 * 2) Sort the temp array arr[]. Time complexity of this step depends upon the sorting algorithm. 
 * In the following implementation, Quick Sort is used which takes (n^2) time. This can be done in O(nLogn) time using Heap Sort or Merge Sort.
 * 
 * 3) Again do inorder traversal of tree and copy array elements to tree nodes one by one. This step takes O(n) time.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q043_Binary_Tree_to_Binary_Search_Tree_Conversion {
	
	static int index;
	
	public static void main(String[] args) {
		TreeNode root = Tree.tree3();
		binaryTreeToBST(root);
		root.print();
	}
	
	static void binaryTreeToBST(TreeNode root) {
		if (root == null) {
			return;
		}
		
		// step 1. inorder traversal and store values in arraylist
		ArrayList<Integer> list = new ArrayList<Integer>();
		storeInorder(root, list);
		
		// step 2. sort list
		Collections.sort(list);
		
		// step 3. inorder to update the tree
		index = 0;
		updateInorder(root, list);
	}
	
	static void storeInorder(TreeNode node, ArrayList<Integer> list) {
		if (node == null) {
			return;
		}
		
		storeInorder(node.left, list);
		list.add(node.val);
		storeInorder(node.right, list);
	}
	
	static void updateInorder(TreeNode node, ArrayList<Integer> list) {
		if (node == null) {
			return;
		}
		
		updateInorder(node.left, list);
		node.val = list.get(index++);
		updateInorder(node.right, list);
	}
	
}
