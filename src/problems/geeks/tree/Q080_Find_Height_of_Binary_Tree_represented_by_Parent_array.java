package problems.geeks.tree;

/**
 * Find Height of Binary Tree represented by Parent array
 * 
 * http://www.geeksforgeeks.org/find-height-binary-tree-represented-parent-array/
 * 
 * A given array represents a tree in such a way that the array value gives the parent node of that particular index. 
 * The value of the root node index would always be -1. Find the height of the tree.
 * Height of a Binary Tree is number of nodes on the path from root to the deepest leaf node, the number includes both root and leaf.
 * 
 * Input: parent[] = {1, 5, 5, 2, 2, -1, 3};
 * Output: 4
 * The given array represents following Binary Tree 
 *           5
 *         /  \
 *        1    2
 *       /    / \
 *      0    3   4
 *          /
 *         6 
 * 
 * 
 * Input: parent[] = {-1, 0, 0, 1, 1, 3, 5};
 * Output: 5
 * The given array represents following Binary Tree 
 *          0
 *        /   \
 *       1     2
 *      / \
 *     3   4
 *    /
 *   5 
 *  /
 * 6
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q080_Find_Height_of_Binary_Tree_represented_by_Parent_array {

	public static void main(String[] args) {
		int[] parent = {1, 5, 5, 2, 2, -1, 3};
		
		int max = findHeight(parent, 7);
		
		/* Method 1
		TreeNode root = constructTree(parent, 7);
		root.print();
		*/
	}
	
	// --------------------------------
	//  Method 2: depth[]
	// --------------------------------
	
	static int findHeight(int[] parent, int n) {
		// Create an array to store depth of all nodes/ and
	    // initialize depth of every node as 0 (an invalid
	    // value). Depth of root is 1
		int[] depth = new int[n];
		for (int i = 0; i < n; i++) {
			depth[i] = 0;
		}
		
		// fill depth of all nodes
		for (int i = 0; i < n; i++) {
			fillDepth(parent, i, depth);
		}
		
		// The height of binary tree is maximum of all depths.
	    // Find the maximum value in depth[] and assign it to ht.
		int max = depth[0];
		for (int i = 1; i < n; i++) {
			max = Math.max(max, depth[i]);
		}
		return max;
	}
	
	static void fillDepth(int[] parent, int i, int[] depth) {
		// If depth[i] is already filled
		if (depth[i] > 0) {
			return;
		}
		
		// If node at index i is root
	    if (parent[i] == -1)
	    {
	        depth[i] = 1;
	        return;
	    }
	    
	    // If depth of parent is not evaluated before, then evaluate
	    // depth of parent first
	    if (depth[parent[i]] == 0) {
	    	fillDepth(parent, parent[i], depth);
	    }
	    
	    // Depth of this node is depth of parent plus 1
	    depth[i] = depth[parent[i]] + 1;
	}
	
	// --------------------------------
	//  Method 1: Build the tree
	// --------------------------------
	static TreeNode constructTree(int[] parent, int n) {
		// step 1. build value - list map
		Map<Integer, ArrayList<Integer>> listMap = new HashMap<Integer, ArrayList<Integer>>();
		buildListMap(parent, n, listMap);
		
		// step 2. build the tree, start from -1 (root node)
		int index = find(parent, n, -1);
		if (index == -1) {
			return null;
		}
		
		return buildTree(index, listMap);
	}
	
	static TreeNode buildTree(int val, Map<Integer, ArrayList<Integer>> listMap) {
		TreeNode root = new TreeNode(val);
		
		ArrayList<Integer> list = listMap.get(val);
		
		if (list == null) {
			return root;
		}
		
		if (list.size() > 0) {
			root.left = buildTree(list.get(0), listMap);
		}
		
		if (list.size() > 1) {
			root.right = buildTree(list.get(1), listMap);
		}
		
		return root;
	}
	
	static void buildListMap(int[] parent, int n, Map<Integer, ArrayList<Integer>> map) {
		for (int i = 0; i < n; i++) {
			int p = parent[i];
			ArrayList<Integer> list;
			
			if (!map.containsKey(p)) {
				list = new ArrayList<Integer>();
			} else {
				list = map.get(p);
			}
			
			list.add(i);
			map.put(p, list);
		}
	}
	
	static int find(int[] arr, int n, int key) {
		for (int i = 0; i < n; i++) {
			if (arr[i] == key) {
				return i;
			}
		}
		
		return -1;
	}
	
}
