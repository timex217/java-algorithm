package problems.geeks.tree;

/**
 * Check for Children Sum Property in a Binary Tree.
 * 
 * http://www.geeksforgeeks.org/check-for-children-sum-property-in-a-binary-tree/
 * 
 * Given a binary tree, write a function that returns true if the tree satisfies below property.
 * 
 * For every node, data value must be equal to sum of data values in left and right children. Consider data value as 0 for NULL children.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q014_Check_for_Children_Sum_Property_in_a_Binary_Tree {

	public static void main(String[] args) {
		TreeNode root = Tree.tree2();
		boolean res = isSumProperty(root);
		System.out.println(res);
	}
	
	static boolean isSumProperty(TreeNode root) {
		if (root == null) {
			return true;
		}
		
		// leaf node is satisfied!
		if (root.left == null && root.right == null) {
			return true;
		}
		
		int left = (root.left == null) ? 0 : root.left.val;
		int right = (root.right == null) ? 0 : root.right.val;
		
		if (root.val != left + right) {
			return false;
		}
		
		return isSumProperty(root.left) && isSumProperty(root.right);
	}
	
}
