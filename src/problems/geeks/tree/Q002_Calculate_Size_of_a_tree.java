package problems.geeks.tree;

/**
 * Calculate Size of a tree
 * 
 * http://www.geeksforgeeks.org/write-a-c-program-to-calculate-size-of-a-tree/
 * 
 * Size of a tree is the number of elements present in the tree.
 * 
 * Size() function recursively calculates the size of a tree. It works as follows:
 * 
 * Size of a tree = Size of left subtree + 1 + Size of right subtree
 * 
 * Algorithm:
 * 
 * size(tree)
 * 1. If tree is empty then return 0
 * 2. Else
 *      (a) Get the size of left subtree recursively  i.e., call 
 *           size( tree->left-subtree)
 *      (a) Get the size of right subtree recursively  i.e., call 
 *           size( tree->right-subtree)
 *      (c) Calculate size of the tree as following:
 *             tree_size  =  size(left-subtree) + size(right-
 *                                subtree) + 1
 *      (d) Return tree_size
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q002_Calculate_Size_of_a_tree {

	public static void main(String[] args) {
		TreeNode root = Tree.tree1();
		int res = size(root);
		System.out.println(res);
	}
	
	static int size(TreeNode root) {
		if (root == null) {
			return 0;
		}
		
		return size(root.left) + 1 + size(root.right);
	}
	
}
