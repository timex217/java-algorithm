package problems.geeks.tree;

/**
 * Print all paths which sum to a given value
 * 
 * You are given a binary tree in which each node contains a value. Design an algorithm to print all paths which
 * sum to a given value. Note that a path can start or end anywhere in the tree.
 * 
 * Solution:
 * 
 * On every node, we look "up" to see if we've found the sum. That is, rather than asking "Does this node start a path with the sum?", 
 * we ask, "Does this node complete a path with the sum?"
 * 
 * When we recurse through each node n, we pass the function the full path from root to n. This function then adds the nodes along the path in
 * reverse order from n to root.
 * 
 * When the sum of each subpath equals sum, then we print this path.
 * 
 * Time complexity: O(n log(n)) since there are n nodes doing an average of log(n) amount of work on each step.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q019_Print_all_paths_which_sum_to_a_given_value {

	public static void main(String[] args) {
		TreeNode root = Tree.bst2();
		findSum(root, 20);
	}
	
	static void findSum(TreeNode node, int sum) {
		int depth = depth(node);
		int[] path = new int[depth];
		
		findSum(node, sum, path, 0);
	}
	
	static void findSum(TreeNode node, int sum, int[] path, int level) {
		if (node == null) {
			return;
		}
		
		/* Insert current node into path */
		path[level] = node.val;
		
		/* Look for paths with a sum that ends at this node. */
		int t = 0;
		for (int i = level; i >= 0; i--) {
			t += path[i];
			if (t == sum) {
				print(path, i, level);
			}
		}
		
		/* Search nodes beneath this one. */
		findSum(node.left, sum, path, level + 1);
		findSum(node.right, sum, path, level + 1);
		
		/* Remove current node from path. Not strictly necessary, 
		 * since we would ignore this value, but it's a good practice. */
		path[level] = Integer.MIN_VALUE;
	}
	
	/* Print the path */
	static void print(int[] path, int start, int end) {
		for (int i = start; i <= end; i++) {
			System.out.format("%d ", path[i]);
		}
		System.out.println();
	}
	
	static int depth(TreeNode node) {
		if (node == null) {
			return 0;
		}
		return 1 + Math.max(depth(node.left), depth(node.right));
	}
	
}
