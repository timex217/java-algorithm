package problems.geeks.tree;

/**
 * Decide if tree2 is a subtree of tree1
 * 
 * You have two very large binary trees: T1, with millions of nodes, and T2, with hundreds of nodes.
 * Create an algorithm to decide if T2 is a subtree of T1.
 * 
 * A tree T2 is a subtree of T1 if there exists a node n in T1 such that the subtree of n is identical to T2.
 * That is, if you cut off the tree at node n, the two trees would be identical.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q018_Decide_if_tree2_is_a_subtree_of_tree1 {

	public static void main(String[] args) {
		TreeNode t1 = Tree.tree3();
		TreeNode t2 = Tree.tree4();
		boolean res = containsTree(t1, t2);
		System.out.println(res);
	}
	
	static boolean containsTree(TreeNode t1, TreeNode t2) {
		// empty tree is always a sub tree
		if (t2 == null) {
			return true;
		}
		
		return isSubTree(t1, t2);
	}
	
	// 判断 t2 是否是 t1 的子树
	static boolean isSubTree(TreeNode t1, TreeNode t2) {
		// big tree is empty
		if (t1 == null) {
			return false;
		}
		
		// 当发现两棵树的根相同，从这里开始判断
		if (t1.val == t2.val) {
			if (matchTree(t1, t2)) {
				return true;
			}
		}
		
		// 否则分别从左右子树里找
		return isSubTree(t1.left, t2) || isSubTree(t1.right, t2);
	}
	
	// 判断两棵树是否相同
	static boolean matchTree(TreeNode t1, TreeNode t2) {
		if (t1 == null && t2 == null) {
			return true;
		}
		
		if (t1 == null || t2 == null) {
			return false;
		}
		
		if (t1.val != t2.val) {
			return false;
		}
		
		return matchTree(t1.left, t2.left) && matchTree(t1.right, t2.right);
	}
	
}
