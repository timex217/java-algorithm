package problems.geeks.tree;

/**
 * Print all nodes that are at distance k from a leaf node
 * 
 * http://www.geeksforgeeks.org/print-nodes-distance-k-leaf-node/
 * 
 * Given a Binary Tree and a positive integer k, print all nodes that are distance k from a leaf node.
 * 
 * Here the meaning of distance is different from previous post. Here k distance from a leaf means k levels higher than a leaf node. 
 * For example if k is more than height of Binary Tree, then nothing should be printed. 
 * Expected time complexity is O(n) where n is the number nodes in the given Binary Tree.
 * 
 *              1                   Examples:
 *           /     \                
 *          2       3               Nodes at distance 1 from leaf node are 2, 6, 3
 *         / \     / \              
 *        4   5   6   7             Nodes at distance 2 from a leaf node are 1 and 3
 *                 \                
 *                  8
 *                  
 * The idea is to traverse the tree. Keep storing all ancestors till we hit a leaf node. When we reach a leaf node, we print the 
 * ancestor at distance k. We also need to keep track of nodes that are already printed as output. For that we use a boolean array visited[].
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q073A_Print_all_nodes_that_are_at_distance_k_from_a_leaf_node {

	public static void main(String[] args) {
		TreeNode root = Tree.tree15();
		int k = 1;
		printKDistantfromLeaf(root, k);
	}
	
	static void printKDistantfromLeaf(TreeNode root, int k) {
		ArrayList<Integer> path = new ArrayList<Integer>();
		Set<Integer> set = new HashSet<Integer>();
		
		helper(root, k, path, set);
	}
	
	static void helper(TreeNode root, int k, ArrayList<Integer> path, Set<Integer> set) {
		if (root == null) {
			return;
		}
		
		path.add(root.val);
		
		// 碰到了 leaf 节点，就往回走 k 步，打印出path里的值
		if (root.left == null && root.right == null) {
			int n = path.size();
			
			if (n >= k + 1) {
				int v = path.get(n - 1 - k);
				
				if (!set.contains(v)) {
					set.add(v);
					System.out.format("%d ", v);
				}
			}
		} else {
			helper(root.left, k, path, set);
			helper(root.right, k, path, set);
		}
		
		path.remove(path.size() - 1);
	}
	
}
