package problems.geeks.tree;

/**
 * Find the maximum sum leaf to root path in a Binary Tree
 * 
 * http://www.geeksforgeeks.org/find-the-maximum-sum-path-in-a-binary-tree/
 * 
 * Given a Binary Tree, find the maximum sum path from a leaf to root. 
 * For example, in the following tree, there are three leaf to root paths 8->-2->10, -4->-2->10 and 7->10. 
 * The sums of these three paths are 16, 4 and 17 respectively. The maximum of them is 17 and the path for maximum is 7->10.
 * 
 *                 10
 *                /  \
 * 	            -2    7
 *             /  \     
 * 	          8   -4 
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q042_Find_the_maximum_sum_leaf_to_root_path_in_a_Binary_Tree {

	static int max;
	
	public static void main(String[] args) {
		TreeNode root = Tree.bst2();
		max = 0;
		maxSumPath(root, 0);
		
		System.out.println(max);
	}
	
	// ---------------------
	//  DFS
	// ---------------------
	static void maxSumPath(TreeNode node, int sum) {
		// 这条路走完了
		if (node == null) {
			max = Math.max(max, sum);
			return;
		}
		
		maxSumPath(node.left, sum + node.val);
		maxSumPath(node.right, sum + node.val);
	}
	
}
