package problems.geeks.tree;

/**
 * Root to leaf path sum equal to a given number
 * 
 * http://www.geeksforgeeks.org/root-to-leaf-path-sum-equal-to-a-given-number/
 * 
 * Given a binary tree and a number, return true if the tree has a root-to-leaf path such that adding up all the values along the path equals the given number. 
 * Return false if no such path can be found.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q020_Root_to_leaf_path_sum_equal_to_a_given_number {

	public static void main(String[] args) {
		TreeNode root = Tree.bst2();
		
		List<Integer> sol = new ArrayList<Integer>();
		List<List<Integer>> list = new ArrayList<List<Integer>>();
		pathSum(root, 54, sol, list);
		
		System.out.println(list.toString());
	}
	
	static void pathSum(TreeNode root, int sum, List<Integer> sol, List<List<Integer>> list) {
		if (root == null) {
			return;
		}
		
		sol.add(root.val);
		
		if (root.left == null && root.right == null && root.val == sum) {
			list.add(new ArrayList<Integer>(sol));
		} else {
			pathSum(root.left, sum - root.val, sol, list);
			pathSum(root.right, sum - root.val, sol, list);
		}
		
		sol.remove(sol.size() - 1);
	}
	
	
}
