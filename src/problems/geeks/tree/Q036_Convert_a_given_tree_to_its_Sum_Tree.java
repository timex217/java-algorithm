package problems.geeks.tree;

/**
 * Convert a given tree to its Sum Tree
 * 
 * http://www.geeksforgeeks.org/convert-a-given-tree-to-sum-tree/
 * 
 * Given a Binary Tree where each node has positive and negative values. Convert this to a tree where each node contains the sum of the left and right sub trees 
 * in the original tree. The values of leaf nodes are changed to 0.
 * 
 * For example, the following tree
 * 
 *                    10
 *                  /    \
 * 	              -2      6
 *               /  \    / \ 
 * 	            8   -4  7   5
 * 
 * should be changed to
 * 
 *                  20(4-2+12+6)
 *                 /     \
 * 	            4(8-4)  12(7+5)
 *             /   \     /  \ 
 * 	          0     0   0    0
 * 
 * Solution:
 * 
 * Do a traversal of the given tree. In the traversal, store the old value of the current node, 
 * recursively call for left and right subtrees and change the value of current node as sum of the values returned by the recursive calls. 
 * Finally return the sum of new value and value (which is sum of values in the subtree rooted with this node).
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q036_Convert_a_given_tree_to_its_Sum_Tree {

	public static void main(String[] args) {
		TreeNode root = Tree.tree5();
		toSumTree(root);
		root.print();
	}
	
	static int toSumTree(TreeNode node) {
		if (node == null) {
			return 0;
		}
		
		int old_val = node.val;
		
		// 利用了postorder的特性，先处理好左子树和右子树，然后再处理当前节点
		node.val = toSumTree(node.left) + toSumTree(node.right);
		
		// 这道题的难点在于返回的是：左右子树的返回值 ＋ 当前节点本来的值
		return node.val + old_val;
	}
	
}
