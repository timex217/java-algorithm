package problems.geeks.tree;

/**
 * Check whether a given Binary Tree is Complete or not
 * 
 * http://www.geeksforgeeks.org/check-if-a-given-binary-tree-is-complete-tree-or-not/
 * 
 * Given a Binary Tree, write a function to check whether the given Binary Tree is Complete Binary Tree or not.
 * 
 * A complete binary tree is a binary tree in which every level, except possibly the last, is completely filled, and all nodes are as far left as possible. See following examples.
 * 
 * The following trees are examples of Complete Binary Trees
 * 
 *     1
 *   /   \
 *  2     3
 *   
 *        1
 *     /    \
 *    2       3
 *   /
 *  4
 * 
 *        1
 *     /    \
 *    2      3
 *   /  \    /
 *  4    5  6
 *  
 * The following trees are examples of Non-Complete Binary Trees
 * 
 *     1
 *       \
 *        3
 *   
 *        1
 *     /    \
 *    2       3
 *     \     /  \   
 *      4   5    6
 * 
 *        1
 *     /    \
 *    2      3
 *          /  \
 *         4    5
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q046_Check_whether_a_given_Binary_Tree_is_Complete_or_not {

	public static void main(String[] args) {
		TreeNode root = Tree.bst1();
		boolean res = isCompleteTree(root);
		System.out.println(res);
	}
	
	// --------------------------
	//  Use level traversal
	// --------------------------
	
	static boolean isCompleteTree(TreeNode root) {
		if (root == null) {
			return false;
		}
		
		Queue<TreeNode> curr = new LinkedList<TreeNode>();
		Queue<TreeNode> next = new LinkedList<TreeNode>();
		curr.add(root);
		
		boolean must_have_no_child = false;
		
		while (!curr.isEmpty()) {
			TreeNode node = curr.poll();
			
			if (must_have_no_child) {
				if (node.left != null || node.right != null) {
					return false;
				}
			} else {
				if (node.left != null && node.right != null) {
					next.add(node.left);
					next.add(node.right);
				} else if (node.left != null && node.right == null) {
					next.add(node.left);
					must_have_no_child = true;
				} else if (node.left == null && node.right != null) {
					return false;
				} else {
					must_have_no_child = true;
				}
			}
			
			if (curr.isEmpty()) {
				curr = next;
				next = new LinkedList<TreeNode>();
			}
		}
		
		return true;
	}
	
}
