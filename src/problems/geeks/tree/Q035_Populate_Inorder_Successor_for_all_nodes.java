package problems.geeks.tree;

/**
 * Populate Inorder Successor for all nodes
 * 
 * http://www.geeksforgeeks.org/populate-inorder-successor-for-all-nodes/
 * 
 * Given a Binary Tree where each node has following structure, write a function to populate next pointer for all nodes. 
 * The next pointer for every node should be set to point to inorder successor.
 * 
 * Initially, all next pointers have NULL values. Your function should fill these next pointers so that they point to inorder successor.
 * 
 * Solution (Use Reverse Inorder Traversal)
 * 
 * Traverse the given tree in reverse inorder traversal and keep track of previously visited node. 
 * When a node is being visited, assign previously visited node as next.
 */

import java.util.*;

public class Q035_Populate_Inorder_Successor_for_all_nodes {

	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode next;
	}
	
	static TreeNode next;
	
	static void populateNext(TreeNode root) {
		if (root == null) {
			return;
		}
		
		// First set the next pointer in right subtree
		populateNext(root.right);
		// Set the next as previously visited node in reverse Inorder
		root.next = next;
		// Change the prev for subsequent node
		next = root;
		// Finally, set the next pointer in left subtree
		populateNext(root.left);
	}
	
	public static void main(String[] args) {
		
	}
	
}
