package problems.geeks.tree;

/**
 * Given a binary tree, print out all of its root-to-leaf paths one per line.
 * 
 * Algorithm:
 * 
 * initialize: pathlen = 0, path[1000] 
 * 1000 is some max limit for paths, it can change
 * 
 * printPathsRecur traverses nodes of tree in preorder
 * printPathsRecur(tree, path[], pathlen)
 *    1) If node is not NULL then 
 *          a) push data to path array: 
 *                 path[pathlen] = node->data.
 *          b) increment pathlen 
 *                 pathlen++
 *    2) If node is a leaf node then print the path array.
 *    3) Else
 *         a) Call printPathsRecur for left subtree
 *                  printPathsRecur(node->left, path, pathLen)
 *         b) Call printPathsRecur for right subtree.
 *                 printPathsRecur(node->right, path, pathLen)
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q007_Print_out_all_of_its_root_to_leaf_paths_one_per_line {

	public static void main(String[] args) {
		TreeNode root = Tree.bst2();
		printPath(root, new ArrayList<Integer>());
	}
	
	static void printPath(TreeNode root, ArrayList<Integer> sol) {
		if (root == null) {
			return;
		}
		
		// 处理当前解
		sol.add(root.val);
		
		// 检查是否需要终止
		if (root.left == null && root.right == null) {
			System.out.println(sol.toString());
		}
		// 否则处理下一层
		else {
			printPath(root.left, sol);
			printPath(root.right, sol);
		}
		
		// 回溯
		sol.remove(sol.size() - 1);
	}
	
}
