package problems.geeks.tree;

/**
 * Find the node with minimum value in a Binary Search Tree
 * 
 * http://www.geeksforgeeks.org/find-the-minimum-element-in-a-binary-search-tree/
 * 
 * This is quite simple. Just traverse the node from root to left recursively until left is NULL. The node whose left is NULL is the node with minimum value.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q010_Find_the_node_with_minimum_value_in_a_Binary_Search_Tree {

	public static void main(String[] args) {
		TreeNode root = Tree.bst2();
		int min = findMin(root);
		System.out.println(min);
	}
	
	// ----------------------------
	//  Recursion
	// ----------------------------
	
	static int findMinR(TreeNode root) {
		if (root == null) {
			throw new NullPointerException("Root node is null");
		}
		
		// Found the last leaf that doesn't have a left subtree
		if (root.left == null) {
			return root.val;
		}
		
		return findMinR(root.left);
	}
	
	// ----------------------------
	//  Iterative
	// ----------------------------
	
	static int findMin(TreeNode root) {
		if (root == null) {
			throw new NullPointerException("Root node is null");
		}
		
		while (root.left != null) {
			root = root.left;
		}
		
		return root.val;
	}
}
