package problems.geeks.tree;

/**
 * Deepest left leaf node in a binary tree
 * 
 * http://www.geeksforgeeks.org/deepest-left-leaf-node-in-a-binary-tree/
 * 
 * Given a Binary Tree, find the deepest leaf node that is left child of its parent. For example, consider the following tree. 
 * The deepest left leaf node is the node with value 9.
 * 
 *        1
 *      /   \
 *     2     3
 *   /      /  \  
 *  4      5    6
 *         \     \
 *          7     8
 *         /       \
 *        9         10
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q068_Deepest_left_leaf_node_in_a_binary_tree {

	public static void main(String[] args) {
		TreeNode root = Tree.tree12();
		findDeepestLeftLeaf(root, 1, false);
		System.out.format("%d\n", val);
	}
	
	// --------------------------
	//
	// --------------------------
	
	static int max = 0;
	static int val = 0;
	
	static void findDeepestLeftLeaf(TreeNode root, int level, boolean isLeft) {
		if (root == null) {
			return;
		}
		
		if (root.left == null && root.right == null && isLeft) {
			if (level > max) {
				max = level;
				val = root.val;
			}
		}
		
		findDeepestLeftLeaf(root.left, level + 1, true);
		findDeepestLeftLeaf(root.right, level + 1, false);
	}
}
