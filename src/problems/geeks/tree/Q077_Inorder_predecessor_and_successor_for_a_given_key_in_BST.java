package problems.geeks.tree;

/**
 * Inorder predecessor and successor for a given key in BST
 * 
 * http://www.geeksforgeeks.org/inorder-predecessor-successor-given-key-bst/
 * 
 * There is BST given with root node with key part as integer only. You need to find the inorder successor and predecessor of a given key. 
 * In case the given key is not found in BST, then return the two values within which this key will lie.
 * 
 *              20
 *             /  \
 *            8    22
 *           / \    \
 *          4  12    25
 *             / \
 *           10   14
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q077_Inorder_predecessor_and_successor_for_a_given_key_in_BST {

	public static void main(String[] args) {
		TreeNode root = Tree.bst2();
		int key = 14;
		
		int floor = getFloor(root, key);
		int ceil = getCeil(root, key);
		System.out.format("%d, %d\n", floor, ceil);
	}
	
	// --------------------------------
	//  Ceil
	// --------------------------------
	static int getCeil(TreeNode root, int key) {
		if (root == null) {
			return Integer.MIN_VALUE;
		}
		
		// 如果当前值比 key 要小，那么结果一定在右边
		if (root.val <= key) {
			return getCeil(root.right, key);
		} else {
			int left = getCeil(root.left, key);
			return (left > key) ? left : root.val;
		}
	}
	
	// --------------------------------
	//  Floor
	// --------------------------------
	static int getFloor(TreeNode root, int key) {
		if (root == null) {
			return Integer.MAX_VALUE;
		}
		
		if (root.val >= key) {
			return getFloor(root.left, key);
		} else {
			int right = getFloor(root.right, key);
			return (right < key) ? right : root.val;
		}
	}
}
