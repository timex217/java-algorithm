package problems.geeks.tree;

/**
 * Print nodes at k distance from root
 * 
 * http://www.geeksforgeeks.org/print-nodes-at-k-distance-from-root/
 * 
 * Given a root of a tree, and an integer k. Print all the nodes which are at k distance from root.
 * 
 * For example, in the below tree, 4, 5 & 8 are at distance 2 from root.
 *             1
 *           /   \
 *         2      3
 *       /  \    /
 *     4     5  8 
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q024_Print_nodes_at_k_distance_from_root {

	public static void main(String[] args) {
		TreeNode root = Tree.bst2();
		printNodes(root, 3);
	}
	
	static void printNodes(TreeNode root, int k) {
		if (root == null || k < 1) {
			return;
		}
		
		if (k == 1) {
			System.out.format("%d ", root.val);
			return;
		}
		
		printNodes(root.left, k - 1);
		printNodes(root.right, k - 1);
	}
	
}
