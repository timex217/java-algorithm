package problems.geeks.tree;

/**
 * Check if a given Binary Tree is SumTree
 * 
 * http://www.geeksforgeeks.org/check-if-a-given-binary-tree-is-sumtree/
 * 
 * Write a function that returns true if the given Binary Tree is SumTree else false. 
 * 
 * A SumTree is a Binary Tree where the value of a node is equal to sum of the nodes present in its left subtree and right subtree. 
 * An empty tree is SumTree and sum of an empty tree can be considered as 0. A leaf node is also considered as SumTree.
 * 
 * Following is an example of SumTree.
 * 
 *           26
 *         /   \
 *       10     3
 *     /    \     \
 *   4      6      3
 *   
 * 
 * Method 1 ( Simple ) 
 * Get the sum of nodes in left subtree and right subtree. Check if the sum calculated is equal to root�s data. 
 * Also, recursively check if the left and right subtrees are SumTrees.
 * 
 * Method 2 ( Tricky ) 
 * The Method 1 uses sum() to get the sum of nodes in left and right subtrees. The method 2 uses following rules to get the sum directly.
 * 1) If the node is a leaf node then sum of subtree rooted with this node is equal to value of this node.
 * 2) If the node is not a leaf node then sum of subtree rooted with this node is twice the value of this node (Assuming that the tree rooted with this node is SumTree).
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q032_Check_if_a_given_Binary_Tree_is_SumTree {

	public static void main(String[] args) {
		TreeNode root = Tree.sumtree();
		boolean res = isSumTree(root);
		System.out.println(res);
	}
	
	static boolean isLeaf(TreeNode root) {
		if (root == null) {
			return false;
		}
		return root.left == null && root.right == null;
	}
	
	// ---------------------------
	//  Method 1
	// ---------------------------
	/*
	static boolean isSumTree(TreeNode root) {
		if (root == null || isLeaf(root)) {
			return true;
		}
		
		int leftsum = sum(root.left);
		int rightsum = sum(root.right);
		
		if (root.val != leftsum + rightsum) {
			return false;
		}
		
		return isSumTree(root.left) && isSumTree(root.right);
	}
	
	static int sum(TreeNode node) {
		if (node == null) {
			return 0;
		}
		return sum(node.left) + node.val + sum(node.right);
	}
	*/
	
	// ---------------------------
	//  Method 2
	// ---------------------------
	static boolean isSumTree(TreeNode node) {
		if (node == null || isLeaf(node)) {
			return true;
		}
		
		int ls, rs;
		
		// calculate left sum
		if (node.left == null) {
			ls = 0;
		} else if (isLeaf(node.left)) {
			ls = node.left.val;
		} else {
			ls = 2 * node.left.val;
		}
		
		// calculate right sum
		if (node.right == null) {
			rs = 0;
		} else if (isLeaf(node.right)) {
			rs = node.right.val;
		} else {
			rs = 2 * node.right.val;
		}
		
		if (node.val != ls + rs) {
			return false;
		}
		
		return isSumTree(node.left) && isSumTree(node.right);
	}
	
}
