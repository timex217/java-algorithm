package problems.geeks.tree;

/**
 * Determine if Two Trees are Identical
 * 
 * http://www.geeksforgeeks.org/write-c-code-to-determine-if-two-trees-are-identical/
 * 
 * Two trees are identical when they have same data and arrangement of data is also same.
 * 
 * To identify if two trees are identical, we need to traverse both trees simultaneously, and while traversing we need to compare data and children of the trees.
 * 
 * Algorithm:
 * 
 * sameTree(tree1, tree2)
 * 1. If both trees are empty then return 1.
 * 2. Else If both trees are non -empty
 *      (a) Check data of the root nodes (tree1->data ==  tree2->data)
 *      (b) Check left subtrees recursively  i.e., call sameTree( 
 *           tree1->left_subtree, tree2->left_subtree)
 *      (c) Check right subtrees recursively  i.e., call sameTree( 
 *           tree1->right_subtree, tree2->right_subtree)
 *      (d) If a,b and c are true then return 1.
 * 3  Else return 0 (one is empty and other is not)
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q003_Determine_if_Two_Trees_are_Identical {

	public static void main(String[] args) {
		TreeNode r1 = Tree.tree1();
		TreeNode r2 = Tree.bst1();
		
		boolean res = isIdentical(r1, r2);
		System.out.println(res);
	}
	
	static boolean isIdentical(TreeNode r1, TreeNode r2) {
		if (r1 == null && r2 == null) {
			return true;
		}
		
		if (r1 == null || r2 == null) {
			return false;
		}
		
		if (r1.val != r2.val) {
			return false;
		}
		
		return isIdentical(r1.left, r2.left) && isIdentical(r1.right, r2.right);
	}
	
}
