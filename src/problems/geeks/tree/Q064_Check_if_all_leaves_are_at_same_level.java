package problems.geeks.tree;

/**
 * Check if all leaves are at same level
 * 
 * http://www.geeksforgeeks.org/check-leaves-level/
 * 
 * Given a Binary Tree, check if all leaves are at same level or not.
 * 
 *           12
 *         /    \
 *       5       7       
 *     /          \ 
 *    3            1
 *   Leaves are at same level
 * 
 *           12
 *         /    \
 *       5       7       
 *     /          
 *    3          
 *    Leaves are Not at same level
 * 
 *           12
 *         /    
 *       5             
 *     /   \        
 *    3     9
 *   /      /
 *  1      2
 *  Leaves are at same level
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q064_Check_if_all_leaves_are_at_same_level {

	public static void main(String[] args) {
		TreeNode root = Tree.tree1();
		boolean res = checkLeavesLevel(root, 1);
		System.out.println(res);
	}
	
	// --------------------------
	//  preorder
	// --------------------------
	static int leafLevel = 0;
	
	static boolean checkLeavesLevel(TreeNode root, int level) {
		if (root == null) {
			return true;
		}
		
		if (root.left == null && root.right == null) {
			if (leafLevel == 0) {
				leafLevel = level;
				return true;
			}
			return leafLevel == level;
		}
		
		return checkLeavesLevel(root.left, level + 1) && checkLeavesLevel(root.right, level + 1);
	}
	
}
