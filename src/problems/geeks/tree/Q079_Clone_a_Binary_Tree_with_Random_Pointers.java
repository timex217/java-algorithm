package problems.geeks.tree;

/**
 * Clone a Binary Tree with Random Pointers
 * 
 * http://www.geeksforgeeks.org/clone-binary-tree-random-pointers/
 * 
 */

import java.util.*;

public class Q079_Clone_a_Binary_Tree_with_Random_Pointers {
	
	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode random;
		TreeNode(int x) {
			this.val = x;
			this.left = null;
			this.right = null;
			this.random = null;
		}
	}
	
	public static void main(String[] args) {
		
	}
	
	static TreeNode copyTree(TreeNode root) {
		if (root == null) {
			return null;
		}
		
		// step 1. copy the whole structure
		Map<TreeNode, TreeNode> map = new HashMap<TreeNode, TreeNode>();
		copyTree(root, map);
		
		// step 2. copy random node
		copyRandomNode(root, map);
		
		return map.get(root);
	}
	
	static void copyTree(TreeNode root, Map<TreeNode, TreeNode> map) {
		if (root == null) {
			return;
		}
		
		TreeNode copy = new TreeNode(root.val);
		map.put(root, copy);
		
		copyTree(root.left, map);
		copyTree(root.right, map);
	}
	
	static void copyRandomNode(TreeNode root, Map<TreeNode, TreeNode> map) {
		if (root == null) {
			return;
		}
		
		TreeNode copy = map.get(root);
		copy.left = map.get(root.left);
		copy.right = map.get(root.right);
		copy.random = map.get(root.random);
		
		copyRandomNode(root.left, map);
		copyRandomNode(root.right, map);
	}
	
}
