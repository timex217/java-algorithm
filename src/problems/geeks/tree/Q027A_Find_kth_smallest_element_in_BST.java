package problems.geeks.tree;

/**
 * Find k-th smallest element in BST
 * 
 * http://www.geeksforgeeks.org/find-k-th-smallest-element-in-bst-order-statistics-in-bst/
 * 
 * Given root of binary search tree and K as input, find K-th smallest element in BST.
 * 
 * For example, in the following BST, if k = 3, then output should be 10, and if k = 5, then output should be 14.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q027A_Find_kth_smallest_element_in_BST {

	public static void main(String[] args) {
		TreeNode root = Tree.bst2();
		TreeNode node = findKthSmallest(root, 5);
		System.out.println(node.val);
	}
	
	/**
	 * BST 2:
	 *              20
	 *             /  \
	 *            8    22
	 *           / \
	 *          4  12
	 *             / \
	 *           10   14
	 *           
	 * Method 1: Using Inorder Traversal.
	 * 
	 * Inorder traversal of BST retrieves elements of tree in the sorted order. 
	 * The inorder traversal uses stack to store to be explored nodes of tree (threaded tree avoids stack and recursion for traversal, see this post). 
	 * The idea is to keep track of popped elements which participate in the order statics. 
	 */
	static TreeNode findKthSmallest(TreeNode root, int k) {
		if (root == null || k < 1) {
			return null;
		}
		
		Stack<TreeNode> stack = new Stack<TreeNode>();
		TreeNode curr = root;
		
		int count = 1;
		while (!stack.isEmpty() || curr != null) {
			if (curr != null) {
				stack.push(curr);
				curr = curr.left;
			} else {
				curr = stack.pop();
				
				if (count == k) {
					return curr;
				}
				
				curr = curr.right;
				count++;
			}
		}
		
		return null;
	}
	
}
