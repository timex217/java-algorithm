package problems.geeks.tree;

/**
 * Construct Full Binary Tree from given preorder and postorder traversals
 * 
 * http://www.geeksforgeeks.org/full-and-complete-binary-tree-from-given-preorder-and-postorder-traversals/
 * 
 * Given two arrays that represent preorder and postorder traversals of a full binary tree, construct the binary tree.
 * 
 * A Full Binary Tree is a binary tree where every node has either 0 or 2 children
 * 
 * Following are examples of Full Trees.
 * 
 *         1
 *       /   \
 *     2       3
 *   /  \     /  \
 *  4    5   6    7
 * 
 * 
 *        1
 *      /   \
 *    2      3
 *         /   \  
 *        4     5
 *            /   \  
 *           6    7
 *                   
 * 
 *           1
 *         /   \
 *       2       3
 *     /  \     /  \
 *    4    5   6    7
 *  /  \  
 * 8    9 
 * 
 * It is not possible to construct a general Binary Tree from preorder and postorder traversals (See this). 
 * But if know that the Binary Tree is Full, we can construct the tree without ambiguity. Let us understand this with the help of following example.
 * 
 * Let us consider the two given arrays as pre[] = {1, 2, 4, 8, 9, 5, 3, 6, 7} and post[] = {8, 9, 4, 5, 2, 6, 7, 3, 1};
 * In pre[], the leftmost element is root of tree. Since the tree is full and array size is more than 1. The value next to 1 in pre[], 
 * must be left child of root. So we know 1 is root and 2 is left child. How to find the all nodes in left subtree? 
 * We know 2 is root of all nodes in left subtree. All nodes before 2 in post[] must be in left subtree. 
 * Now we know 1 is root, elements {8, 9, 4, 5, 2} are in left subtree, and the elements {6, 7, 3} are in right subtree.
 * 
 *                   1
 *                 /   \
 *                /     \
 *    {8, 9, 4, 5, 2}  {6, 7, 3}
 * We recursively follow the above approach and get the following tree.
 * 
 *           1
 *         /   \
 *       2       3
 *     /  \     /  \
 *    4    5   6    7
 *   / \  
 *  8   9 
 * 
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q049A_Construct_Full_Binary_Tree_from_given_preorder_and_postorder_traversals {
	
	public static void main(String[] args) {
		int[] pre = {1, 2, 4, 8, 9, 5, 3, 6, 7};
		int[] post = {8, 9, 4, 5, 2, 6, 7, 3, 1};
		TreeNode root = construct(pre, post, 9);
		root.print();
	}
	
	static TreeNode construct(int[] pre, int[] post, int size) {
		// step 1. preparation. use a hash map to store the index of each element in post
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (int i = 0; i < size; i++) {
			map.put(post[i], i);
		}
		
		return helper(pre, 0, size - 1, post, 0, size - 1, map);
	}
	
	static TreeNode helper(int[] pre, int i, int i2, int[] post, int j, int j2, Map<Integer, Integer> map) {
		int n = i2 - i + 1;
		
		if (n <= 0) {
			return null;
		}
		
		// pre的第一个就是root node
		TreeNode root = new TreeNode(pre[i]);
		
		if (n == 1) {
			return root;
		}
		
		// 找到root左孩子的index
		int index = map.get(pre[i + 1]);
		
		// 计算出左子树大小
		int n_left = index - j + 1;
		
		// Recursion
		root.left = helper(pre, i + 1, i + n_left, post, j, j + n_left - 1, map);
		root.right = helper(pre, i + n_left + 1, i2, post, j + n_left, j2 - 1, map);
		
		return root;
	}
	
}
