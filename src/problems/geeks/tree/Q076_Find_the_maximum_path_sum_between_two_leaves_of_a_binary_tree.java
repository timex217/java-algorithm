package problems.geeks.tree;

/**
 * Find the maximum path sum between two leaves of a binary tree
 * 
 * http://www.geeksforgeeks.org/find-maximum-path-sum-two-leaves-binary-tree/
 * 
 * Given a binary tree in which each node element contains a number. Find the maximum possible sum from one leaf node to another.
 * The maximum sum path may or may not go through root. For example, in the following binary tree, the maximum sum is 27(3 + 6 + 9 + 0 – 1 + 10). 
 * Expected time complexity is O(n).
 * 
 *                     -15
 *                    /    \
 *                   5     (6)
 *                  / \    / \
 *                -8   1 (3) (9)
 *                / \          \
 *               2   6         (0)
 *                             / \
 *                            4 (-1)
 *                               /
 *                             (10)
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q076_Find_the_maximum_path_sum_between_two_leaves_of_a_binary_tree {

	public static void main(String[] args) {
		TreeNode root = Tree.tree17();
		maxPathSum(root);
		System.out.println(max);
	}
	
	// ------------------------------
	//  Postorder
	// ------------------------------
	
	static int max = Integer.MIN_VALUE;
	
	static int maxPathSum(TreeNode root) {
		if (root == null) {
			return 0;
		}
		
		int left = maxPathSum(root.left);
		int right = maxPathSum(root.right);
		
		int curr = left + right + root.val;
		max = Math.max(max, curr);
		
		// 处理需要返回的值
		int res = (left > right) ? left + root.val : right + root.val;
		return (res > 0) ? res : 0;
	}
	
}
