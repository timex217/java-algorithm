package problems.geeks.tree;

/**
 * Print Postorder traversal from given Inorder and Preorder traversals
 * 
 * http://www.geeksforgeeks.org/print-postorder-from-given-inorder-and-preorder-traversals/
 * 
 * Given Inorder and Preorder traversals of a binary tree, print Postorder traversal.
 * 
 * Example:
 * 
 * Input:
 * Inorder traversal in[] = {4, 2, 5, 1, 3, 6}
 * Preorder traversal pre[] = {1, 2, 4, 5, 3, 6}
 * 
 * Output:
 * Postorder traversal is {4, 5, 2, 6, 3, 1}
 * Trversals in the above example represents following tree
 * 
 *          1
 *       /     \   
 *      2       3
 *    /   \      \
 *   4     5      6
 * A naive method is to first construct the tree, then use simple recursive method to print postorder traversal of the constructed tree.
 * 
 * We can print postorder traversal without constructing the tree. The idea is, root is always the first item in preorder traversal and it must be the last 
 * item in postorder traversal. We first recursively print left subtree, then recursively print right subtree. Finally, print root. To find boundaries of 
 * left and right subtrees in pre[] and in[], we search root in in[], all elements before root in in[] are elements of left subtree and all elements after 
 * root are elements of right subtree. In pre[], all elements after index of root in in[] are elements of right subtree. And elements before index 
 * (including the element at index and excluding the first element) are elements of left subtree.
 * 
 * 
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q062_Print_Postorder_traversal_from_given_Inorder_and_Preorder_traversals {

	public static void main(String[] args) {
		int[] in = {4, 2, 5, 1, 3, 6};
		int[] pre = {1, 2, 4, 5, 3, 6};
		printPostOrder(in, pre, 6);
	}
	
	static void printPostOrder(int[] in, int[] pre, int n) {
		// use a hashmap to store the index in in[]
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (int i = 0; i < n; i++) {
			map.put(in[i], i);
		}
		
		// step 2. print in postorder manner
		print(in, 0, n - 1, pre, 0, n - 1, map);
	}
	
	static void print(int[] in, int i, int i2, int[] pre, int j, int j2, Map<Integer, Integer> map) {
		// base case
		if (i > i2 || j > j2) {
			return;
		}
		
		int val = pre[j];
		int index = map.get(val);
		int n_left = index - i;
		
		// print left
		print(in, i, index - 1, pre, j + 1, j + n_left, map);
		// print right
		print(in, index + 1, i2, pre, j + n_left + 1, j2, map);
		// print current value
		System.out.format("%d ", val);
	}
}
