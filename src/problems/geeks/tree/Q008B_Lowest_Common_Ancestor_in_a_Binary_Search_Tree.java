package problems.geeks.tree;

/**
 * Lowest Common Ancestor in a Binary Search Tree
 * 
 * http://www.geeksforgeeks.org/lowest-common-ancestor-in-a-binary-search-tree/
 * 
 * Given values of two nodes in a Binary Search Tree, write a c program to find the Lowest Common Ancestor (LCA). You may assume that both the values exist in the tree.
 *  
 *  Solutions:
 * If we are given a BST where every node has parent pointer, then LCA can be easily determined by traversing up using parent pointer and printing the 
 * first intersecting node.
 * 
 * We can solve this problem using BST properties. We can recursively traverse the BST from root. The main idea of the solution is, 
 * while traversing from top to bottom, the first node n we encounter with value between n1 and n2, i.e., n1 < n < n2 or same as one of the n1 or n2, 
 * is LCA of n1 and n2 (assuming that n1 < n2). So just recursively traverse the BST in, if node's value is greater than both n1 and n2 then our LCA lies in 
 * left side of the node, if it's is smaller than both n1 and n2, then LCA lies on right side. Otherwise root is LCA (assuming that both n1 and n2 are present in BST)
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q008B_Lowest_Common_Ancestor_in_a_Binary_Search_Tree {

	public static void main(String[] args) {
		
	}
	
	// ------------------------
	//  Recursion
	// ------------------------
	
	/* Function to find LCA of n1 and n2. The function assumes that both
	   n1 and n2 are present in BST */
	static TreeNode lcaR(TreeNode root, TreeNode n1, TreeNode n2) {
		if (root == null) {
			return null;
		}
		
		// If both n1 and n2 are smaller than root, then LCA lies in left
		if (n1.val < root.val && n2.val < root.val) {
			return lcaR(root.left, n1, n2);
		}
		
		// If both n1 and n2 are greater than root, then LCA lies in right
		if (n1.val > root.val && n2.val > root.val) {
			return lcaR(root.right, n1, n2);
		}
		
		return root;
	}
	
	// ------------------------
	//  Iterative
	// ------------------------
	
	static TreeNode lca(TreeNode root, TreeNode n1, TreeNode n2) {
		while (root != null) {
			// If both n1 and n2 are smaller than root, then LCA lies in left
			if (n1.val < root.val && n2.val < root.val) {
				root = root.left;
			}
			// If both n1 and n2 are greater than root, then LCA lies in right
			else if (n1.val > root.val && n2.val > root.val) {
				root = root.right;
			}
			else {
				break;
			}
		}
		
		return root;
	}
	
}
