package problems.geeks.tree;

/**
 * Find distance between two given keys of a Binary Tree
 * 
 * http://www.geeksforgeeks.org/find-distance-two-given-nodes/
 * 
 * Find the distance between two keys in a binary tree, no parent pointers are given. 
 * Distance between two nodes is the minimum number of edges to be traversed to reach one node from other.
 * 
 *              1                   Examples
 *           /     \                Dist(4, 5) = 2
 *          2       3               Dist(4, 6) = 4
 *         / \     / \              Dist(3, 4) = 3
 *        4   5   6   7             Dist(2, 4) = 1
 *                 \                Dist(8, 5) = 5
 *                  8
 *                  
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q072_Find_distance_between_two_given_keys_of_a_Binary_Tree {

	public static void main(String[] args) {
		TreeNode root = Tree.tree15();
		int res = dist(root, 5, 8);
		System.out.println(res);
	}
	
	static int dist(TreeNode root, int v1, int v2) {
		// step 1. get the LCA
		TreeNode lca = LCA(root, v1, v2);
		
		if (lca == null) {
			return -1;
		}
		
		// step 2. get v1, v2 levels based on lca
		int l1 = getLevel(lca, v1, 0);
		int l2 = getLevel(lca, v2, 0);
		
		return l1 + l2;
	}
	
	static TreeNode LCA(TreeNode root, int v1, int v2) {
		if (root == null) {
			return null;
		}
		
		// 如果 v1 或者 v2 就是 root，那么root就是最小公共父节点
		if (root.val == v1 || root.val == v2) {
			return root;
		}
		
		// 从左右子树去找他们的最小公共父节点
		TreeNode left = LCA(root.left, v1, v2);
		TreeNode right = LCA(root.right, v1, v2);
		
		// 如果v1, v2分别在两颗子树上，那么root就是最小公共父节点
		if (left != null && right != null) {
			return root;
		}
		
		return (left != null) ? left : right;
	}
	
	static int getLevel(TreeNode root, int v, int level) {
		if (root == null) {
			return -1;
		}
		
		if (root.val == v) {
			return level;
		}
		
		int left = getLevel(root.left, v, level + 1);
		int right = getLevel(root.right, v, level + 1);
		
		if (left == -1 && right == -1) {
			return -1;
		}
		
		return (left != -1) ? left : right;
	}
	
}
