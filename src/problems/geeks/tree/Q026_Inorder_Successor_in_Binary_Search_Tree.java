package problems.geeks.tree;

/**
 * Inorder Successor in Binary Search Tree
 * 
 * http://www.geeksforgeeks.org/inorder-successor-in-binary-search-tree/
 * 
 * In Binary Tree, Inorder successor of a node is the next node in Inorder traversal of the Binary Tree. 
 * Inorder Successor is NULL for the last node in Inoorder traversal.
 * 
 * In Binary Search Tree, Inorder Successor of an input node can also be defined as the node with the smallest key greater than the key of input node. 
 * So, it is sometimes important to find next node in sorted order.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q026_Inorder_Successor_in_Binary_Search_Tree {

	public static void main(String[] args) {
		TreeNode root = Tree.bst2();
		TreeNode node = Tree.getTreeNode(root, 8);
		
		inOrderSuccessorR(root, node);
		
		/*
		TreeNode successor = inOrderSuccessor(root, node);
		if (successor == null) {
			System.out.println("null");
		} else {
			System.out.println(successor.val);
		}
		*/
	}
	
	
	static TreeNode successor = null;
	
	static void inOrderSuccessorR(TreeNode root, TreeNode node) {
		if (root == null) {
			return;
		}
		
		inOrderSuccessorR(root.right, node);
		
		if (node.val == root.val) {
			System.out.println(successor.val);
		}
		
		successor = root;
		
		inOrderSuccessorR(root.left, node);
	}
	
	/**
	 * BST 2:
	 *              20
	 *             /  \
	 *            8    22
	 *           / \
	 *          4  12
	 *             / \
	 *           10   14
	 */
	static TreeNode inOrderSuccessor(TreeNode root, TreeNode node) {
		if (root == null || node == null) {
			return null;
		}
		
		// 如果右子树不为空，则从右边找最小的
		if (node.right != null) {
			return minNode(node.right);
		}
		
		// 否则从 root 开始找一直到找到 node 为止
		// 在找的过程中，如果 root 的值比 node 大，那么记录下它
		TreeNode succ = null;
		
		while (root != null) {
			if (root.val > node.val) {
				succ = root;
				root = root.left;
			} else if (root.val < node.val) {
				root = root.right;
			} else {
				break;
			}
		}
		
		return succ;
	}
	
	static TreeNode minNode(TreeNode node) {
		while (node.left != null) {
			node = node.left;
		}
		return node;
	}
	
}
