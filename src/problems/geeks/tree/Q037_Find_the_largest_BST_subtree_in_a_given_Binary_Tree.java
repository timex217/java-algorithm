package problems.geeks.tree;

/**
 * Find the largest BST subtree in a given Binary Tree
 * 
 * http://www.geeksforgeeks.org/find-the-largest-subtree-in-a-tree-that-is-also-a-bst/
 * 
 * Given a Binary Tree, write a function that returns the size of the largest subtree which is also a Binary Search Tree (BST). If the complete Binary Tree is BST, then return the size of whole tree.
 * 
 * Examples:
 * 
 * Input: 
 *       5
 *     /  \
 *    2    4
 *  /  \
 * 1    3
 * 
 * Output: 3 
 * The following subtree is the maximum size BST subtree 
 *    2  
 *  /  \
 * 1    3
 * 
 * 
 * Input: 
 *        50
 *      /    \
 *   30       60
 *  /  \     /  \ 
 * 5   20   45    70
 *               /  \
 *             65    80
 * Output: 5
 * The following subtree is the maximum size BST subtree 
 *       60
 *      /  \ 
 *    45    70
 *         /  \
 *       65    80
 *       
 * Solution:
 * 
 * Method 2 (Tricky and Efficient)
 * In method 1, we traverse the tree in top down manner and do BST test for every node. 
 * 
 * If we traverse the tree in bottom up manner, then we can pass information about subtrees to the parent. 
 * 
 * The passed information can be used by the parent to do BST test (for parent node) only in constant time (or O(1) time). 
 * 
 * A left subtree need to tell the parent whether it is BST or not and also need to pass maximum value in it. 
 * So that we can compare the maximum value with the parent’s data to check the BST property. 
 * Similarly, the right subtree need to pass the minimum value up the tree. 
 * The subtrees need to pass the following information up the tree for the finding the largest BST.
 * 
 * 1) Whether the subtree itself is BST or not (In the following code, is_bst_ref is used for this purpose)
 * 2) If the subtree is left subtree of its parent, then maximum value in it. And if it is right subtree then minimum value in it.
 * 3) Size of this subtree if this subtree is BST (In the following code, return value of largestBSTtil() is used for this purpose)
 * 
 * max_ref is used for passing the maximum value up the tree and min_ptr is used for passing minimum value up the tree.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q037_Find_the_largest_BST_subtree_in_a_given_Binary_Tree {

	public static void main(String[] args) {
		TreeNode root = Tree.tree6();
		largestBST(root);
		System.out.println(max_bst);
	}
	
	// ------------------------
	//  Postorder
	// ------------------------
	static int max_bst;
	static int max_ref;  // 左子树最大值
	static int min_ref;  // 右子树最小值
	static boolean is_bst;
	
	static void largestBST(TreeNode root) {
		max_ref = Integer.MIN_VALUE;
		min_ref = Integer.MAX_VALUE;
		max_bst = 0;
		
		postorder(root);
	}
	
	static int postorder(TreeNode node) {
		if (node == null) {
			is_bst = true;
			return 0;
		}
		
		// --------------------
		//  left subtree
		// --------------------
		boolean is_left_bst = false;
		
		max_ref = Integer.MIN_VALUE;
		
		int left_bst = postorder(node.left);
		
		// 如果左子树是BST并且当前值大于左子树的最大值, 标记左边OK了
		if (is_bst && node.val > max_ref) {
			is_left_bst = true;
		}
		// 把左子树里的最小值保存下来
		int min = min_ref;
		
		// --------------------
		//  right subtree
		// --------------------
		boolean is_right_bst = false;
		
		min_ref = Integer.MAX_VALUE;
		
		int right_bst = postorder(node.right);
		
		// 如果右子树是BST并且当前值小于右子树的最小值，标记右边OK了
		if (is_bst && node.val < min_ref) {
			is_right_bst = true;
		}
		
		// --------------------
		//  current node
		// --------------------
		
		min_ref = Math.min(min, Math.min(node.val, min_ref));
		max_ref = Math.max(node.val, max_ref);
		
		if (is_left_bst && is_right_bst) {
			is_bst = true;
			int curr_bst = left_bst + right_bst + 1;
			max_bst = Math.max(max_bst, curr_bst);
			return curr_bst;
		} else {
			is_bst = false;
			return 0;
		}
		
	}
	
}
