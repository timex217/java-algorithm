package problems.geeks.tree;

/**
 * Find a pair with given sum in a Balanced BST
 * 
 * http://www.geeksforgeeks.org/find-a-pair-with-given-sum-in-bst/
 * 
 * Given a Balanced Binary Search Tree and a target sum, write a function that returns true if there is a pair with sum equals to target sum, 
 * otherwise return false. Expected time complexity is O(n) and only O(Logn) extra space can be used. Any modification to Binary Search Tree is not allowed. 
 * Note that height of a Balanced BST is always O(Logn).
 * 
 * The Brute Force Solution is to consider each pair in BST and check whether the sum equals to X. The time complexity of this solution will be O(n^2).
 * 
 * A Better Solution is to create an auxiliary array and store Inorder traversal of BST in the array. 
 * The array will be sorted as Inorder traversal of BST always produces sorted data. Once we have the Inorder traversal, we can pair in O(n) 
 * time (See this for details). This solution works in O(n) time, but requires O(n) auxiliary space.
 * 
 * A space optimized solution is discussed in previous post. The idea was to first in-place convert BST to Doubly Linked List (DLL), 
 * then find pair in sorted DLL in O(n) time. This solution takes O(n) time and O(Logn) extra space, but it modifies the given BST.
 * 
 * The solution discussed below takes O(n) time, O(Logn) space and doesn�t modify BST. The idea is same as finding the pair in sorted array 
 * (See method 1 of this for details). We traverse BST in Normal Inorder and Reverse Inorder simultaneously. In reverse inorder, we start 
 * from the rightmost node which is the maximum value node. In normal inorder, we start from the left most node which is minimum value node. 
 * We add sum of current nodes in both traversals and compare this sum with given target sum. If the sum is same as target sum, we return true. 
 * If the sum is more than target sum, we move to next node in reverse inorder traversal, otherwise we move to next node in normal inorder traversal. 
 * If any of the traversals is finished without finding a pair, we return false. Following is C++ implementation of this approach.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q055_Find_a_pair_with_given_sum_in_a_Balanced_BST {

	public static void main(String[] args) {
		TreeNode root = Tree.tree9();
		boolean res = isPairPresent(root, 0);
		System.out.println(res);
	}
	
	static boolean isPairPresent(TreeNode root, int sum) {
		if (root == null) {
			return false;
		}
		
		// step 1. convert BST to DLL
		convertBSTtoDLL(root);
		
		// step 2. solve 2-sum
		return find2Sum(head, tail, sum);
	}
	
	static TreeNode head = null;
	static TreeNode tail = null;
	
	static void convertBSTtoDLL(TreeNode root) {
		if (root == null) {
			return;
		}
		
		convertBSTtoDLL(root.left);
		
		root.left = tail;
		if (tail != null) {
			tail.right = root;
		} else {
			head = root;
		}
		tail = root;
		
		convertBSTtoDLL(root.right);
	}
	
	static boolean find2Sum(TreeNode head, TreeNode tail, int sum) {
		while (head != tail) {
			int val = head.val + tail.val;
			if (val == sum) {
				return true;
			}
			
			if (val < sum) {
				head = head.right;
			} else {
				tail = tail.left;
			}
		}
		return false;
	}
	
}
