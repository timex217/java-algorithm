package problems.geeks.tree;

/**
 * Sum of all the numbers that are formed from root to leaf paths
 * 
 * http://www.geeksforgeeks.org/sum-numbers-formed-root-leaf-paths/
 * 
 * Given a binary tree, where every node value is a Digit from 1-9 .Find the sum of all the numbers which are formed from root to leaf paths.
 * 
 * For example consider the following Binary Tree.
 * 
 *                                           6
 *                                       /      \
 *                                     3          5
 *                                   /   \          \
 *                                  2     5          4  
 *                                       /   \
 *                                      7     4
 *                                      
 *   There are 4 leaves, hence 4 root to leaf paths:
 *   
 *    Path                    Number
 *    
 *   6->3->2                   632
 *   6->3->5->7                6357
 *   6->3->5->4                6354
 *   6->5->4                   654
 *      
 * Answer = 632 + 6357 + 6354 + 654 = 13997
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q070_Sum_of_all_the_numbers_that_are_formed_from_root_to_leaf_paths {

	public static void main(String[] args) {
		TreeNode root = Tree.tree13();
		int res = treePathSum(root, 0);
		System.out.println(res);
	}
	
	// ---------------------------
	//  Preorder
	// ---------------------------
	static int treePathSum(TreeNode root, int sum) {
		if (root == null) {
			return 0;
		}
		
		sum = sum * 10 + root.val;
		
		// 一旦遇到叶子节点，path结束
		if (root.left == null && root.right == null) {
			return sum;
		}
		
		return treePathSum(root.left, sum) + 
		       treePathSum(root.right, sum);
	}
	
}
