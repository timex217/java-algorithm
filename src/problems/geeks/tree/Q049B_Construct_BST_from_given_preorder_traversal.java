package problems.geeks.tree;

/**
 * Construct BST from given preorder traversal | Set 1
 * 
 * http://www.geeksforgeeks.org/construct-bst-from-given-preorder-traversa/
 * 
 * Construct BST from given preorder traversal | Set 1
 * Given preorder traversal of a binary search tree, construct the BST.
 * 
 * For example, if the given traversal is {10, 5, 1, 7, 40, 50}, then the output should be root of following tree.
 * 
 *      10
 *    /   \
 *   5     40
 *  /  \      \
 * 1    7      50
 * 
 * Method 2 ( O(n) time complexity )
 * The idea used here is inspired from method 3 of this post. The trick is to set a range {min .. max} for every node. 
 * 
 * Initialize the range as {INT_MIN .. INT_MAX}. The first node will definitely be in range, so create root node. 
 * To construct the left subtree, set the range as {INT_MIN �root->data}. If a values is in the range {INT_MIN .. root->data}, 
 * the values is part part of left subtree. To construct the right subtree, set the range as {root->data..max .. INT_MAX}.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q049B_Construct_BST_from_given_preorder_traversal {

	public static void main(String[] args) {
		int[] pre = {10, 5, 1, 7, 40, 50};
		TreeNode root = constructBST(pre);
		root.print();
	}
	
	// --------------------------
	//  Preorder construction
	// --------------------------
	static int index = 0;
	
	static TreeNode constructBST(int[] pre) {
		return helper(pre, Integer.MIN_VALUE, Integer.MAX_VALUE);
	}
	
	static TreeNode helper(int[] pre, int min, int max) {
		// base case
		if (index >= pre.length) {
			return null;
		}
		
		int key = pre[index];
		
		if (key <= min || key >= max) {
			return null;
		}
		
		TreeNode root = new TreeNode(key);
		
		index++;
		
		root.left  = helper(pre, min, key);
		root.right = helper(pre, key, max);
		
		return root;
	}
	
}
