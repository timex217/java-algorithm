package problems.geeks.tree;

/**
 * Convert a Binary Tree into its Mirror Tree
 * 
 * http://www.geeksforgeeks.org/write-an-efficient-c-function-to-convert-a-tree-into-its-mirror-tree/
 * 
 * Mirror of a Tree: Mirror of a Binary Tree T is another Binary Tree M(T) with left and right children of all non-leaf nodes interchanged.
 * 
 * Algorithm - Mirror(tree):
 * 
 * (1)  Call Mirror for left-subtree    i.e., Mirror(left-subtree)
 * (2)  Call Mirror for right-subtree  i.e., Mirror(left-subtree)
 * (3)  Swap left and right subtrees.
 *           temp = left-subtree
 *           left-subtree = right-subtree
 *           right-subtree = temp
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q006_Convert_a_Binary_Tree_into_its_Mirror_Tree {

	public static void main(String[] args) {
		TreeNode root = Tree.tree1();
		mirror(root);
		root.print();
	}
	
	/* Change a tree so that the roles of the  left and 
	    right pointers are swapped at every node.
	 
	 So the tree...
	       4
	      / \
	     2   5
	    / \
	   1   3
	 
	 is changed to...
	       4
	      / \
	     5   2
	        / \
	       3   1
	*/
	static void mirror(TreeNode root) {
		if (root == null) {
			return;
		}
		
		/* do the subtrees */
		mirror(root.left);
		mirror(root.right);
		
		/* swap the pointers in this node */
		TreeNode tmp = root.left;
		root.left = root.right;
		root.right = tmp;
	}
	
}
