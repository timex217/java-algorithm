package problems.geeks.tree;

/**
 * Print all nodes at distance k from a given node
 * 
 * http://www.geeksforgeeks.org/print-nodes-distance-k-given-node-binary-tree/
 * 
 * Given a binary tree, a target node in the binary tree, and an integer value k, print all the nodes that are at distance k from the given target node. 
 * No parent pointers are available.
 * 
 *              1                   Input: target = pointer to node with data 3.
 *           /     \                       root = pointer to node with data 1.
 *          2       3                      k = 2.
 *         / \     / \              
 *        4   5   6   7             Output: 2, 8
 *                 \                
 *                  8
 *                  
 * There are two types of nodes to be considered.
 * 1) Nodes in the subtree rooted with target node. For example if the target node is 8 and k is 2, then such nodes are 10 and 14.
 * 2) Other nodes, may be an ancestor of target, or a node in some other subtree. For target node 8 and k is 2, the node 22 comes in this category.
 * 
 * Finding the first type of nodes is easy to implement. Just traverse subtrees rooted with the target node and decrement k in recursive call. 
 * When the k becomes 0, print the node currently being traversed (See this for more details). Here we call the function as printkdistanceNodeDown().
 * 
 * How to find nodes of second type? For the output nodes not lying in the subtree with the target node as the root, we must go through all ancestors. 
 * For every ancestor, we find its distance from target node, let the distance be d, now we go to other subtree (if target was found in left subtree, 
 * then we go to right subtree and vice versa) of the ancestor and find all nodes at k-d distance from the ancestor.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q073B_Print_all_nodes_at_distance_k_from_a_given_node {

	public static void main(String[] args) {
		TreeNode root = Tree.tree15();
		
		// step 1. find the node
		TreeNode node = findNode(root, 3);
		//System.out.println(node.val);
				
		kDistFromNode(root, node, 2);
	}
	
	// Prints all nodes at distance k from a given target node.
	// The k distant nodes may be upward or downward.  This function
	// Returns distance of root from target node, it returns -1 if target
	// node is not present in tree rooted with root.
	static int kDistFromNode(TreeNode root, TreeNode node, int k) {
		// base case, empty tree, return -1
		if (root == null) {
			return -1;
		}
		
		// If target is same as root.  Use the downward function
	    // to print all nodes at distance k in subtree rooted with
	    // target or root
		if (root == node) {
			kDistDown(node, k);
			return 0;
		}
		
		// Recur for left subtree
		int dl = kDistFromNode(root.left, node, k);
		
		// Check if target node was found in left subtree
		if (dl != -1) {
			// If root is at distance k from target, print root
	        // Note that dl is Distance of root's left child from target
			if (dl + 1 == k) {
				System.out.format("%d ", root.val);
			}
			
			// Else go to right subtree and print all k-dl-2 distant nodes
	        // Note that the right child is 2 edges away from left child
			else {
				kDistDown(root.right, k - dl - 2);
			}
			
			// Add 1 to the distance and return value for parent calls
			return dl + 1;
		}
		
		// MIRROR OF ABOVE CODE FOR RIGHT SUBTREE
	    // Note that we reach here only when node was not found in left subtree
		int dr = kDistFromNode(root.right, node, k);
		
		if (dr != -1) {
			if (dr + 1 == k) {
				System.out.format("%d ", root.val);
			} else {
				kDistDown(root.left, k - dr - 2);
			}
			return dr + 1;
		}
		
		// If target was neither present in left nor in right subtree
		return -1;
	}
	
	/* Recursive function to print all the nodes at distance k in the
	   tree (or subtree) rooted with given root. */
	static void kDistDown(TreeNode node, int k) {
		if (node == null || k < 0) {
			return;
		}
		
		if (k == 0) {
			System.out.format("%d ", node.val);
			return;
		}
		
		kDistDown(node.left, k - 1);
		kDistDown(node.right, k - 1);
	}
	
	/**
	 * Find a node by its value
	 */
	static TreeNode findNode(TreeNode root, int v) {
		if (root == null) {
			return null;
		}
		
		if (root.val == v) {
			return root;
		}
		
		TreeNode left = findNode(root.left, v);
		TreeNode right = findNode(root.right, v);
		
		return (left != null) ? left : right;
	}
	
}
