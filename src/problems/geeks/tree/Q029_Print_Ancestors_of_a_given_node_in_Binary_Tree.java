package problems.geeks.tree;

/**
 * Print Ancestors of a given node in Binary Tree
 * 
 * http://www.geeksforgeeks.org/print-ancestors-of-a-given-node-in-binary-tree/
 * 
 * Given a Binary Tree and a key, write a function that prints all the ancestors of the key in the given binary tree.
 * 
 * For example, if the given tree is following Binary Tree and key is 10, then your function should print 12, 8 and 20.
 * 
 *              20
 *             /  \
 *            8    22
 *           / \
 *          4  12
 *             / \
 *           10   14
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q029_Print_Ancestors_of_a_given_node_in_Binary_Tree {

	public static void main(String[] args) {
		TreeNode root = Tree.bst2();
		printAncestorsR(root, 10);
	}
	
	// -----------------------------------
	//  Recursion
	// -----------------------------------
	
	/**
	 * 思路就是从某个节点的左子树和右子树分别去找值为key的节点，
	 * 如果找到了，那么说明这个节点就是其中的一个ancestor，将其打印出来
	 */
	static boolean printAncestorsR(TreeNode root, int key) {
		if (root == null) {
			return false;
		}
		
		if (root.val == key) {
			return true;
		}
		
		if (printAncestorsR(root.left, key) || printAncestorsR(root.right, key)) {
			System.out.format("%d ", root.val);
			return true;
		}
		
		return false;
	}
	
	// -----------------------------------
	//  Iterative
	// -----------------------------------
	
	
	
}
