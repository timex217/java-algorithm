package problems.geeks.tree;

/**
 * Construct BST from given preorder traversal
 * 
 * http://www.geeksforgeeks.org/construct-bst-from-given-preorder-traversal-set-2/
 * 
 * Given preorder traversal of a binary search tree, construct the BST.
 * 
 * For example, if the given traversal is {10, 5, 1, 7, 40, 50}, then the output should be root of following tree.
 * 
 *      10
 *    /   \
 *   5     40
 *  /  \      \
 * 1    7      50  
 * We have discussed O(n^2) and O(n) recursive solutions in the previous post. Following is a stack based iterative solution that works in O(n) time.
 * 
 * 1. Create an empty stack.
 * 
 * 2. Make the first value as root. Push it to the stack.
 * 
 * 3. Keep on popping while the stack is not empty and the next value is greater than stack�s top value. Make this value as the right child of the last popped node. Push the new node to the stack.
 * 
 * 4. If the next value is less than the stack�s top value, make this value as the left child of the stack�s top node. Push the new node to the stack.
 * 
 * 5. Repeat steps 2 and 3 until there are items remaining in pre[].
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q049C_Construct_BST_from_given_preorder_traversal {

	public static void main(String[] args) {
		int[] pre = {10, 5, 1, 7, 40, 50};
		TreeNode root = constructBST(pre);
		root.print();
	}
	
	static TreeNode constructBST(int[] pre) {
		if (pre == null || pre.length == 0) {
			return null;
		}
		
		// use a stack to help construct the BST
		Stack<TreeNode> stack = new Stack<TreeNode>();
		
		TreeNode root = new TreeNode(pre[0]);
		
		stack.push(root);
		
		for (int i = 1; i < pre.length; i++) {
			int key = pre[i];
			TreeNode node = new TreeNode(key);
			TreeNode tmp = null;
			
			/* Keep on popping while the next value is greater than
	           stack's top value. */
			while (!stack.isEmpty() && stack.peek().val < key) {
				tmp = stack.pop();
			}
			
			// If the next value is less than the stack's top value, make this value
	        // as the left child of the stack's top node. Push the new node to stack
			if (tmp == null) {
				stack.peek().left = node;
			} 
			// Make this greater value as the right child and push it to the stack
			else {
				tmp.right = node;
			}
			
			stack.push(node);
		}
		
		return root;
	}
	
}
