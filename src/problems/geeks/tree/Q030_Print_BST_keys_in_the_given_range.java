package problems.geeks.tree;

/**
 * Print BST keys in the given range
 * 
 * http://www.geeksforgeeks.org/print-bst-keys-in-the-given-range/
 * 
 * Given two values k1 and k2 (where k1 < k2) and a root pointer to a Binary Search Tree. 
 * Print all the keys of tree in range k1 to k2. i.e. print all x such that k1<=x<=k2 and x is a key of given BST. Print all the keys in increasing order.
 * 
 * For example, if k1 = 10 and k2 = 22, then your function should print 10, 12, 14, 20 and 22.
 * 
 *              20
 *             /  \
 *            8    22
 *           / \
 *          4  12
 *             / \
 *           10   14     
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q030_Print_BST_keys_in_the_given_range {

	public static void main(String[] args) {
		TreeNode root = Tree.bst2();
		print(root, 10, 22);
	}
	
	static void print(TreeNode root, int k1, int k2) {
		if (root == null) {
			return;
		}
		
		print(root.left, k1, k2);
		
		if (root.val >= k1 && root.val <= k2) {
			System.out.format("%d ", root.val);
		}
		
		print(root.right, k1, k2);
	}
	
}
