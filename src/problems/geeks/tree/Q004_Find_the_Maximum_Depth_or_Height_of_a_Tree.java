package problems.geeks.tree;

/**
 * Find the Maximum Depth or Height of a Tree
 * 
 * http://www.geeksforgeeks.org/write-a-c-program-to-find-the-maximum-depth-or-height-of-a-tree/
 * 
 * Recursively calculate height of left and right subtrees of a node and assign height to the node as max of the heights of two children plus 1. See below pseudo code and program for details.
 * 
 * Algorithm:
 * 
 *  maxDepth()
 * 1. If tree is empty then return 0
 * 2. Else
 *      (a) Get the max depth of left subtree recursively  i.e., 
 *           call maxDepth( tree->left-subtree)
 *      (a) Get the max depth of right subtree recursively  i.e., 
 *           call maxDepth( tree->right-subtree)
 *      (c) Get the max of max depths of left and right 
 *           subtrees and add 1 to it for the current node.
 *          max_depth = max(max dept of left subtree,  
 *                              max depth of right subtree) 
 *                              + 1
 *      (d) Return max_depth
 * See the below diagram for more clarity about execution of the recursive function maxDepth() for above example tree.
 * 
 *             maxDepth('1') = max(maxDepth('2'), maxDepth('3')) + 1
 *                                = 2 + 1
 *                                   /    \
 *                                 /         \
 *                               /             \
 *                             /                 \
 *                           /                     \
 *                maxDepth('1')                  maxDepth('3') = 1
 * = max(maxDepth('4'), maxDepth('5')) + 1
 * = 1 + 1   = 2         
 *                    /    \
 *                  /        \
 *                /            \
 *              /                \
 *            /                    \
 *  maxDepth('4') = 1     maxDepth('5') = 1
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q004_Find_the_Maximum_Depth_or_Height_of_a_Tree {

	public static void main(String[] args) {
		TreeNode root = Tree.tree1();
		int height = maxDepth(root);
		System.out.println(height);
	}
	
	static int maxDepth(TreeNode root) {
		if (root == null) {
			return 0;
		}
		
		return 1 + Math.max(maxDepth(root.left), maxDepth(root.right));
	}
	
}
