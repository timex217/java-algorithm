package problems.geeks.tree;

/**
 * Tree Isomorphism Problem
 * 
 * http://www.geeksforgeeks.org/tree-isomorphism-problem/
 * 
 * Write a function to detect if two trees are isomorphic. Two trees are called isomorphic if one of them can be obtained from other by a series of flips, 
 * i.e. by swapping left and right children of a number of nodes. Any number of nodes at any level can have their children swapped. Two empty trees are isomorphic.
 * 
 * For example, following two trees are isomorphic with following sub-trees flipped: 2 and 3, NULL and 6, 7 and 8.
 * 
 *              1                 1
 *            /   \             /   \
 *           2     3           3     2
 *          / \    /            \   / \
 *         4   5  6              6 4   5 
 *            / \                     / \
 *           7   8                   8   7
 * 
 * We simultaneously traverse both trees. Let the current internal nodes of two trees being traversed be n1 and n2 respectively. 
 * There are following two conditions for subtrees rooted with n1 and n2 to be isomorphic.
 * 1) Data of n1 and n2 is same.
 * 2) One of the following two is true for children of n1 and n2
 * ��a) Left child of n1 is isomorphic to left child of n2 and right child of n1 is isomorphic to right child of n2.
 * ��b) Left child of n1 is isomorphic to right child of n2 and right child of n1 is isomorphic to left child of n2.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q058_Tree_Isomorphism_Problem {
	
	public static void main(String[] args) {
		
	}
	
	static boolean isIsomorphic(TreeNode r1, TreeNode r2) {
		// Both roots are NULL, trees isomorphic by definition
		if (r1 == null && r2 == null) {
			return true;
		}
		// Exactly one of the n1 and n2 is NULL, trees not isomorphic
		if (r1 == null || r2 == null) {
			return false;
		}
		
		if (r1.val != r2.val) {
			return false;
		}
		
		// There are two possible cases for n1 and n2 to be isomorphic
		// Case 1: The subtrees rooted at these nodes have NOT been "Flipped".
		// Both of these subtrees have to be isomorphic, hence the &&
		// Case 2: The subtrees rooted at these nodes have been "Flipped"
		return (isIsomorphic(r1.left, r2.left) && isIsomorphic(r1.right, r2.right)) ||
			   (isIsomorphic(r1.left, r2.right) && isIsomorphic(r1.right, r2.left));
	}
	
}
