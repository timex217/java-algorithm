package problems.geeks.tree;

/**
 * Count leaf nodes in a binary tree
 * 
 * http://www.geeksforgeeks.org/write-a-c-program-to-get-count-of-leaf-nodes-in-a-binary-tree/
 * 
 * A node is a leaf node if both left and right child nodes of it are NULL.
 * 
 * Here is an algorithm to get the leaf node count.
 * 
 * getLeafCount(node)
 * 1) If node is NULL then return 0.
 * 2) Else If left and right child nodes are NULL return 1.
 * 3) Else recursively calculate leaf count of the tree using below formula.
 *     Leaf count of a tree = Leaf count of left subtree + 
 *                                  Leaf count of right subtree
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q011_Count_leaf_nodes_in_a_binary_tree {

	public static void main(String[] args) {
		TreeNode root = Tree.bst2();
		int res = getLeafCount(root);
		System.out.println(res);
	}
	
	static int getLeafCount(TreeNode root) {
		if (root == null) {
			return 0;
		}
		
		if (root.left == null && root.right == null) {
			return 1;
		}
		
		return getLeafCount(root.left) + getLeafCount(root.right);
	}
	
}
