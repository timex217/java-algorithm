package problems.geeks.tree;

/**
 * Construct a tree from Inorder and Level order traversals
 * 
 * http://www.geeksforgeeks.org/construct-tree-inorder-level-order-traversals/
 * 
 * Given inorder and level-order traversals of a Binary Tree, construct the Binary Tree. Following is an example to illustrate the problem.
 * 
 *              20
 *             /  \
 *            8    22
 *           / \    \
 *          4  12    25
 *             / \
 *           10   14
 *           
 * Input: Two arrays that represent Inorder and level order traversals of a Binary Tree
 * 
 * in[] = {4, 8, 10, 12, 14, 20, 22, 25};
 * level[] = {20, 8, 22, 4, 12, 25, 10, 14};
 * 
 * Output: Construct the tree represented by the two arrays.
 *  
 * For the above two arrays, the constructed tree is shown in the diagram on right side
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q074_Construct_a_tree_from_Inorder_and_Level_order_traversals {

	public static void main(String[] args) {
		int[] in = {4, 8, 10, 12, 14, 20, 22, 25};
		int[] level = {20, 8, 22, 4, 12, 25, 10, 14};
		TreeNode root = constructTree(in, level, 8);
		root.print();
	}
	
	static TreeNode constructTree(int[] in, int[] level, int n) {
		// use a hashmap to store the index of in[]
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (int i = 0; i < n; i++) {
			map.put(in[i], i);
		}
		
		return helper(in, 0, n - 1, level, n, map);
	}
	
	static TreeNode helper(int[] in, int i, int i2, int[] level, int n, Map<Integer, Integer> map) {
		// base case
		if (i > i2 || n <= 0) {
			return null;
		}
		
		// root node
		int val = level[0];
		TreeNode root = new TreeNode(val);
		
		int index = map.get(val);
		int n_left = index - i;
		int n_right = n - n_left - 1;
		
		int[] left = extract(level, i, index - 1, map);
		int[] right = extract(level, index + 1, i2, map);
		
		root.left = helper(in, i, index - 1, left, n_left, map);
		root.right = helper(in, index + 1, i2, right, n_right, map);
		
		return root;
	}
	
	static int[] extract(int[] level, int i, int i2, Map<Integer, Integer> map) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		
		for (int k = 0; k < level.length; k++) {
			int val = level[k];
			int index = map.get(val);
			
			if (index >= i && index <= i2) {
				list.add(val);
			}
		}
		
		int[] res = new int[list.size()];
		for (int k = 0; k < list.size(); k++) {
			res[k] = list.get(k);
		}
		
		return res;
	}
	
}
