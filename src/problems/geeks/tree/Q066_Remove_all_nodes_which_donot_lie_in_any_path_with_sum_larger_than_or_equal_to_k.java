package problems.geeks.tree;

/**
 * Remove all nodes which don’t lie in any path with sum>= k
 * 
 * http://www.geeksforgeeks.org/remove-all-nodes-which-lie-on-a-path-having-sum-less-than-k/
 * 
 * Given a binary tree, a complete path is defined as a path from root to a leaf. The sum of all nodes on that path is defined as the sum of that path. 
 * Given a number K, you have to remove (prune the tree) all nodes which don’t lie in any path with sum>=k.
 * 
 * Note: A node can be part of multiple paths. So we have to delete it only in case when all paths from it have sum less than K.
 * 
 * Consider the following Binary Tree
 * 
 *           1 
 *       /      \
 *      2        3
 *    /   \     /  \
 *   4     5   6    7
 *  / \    /       /
 * 8   9  12      10
 *    / \           \
 *   13  14         11
 *       / 
 *      15 
 * 
 * For input k = 20, the tree should be changed to following
 * (Nodes with values 6 and 8 are deleted)
 * 
 *           1 
 *       /      \
 *      2        3
 *    /   \        \
 *   4     5        7
 *    \    /       /
 *     9  12      10
 *    / \           \
 *   13  14         11
 *       / 
 *      15 
 * 
 * For input k = 45, the tree should be changed to following.
 * 
 *       1 
 *     / 
 *    2   
 *   / 
 *  4  
 *   \   
 *    9    
 *     \   
 *      14 
 *      /
 *     15 
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q066_Remove_all_nodes_which_donot_lie_in_any_path_with_sum_larger_than_or_equal_to_k {

	public static void main(String[] args) {
		TreeNode root = Tree.tree10();
		root = prune(root, 45, 0);
		root.print();
	}
	
	// -------------------------
	//  Postorder
	// -------------------------
	static TreeNode prune(TreeNode root, int k, int sum) {
		if (root == null) {
			return null;
		}
		
		// 将 path sum 由上至下传递到叶子节点
		sum += root.val;
		
		// 先清理左子树，再清理右子树
		root.left = prune(root.left, k, sum);
		root.right = prune(root.right, k, sum);
		
		// 最后清理自己
		if (root.left == null && root.right == null && sum < k) {
			return null;
		}
		
		return root;
	}
	
}
