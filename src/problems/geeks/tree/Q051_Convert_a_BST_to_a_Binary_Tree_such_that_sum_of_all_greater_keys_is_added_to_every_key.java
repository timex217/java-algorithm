package problems.geeks.tree;

/**
 * Convert a BST to a Binary Tree such that sum of all greater keys is added to every key
 * 
 * http://www.geeksforgeeks.org/convert-bst-to-a-binary-tree/
 * 
 * Given a Binary Search Tree (BST), convert it to a Binary Tree such that every key of the original BST is changed to key plus sum of all greater keys in BST.
 * 
 * Examples:
 * 
 * Input: Root of following BST
 *               5
 *             /   \
 *            2     13
 * 
 * Output: The given BST is converted to following Binary Tree
 *               18
 *             /   \
 *           20     13
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q051_Convert_a_BST_to_a_Binary_Tree_such_that_sum_of_all_greater_keys_is_added_to_every_key {

	public static void main(String[] args) {
		TreeNode root = Tree.bst1();
		convertBST(root);
		root.print();
	}
	
	static void convertBST(TreeNode root) {
		// step 1. reverse inorder traversal
		Deque<Integer> deque = new ArrayDeque<Integer>();
		reverseInorderStore(root, deque);
		
		// step 2. reverse inorder traversal again and update the tree
		reverseInorderUpdate(root, deque);
	}
	
	static void reverseInorderStore(TreeNode node, Deque<Integer> deque) {
		if (node == null) {
			return;
		}
		
		reverseInorderStore(node.right, deque);
		
		if (deque.isEmpty()) {
			deque.add(node.val);
		} else {
			deque.add(deque.peekLast() + node.val);
		}
		
		reverseInorderStore(node.left, deque);
	}
	
	static void reverseInorderUpdate(TreeNode node, Deque<Integer> deque) {
		if (node == null) {
			return;
		}
		
		reverseInorderUpdate(node.right, deque);
		
		node.val = deque.pollFirst();
		
		reverseInorderUpdate(node.left, deque);
	}
	
}
