package problems.geeks.tree;

/**
 * Level order traversal in spiral form
 * 
 * http://www.geeksforgeeks.org/level-order-traversal-in-spiral-form/
 *
 * Example:
 *               1
 *             /   \
 *           2      3
 *          / \    / \
 *         4   5  6   7
 *         
 * should print: 1 3 2 4 5 6 7
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q013_Level_order_traversal_in_spiral_form {

	public static void main(String[] args) {
		TreeNode root = Tree.tree1();
		zigzagPrint(root);
	}
	
	static void zigzagPrint(TreeNode root) {
		if (root == null) {
			return;
		}
		
		Stack<TreeNode> curr = new Stack<TreeNode>();
		Stack<TreeNode> next = new Stack<TreeNode>();
		
		boolean leftToRight = true;
		
		curr.push(root);
		
		while (!curr.isEmpty()) {
			TreeNode node = curr.pop();
			System.out.format("%d ", node.val);
			
			if (leftToRight) {
				if (node.left != null) {
					next.push(node.left);
				}
				
				if (node.right != null) {
					next.push(node.right);
				}
			} else {
				if (node.right != null) {
					next.push(node.right);
				}
				
				if (node.left != null) {
					next.push(node.left);
				}
			}
			
			if (curr.isEmpty()) {
				leftToRight = !leftToRight;
				
				// swap stacks
				Stack<TreeNode> tmp = curr;
				curr = next;
				next = tmp;
			}
		}
	}
	
}
