package problems.geeks.tree;

/**
 * Boundary Traversal of binary tree
 * 
 * http://www.geeksforgeeks.org/boundary-traversal-of-binary-tree/
 * 
 * Given a binary tree, print boundary nodes of the binary tree Anti-Clockwise starting from the root. For example, 
 * boundary traversal of the following tree is “20 8 4 10 14 25 22″
 * 
 *              20
 *             /  \
 *            8    22
 *           / \    \
 *          4  12    25
 *             / \
 *           10   14
 *           
 * We break the problem in 3 parts:
 * 1. Print the left boundary in top-down manner.
 * 2. Print all leaf nodes from left to right, which can again be sub-divided into two sub-parts:
 * …..2.1 Print all leaf nodes of left sub-tree from left to right.
 * …..2.2 Print all leaf nodes of right subtree from left to right.
 * 3. Print the right boundary in bottom-up manner.
 * 
 * We need to take care of one thing that nodes are not printed again. e.g. The left most node is also the leaf node of the tree.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q047_Boundary_Traversal_of_binary_tree {

	public static void main(String[] args) {
		TreeNode root = Tree.bst2();
		printBoundary(root);
	}
	
	static void printBoundary(TreeNode root) {
		if (root == null) {
			return;
		}
		
		System.out.format("%d ", root.val);
		
		// Print the left boundary in top-down manner.
        printBoundaryLeft(root.left);
        
        // Print all leaf nodes
        printLeaves(root.left);
        printLeaves(root.right);
        
        // Print the right boundary in bottom-up manner
        printBoundaryRight(root.right);
	}
	
	// ----------------------------------------------------------------
	// A function to print all left boundry nodes, except a leaf node.
	// Print the nodes in TOP DOWN manner
	// ----------------------------------------------------------------
	static void printBoundaryLeft(TreeNode root) {
		if (root == null) {
			return;
		}
		
		if (root.left != null) {
			System.out.format("%d ", root.val);
			printBoundaryLeft(root.left);
		} else if (root.right != null) {
			System.out.format("%d ", root.val);
			printBoundaryLeft(root.right);
		} else {
			// do nothing if it is a leaf node, this way we avoid
			// duplicates in output
		}
	}
	
	// ----------------------------------------------------------------
	// A simple function to print leaf nodes of a binary tree
	// ----------------------------------------------------------------
	static void printLeaves(TreeNode root) {
		if (root == null) {
			return;
		}
		
		printLeaves(root.left);
		
		if (root.left == null && root.right == null) {
			System.out.format("%d ", root.val);
		}
		
		printLeaves(root.right);
	}
	
	// ----------------------------------------------------------------
	// A function to print all right boundry nodes, except a leaf node
	// Print the nodes in BOTTOM UP manner
	// ----------------------------------------------------------------
	static void printBoundaryRight(TreeNode root) {
		if (root == null) {
			return;
		}
		
		if (root.right != null) {
			// to ensure bottom up order, first call for right
            // subtree, then print this node
			printBoundaryRight(root.right);
			System.out.format("%d ", root.val);
		} else if (root.left != null) {
			printBoundaryRight(root.left);
			System.out.format("%d ", root.val);
		} else {
			// do nothing if it is a leaf node, this way we avoid
		    // duplicates in output
		}
		
	}
	
}
