package problems.geeks.tree;

/**
 * Construct Special Binary Tree from given Inorder traversal
 * 
 * http://www.geeksforgeeks.org/construct-binary-tree-from-inorder-traversal/
 * 
 * Given Inorder Traversal of a Special Binary Tree in which key of every node is greater than keys in left and right children, 
 * construct the Binary Tree and return root.
 * 
 * Examples:
 * 
 * Input: inorder[] = {5, 10, 40, 30, 28}
 * Output: root of following tree
 * 
 *          40
 *        /   \
 *       10     30
 *      /         \
 *     5          28 
 *     
 * The idea used in Construction of Tree from given Inorder and Preorder traversals can be used here. 
 * Let the given array is {1, 5, 10, 40, 30, 15, 28, 20}. The maximum element in given array must be root. 
 * The elements on left side of the maximum element are in left subtree and elements on right side are in right subtree.
 * 
 *          40
 *       /       \  
 *    {1,5,10}   {30,15,28,20}
 *    
 * We recursively follow above step for left and right subtrees, and finally get the following tree.
 * 
 *           40
 *         /   \
 *        10     30
 *       /         \
 *      5          28
 *     /          /  \
 *    1         15    20
 *    
 * Algorithm: buildTree()
 * 1) Find index of the maximum element in array. The maximum element must be root of Binary Tree.
 * 2) Create a new tree node ‘root’ with the data as the maximum value found in step 1.
 * 3) Call buildTree for elements before the maximum element and make the built tree as left subtree of ‘root’.
 * 5) Call buildTree for elements after the maximum element and make the built tree as right subtree of ‘root’.
 * 6) return ‘root’.
 * 
 * 思想很简单，每次都拿最大的作为root，那么左孩子和右孩子都会小于root.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q044_Construct_Special_Binary_Tree_from_given_Inorder_traversal {
	
	public static void main(String[] args) {
		int[] arr = {1, 5, 10, 40, 30, 15, 28, 20};
		TreeNode root = buildTree(arr, 0, arr.length - 1);
		root.print();
	}
	
	static TreeNode buildTree(int[] arr, int lo, int hi) {
		if (lo > hi) {
			return null;
		}
		
		// find the max element
		int max_index = lo;
		for (int i = lo + 1; i <= hi; i++) {
			if (arr[i] > arr[max_index]) {
				max_index = i;
			}
		}
		
		TreeNode root = new TreeNode(arr[max_index]);
		
		root.left = buildTree(arr, lo, max_index - 1);
		root.right = buildTree(arr, max_index + 1, hi);
		
		return root;
	}
	
}
