package problems.geeks.tree;

/**
 * Convert an arbitrary Binary Tree to a tree that holds Children Sum Property
 * 
 * http://www.geeksforgeeks.org/convert-an-arbitrary-binary-tree-to-a-tree-that-holds-children-sum-property/
 * 
 * Question: Given an arbitrary binary tree, convert it to a binary tree that holds Children Sum Property. 
 * You can only increment data values in any node (You cannot change structure of tree and cannot decrement value of any node).
 * 
 * For example, the below tree doesn�t hold the children sum property, convert it to a tree that holds the property.
 * 
 *              50
 *            /     \     
 *          /         \
 *        7             2
 *      / \             /\
 *    /     \          /   \
 *   3        5      1      30
 *   
 * Algorithm:
 * Traverse given tree in post order to convert it, i.e., first change left and right children to hold the children sum property then change the parent node.
 * 
 * Let difference between node�s data and children sum be diff.
 * 
 *      diff = node�s children sum - node�s data  
 *      
 * If diff is 0 then nothing needs to be done.
 * 
 * If diff > 0 ( node�s data is smaller than node�s children sum) increment the node�s data by diff.
 * 
 * If diff < 0 (node�s data is greater than the node's children sum) then increment one child�s data. We can choose to increment either left or right child 
 * if they both are not NULL. Let us always first increment the left child. Incrementing a child changes the subtree�s children sum property 
 * so we need to change left subtree also. So we recursively increment the left child. If left child is empty then we recursively call increment() for right child.
 * 
 * Let us run the algorithm for the given example.
 * 
 * First convert the left subtree (increment 7 to 8).
 * 
 *              50
 *            /     \     
 *          /         \
 *        8             2
 *      / \             /\
 *    /     \          /   \
 *   3        5      1      30
 * Then convert the right subtree (increment 2 to 31)
 * 
 *           50
 *         /    \     
 *       /        \
 *     8            31
 *    / \           / \
 *  /     \       /     \
 * 3       5    1       30
 * Now convert the root, we have to increment left subtree for converting the root.
 * 
 *           50
 *         /    \     
 *       /        \
 *     19           31
 *    / \           /  \
 *  /     \       /      \
 * 14      5     1       30
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q015_Convert_an_arbitrary_Binary_Tree_to_a_tree_that_holds_Children_Sum_Property {

	public static void main(String[] args) {
		TreeNode root = Tree.tree3();
		convertTree(root);
		
		root.print();
	}
	
	static void convertTree(TreeNode root) {
		// base case
		if (root == null) {
			return;
		}
		
		convertTree(root.left);
		convertTree(root.right);
		
		int left = (root.left == null) ? 0 : root.left.val;
		int right = (root.right == null) ? 0 : root.right.val;
		int diff = root.val - (left + right);
		
		if (diff < 0) {
			root.val = left + right;
		}
		
		if (diff > 0) {
			incrementChild(root, diff);
		}
		
	}
	
	static void incrementChild(TreeNode root, int diff) {
		if (root == null) {
			return;
		}
		
		if (root.left != null) {
			root.left.val += diff;
			incrementChild(root.left, diff);
			return;
		} 
		
		if (root.right != null) {
			root.right.val += diff;
			incrementChild(root.right, diff);
			return;
		}
	}
	
}
