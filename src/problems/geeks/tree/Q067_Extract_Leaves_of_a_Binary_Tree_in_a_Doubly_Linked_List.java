package problems.geeks.tree;

/**
 * Extract Leaves of a Binary Tree in a Doubly Linked List
 * 
 * http://www.geeksforgeeks.org/connect-leaves-doubly-linked-list/
 * 
 * Given a Binary Tree, extract all leaves of it in a Doubly Linked List (DLL). Note that the DLL need to be created in-place. Assume that the node structure of DLL and Binary Tree is same, only the meaning of left and right pointers are different. In DLL, left means previous pointer and right means next pointer.
 * 
 * Let the following be input binary tree
 *         1
 *      /     \
 *     2       3
 *    / \       \
 *   4   5       6
 *  / \         / \
 * 7   8       9   10
 * 
 * 
 * Output:
 * Doubly Linked List
 * 7<->8<->5<->9<->10
 * 
 * Modified Tree:
 *         1
 *      /     \
 *     2       3
 *    /         \
 *   4           6
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q067_Extract_Leaves_of_a_Binary_Tree_in_a_Doubly_Linked_List {

	public static void main(String[] args) {
		TreeNode root = Tree.tree11();
		root = extractLeafList(root);
		root.print();
	}
	
	// ---------------------------
	//  Reverse preorder
	//
	//  We cannot do it in postorder, otherwise, all nodes will be removed!
	// ---------------------------
	
	static TreeNode head = null;
	
	static TreeNode extractLeafList(TreeNode root) {
		if (root == null) {
			return null;
		}
		
		// handle current node first
		if (root.left == null && root.right == null) {
			root.right = head;
			if (head != null) {
				head.left = root;
			}
			head = root;
			
			return null;
		}
		
		root.right = extractLeafList(root.right);
		root.left = extractLeafList(root.left);
		
		return root;
	}
	
}
