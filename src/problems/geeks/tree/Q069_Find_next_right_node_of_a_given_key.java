package problems.geeks.tree;

/**
 * Find next right node of a given key
 * 
 * http://www.geeksforgeeks.org/find-next-right-node-of-a-given-key/
 * 
 * Given a Binary tree and a key in the binary tree, find the node right to the given key. 
 * If there is no node on right side, then return NULL. Expected time complexity is O(n) where n is the number of nodes in the given binary tree.
 * 
 * For example, consider the following Binary Tree. Output for 5 is 15, output for 8 is 12. Output for 10, 15 and 17 is NULL.
 * 
 *               10
 *             /    \
 *           5       15
 *          / \     /  \
 *         1   8   12  17
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q069_Find_next_right_node_of_a_given_key {

	public static void main(String[] args) {
		TreeNode root = Tree.bst1();
		TreeNode node = getNextRight(root, 8);
		if (node != null) {
			System.out.format("%d\n", node.val);
		} else {
			System.out.println("NULL");
		}
	}
	
	static TreeNode getNextRight(TreeNode root, int key) {
		if (root == null) {
			return null;
		}
		
		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		queue.add(root);
		queue.add(null);
		
		TreeNode prev = null;
		
		while (!queue.isEmpty()) {
			TreeNode node = queue.poll();
			
			if (node != null) {
				if (node.val == key) {
					return prev;
				}
				
				// update previous node
				prev = node;
				
				if (node.right != null) {
					queue.add(node.right);
				}
				
				if (node.left != null) {
					queue.add(node.left);
				}
			} else {
				prev = null;
				
				if (!queue.isEmpty()) {
					queue.add(null);
				}
			}
		}
		
		// cannot find the key from tree
		return null;
	}
	
}
