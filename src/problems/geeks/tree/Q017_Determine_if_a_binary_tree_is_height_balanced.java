package problems.geeks.tree;

/**
 * Determine if a binary tree is height-balanced
 * 
 * http://www.geeksforgeeks.org/how-to-determine-if-a-binary-tree-is-balanced/
 * 
 * A tree where no leaf is much farther away from the root than any other leaf. 
 * Different balancing schemes allow different definitions of �much farther� and different amounts of work to keep them balanced.
 * 
 * Consider a height-balancing scheme where following conditions should be checked to determine if a binary tree is balanced.
 * An empty tree is height-balanced. A non-empty binary tree T is balanced if:
 * 1) Left subtree of T is balanced
 * 2) Right subtree of T is balanced
 * 3) The difference between heights of left subtree and right subtree is not more than 1.
 * 
 * The above height-balancing scheme is used in AVL trees. The diagram below shows two trees, one of them is height-balanced and other is not. 
 * The second tree is not height-balanced because height of left subtree is 2 more than height of right subtree.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q017_Determine_if_a_binary_tree_is_height_balanced {

	public static void main(String[] args) {
		TreeNode root = Tree.tree2();
		boolean res = isBalanced(root);
		System.out.println(res);
	}
	
	static boolean isBalanced(TreeNode root) {
		return height(root) != -1;
	}
	
	static int height(TreeNode root) {
		if (root == null) {
			return 0;
		}
		
		int left = height(root.left);
		int right = height(root.right);
		
		if (left == -1 || right == -1) {
			return -1;
		}
		
		if (Math.abs(right - left) > 1) {
			return -1;
		}
		
		return 1 + Math.max(left, right);
	}
	
}
