package problems.geeks.tree;

/**
 * Find depth of the deepest odd level leaf node
 * 
 * http://www.geeksforgeeks.org/find-depth-of-the-deepest-odd-level-node/
 * 
 * Write a C code to get the depth of the deepest odd level leaf node in a binary tree. Consider that level starts with 1. Depth of a leaf node is number of nodes on the path from root to leaf (including both leaf and root).
 * 
 * For example, consider the following tree. The deepest odd level node is the node with value 9 and depth of this node is 5.
 * 
 *        1
 *      /   \
 *     2     3
 *   /      /  \  
 *  4      5    6
 *         \     \
 *          7     8
 *         /       \
 *        9         10
 *                  /
 *                 11
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q063_Find_depth_of_the_deepest_odd_level_leaf_node {

	public static void main(String[] args) {
		/**
		 *        50
		 *      /    \
		 *   30       60
		 *  /  \     /  \ 
		 * 5   20   45    70
		 *               /  \
		 *             65    80
		 *            / \
		 *           90  17 
		 */
		TreeNode root = Tree.tree6();
		findDepth(root, 1);
		System.out.println(max);
	}
	
	static int max = 0;
	
	static void findDepth(TreeNode root, int level) {
		if (root == null) {
			return;
		}
		
		if (root.left == null && root.right == null && level % 2 == 1) {
			max = Math.max(max, level);
			return;
		}
		
		findDepth(root.left, level + 1);
		findDepth(root.right, level + 1);
	}
	
}
