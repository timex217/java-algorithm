package problems.geeks.tree;

/**
 * Merge two BSTs with limited extra space
 * 
 * http://www.geeksforgeeks.org/merge-two-bsts-with-limited-extra-space/
 * 
 * Given two Binary Search Trees(BST), print the elements of both BSTs in sorted form. 
 * The expected time complexity is O(m+n) where m is the number of nodes in first tree and n is the number of nodes in second tree. 
 * Maximum allowed auxiliary space is O(height of the first tree + height of the second tree).
 * 
 * Examples:
 * 
 * First BST 
 *        3
 *     /     \
 *    1       5
 * Second BST
 *     4
 *   /   \
 * 2       6
 * Output: 1 2 3 4 5 6
 * 
 * 
 * First BST 
 *           8
 *          / \
 *         2   10
 *        /
 *       1
 * Second BST 
 *           5
 *          / 
 *         3  
 *        /
 *       0
 * Output: 0 1 2 3 5 8 10 
 * Source: Google interview question
 * 
 * A similar question has been discussed earlier. Let us first discuss already discussed methods of the previous post which was for Balanced BSTs. 
 * The method 1 can be applied here also, but the time complexity will be O(n^2) in worst case. 
 * The method 2 can also be applied here, but the extra space required will be O(n) which violates the constraint given in this question. 
 * Method 3 can be applied here but the step 3 of method 3 can’t be done in O(n) for an unbalanced BST.
 * 
 * The idea is to use iterative inorder traversal. 
 * 
 * We use two auxiliary stacks for two BSTs. Since we need to print the elements in sorted form, 
 * whenever we get a smaller element from any of the trees, we print it. 
 * If the element is greater, then we push it back to stack for the next iteration.
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q041B_Merge_two_BSTs_with_limited_extra_space {

	public static void main(String[] args) {
		TreeNode r1 = Tree.bst1();
		TreeNode r2 = Tree.bst2();
		merge(r1, r2);
	}
	
	static void merge(TreeNode r1, TreeNode r2) {
		// base cases
		if (r1 == null) {
			inorder(r2);
			return;
		}
		
		if (r2 == null) {
			inorder(r1);
			return;
		}
		
		Stack<TreeNode> s1 = new Stack<TreeNode>();
		Stack<TreeNode> s2 = new Stack<TreeNode>();
		
		TreeNode curr1 = r1;
		TreeNode curr2 = r2;
		
		// Run the loop while there are nodes not yet printed.
	    // The nodes may be in stack(explored, but not printed)
	    // or may be not yet explored
		while (!s1.isEmpty() || !s2.isEmpty() || curr1 != null || curr2 != null) {
			if (curr1 != null || curr2 != null) {
				// Reach the leftmost node of both BSTs and push ancestors of
	            // leftmost nodes to stack s1 and s2 respectively
				if (curr1 != null) {
					s1.push(curr1);
					curr1 = curr1.left;
				}
				
				if (curr2 != null) {
					s2.push(curr2);
					curr2 = curr2.left;
				}
			} else {
				// If we reach a NULL node and either of the stacks is empty,
	            // then one tree is exhausted, ptint the other tree
				if (s1.isEmpty()) {
					while (!s2.isEmpty()) {
						curr2 = s2.pop();
						curr2.left = null;
						inorder(curr2);
					}
					return;
				}
				
				if (s2.isEmpty()) {
					while (!s1.isEmpty()) {
						curr1  = s1.pop();
						curr1.left = null;
						inorder(curr1);
					}
					return;
				}
				
				// Pop an element from both stacks and compare the
	            // popped elements
				curr1 = s1.pop();
				curr2 = s2.pop();
				
				// If element of first tree is smaller, then print it
	            // and push the right subtree. If the element is larger,
	            // then we push it back to the corresponding stack.
				if (curr1.val < curr2.val) {
					print(curr1.val);
					curr1 = curr1.right;
					s2.push(curr2);
					curr2 = null;
				} else {
					print(curr2.val);
					curr2 = curr2.right;
					s1.push(curr1);
					curr1 = null;
				}
			}
			
		}
	}
	
	static void inorder(TreeNode node) {
		if (node == null) {
			return;
		}
		
		inorder(node.left);
		print(node.val);
		inorder(node.right);
	}
	
	static void print(int x) {
		System.out.format("%d ", x);
	}
	
}
