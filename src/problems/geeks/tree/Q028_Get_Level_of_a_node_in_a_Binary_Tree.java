package problems.geeks.tree;

/**
 * Get Level of a node in a Binary Tree
 * 
 * http://www.geeksforgeeks.org/get-level-of-a-node-in-a-binary-tree/
 * 
 * Given a Binary Tree and a key, write a function that returns level of the key.
 * 
 * For example, consider the following tree. If the input key is 3, then your function should return 1. 
 * If the input key is 4, then your function should return 3. And for key which is not present in key, then your function should return 0.
 * 
 *            3
 *           / \
 *          2   5
 *         / \
 *        1   4
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q028_Get_Level_of_a_node_in_a_Binary_Tree {

	public static void main(String[] args) {
		TreeNode root = Tree.bst2();
		int level = getLevel(root, 10);
		System.out.println(level);
	}
	
	static int getLevel(TreeNode root, int key) {
		return helper(root, key, 1);
	}
	
	static int helper(TreeNode root, int key, int level) {
		if (root == null) {
			return -1;
		}
		
		if (root.val == key) {
			return level;
		}
		
		int left = helper(root.left, key, level + 1);
		int right = helper(root.right, key, level + 1);
		
		return (left != -1) ? left : right;
	}
	
}
