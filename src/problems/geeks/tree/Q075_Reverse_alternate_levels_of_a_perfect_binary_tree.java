package problems.geeks.tree;

/**
 * Reverse alternate levels of a perfect binary tree
 * 
 * Given a Perfect Binary Tree, reverse the alternate level nodes of the binary tree.
 * 
 *   
 * Given tree: 
 *                     1
 *               /           \
 *              2             3
 *            /   \         /   \
 *           4     5       6     7
 *          / \   / \     / \   / \
 *         8   9 10 11   12 13 14 15
 * 
 * Modified tree:
 * 
 *                     1
 *               /           \
 *              3             2
 *            /   \         /   \
 *           4     5       6     7
 *          / \   / \     / \   / \
 *         15 14 13 12   11 10 9   8
 */

import java.util.*;
import adt.tree.Tree;
import adt.tree.TreeNode;

public class Q075_Reverse_alternate_levels_of_a_perfect_binary_tree {

	public static void main(String[] args) {
		TreeNode root = Tree.tree16();
		reverseAlternateLevels(root);
		root.print();
	}
	
	static void reverseAlternateLevels(TreeNode root) {
		if (root == null) {
			return;
		}
		
		int level = 0;
		
		Deque<TreeNode> curr = new ArrayDeque<TreeNode>();
		Deque<TreeNode> next = new ArrayDeque<TreeNode>();
		
		curr.add(root);
		
		ArrayList<Integer> list = new ArrayList<Integer>();
		
		// --------------------------------
		//  1st level order: store values
		// --------------------------------
		while (!curr.isEmpty()) {
			TreeNode node = curr.pollLast();
			list.add(node.val);
			
			if (level % 2 == 0) {
				if (node.left != null) {
					next.add(node.left);
				}
				if (node.right != null) {
					next.add(node.right);
				}
			} else {
				if (node.right != null) {
					next.add(node.right);
				}
				if (node.left != null) {
					next.add(node.left);
				}
			}
			
			if (curr.isEmpty()) {
				curr = next;
				next = new ArrayDeque<TreeNode>();
				level++;
			}
		}
		
		// --------------------------------
		//  2nd level order: update values
		// --------------------------------
		curr = new ArrayDeque<TreeNode>();
		next = new ArrayDeque<TreeNode>();
		
		curr.add(root);
		
		int index = 0;
		
		while (!curr.isEmpty()) {
			TreeNode node = curr.pollFirst();
			// update the value
			node.val = list.get(index++);
			
			if (node.left != null) {
				next.add(node.left);
			}
			
			if (node.right != null) {
				next.add(node.right);
			}
			
			if (curr.isEmpty()) {
				curr = next;
				next = new ArrayDeque<TreeNode>();
			}
		}
	}
	
}
