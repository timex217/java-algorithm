package problems.geeks.array.rearrange;

/**
 * Segregate Even and Odd numbers
 * 
 * http://www.geeksforgeeks.org/segregate-even-and-odd-numbers/
 * 
 * Given an array A[], write a function that segregates even and odd numbers. The functions should put all even numbers first, and then odd numbers.
 * 
 * Example
 * Input = {12, 34, 45, 9, 8, 90, 3}
 * Output = {12, 34, 8, 90, 45, 9, 3}
 * 
 * In the output, order of numbers can be changed, i.e., in the above example 34 can come before 12 and 3 can come before 9.
 */

import java.util.*;

public class Q095_Segregate_Even_And_Odd_Numbers {
	
	public static void main(String[] args) {
		int[] a = {12, 34, 45, 9, 8, 90, 3};
		segregate(a, 7);
		System.out.println(Arrays.toString(a));
	}
	
	static void segregate(int[] a, int n) {
		int i = 0;
		
		for (int j = 0; j < n; j++) {
			if (a[j] % 2 == 0) {
				swap(a, i++, j);
			}
		}
	}
	
	/*
	static void segregate(int[] a, int n) {
		if (a == null || n <= 0) {
			return;
		}
		
		int i = 0, j = n - 1;
		while (i < j) {
			while (a[i] % 2 == 0 && i < j) i++;
			while (a[j] % 2 == 1 && i < j) j--;
			
			swap(a, i++, j--);
		}
	}
	*/
	
	static void swap(int[] a, int i, int j) {
		int tmp = a[i];
		a[i] = a[j];
		a[j] = tmp;
	}
	
}
