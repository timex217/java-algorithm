package problems.geeks.array.rearrange;

/**
 * Move all zeroes to end of array
 * 
 * http://www.geeksforgeeks.org/move-zeroes-end-array/
 * 
 * Given an array of random numbers, Push all the zero’s of a given array to the end of the array. 
 * For example, if the given arrays is {1, 9, 8, 4, 0, 0, 2, 7, 0, 6, 0}, 
 * it should be changed to {1, 9, 8, 4, 2, 7, 6, 0, 0, 0, 0}. 
 * The order of all other elements should be same. 
 * 
 * Expected time complexity is O(n) and extra space is O(1).
 * 
 * There can be many ways to solve this problem. Following is a simple and interesting way to solve this problem.
 * Traverse the given array ‘arr’ from left to right. While traversing, maintain count of non-zero elements in array. Let the count be ‘count’. For every non-zero element arr[i], put the element at ‘arr[count]‘ and increment ‘count’. After complete traversal, all non-zero elements have already been shifted to front end and ‘count’ is set as index of first 0. Now all we need to do is that run a loop which makes all elements zero from ‘count’ till end of the array.
 */

import java.util.*;

public class Q027_Move_Zeroes_To_End_Of_Array {
    
    public static void main(String[] args) {
        int[] a = {1, 9, 8, 4, 0, 0, 2, 7, 0, 6, 0};
        //moveZerosToEnd(a);
        moveZerosToEnd2(a);
        System.out.println(Arrays.toString(a));
    }
    
    static void moveZerosToEnd(int[] a) {
        if (a == null) 
            return;
        
        int n = a.length;
        
        int count = 0;
        
        for (int i = 0; i < n; i++) {
            if (a[i] != 0)
                a[count++] = a[i];
        }
        
        while (count < n)
            a[count++] = 0;
    }
    
    // ---------------------------
    //  隔板思想
    // ---------------------------
    static void moveZerosToEnd2(int[] a) {
        if (a == null) {
            return;
        }
        
        int i = 0, j = a.length - 1;
        
        while (i < j) {
            if (a[i] == 0) {
                swap(a, i, j--);
            } else {
                i++;
            }
        }
    }
    
    static void swap(int[] a, int i, int j) {
        int tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }

}
