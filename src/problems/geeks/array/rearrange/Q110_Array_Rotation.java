package problems.geeks.array.rearrange;

/**
 * Array Rotation
 * 
 * http://www.geeksforgeeks.org/array-rotation/
 * 
 * Write a function rotate(ar[], d, n) that rotates arr[] of size n by d elements.
 */

import java.util.*;

public class Q110_Array_Rotation {
	
	public static void main(String[] args) {
		int[] a = {1, 2, 3, 4, 5, 6, 7};
		int d = 2;
		
		rotateArray(a, 7, d);
		System.out.println(Arrays.toString(a));
	}
	
	static void rotateArray(int[] a, int n, int d) {
		reverse(a, 0, d - 1);
		reverse(a, d, n - 1);
		reverse(a, 0, n - 1);
	}
	
	static void reverse(int[] a, int lo, int hi) {
		if (lo >= hi) {
			return;
		}
		
		while (lo < hi) {
			swap(a, lo++, hi--);
		}
	}
	
	static void swap(int[] a, int i, int j) {
		int tmp = a[i];
		a[i] = a[j];
		a[j] = tmp;
	}
	
}
