package problems.geeks.array.rearrange;

/**
 * Sort an array of 0s, 1s and 2s
 * 
 * http://www.geeksforgeeks.org/sort-an-array-of-0s-1s-and-2s/
 * 
 * Given an array A[] consisting 0s, 1s and 2s, write a function that sorts A[]. The functions should put all 0s first, then all 1s and all 2s in last.
 * 
 * Example
 * Input = {0, 1, 1, 0, 1, 2, 1, 2, 0, 0, 0, 1};
 * Output = {0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2}
 */

import java.util.*;

public class Q093_Sort_Array_Of_0s_1s_And_2s {
	
	public static void main(String[] args) {
		int[] a = {0, 1, 1, 0, 1, 2, 1, 2, 0, 0, 0, 1};
		sort012(a, 12);
		System.out.println(Arrays.toString(a));
	}
	
	static void sort012(int[] a, int n) {
		if (a == null || n <= 0) {
			return;
		}
		
		int i = 0, k = 0, j = n - 1;
		
		while (k <= j) {
			if (a[k] == 1) {
				k++;
			} else if (a[k] == 0) {
				swap(a, k++, i++);
			} else {
				swap(a, k, j--);
			}
		}
	}
	
	static void swap(int[] a, int i, int j) {
		int tmp = a[i];
		a[i] = a[j];
		a[j] = tmp;
	}
	
}
