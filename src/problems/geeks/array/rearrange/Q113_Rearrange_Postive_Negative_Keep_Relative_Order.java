package problems.geeks.array.rearrange;

/**
 * Rearrange positive and negative numbers
 * 
 * Rearrange positive and negative numbers in an array such that their relative order remains same. 
 * 
 * No extra space is allowed!
 * 
 * Example 
 * input:  {7, -2, 3, -1, -4, 6, 9, 1, -8}
 * output: {-2, -1, -4, -8, 7, 3, 6, 9, 1}
 * 
 * 这道题的难点在于考虑rotate
 */

import java.util.*;

public class Q113_Rearrange_Postive_Negative_Keep_Relative_Order {

	public static void main(String[] args) {
		int[] a = {7, -2, 3, -1, -4, 6, 9, 1, -8};
		rearrange(a, a.length);
		System.out.println(Arrays.toString(a));
	}
	
	static void rearrange(int[] a, int n) {
		if (a == null || n == 0) return;
		
		int i = 0, j = 0;
		while (i < n && j < n) {
			while (i < n && a[i] < 0)
				i++;
			while (j < n && (j < i || a[j] >= 0))
				j++;
			
			if (i < n && j < n)
				rotate(a, i, j);
		}
	}
	
	static void rotate(int[] a, int i, int j) {
		int tmp = a[j];
		for (int k = j; k > i; k--)
			a[k] = a[k - 1];
		
		a[i] = tmp;
	}
	
}
