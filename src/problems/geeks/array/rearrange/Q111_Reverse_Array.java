package problems.geeks.array.rearrange;

/**
 * Reverse an array
 * 
 * http://www.geeksforgeeks.org/write-a-program-to-reverse-an-array/
 */

import java.util.*;

public class Q111_Reverse_Array {
	
	public static void main(String[] args) {
		int[] a = {1, 2, 3, 4, 5, 6, 7};
		reverse(a, 0, 6);
		System.out.println(Arrays.toString(a));
	}
	
	static void reverse(int[] a, int lo, int hi) {
		if (a == null || lo >= hi) {
			return;
		}
		
		while (lo < hi) {
			swap(a, lo++, hi--);
		}
	}
	
	static void swap(int[] a, int i, int j) {
		int tmp = a[i];
		a[i] = a[j];
		a[j] = tmp;
	}
}
