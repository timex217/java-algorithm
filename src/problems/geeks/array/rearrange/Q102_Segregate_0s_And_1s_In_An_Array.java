package problems.geeks.array.rearrange;

/**
 * Segregate 0s and 1s in an array
 * 
 * http://www.geeksforgeeks.org/segregate-0s-and-1s-in-an-array-by-traversing-array-once/
 * 
 * You are given an array of 0s and 1s in random order. Segregate 0s on left side and 1s on right side of the array. 
 * 
 * Traverse array only once!
 * 
 * Input array   =  [0, 1, 0, 1, 0, 0, 1, 1, 1, 0] 
 * Output array =  [0, 0, 0, 0, 0, 1, 1, 1, 1, 1] 
 */

import java.util.*;

public class Q102_Segregate_0s_And_1s_In_An_Array {
	
	public static void main(String[] args) {
		int[] a = {0, 1, 0, 1, 0, 0, 1, 1, 1, 0};
		segregate(a, 10);
		System.out.println(Arrays.toString(a));
	}
	
	static void segregate(int[] a, int n) {
		int i = 0;
		for (int j = 0; j < n; j++) {
			if (a[j] == 0) {
				swap(a, i++, j);
			}
		}
	}
	
	/*
	static void segregate(int[] a, int n) {
		int i = 0, j = n - 1;
		while (i < j) {
			while (i < j && a[i] == 0) i++;
			while (i < j && a[j] == 1) j--;
			swap(a, i++, j--);
		}
	}
	*/
	
	static void swap(int[] a, int i, int j) {
		int tmp = a[i];
		a[i] = a[j];
		a[j] = tmp;
	}
	
}
