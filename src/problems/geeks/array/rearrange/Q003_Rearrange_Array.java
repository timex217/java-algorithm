package problems.geeks.array.rearrange;

/**
 * Rearrange array in alternating positive & negative items with O(1) extra space
 * 
 * Given an array of positive and negative numbers, arrange them in an alternate fashion such that 
 * every positive number is followed by negative and vice-versa maintaining the order of appearance.
 * Number of positive and negative numbers need not be equal. 
 * If there are more positive numbers they appear at the end of the array. 
 * If there are more negative numbers, they too appear in the end of the array.
 * 
 * Example:
 * 
 * Input:  arr[] = {1, 2, 3, -4, -1, 4}
 * Output: arr[] = {-4, 1, -1, 2, 3, 4}
 * 
 * Input:  arr[] = {-5, -2, 5, 2, 4, 7, 1, 8, 0, -8}
 * output: arr[] = {-5, 5, -2, 2, -8, 4, 7, 1, 8, 0} 
 */

import java.util.*;

public class Q003_Rearrange_Array {
    
    public static void main(String[] args) {
        //int[] a = {1, 2, 3, -4, -1, 4};
        int[] a = {-5, -2, 5, 2, 4, 7, 1, 8, 0, -8};
        rearrange(a);
        System.out.println(Arrays.toString(a));
    }
    
    static void rearrange(int[] a) {
    	int i = 0, j = 0;
    	int n = a.length;
    	
    	while (j < n) {
    		// locate i
    		while (i < n && (a[i] >= 0 && i % 2 == 1) || (a[i] < 0 && i % 2 == 0)) { i++; }
    		if (i == n) return;
    		
    		// locate j
    		j = i + 1;
    		if (a[i] >= 0) {
    			while (j < n && a[j] >= 0) { j++; }
    		} else {
    			while (j < n && a[j] < 0) { j++; }
    		}
    		if (j == n) return;
    		
    		// rotate
    		rightRotate(a, i, j);
    	}
    }
    
    static void rearrange2(int[] a) {
        if (a == null) {
            return;
        }
        
        int outofplace = -1;
        
        for (int i = 0; i < a.length; i++) {
            if (outofplace >= 0) {
                if ((a[outofplace] < 0 && a[i] >= 0) || (a[outofplace] >= 0 && a[i] < 0)) {
                    rightRotate(a, outofplace, i);
                    
                    if (i - outofplace >= 2) {
                        outofplace += 2;
                    } else {
                        outofplace = -1;
                    }
                }
            }
            
            if (outofplace == -1) {
                if ((a[i] < 0 && i % 2  == 1) || (a[i] >= 0 && i % 2 == 0)) {
                    outofplace = i;
                }
            }
        }
    }
    
    static void rightRotate(int[] a, int outofplace, int index) {
        int tmp = a[index];
        for (int i = index; i > outofplace; i--) {
            a[i] = a[i - 1];
        }
        a[outofplace] = tmp;
    }

}
