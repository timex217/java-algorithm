package problems.geeks.array.rearrange;

/**
 * Rearrange positive and negative numbers in O(n) time and O(1) extra space
 * 
 * http://www.geeksforgeeks.org/rearrange-positive-and-negative-numbers-publish/
 * 
 * An array contains both positive and negative numbers in random order. 
 * Rearrange the array elements so that positive and negative numbers are placed alternatively. 
 * Number of positive and negative numbers need not be equal. 
 * If there are more positive numbers they appear at the end of the array. 
 * If there are more negative numbers, they too appear in the end of the array.
 * 
 * For example, if the input array is [-1, 2, -3, 4, 5, 6, -7, 8, 9], then the output should be [9, -7, 8, -3, 5, -1, 2, 4, 6]
 * 
 * 这道题考的是quick sort的思想，将数组变为左边一半是负数，右边一半是正数。(其实也可以用隔板思想)
 * 
 * 这道题非常重要，基本上凡是要将一个数组按照某种形式分割成2部分都可以考虑用这种方法, 但如果是需要保留顺序的话，就采用Q003的办法(outofplace)
 */

import java.util.*;

public class Q034_Rearrange_Positive_And_Negative_Numbers {
    
    public static void main(String[] args) {
        int[] a = {-1, 2, -3, 4, 5, 6, -7, 8, 9};
        rearrange(a);
        System.out.println(Arrays.toString(a));
        
    }
    
    static void rearrange(int[] a) {
        if (a == null) {
            return;
        }
        
        int n = a.length;
        
        int i = 0;
        for (int j = 0; j < n; j++) {
            if (a[j] < 0) {
                swap(a, i++, j);
            }
        }
        
        int neg = 0, pos = i;
        
        while (pos < n && neg < pos && a[neg] < 0) {
            swap(a, neg, pos);
            pos++;
            neg += 2;
        }
    }
    
    static void swap(int[] a, int i, int j) {
        int tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }

}
