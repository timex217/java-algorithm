package problems.geeks.array.sort;

/**
 * Sort a nearly sorted (or K sorted) array
 * 
 * http://www.geeksforgeeks.org/nearly-sorted-algorithm/
 * 
 * Given an array of n elements, where each element is at most k away from its target position, devise an algorithm that sorts in O(n log k) time. 
 * For example, let us consider k is 2, an element at index 7 in the sorted array, can be at indexes 5, 6, 7, 8, 9 in the given array.
 */

import java.util.*;

public class Q054_Sort_Nearly_K_Sorted_Array {
    
    public static void main(String[] args) {
        int k = 3;
        int[] a = {2, 6, 3, 12, 56, 8};
        sortK(a, k);
        
        System.out.println(Arrays.toString(a));
    }
    
    static void sortK(int[] a, int k) {
        if (a == null) return;
        
        int n = a.length;
        
        // use a min heap
        PriorityQueue<Integer> heap = new PriorityQueue<Integer>(k + 1, new Comparator<Integer>() {
            @Override
            public int compare(Integer a, Integer b) {
                return a - b;
            }
        });
        
        int i;
        for (i = 0; i <= k && i < n; i++) {
            heap.offer(a[i]);
        }
        
        int index = 0;
        while (!heap.isEmpty()) {
            a[index++] = heap.poll();
            if (i < n) {
                heap.offer(a[i++]);
            }
        }
    }

}
