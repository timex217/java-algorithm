package problems.geeks.array.sort;

/**
 * Sort elements by frequency
 * 
 * http://www.geeksforgeeks.org/sort-elements-by-frequency-set-2/
 * 
 * Given an array of integers, sort the array according to frequency of elements. 
 * For example, if the input array is {2, 3, 2, 4, 5, 12, 2, 3, 3, 3, 12}, then modify the array to {3, 3, 3, 3, 2, 2, 2, 12, 12, 4, 5}. 
 */

import java.util.*;

public class Q033_Sort_Elements_By_Frequency {
    
    static class Element {
        int val;
        int index;
        int count;
        Element(int v, int i, int c) {
            this.val = v;
            this.index = i;
            this.count = c;
        }
    }
    
    public static void main(String[] args) {
        int[] a = {2, 3, 2, 5, 4, 12, 2, 3, 3, 3, 12};
        sortByFrequency(a);
        System.out.println(Arrays.toString(a));
    }
    
    static void sortByFrequency(int[] a) {
        if (a == null || a.length == 0) {
            return;
        }
        
        int n = a.length;
        
        // use a priority queue to help us sort by frequency
        PriorityQueue<Element> heap = new PriorityQueue<Element>(n, new Comparator<Element>(){
            @Override
            public int compare(Element a, Element b) {
                // 返回负数，表示会被放在前面
                // 返回正数，表示会被放在后面
                if (a.count < b.count) {
                    return 1;
                } else if (b.count < a.count) {
                    return -1;
                } else {
                    return a.index - b.index;
                }
            }
        });
        
        // use a hashmap to store the count of each element
        HashMap<Integer, Element> map = new HashMap<Integer, Element>();
        
        for (int i = 0; i < n; i++) {
            if (map.containsKey(a[i])) {
                Element elem = map.get(a[i]);
                elem.count++;
            } else {
                Element elem = new Element(a[i], i, 1);
                map.put(a[i], elem);
            }
        }
        
        // put map entries to the priority queue
        for (Map.Entry<Integer, Element> entry : map.entrySet()) {
            heap.offer(entry.getValue());
        }
        
        int k = 0;
        while (!heap.isEmpty()) {
            Element elem = heap.poll();
            for (int i = 0; i < elem.count; i++) {
                a[k++] = elem.val;
            }
        }
    }

}
