package problems.geeks.array.sort;

import java.util.*;

public class Q114_Radix_Sort_Bit_Operation {
	
	public static void main(String[] args) {
		int[] a = {170, 45, 75, 90, 802, 24, 2, 66};
		radixSort(a);
		System.out.println(Arrays.toString(a));
	}
	
	static void radixSort(int[] a) {
		if (a == null || a.length == 0) {
			return;
		}
		
		for (int k = 0; k < 32; k++) {
			int b = 1 << k;
			
			ArrayList<Integer> res = new ArrayList<Integer>();
			ArrayList<Integer> ones = new ArrayList<Integer>();
			ArrayList<Integer> zeros = new ArrayList<Integer>();
			
			for (int i = 0; i < a.length; i++) {
				if ((a[i] & b) != 0) {
					ones.add(a[i]);
				} else {
					zeros.add(a[i]);
				}
			}
			
			res.addAll(zeros);
			res.addAll(ones);
			
			for (int i = 0; i < a.length; i++) {
				a[i] = res.get(i);
			}
		}
	}
	
}
