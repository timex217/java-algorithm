package problems.geeks.array.sort;

/**
 * Radix Sort
 * 
 * 1) Do following for each digit i where i varies from least significant digit to the most significant digit.
 * ����.a) Sort input array using counting sort (or any stable sort) according to the i�th digit.
 * 
 * Example:
 * Original, unsorted list:
 * 
 * 170, 45, 75, 90, 802, 24, 2, 66
 * Sorting by least significant digit (1s place) gives: [*Notice that we keep 802 before 2, because 802 occurred before 2 in the original list, and similarly for pairs 170 & 90 and 45 & 75.]
 * 
 * 170, 90, 802, 2, 24, 45, 75, 66
 * Sorting by next digit (10s place) gives: [*Notice that 802 again comes before 2 as 802 comes before 2 in the previous list.]
 * 
 * 802, 2, 24, 45, 66, 170, 75, 90
 * Sorting by most significant digit (100s place) gives:
 * 
 * 2, 24, 45, 66, 75, 90, 170, 802
 */

import java.util.*;

public class Q113_Radix_Sort {
	
	public static void main(String[] args) {
		int[] a = {170, 45, 75, 90, 802, 24, 2, 66};
		radixSort(a);
		System.out.println(Arrays.toString(a));
	}
	
	static void radixSort(int[] a) {
		if (a == null || a.length == 0) {
			return;
		}
		
		// step 1. get max
		int max = a[0];
		for (int i = 0; i < a.length; i++) {
			max = Math.max(max, a[i]);
		}
		
		// step 2. do couting sort for every digit
		for (int d = 1; max / d > 0; d *= 10) {
			countingSort(a, d);
		}
	}
	
	static void countingSort(int[] a, int d) {
		int[] count = new int[10];
		int[] res = new int[a.length];
		
		for (int i = 0; i < a.length; i++) {
			int k = (a[i] / d) % 10;
			count[k]++;
		}
		
		for (int i = 1; i < count.length; i++) {
			count[i] += count[i - 1];
		}
		
		for (int i = a.length - 1; i >= 0; i--) {
			int k = (a[i] / d) % 10;
			res[count[k] - 1] = a[i];
			count[k]--;
		}
		
		for (int i = 0; i < a.length; i++) {
			a[i] = res[i];
		}
	}
	
}
