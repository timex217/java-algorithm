package problems.geeks.array.sort;

/**
 * Sort an array according to the order defined by another array
 * 
 * Given two arrays A1[] and A2[], sort A1 in such a way that the relative order among the elements will be same as those are in A2. 
 * For the elements not present in A2, append them at last in sorted order.
 * 
 * Input: A1[] = {2, 1, 2, 5, 7, 1, 9, 3, 6, 8, 8}
 *        A2[] = {2, 1, 8, 3}
 *        
 * Output: A1[] = {2, 2, 1, 1, 8, 8, 3, 5, 6, 7, 9}
 * 
 * The code should handle all cases like number of elements in A2[] may be more or less compared to A1[]. 
 * A2[] may have some elements which may not be there in A1[] and vice versa is also possible.
 */

import java.util.*;

public class Q004_Sort_Array_According_To_Another_Array {
    
    public static void main(String[] args) {
        int[] A1 = {2, 1, 2, 5, 7, 1, 9, 3, 6, 8, 8};
        int[] A2 = {2, 1, 8, 3};
        
        sort(A1, A2);
        System.out.println(Arrays.toString(A1));
    }
    
    static void sort(int[] A1, int[] A2) {
        if (A1 == null || A2 == null) {
            return;
        }
        
        // step 1. use a hash map to count the number of elements in A1
        Map<Integer, Integer> map = new HashMap<Integer, Integer>();
        
        for (int i = 0; i < A1.length; i++) {
            if (map.containsKey(A1[i])) {
                map.put(A1[i], map.get(A1[i]) + 1);
            } else {
                map.put(A1[i], 1);
            }
        }
        
        // step 2. traverse A2 and put the elements to A1
        int k = 0;
        for (int i = 0; i < A2.length; i++) {
            if (map.containsKey(A2[i])) {
                for (int j = 0; j < map.get(A2[i]); j++) {
                    A1[k++] = A2[i];
                }
                
                // remove from the map
                map.remove(A2[i]);
            }
        }
        
        // step 3. sort the rest of the map
        Map<Integer, Integer> sortedMap = new TreeMap<Integer, Integer>(map);
        
        // step 4. put the rest to A1
        for(Map.Entry<Integer, Integer> entry : sortedMap.entrySet()) {
            A1[k++] = entry.getKey();
        }
        
    }

}
