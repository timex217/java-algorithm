package problems.geeks.array.sort;

/**
 * Quick Sort
 */

import java.util.*;

public class Q040_QuickSort {
    
    public static void main(String[] args) {
        int[] a = {7, 5, 3, 9, 8, 1, 0, 2, -4, -3, -8};
        quickSortR(a, 0, a.length - 1);
        //quickSort(a);
        System.out.println(Arrays.toString(a));
    }
    
    // -------------------------
    //  Recursion
    // -------------------------
    static void quickSortR(int[] a, int lo, int hi) {
        if (lo >= hi) return;
        int p = partition(a, lo, hi);
        quickSortR(a, lo, p - 1);
        quickSortR(a, p + 1, hi);
    }
    
    static int partition(int[] a, int lo, int hi) {
        int i = lo;
        for (int j = lo; j <= hi - 1; j++) {
            if (a[j] <= a[hi]) {
            	swap(a, i++, j);
            }
        }
        swap(a, i, hi);
        return i;
    }
    
    static void swap(int[] a, int i, int j) {
        int tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }
    
    // ---------------------------------
    //  Iterative (preorder traverse)
    // ---------------------------------
    static void quickSort(int[] a) {
        if (a == null) return;
        
        int lo = 0, hi = a.length - 1;
        
        Stack<Integer> stack = new Stack<Integer>();
        stack.push(lo);
        stack.push(hi);
        
        while (!stack.isEmpty()) {
            hi = stack.pop();
            lo = stack.pop();
            
            int p = partition(a, lo, hi);
            
            if (p - 1 > lo) {
                stack.push(lo);
                stack.push(p - 1);
            }
            
            if (p + 1 < hi) {
                stack.push(p + 1);
                stack.push(hi);
            }
        }
    }
    
}
