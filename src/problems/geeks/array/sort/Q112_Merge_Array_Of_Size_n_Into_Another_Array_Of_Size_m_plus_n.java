package problems.geeks.array.sort;

/**
 * Merge an array of size n into another array of size m+n
 * 
 * http://www.geeksforgeeks.org/merge-one-array-of-size-n-into-another-one-of-size-mn/
 * 
 * There are two sorted arrays. First one is of size m+n containing only m elements. 
 * Another one is of size n and contains n elements. 
 * Merge these two arrays into the first array of size m+n such that the output is sorted.
 */

import java.util.*;

public class Q112_Merge_Array_Of_Size_n_Into_Another_Array_Of_Size_m_plus_n {
	
	public static void main(String[] args) {
		int mPlusN[] = {2, 8, -1, -1, -1, 13, -1, 15, 20};
		int N[] = {5, 7, 9, 25};
		
		int m = 9, n = 4;
		
		// step 1. rearrange mPlusN, put all the -1 in front
		int p = rearrange(mPlusN, m);
		System.out.println(Arrays.toString(mPlusN));
		System.out.println(p);
		
		// step 2. merge mPlusN and N
		merge(mPlusN, p, m, N, n);
		
		System.out.println(Arrays.toString(mPlusN));
	}
	
	static int rearrange(int[] a, int n) {
		if (a == null || n == 0) {
			return -1;
		}
		
		// 从后往前，可以保证后面的顺序
		int i = n;
		for (int j = n - 1; j >= 0; j--) {
			if (a[j] != -1) {
				swap(a, --i, j);
			}
		}
		
		return i;
	}
	
	static void merge(int[] a1, int p, int m, int[] a2, int n) {
		int i = p, j = 0, k = 0;
		
		while (i < m && j < n) {
			if (a1[i] < a2[j]) {
				a1[k++] = a1[i++];
			} else {
				a1[k++] = a2[j++];
			}
		}
		
		while (i < m) {
			a1[k++] = a1[i++];
		}
		
		while (j < n) {
			a1[k++] = a2[j++];
		}
	}
	
	static void swap(int[] a, int i, int j) {
		int tmp = a[i];
		a[i] = a[j];
		a[j] = tmp;
	}
	
}
