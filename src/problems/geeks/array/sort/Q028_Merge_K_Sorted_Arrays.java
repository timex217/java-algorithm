package problems.geeks.array.sort;

/**
 * Merge k sorted arrays | Set 1
 * 
 * http://www.geeksforgeeks.org/merge-k-sorted-arrays/
 * 
 * Given k sorted arrays of size n each, merge them and print the sorted output.
 * 
 * Example:
 * 
 * Input:
 * k = 3, n =  4
 * arr[][] = { {1, 3, 5, 7},
 *             {2, 4, 6, 8},
 *             {0, 9, 10, 11}} ;
 * 
 * Output: 0 1 2 3 4 5 6 7 8 9 10 11 
 * A simple solution is to create an output array of size n*k and one by one copy all arrays to it. Finally, sort the output array using any O(nLogn) sorting algorithm. This approach takes O(nkLognk) time.
 * 
 * We can merge arrays in O(nk*Logk) time using Min Heap. Following is detailed algorithm.
 * 
 * 1. Create an output array of size n*k.
 * 2. Create a min heap of size k and insert 1st element in all the arrays into a the heap
 * 3. Repeat following steps n*k times.
 *      a) Get minimum element from heap (minimum is always at root) and store it in output array.
 *      b) Replace heap root with next element from the array from which the element is extracted. If the array doesn�t have any more elements, then replace root with infinite. After replacing the root, heapify the tree.
 */

public class Q028_Merge_K_Sorted_Arrays {

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

}
