package problems.geeks.array.sort;

/**
 * Sort n numbers in range from 0 to n^2 � 1 in linear time
 * 
 * http://www.geeksforgeeks.org/sort-n-numbers-range-0-n2-1-linear-time/
 * 
 * Given an array of numbers of size n. It is also given that the array elements are in range from 0 to n2 � 1. Sort the given array in linear time.
 * 
 * Examples:
 * Since there are 5 elements, the elements can be from 0 to 24.
 * Input: arr[] = {0, 23, 14, 12, 9}
 * Output: arr[] = {0, 9, 12, 14, 23}
 * 
 * Since there are 3 elements, the elements can be from 0 to 8.
 * Input: arr[] = {7, 0, 2}
 * Output: arr[] = {0, 2, 7}
 */

public class Q021_Sort_Numbers_1 {
    
    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

}
