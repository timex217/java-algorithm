package problems.geeks.array.basic;

/**
 * Check if array elements are consecutive 
 * 
 * http://www.geeksforgeeks.org/check-if-array-elements-are-consecutive/
 * 
 * Given an unsorted array of numbers, write a function that returns true if array consists of consecutive numbers.
 * 
 * Examples:
 * a) If array is {5, 2, 3, 1, 4}, then the function should return true because the array has consecutive numbers from 1 to 5.
 * 
 * b) If array is {83, 78, 80, 81, 79, 82}, then the function should return true because the array has consecutive numbers from 78 to 83.
 * 
 * c) If the array is {34, 23, 52, 12, 3}, then the function should return false because the elements are not consecutive.
 * 
 * d) If the array is {7, 6, 5, 5, 3, 4}, then the function should return false because 5 and 5 are not consecutive.
 */

import java.util.*;

public class Q086_Check_If_Array_Elements_Are_Consecutive {
	
	public static void main(String[] args) {
		int[] a = {7, 6, 5, 2, 3, 4};
		boolean res = areConsecutive(a, 6);
		System.out.println(res);
	}
	
	static boolean areConsecutive(int[] a, int n) {
		if (a == null || n == 0) {
			return false;
		}
		
		int min = a[0];
		Set<Integer> set = new HashSet<Integer>();
		for (int i = 0; i < a.length; i++) {
			if (set.contains(a[i])) {
				return false;
			}
			set.add(a[i]);
			min = Math.min(min, a[i]);
		}
		
		while (set.contains(min)) {
			set.remove(min);
			min = min + 1;
		}
		
		if (set.size() > 0) {
			return false;
		} else {
			return true;
		}
	}
	
	static boolean areConsecutive2(int[] a, int n) {
		if (a == null || n == 0) {
			return false;
		}
		
		int min = a[0], max = a[0];
		
		// step 1. get min and max
		for (int i = 1; i < n; i++) {
			min = Math.min(min, a[i]);
			max = Math.max(max, a[i]);
		}
		
		// check size
		if (max - min + 1 != n) {
			return false;
		}
		
		// check duplication
		for (int i = 0; i < n; i++) {
			int j = Math.abs(a[i]) - min;
			if (a[j] > 0) {
				a[j] = -a[j];
			} else {
				return false;
			}
		}
		
		return true;
	}
	
}
