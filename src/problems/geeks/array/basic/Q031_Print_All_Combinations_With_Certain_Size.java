package problems.geeks.array.basic;

/**
 * Print all possible combinations of r elements in a given array of size n
 * 
 * Given an array of size n, generate and print all possible combinations of r elements in array. 
 * For example, if input array is {1, 2, 3, 4} and r is 2, 
 * then output should be {1, 2}, {1, 3}, {1, 4}, {2, 3}, {2, 4} and {3, 4}.
 */

import java.util.*;

public class Q031_Print_All_Combinations_With_Certain_Size {
    
    public static void main(String[] args) {
        int[] a = {1, 2, 3, 4};
        ArrayList<List<Integer>> res = new ArrayList<List<Integer>>();
        dfs(a, 2, 0, new ArrayList<Integer>(), res);
    }
    
    static void dfs(int[] a, int k, int start, List<Integer> sol, List<List<Integer>> res) {
        if (sol.size() == k) {
            System.out.println(sol.toString());
            res.add(new ArrayList<Integer>(sol));
            return;
        }
        
        for (int i = start; i < a.length; i++) {
            sol.add(a[i]);
            dfs(a, k, i + 1, sol, res);
            sol.remove(sol.size() - 1);
        }
    }

}
