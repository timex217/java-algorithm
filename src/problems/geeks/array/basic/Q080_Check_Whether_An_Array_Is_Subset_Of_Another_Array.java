package problems.geeks.array.basic;

/**
 * Find whether an array is subset of another array
 * 
 * http://www.geeksforgeeks.org/find-whether-an-array-is-subset-of-another-array-set-1/
 * 
 * Given two arrays: arr1[0..m-1] and arr2[0..n-1]. Find whether arr2[] is a subset of arr1[] or not. Both the arrays are not in sorted order.
 * 
 * Examples:
 * Input: arr1[] = {11, 1, 13, 21, 3, 7}, arr2[] = {11, 3, 7, 1}
 * Output: arr2[] is a subset of arr1[]
 * 
 * Input: arr1[] = {1, 2, 3, 4, 5, 6}, arr2[] = {1, 2, 4}
 * Output: arr2[] is a subset of arr1[]
 * 
 * Input: arr1[] = {10, 5, 2, 23, 19}, arr2[] = {19, 5, 3}
 * Output: arr2[] is not a subset of arr1[]
 */

import java.util.*;

public class Q080_Check_Whether_An_Array_Is_Subset_Of_Another_Array {
	
	public static void main(String[] args) {
		int[] a1 = {11, 1, 13, 21, 3, 7};
		int[] a2 = {11, 3, 7, 1};
		boolean res = isSubset(a1, 6, a2, 4);
		System.out.println(res);
	}
	
	static boolean isSubset(int[] a1, int m, int[] a2, int n) {
		if (m < n) {
			return false;
		}
		
		// step 1. sort a1 and a2
		qsort(a1, 0, m - 1);
		qsort(a2, 0, n - 1);
		
		// step 2. use merge sort idea
		int i = 0, j = 0;
		while (j < n && i < m) {
			if (a1[i] < a2[j]) {
				i++;
			} else if (a1[i] == a2[j]) {
				i++;
				j++;
			} else {
				return false;
			}
		}
		
		if (j < n) {
			return false;
		} else {
			return true;
		}
	}
	
	static void qsort(int[] a, int lo, int hi) {
		if (lo >= hi) {
			return;
		}
		
		int p = partition(a, lo, hi);
		
		qsort(a, lo, p - 1);
		qsort(a, p + 1, hi);
	}
	
	static int partition(int[] a, int lo, int hi) {
		int mid = lo + (hi - lo) / 2;
		swap(a, mid, hi);
		
		int i = lo;
		for (int j = lo; j < hi; j++) {
			if (a[j] < a[hi]) {
				swap(a, i++, j);
			}
		}
		
		swap(a, i, hi);
		
		return i;
	}
	
	static void swap(int[] a, int i, int j) {
		int tmp = a[i];
		a[i] = a[j];
		a[j] = tmp;
	}
}
