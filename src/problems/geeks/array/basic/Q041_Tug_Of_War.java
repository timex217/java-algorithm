package problems.geeks.array.basic;

/**
 * Tug of War
 * 
 * http://www.geeksforgeeks.org/tug-of-war/
 * 
 * Given a set of n integers, divide the set in two subsets of n/2 sizes each such that the difference of the sum of two subsets is as minimum as possible. 
 * If n is even, then sizes of two subsets must be strictly n/2 and if n is odd, then size of one subset must be (n-1)/2 and size of other subset must be (n+1)/2.
 * 
 * For example, let given set be {3, 4, 5, -3, 100, 1, 89, 54, 23, 20}, the size of set is 10. 
 * Output for this set should be {4, 100, 1, 23, 20} and {3, 5, -3, 89, 54}. 
 * Both output subsets are of size 5 and sum of elements in both subsets is same (148 and 148).
 * 
 * Let us consider another example where n is odd. Let given set be {23, 45, -34, 12, 0, 98, -99, 4, 189, -1, 4}. 
 * The output subsets should be {45, -34, 12, 98, -1} and {23, 0, -99, 4, 189, 4}. 
 * The sums of elements in two subsets are 120 and 121 respectively.
 */

import java.util.*;

public class Q041_Tug_Of_War {
    
    public static void main(String[] args) {
        int[] a = {23, 45, -34, 12, 0, 98, -99, 4, 189, -1, 4};
        tugOfWar(a);
    }
    
    static void tugOfWar(int[] a) {
        if (a == null) return;
        
        int n = a.length;
        int k = (n % 2 == 0) ? n / 2 : (n - 1) / 2;
        
        // step 1. find all combinations
        ArrayList<ArrayList<Integer>> lists = new ArrayList<ArrayList<Integer>>();
        getCombinations(a, k, 0, new ArrayList<Integer>(), lists);
        
        // step 2. go through the lists, for each list, find the other list
        // then calculate the diff of their sum, and get the minimum value
        int min = Integer.MAX_VALUE;
        
        for (int i = 0; i < lists.size(); i++) {
            int diff = calcSumDiff(a, lists.get(i));
            min = Math.min(min, diff);
        }
        
        System.out.println(min);
    }
    
    static void getCombinations(int[] a, int k, int index, ArrayList<Integer> sol, ArrayList<ArrayList<Integer>> lists) {
        if (sol.size() == k) {
            lists.add(new ArrayList<Integer>(sol));
            return;
        }
        
        for (int i = index; i < a.length; i++) {
            sol.add(i);
            getCombinations(a, k, i + 1, sol, lists);
            sol.remove(sol.size() - 1);
        }
    }
    
    static int calcSumDiff(int[] a, ArrayList<Integer> list) {
        int sum = 0, sum1 = 0;
        
        for (int i = 0; i < a.length; i++)
            sum += a[i];
        
        for (int i = 0; i < list.size(); i++)
            sum1 += a[list.get(i)];
        
        return Math.abs(2 * sum1 - sum);
    }

}
