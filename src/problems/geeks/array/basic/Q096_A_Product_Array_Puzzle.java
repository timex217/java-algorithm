package problems.geeks.array.basic;

/**
 * A Product Array Puzzle
 * 
 * Given an array arr[] of n integers, construct a Product Array prod[] (of same size) such that prod[i] is equal to the product of all the elements of arr[] except arr[i]. 
 * Solve it without division operator and in O(n)!
 * 
 * Example:
 * arr[] = {10, 3, 5, 6, 2}
 * prod[] = {180, 600, 360, 300, 900}
 */

import java.util.*;

public class Q096_A_Product_Array_Puzzle {
	
	public static void main(String[] args) {
		int[] a = {10, 3, 5, 6, 2};
		int[] prod = productArray(a, 5);
		System.out.println(Arrays.toString(prod));
	}
	
	static int[] productArray(int[] a, int n) {
		// construct left array
		int[] left = new int[n];
		left[0] = 1;
		for (int i = 1; i < n; i++) {
			left[i] = left[i - 1] * a[i - 1];
		}
		
		// construct the right array
		int[] right = new int[n];
		right[n - 1] = 1;
		for (int i = n - 2; i >= 0; i--) {
			right[i] = right[i + 1] * a[i + 1];
		}
		
		// get the prod array
		int[] prod = new int[n];
		for (int i = 0; i < n; i++) {
			prod[i] = left[i] * right[i];
		}
		
		return prod;
	}
	
}
