package problems.geeks.array.basic;

/**
 * Partition problem
 * 
 * http://www.geeksforgeeks.org/dynamic-programming-set-18-partition-problem/
 * 
 * Partition problem is to determine whether a given set can be partitioned into two subsets such that the sum of elements in both subsets is same.
 * 
 * Examples
 * 
 * arr[] = {1, 5, 11, 5}
 * Output: true 
 * The array can be partitioned as {1, 5, 5} and {11}
 * 
 * arr[] = {1, 5, 3}
 * Output: false 
 * The array cannot be partitioned into equal sum sets.
 * 
 * Following are the two main steps to solve this problem:
 * 1) Calculate sum of the array. If sum is odd, there can not be two subsets with equal sum, so return false.
 * 2) If sum of array elements is even, calculate sum/2 and find a subset of array with sum equal to sum/2.
 * 
 * The first step is simple. The second step is crucial, it can be solved either using recursion or Dynamic Programming.
 */

public class Q060_Partition_Problem {
    
    public static void main(String[] args) {
        int[] a = {1, 5, 11, 5};
        boolean res = findPartition(a, 4);
        System.out.println(res);
    }
    
    static boolean isSubsetSum(int[] a, int n, int sum) {
        // base case
        if (sum == 0) {
            return true;
        }
        
        if (n == 0 && sum != 0) {
            return false;
        }
        
        // if last element is larger than sum, then ignore it
        if (a[n - 1] > sum) {
            return isSubsetSum(a, n - 1, sum);
        }
        
        /* else, check if sum can be obtained by any of the following
           (a) including the last element
           (b) excluding the last element
        */
        return isSubsetSum(a, n - 1, sum) || isSubsetSum(a, n - 1, sum - a[n - 1]);
    }
    
    static boolean findPartition(int[] a, int n) {
        int sum = 0;
        for (int i = 0; i < n; i++) {
            sum += a[i];
        }
        
        // 1)
        if (sum % 2 == 1) {
            return false;
        } else {
            return isSubsetSum(a, n, sum / 2);
        }
    }

}
