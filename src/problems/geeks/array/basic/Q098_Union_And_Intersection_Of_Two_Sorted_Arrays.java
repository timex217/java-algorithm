package problems.geeks.array.basic;

/**
 * Union and Intersection of two sorted arrays
 * 
 * http://www.geeksforgeeks.org/union-and-intersection-of-two-sorted-arrays-2/
 * 
 * For example, if the input arrays are:
 * arr1[] = {1, 3, 4, 5, 7}
 * arr2[] = {2, 3, 5, 6}
 * Then your program should print Union as {1, 2, 3, 4, 5, 6, 7} and Intersection as {3, 5}.
 */

import java.util.*;

public class Q098_Union_And_Intersection_Of_Two_Sorted_Arrays {
	
	public static void main(String[] args) {
		int[] a1 = {1, 3, 4, 5, 7};
		int[] a2 = {2, 3, 5, 6};
		
		union(a1, 5, a2, 4);
		intersection(a1, 5, a2, 4);
	}
	
	static void union(int[] a1, int m, int[] a2, int n) {
		ArrayList<Integer> res = new ArrayList<Integer>();
		
		int i = 0, j = 0;
		while (i < m && j < n) {
			if (a1[i] < a2[j]) {
				res.add(a1[i]);
				i++;
			} else if (a2[j] < a1[i]) {
				res.add(a2[j]);
				j++;
			} else {
				res.add(a1[i]);
				i++;
				j++;
			}
		}
		
		while (i < m) {
			res.add(a1[i]);
			i++;
		}
		
		while (j < n) {
			res.add(a2[j]);
			j++;
		}
		
		System.out.println(res.toString());
	}
	
	static void intersection(int[] a1, int m, int[] a2, int n) {
		ArrayList<Integer> res = new ArrayList<Integer>();
		
		int i = 0, j = 0;
		while (i < m && j < n) {
			if (a1[i] < a2[j]) {
				i++;
			} else if (a2[j] < a1[i]) {
				j++;
			} else {
				res.add(a1[i]);
				i++; j++;
			}
		}
		
		System.out.println(res.toString());
	}
	
}
