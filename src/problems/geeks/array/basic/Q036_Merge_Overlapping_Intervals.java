package problems.geeks.array.basic;

/**
 * Merge Overlapping Intervals
 * 
 * http://www.geeksforgeeks.org/merging-intervals/
 * 
 * Given a set of time intervals in any order, merge all overlapping intervals into one and output the result which should have only mutually exclusive intervals. 
 * Let the intervals be represented as pairs of integers for simplicity. 
 * For example, let the given set of intervals be {{1,3}, {2,4}, {5,7}, {6,8}}. 
 * The intervals {1,3} and {2,4} overlap with each other, so they should be merged and become {1, 4}. 
 * Similarly {5, 7} and {6, 8} should be merged and become {5, 8}
 * 
 * Write a function which produces the set of merged intervals for the given set of intervals.
 */

import java.util.*;

public class Q036_Merge_Overlapping_Intervals {
    
    static class Interval implements Comparable<Interval> {
        int start;
        int end;
        Interval(int s, int e) {
            this.start = s;
            this.end = e;
        }
        public int compareTo(Interval interval) {
            return this.start - interval.start;
        }
    }
    
    static void mergeIntervals(ArrayList<Interval> intervals) {
        // sort the intervals by start time
        Collections.sort(intervals);
        
        // use a stack to help
        Stack<Interval> stack = new Stack<Interval>();
        
        for (int i = 0; i < intervals.size(); i++) {
            Interval curr = intervals.get(i);
            
            if (stack.isEmpty()) {
                stack.push(curr);
                continue;
            }
            
            Interval prev = stack.peek();
            
            if (curr.start <= prev.end) {
                prev.end = Math.max(prev.end, curr.end);
            } else {
                stack.push(curr);
            }
        }
        
        intervals.clear();
        
        while (!stack.isEmpty()) {
            intervals.add(stack.pop());
        }
        
        Collections.sort(intervals);
    }
    
    public static void main(String[] args) {
        ArrayList<Interval> intervals = new ArrayList<Interval>();
        
        intervals.add(new Interval(1, 3));
        intervals.add(new Interval(2, 4));
        intervals.add(new Interval(5, 7));
        intervals.add(new Interval(6, 8));
        
        mergeIntervals(intervals);
        
        for (int i = 0; i < intervals.size(); i++) {
            Interval interval = intervals.get(i);
            System.out.format("start: %d, end: %d\n", interval.start, interval.end);
        }
    }

}
