package problems.geeks.array.basic;

/**
 * Implement two stacks in an array
 * 
 * http://www.geeksforgeeks.org/implement-two-stacks-in-an-array/
 * 
 * Create a data structure twoStacks that represents two stacks. 
 * Implementation of twoStacks should use only one array, i.e., both stacks should use the same array for storing elements. 
 * Following functions must be supported by twoStacks.
 * 
 * push1(int x) �> pushes x to first stack
 * push2(int x) �> pushes x to second stack
 * 
 * pop1() �> pops an element from first stack and return the popped element
 * pop2() �> pops an element from second stack and return the popped element
 * 
 * Implementation of twoStack should be space efficient.
 * 
 * Solution:
 * This method efficiently utilizes the available space. It doesn�t cause an overflow if there is space available in arr[]. 
 * The idea is to start two stacks from two extreme corners of arr[]. 
 * stack1 starts from the leftmost element, the first element in stack1 is pushed at index 0. 
 * The stack2 starts from the rightmost corner, the first element in stack2 is pushed at index (n-1). 
 * Both stacks grow (or shrink) in opposite direction. To check for overflow, all we need to check is for space between top elements of both stacks. 
 * This check is highlighted in the below code.
 */

public class Q069_Implement_Two_Stacks_In_An_Array {
	
	static class DualStack {
		int size;
		int[] arr;
		int top1;
		int top2;
		
		DualStack(int n) {
			this.size = n;
			this.arr = new int[n];
			this.top1 = -1;
			this.top2 = n;
		}
		
		void push1(int x) {
			if (this.top2 - this.top1 > 1) {
				this.arr[++top1] = x;
			}
		}
		
		void push2(int x) {
			if (this.top2 - this.top1 > 1) {
				this.arr[--top2] = x;
			}
		}
		
		int pop1() {
			if (this.top1 >= 0) {
				return this.arr[this.top1--];
			} else {
				return Integer.MIN_VALUE;
			}
		}
		
		int pop2() {
			if (this.top2 < this.size) {
				return this.arr[this.top2++];
			} else {
				return Integer.MIN_VALUE;
			}
		}
	}
	
	public static void main(String[] args) {
		
	}
	
	
}
