package problems.geeks.array.basic;

/**
 * Shuffle a given array - Fisher�Yates shuffle Algorithm
 * 
 * http://www.geeksforgeeks.org/shuffle-a-given-array/
 * 
 * Given an array, write a program to generate a random permutation of array elements. 
 * This question is also asked as �shuffle a deck of cards� or �randomize a given array�.
 * 
 * Random.nextInt(n): Returns a pseudorandom, uniformly distributed int value between 0 (inclusive) and the specified value (exclusive), 
 * drawn from this random number generator's sequence.
 */

import java.util.*;

public class Q049_Shuffle_Array {
    
    public static void main(String[] args) {
        int[] a = {1, 2, 3, 4, 5, 6, 7, 8};
        shuffle(a);
        System.out.println(Arrays.toString(a));
    }
    
    static void shuffle(int[] a) {
        if (a == null) return;
        
        Random rand = new Random();
        
        for (int i = a.length - 1; i > 0; i--) {
            int j = rand.nextInt(i + 1);
            swap(a, i, j);
        }
    }
    
    static void swap(int[] a, int i, int j) {
        int tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }

}
