package problems.geeks.array.basic;

/**
 * Arrange given numbers to form the biggest number
 * 
 * http://www.geeksforgeeks.org/given-an-array-of-numbers-arrange-the-numbers-to-form-the-biggest-number/
 * 
 * Given an array of numbers, arrange them in a way that yields the largest value. 
 * For example, if the given numbers are {54, 546, 548, 60}, the arrangement 6054854654 gives the largest value. 
 * And if the given numbers are {1, 34, 3, 98, 9, 76, 45, 4}, then the arrangement 998764543431 gives the largest value.
 */

import java.util.*;

public class Q043_Form_The_Biggest_Number {
    
    public static void main(String[] args) {
        String[] a = {"1", "34", "3", "98", "9", "76", "45", "4"};
        
        Arrays.sort(a, new Comparator<String>() {
            @Override
            public int compare(String a, String b) {
                return b.compareTo(a);
            }
        });
        
        System.out.println(Arrays.toString(a));
    }

}
