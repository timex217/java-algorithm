package problems.geeks.array.matrix;

/**
 * Turn an image by 90 degree
 * 
 * http://www.geeksforgeeks.org/turn-an-image-by-90-degree/
 * 
 * Given an image, how will you turn it by 90 degrees? A vague question. 
 * Minimize the browser and try your solution before going further.
 * 
 * An image can be treated as 2D matrix which can be stored in a buffer. 
 * We are provided with matrix dimensions and it�s base address. How can we turn it?
 */

import java.util.*;

public class Q089_Turn_Image_By_90_Degree {
	
	public static void main(String[] args) {
		int[][] M = {
			{ 1,  2,  3,  4, 5},
			{16, 17, 18, 19, 6},
			{15, 24, 25, 20, 7},
			{14, 23, 22, 21, 8},
			{13, 12, 11, 10, 9}
		};
		
		rotateImage(M, 5);
		
		for (int i = 0; i < 5; i++) {
			System.out.println(Arrays.toString(M[i]));
		}
	}
	
	static void rotateImage(int[][] M, int n) {
		rotateImage(M, n, 0);
	}
	
	static void rotateImage(int[][] M, int n, int k) {
		if (n <= 1) {
			return;
		}
		
		for (int i = 0; i < n - 1; i++) {
			int tmp = M[k][k + i];
			
			// left to top
			M[k][k + i] = M[k + n - 1 - i][k];
			
			// bottom to left
			M[k + n - 1 - i][k] = M[k + n - 1][k + n - 1 - i];
			
			// right to bottom
			M[k + n - 1][k + n - 1 - i] = M[k + i][k + n - 1];
			
			// top to right
			M[k + i][k + n - 1] = tmp;
		}
		
		rotateImage(M, n - 2, k + 1);
	}
	
}
