package problems.geeks.array.matrix;

/**
 * Inplace M x N size matrix transpose
 * 
 * http://www.geeksforgeeks.org/inplace-m-x-n-size-matrix-transpose/
 * 
 * About four months of gap (missing GFG), a new post. Given an M x N matrix, transpose the matrix without auxiliary memory.It is easy to transpose matrix using an auxiliary array. If the matrix is symmetric in size, we can transpose the matrix inplace by mirroring the 2D array across it�s diagonal (try yourself). How to transpose an arbitrary size matrix inplace? See the following matrix,
 * 
 * a b c       a d g j
 * d e f  ==>  b e h k
 * g h i       c f i l
 * j k l
 */

public class Q044_Inplace_Rotate_MXN_Matrix {
    
    public static void main(String[] args) {
        
    }

}
