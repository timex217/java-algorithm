package problems.geeks.array.matrix;

/**
 * Maximum size square sub-matrix with all 1s
 * 
 * http://www.geeksforgeeks.org/maximum-size-sub-matrix-with-all-1s-in-a-binary-matrix/
 * 
 * Given a binary matrix, find out the maximum size square sub-matrix with all 1s.
 * 
 * For example, consider the below binary matrix.
 *  
 *    0  1  1  0  1 
 *    1  1  0  1  0 
 *    0  1  1  1  0
 *    1  1  1  1  0
 *    1  1  1  1  1
 *    0  0  0  0  0
 *    
 * The maximum square sub-matrix with all set bits is
 * 
 *    1  1  1
 *    1  1  1
 *    1  1  1
 */

public class Q100_Maximum_Size_Square_Submatrix_With_All_1s {
	
	public static void main(String[] args) {
		int[][] M = {
			{0, 1, 1, 0, 1},
			{1, 1, 0, 1, 0},
			{0, 1, 1, 1, 0},
			{1, 1, 1, 1, 0},
			{1, 1, 1, 1, 1},
			{0, 0, 0, 0, 0}
		};
		
		int res = maxSquareSize(M, 6, 5);
		System.out.println(res);
	}
	
	static int maxSquareSize(int[][] M, int m, int n) {
		if (M == null || m <= 0 || n <= 0) {
			return 0;
		}
		
		int[][] S = new int[m][n];
		int max = 0;
		
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (M[i][j] == 1) {
					if (i == 0 || j == 0) {
						S[i][j] = 1;
					} else {
						S[i][j] = min(S[i - 1][j - 1], S[i - 1][j], S[i][j - 1]) + 1;
					}
					max = Math.max(max, S[i][j]);
				}
			}
		}
		
		return max;
	}
	
	static int min(int a, int b, int c) {
		return Math.min(a, Math.min(b, c));
	}
	
}
