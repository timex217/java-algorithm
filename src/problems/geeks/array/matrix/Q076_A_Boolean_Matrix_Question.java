package problems.geeks.array.matrix;

/**
 * A Boolean Matrix Question
 * 
 * http://www.geeksforgeeks.org/a-boolean-matrix-question/
 * 
 * Given a boolean matrix mat[M][N] of size M X N, modify it such that if a matrix cell mat[i][j] is 1 (or true) then make all the cells 
 * of ith row and jth column as 1.
 * 
 * Example 1
 * The matrix
 * 1 0
 * 0 0
 * should be changed to following
 * 1 1
 * 1 0
 * 
 * Example 2
 * The matrix
 * 0 0 0
 * 0 0 1
 * should be changed to following
 * 0 0 1
 * 1 1 1
 * 
 * Example 3
 * The matrix
 * 1 0 0 1
 * 0 0 1 0
 * 0 0 0 0
 * should be changed to following
 * 1 1 1 1
 * 1 1 1 1
 * 1 0 1 1
 */

import java.util.*;

public class Q076_A_Boolean_Matrix_Question {
	
	public static void main(String[] args) {
		int[][] M = {
			{1, 0, 0, 1},
			{0, 0, 1, 0},
			{0, 0, 0, 0}
		};
		
		int m = 3, n = 4;
		modifyMatrix(M, m, n);
		
		for (int i = 0; i < m; i++) {
			System.out.println(Arrays.toString(M[i]));
		}
	}
	
	static void modifyMatrix(int[][] M, int m, int n) {
		int[] row = new int[m];
		int[] col = new int[n];
		
		// step 1. go through M and mark row and col
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (M[i][j] == 1) {
					row[i] = 1;
					col[j] = 1;
				}
			}
		}
		
		// step 2. go through M again and mark 1 if row or col is 1
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (row[i] == 1 || col[j] == 1) {
					M[i][j] = 1;
				}
			}
		}
	}
	
}
