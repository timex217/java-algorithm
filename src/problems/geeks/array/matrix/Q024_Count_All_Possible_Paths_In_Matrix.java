package problems.geeks.array.matrix;

/**
 * Count all possible paths from top left to bottom right of a mXn matrix
 * 
 * http://www.geeksforgeeks.org/count-possible-paths-top-left-bottom-right-nxm-matrix/
 * 
 * The problem is to count all the possible paths from top left to bottom right of a mXn matrix 
 * with the constraints that from each cell you can either move only to right or down
 */

public class Q024_Count_All_Possible_Paths_In_Matrix {
    
    public static void main(String[] args) {
        int res = numberOfPaths(3, 3);
        System.out.println(res);
    }
    
    // ---------------------
    //  Recursion
    // ---------------------
    static int numberOfPathsR(int m, int n) {
        if (m < 1 || n < 1)   return 0;
        if (m == 1 && n == 1) return 1;
        return numberOfPathsR(m - 1, n) + numberOfPathsR(m, n - 1);
    }
    
    // ----------------------
    //  Dynamic Programming
    // ----------------------
    static int numberOfPaths(int m, int n) {
        int[][] count = new int[m][n];
        
        // base case 1, 只有一列
        for (int i = 0; i < m; i++) {
            count[i][0] = 1;
        }
        
        // base case 2, 只有一行
        for (int j = 0; j < n; j++) {
            count[0][j] = 1;
        }
        
        // recurrence
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                count[i][j] = count[i - 1][j] + count[i][j - 1];
            }
        }
        
        return count[m - 1][n - 1];
    }

}
