package problems.geeks.array.matrix;

/**
 * Print a given matrix in spiral form
 * 
 * http://www.geeksforgeeks.org/print-a-given-matrix-in-spiral-form/
 * 
 * Given a 2D array, print it in spiral form. See the following examples.
 * 
 * Input:
 *         1    2   3   4
 *         5    6   7   8
 *         9   10  11  12
 *         13  14  15  16
 * Output: 
 * 1 2 3 4 8 12 16 15 14 13 9 5 6 7 11 10 
 * 
 * 
 * Input:
 *         1   2   3   4  5   6
 *         7   8   9  10  11  12
 *         13  14  15 16  17  18
 * Output: 
 * 1 2 3 4 5 6 12 18 17 16 15 14 13 7 8 9 10 11
 */

public class Q077_Spiral_Print_Matrix {
	
	public static void main(String[] args) {
		int[][] M = {
			{1, 2, 3, 4},
			{5, 6, 7, 8},
			{9, 10, 11, 12},
			{13, 14, 15, 16}
		};
		
		spiralPrint(M, 4, 4);
	}
	
	static void spiralPrint(int[][] M, int m, int n) {
		helper(M, m, n, 0);
	}
	
	static void helper(int[][] M, int m, int n, int k) {
		if (m < 0 || n < 0) {
			return;
		}
		
		if (m == 1) {
			for (int j = 0; j < n; j++) {
				System.out.format("%d ", M[k][j + k]);
			}
			return;
		}
		
		if (n == 1) {
			for (int i = 0; i < m; i++) {
				System.out.format("%d ", M[i + k][k]);
			}
		}
		
		// top
		for (int j = 0; j < n - 1; j++) {
			System.out.format("%d ", M[k][j + k]);
		}
		
		// right
		for (int i = 0; i < m - 1; i++) {
			System.out.format("%d ", M[i + k][n - 1 + k]);
		}
		
		// bottom
		for (int j = n - 1; j > 0; j--) {
			System.out.format("%d ", M[m - 1 + k][j + k]);
		}
		
		// left
		for (int i = m - 1; i > 0; i--) {
			System.out.format("%d ", M[i + k][k]);
		}
		
		helper(M, m - 2, n - 2, k + 1);
	}
}
