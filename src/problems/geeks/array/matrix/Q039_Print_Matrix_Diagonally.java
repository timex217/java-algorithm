package problems.geeks.array.matrix;

/**
 * Print Matrix Diagonally
 * 
 * http://www.geeksforgeeks.org/print-matrix-diagonally/
 * 
 * Given a 2D matrix, print all elements of the given matrix in diagonal order. 
 * 
 * For example, consider the following 5 X 4 input matrix.
 * 
 *  1     2     3     4
 *  5     6     7     8
 *  9    10    11    12
 * 13    14    15    16
 * 17    18    19    20
 * 
 * Diagonal printing of the above matrix is
 * 
 *  1
 *  5     2
 *  9     6     3
 * 13    10     7     4
 * 17    14    11     8
 * 18    15    12
 * 19    16
 * 20
 */

import java.util.*;

public class Q039_Print_Matrix_Diagonally {
    
    public static void main(String[] args) {
        int[][] M = {
            {1, 2, 3, 4},
            {5, 6, 7, 8},
            {9, 10, 11, 12},
            {13, 14, 15, 16},
            {17, 18, 19, 20}
        };
        printMatrixDiagonally(M, 5, 4);
    }
    
    static void printMatrixDiagonally(int[][] M, int m, int n) {
        int i = 0, j = 0;
        while (i < m && j < n) {    
            helper(M, m, n, i, j);
            if (i == m - 1)
                j++;
            else
                i++;
        }
    }
    
    static void helper(int[][] M, int m, int n, int i, int j) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        while (i >= 0 && j < n)
            list.add(M[i--][j++]);
        System.out.println(list.toString());
    }

}
