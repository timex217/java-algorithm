package problems.geeks.array.matrix;

/**
 * Divide and Conquer | Set 6 (Search in a Row-wise and Column-wise Sorted 2D Array)
 * 
 * Given an n x n matrix, where every row and column is sorted in increasing order. 
 * Given a key, how to decide whether this key is in the matrix. 
 */

public class Q013_Search_In_Sorted_Matrix {
    
    public static void main(String[] args) {
        int[][] M = {
            {10, 20, 30, 40}, 
            {15, 25, 35, 45},
            {27, 29, 37, 48},
            {32, 33, 39, 50}
        };
        
        //boolean res = search(M, 4, 29);
        boolean res = searchR(M, 4, 0, 3, 29);
        System.out.println(res);
    }
    
    static boolean search(int[][] M, int n, int key) {
        if (M == null || n <= 0) 
            return false;
        
        int i = 0, j = n - 1;
        
        while (i < n && j >= 0) {
            if (M[i][j] == key) 
                return true;
            
            if (M[i][j] > key) 
                j--;
            else
                i++;
        }
        
        return false;
    }
    
    // ------------------------
    //  Recursion
    // ------------------------
    static boolean searchR(int[][] M, int n, int i, int j, int key) {
        if (i >= n || j < 0) return false;
        
        System.out.println(M[i][j]);
        
        if (M[i][j] == key) 
            return true;
        
        if (key < M[i][j]) 
            return searchR(M, n, i, j - 1, key);
        else
            return searchR(M, n, i + 1, j, key);
    }

}
