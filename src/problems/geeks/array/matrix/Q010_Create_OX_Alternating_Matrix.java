package problems.geeks.array.matrix;

/**
 * Create a matrix with alternating rectangles of O and X
 * 
 * Write a code which inputs two numbers m and n and creates a matrix of size m x n (m rows and n columns) in which every elements is either X or 0. 
 * The Xs and 0s must be filled alternatively, the matrix should have outermost rectangle of Xs, then a rectangle of 0s, then a rectangle of Xs, and so on.
 *
 * Examples:
 * 
 * Input: m = 3, n = 3
 * Output: Following matrix 
 * X X X
 * X 0 X
 * X X X
 * 
 * Input: m = 4, n = 5
 * Output: Following matrix
 * X X X X X
 * X 0 0 0 X
 * X 0 0 0 X
 * X X X X X
 * 
 * Input:  m = 5, n = 5
 * Output: Following matrix
 * X X X X X
 * X 0 0 0 X
 * X 0 X 0 X
 * X 0 0 0 X
 * X X X X X
 * 
 * Input:  m = 6, n = 7
 * Output: Following matrix
 * X X X X X X X
 * X 0 0 0 0 0 X
 * X 0 X X X 0 X
 * X 0 X X X 0 X
 * X 0 0 0 0 0 X
 * X X X X X X X 
 */

import java.util.*;

public class Q010_Create_OX_Alternating_Matrix {
    
    public static void main(String[] args) {
        int m = 6, n = 7;
        char[][] M = new char[m][n];
        
        fillOX(M, m, n, 0);
        
        for (int i = 0; i < m; i++) {
            System.out.println(Arrays.toString(M[i]));
        }
    }
    
    static void fillOX(char[][] M, int m, int n, int k) {
        if (m <= 0 || n <= 0) {
            return;
        }
        
        char ch = (k % 2 == 0) ? 'X' : 'O';
        
        if (m == 1) {
            for (int j = 0; j < n; j++) M[k][j + k] = ch;
            return;
        }
        
        if (n == 1) {
            for (int i = 0; i < m; i++) M[i + k][k] = ch;
            return;
        }
        
        // top
        for (int j = 0; j < n - 1; j++) M[k][j + k] = ch;
        // right
        for (int i = 0; i < m - 1; i++) M[i + k][n - 1 + k] = ch;
        // bottom
        for (int j = n - 1; j > 0; j--) M[m - 1 + k][j + k] = ch;
        // left
        for (int i = m - 1; i > 0; i--) M[i + k][k] = ch;
        
        // recursion
        fillOX(M, m - 2, n - 2, k + 1);
    }

}
