package problems.geeks.array.matrix;

/**
 * Search in a row wise and column wise sorted matrix
 * 
 * http://www.geeksforgeeks.org/search-in-row-wise-and-column-wise-sorted-matrix/
 * 
 * Given an n x n matrix, where every row and column is sorted in increasing order. 
 * Given a number x, how to decide whether this x is in the matrix. 
 * The designed algorithm should have linear time complexity.
 */

public class Q088_Search_In_Sorted_Matrix {
	
	public static void main(String[] args) {
		int[][] M = {
			{10, 20, 30, 40},
            {15, 25, 35, 45},
            {27, 29, 37, 48},
            {32, 33, 39, 50}
		};
		
		boolean res = search(M, 4, 4, 29);
		System.out.println(res);
	}
	
	static boolean search(int[][] M, int m, int n, int x) {
		int i = 0, j = n - 1;
		
		while (i < m && j >= 0) {
			if (M[i][j] == x) {
				return true;
			} else if (M[i][j] < x) {
				i++;
			} else {
				j--;
			}
		}
		
		return false;
	}
	
}
