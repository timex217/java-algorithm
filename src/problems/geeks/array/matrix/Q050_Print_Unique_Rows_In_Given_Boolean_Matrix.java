package problems.geeks.array.matrix;

/**
 * Print unique rows in a given boolean matrix
 * 
 * http://www.geeksforgeeks.org/print-unique-rows/
 * 
 * Given a binary matrix, print all unique rows of the given matrix.
 * 
 * Input:
 *     {0, 1, 0, 0, 1}
 *     {1, 0, 1, 1, 0}
 *     {0, 1, 0, 0, 1}
 *     {1, 1, 1, 0, 0}
 *     
 * Output:
 *     0 1 0 0 1 
 *     1 0 1 1 0 
 *     1 1 1 0 0 
 */

import java.util.*;

public class Q050_Print_Unique_Rows_In_Given_Boolean_Matrix {
    
    public static void main(String[] args) {
        int[][] M = {
            {0, 1, 0, 0, 1},
            {1, 0, 1, 1, 0},
            {0, 1, 0, 0, 1},
            {1, 1, 1, 0, 0}
        };
        
        printUniqueRows(M, 4, 5);
    }
    
    static void printUniqueRows(int[][] M, int m, int n) {
        if (m <= 0 || n <= 0) {
            return;
        }
        
        // use a hash set
        Set<String> set = new HashSet<String>();
        
        for (int i = 0; i < m; i++) {
            StringBuilder sb = new StringBuilder();
            
            for (int j = 0; j < n; j++) {
                sb.append(M[i][j]);
            }
            
            String key = sb.toString();
            
            if (!set.contains(key)) {
                set.add(key);
                System.out.println(Arrays.toString(M[i]));
            }
        }
    }

}
