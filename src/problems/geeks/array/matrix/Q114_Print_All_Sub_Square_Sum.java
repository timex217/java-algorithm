package problems.geeks.array.matrix;

/**
 * Given an n x n square matrix, find sum of all sub-squares of size k x k
 * 
 * http://www.geeksforgeeks.org/given-n-x-n-square-matrix-find-sum-sub-squares-size-k-x-k/
 * 
 * Given an n x n square matrix, find sum of all sub-squares of size k x k where k is smaller than or equal to n.
 * 
 * Examples
 * 
 * Input:
 * n = 5, k = 3
 * arr[][] = { {1, 1, 1, 1, 1},
 *             {2, 2, 2, 2, 2},
 *             {3, 3, 3, 3, 3},
 *             {4, 4, 4, 4, 4},
 *             {5, 5, 5, 5, 5},
 *          };
 * Output:
 *        18  18  18
 *        27  27  27
 *        36  36  36
 * 
 * 
 * Input:
 * n = 3, k = 2
 * arr[][] = { {1, 2, 3},
 *             {4, 5, 6},
 *             {7, 8, 9},
 *          };
 * Output:
 *        12  16
 *        24  28
 */

import java.util.*;

public class Q114_Print_All_Sub_Square_Sum {
	
	public static void main(String[] args) {
		int[][] M = {
			{1, 1, 1, 1, 1},
			{2, 2, 2, 2, 2},
            {3, 3, 3, 3, 3},
            {4, 4, 4, 4, 4},
            {5, 5, 5, 5, 5},
        };
		
		int n = 5, k = 3;
		printAllSubSquareSum(M, n, k);
	}
	
	static void printAllSubSquareSum(int[][] M, int n, int k) {
		if (k > n) {
			return;
		}
		
		int m = n - k + 1;
		
		// step 1. calculate the vertical sum
		int[][] sum = new int[n][n];
		
		for (int j = 0; j < n; j++) {
			// initialize the first row
			int s = 0;
			for (int i = 0; i < k; i++) {
				s += M[i][j];
			}
			sum[0][j] = s;
			
			// calculate the rest rows
			for (int i = 1; i < m; i++) {
				s += M[i + k - 1][j] - M[i - 1][j];
				sum[i][j] = s;
			}
		}
		
		// step 2. print sub square sum
		int[][] res = new int[m][m];
		
		for (int i = 0; i < m; i++) {
			// initialize first column
			int s = 0;
			for (int j = 0; j < k; j++) {
				s += sum[i][j];
			}
			res[i][0] = s;
			
			// calculate the rest columns
			for (int j = 1; j < m; j++) {
				s += sum[i][j + k - 1] - sum[i][j - 1];
				res[i][j] = s;
			}
			
			System.out.println(Arrays.toString(res[i]));
		}
		
	}
	
}
