package problems.geeks.array.minmax;

/**
 * Maximum Subarray Sum
 * 
 * http://www.geeksforgeeks.org/divide-and-conquer-maximum-sum-subarray/
 * 
 * You are given a one dimensional array that may contain both positive and negative integers, 
 * find the sum of contiguous subarray of numbers which has the largest sum.
 * 
 * For example, if the given array is {-2, -5, 6, -2, -3, 1, 5, -6}, then the maximum subarray sum is 7 
 */

public class Q038_Maximum_Subarray_Sum {
    
    public static void main(String[] args) {
        int[] a = {-2, -5, 6, -2, -3, 1, 5, -6};
        int res = maxSubArraySum(a);
        System.out.println(res);
    }
    
    static int maxSubArraySum(int[] a) {
        if (a == null || a.length == 0) {
            return Integer.MIN_VALUE;
        }
        
        int curr_sum = a[0];
        int max_sum = a[0];
        
        for (int i = 1; i < a.length; i++) {
            if (a[i] >= curr_sum + a[i]) {
                curr_sum = a[i];
            } else {
                curr_sum += a[i];
            }
            
            max_sum = Math.max(max_sum, curr_sum);
        }
        
        return max_sum;
    }

}
