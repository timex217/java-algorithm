package problems.geeks.array.minmax;

/**
 * Given an unsorted array, find the maximum difference between the successive elements in its sorted form.
 * 
 * Try to solve it in linear time/space.
 * 
 * Return 0 if the array contains less than 2 elements.
 * 
 * You may assume all elements in the array are non-negative integers and fit in the 32-bit signed integer range.
 */

import java.util.*;

public class Q200_Max_Gap_In_Unsorted_Array {
	static class Bucket {
        int min;
        int max;
        Bucket(int x, int y) {
            min = x;
            max = y;
        }
    }
	
	static int maximumGap(int[] a) {
        if (a == null || a.length < 2) {
            return 0;
        }
        
        // step 1. find out the min and max in a[] and calculate the bucket length
        int min = a[0], max = a[0];
        
        for (int i = 1; i < a.length; i++) {
            min = Math.min(min, a[i]);
            max = Math.max(max, a[i]);
        }
        
        double len = (double)(max - min) / (double)(a.length - 1);
        System.out.format("len = %f\n\n", len);
        
        // step 2. create bucket map that tracks the min & max of each bucket
        Map<Integer, Bucket> map = new TreeMap<Integer, Bucket>();
        
        for (int i = 0; i < a.length; i++) {
            int k = (int)(a[i] / len);
            
            System.out.format("%d [%d]\n", a[i], k);
            
            if (map.containsKey(k)) {
                Bucket bucket = map.get(k);
                bucket.min = Math.min(bucket.min, a[i]);
                bucket.max = Math.max(bucket.max, a[i]);
            } else {
                map.put(k, new Bucket(a[i], a[i]));
            }
        }
        
        // step 3. find out the max gap
        int res = 0;
        
        Bucket prev = null, curr = null;
        
        for (Map.Entry<Integer, Bucket> entry : map.entrySet()) {
        	curr = entry.getValue();
        	
        	System.out.println();
        	System.out.format("[%d] min: %d, max: %d\n", entry.getKey(), curr.min, curr.max);
        	
        	if (prev != null) {
        		res = Math.max(res, curr.min - prev.max);
        	}
        	
        	prev = curr;
        }
        
        return res;
    }
	
	public static void main(String[] args) {
		int[] a = {15252,16764,27963,7817,26155,20757,3478,22602,20404,6739,16790,10588,16521,6644,20880,15632,27078,25463,20124,15728,30042,16604,17223,4388,23646,32683,23688,12439,30630,3895,7926,22101,32406,21540,31799,3768,26679,21799,23740};
		int res = maximumGap(a);
		System.out.println(res);
	}
}
