package problems.geeks.array.minmax;

/**
 * Longest Bitonic Subsequence
 * 
 * http://www.geeksforgeeks.org/dynamic-programming-set-15-longest-bitonic-subsequence/
 * 
 * Given an array arr[0 ... n-1] containing n positive integers, a subsequence of arr[] is called Bitonic if it is first increasing, then decreasing. 
 * Write a function that takes an array as argument and returns the length of the longest bitonic subsequence.
 * 
 * A sequence, sorted in increasing order is considered Bitonic with the decreasing part as empty. 
 * Similarly, decreasing order sequence is considered Bitonic with the increasing part as empty.
 * 
 * Examples:
 * 
 * Input arr[] = {1, 11, 2, 10, 4, 5, 2, 1};
 * Output: 6 (A Longest Bitonic Subsequence of length 6 is 1, 2, 10, 4, 2, 1)
 * 
 * Input arr[] = {12, 11, 40, 5, 3, 1}
 * Output: 5 (A Longest Bitonic Subsequence of length 5 is 12, 11, 5, 3, 1)
 * 
 * Input arr[] = {80, 60, 30, 40, 20, 10}
 * Output: 5 (A Longest Bitonic Subsequence of length 5 is 80, 60, 30, 20, 10)
 * 
 * Solution
 * This problem is a variation of standard Longest Increasing Subsequence (LIS) problem. 
 * Let the input array be arr[] of length n. We need to construct two arrays lis[] and lds[] using Dynamic Programming solution of LIS problem. 
 * lis[i] stores the length of the Longest Increasing subsequence ending with arr[i]. 
 * lds[i] stores the length of the longest Decreasing subsequence starting from arr[i]. 
 * Finally, we need to return the max value of lis[i] + lds[i] � 1 where i is from 0 to n-1.
 */
	
public class Q063_Longest_Bitonic_Subsequence {
	
	public static void main(String[] args) {
		int[] a = {1, 11, 2, 10, 4, 5, 2, 1};
		int res = lbs(a);
		System.out.println(res);
	}
	
	static int lbs(int[] a) {
		if (a == null) {
			return 0;
		}
		
		int n = a.length;
		
		// construct the lis (longest increasing subsequence)
		int[] lis = new int[n];
		
		for (int i = 0; i < n; i++) {
			lis[i] = 1;
			
			for (int j = 0; j < i; j++) {
				if (a[i] >= a[j]) {
					lis[i] = Math.max(lis[i], lis[j] + 1);
				}
			}
		}
		
		// construct the lds (longest decreasing subsequence)
		int[] lds = new int[n];
		
		for (int i = n - 1; i >= 0; i--) {
			lds[i] = 1;
			
			for (int j = n - 1; j > i; j--) {
				if (a[i] >= a[j]) {
					lds[i] = Math.max(lds[i], lds[j] + 1);
				}
			}
		}
		
		// go through both lis and lds and get the max lis + lds - 1
		int max = 1;
		
		for (int i = 0; i < n; i++) {
			max = Math.max(max, lis[i] + lds[i] - 1);
		}
		
		return max;
	}
}
