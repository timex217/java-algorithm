package problems.geeks.array.minmax;

/**
 * Maximum of all subarrays of size k (Sliding Window problem)
 * 
 * http://www.geeksforgeeks.org/maximum-of-all-subarrays-of-size-k/
 * 
 * Given an array and an integer k, find the maximum for each and every contiguous subarray of size k.
 * 
 * Examples:
 * 
 * Input :
 * arr[] = {1, 2, 3, 1, 4, 5, 2, 3, 6}
 * k = 3
 * Output :
 * 3 3 4 5 5 5 6
 * 
 * Input :
 * arr[] = {8, 5, 10, 7, 9, 4, 15, 12, 90, 13}
 * k = 4
 * Output :
 * 10 10 10 15 15 90 90
 */

import java.util.*;

public class Q081_Maximum_Of_All_Subarrays_Of_Size_K {
	
	public static void main(String[] args) {
		int[] a = {1, 2, 3, 1, 4, 5, 2, 3, 6};
		int k = 3;
		slideWindow(a, k);
	}
	
	static void slideWindow(int[] a, int k) {
		if (a == null) {
			return;
		}
		
		ArrayList<Integer> list = new ArrayList<Integer>();
		
		int n = a.length;
		
		// use a deque to help track, deque stores the index
		Deque<Integer> q = new ArrayDeque<Integer>();
		
		// initialize the deque
		for (int i = 0; i < k; i++) {
			while (!q.isEmpty() && a[q.peekLast()] <= a[i]) {
				q.removeLast();
			}
			q.add(i);
		}
		
		// go through till the end
		for (int i = k; i < n; i++) {
			list.add(a[q.peekFirst()]);
			
			while (!q.isEmpty() && a[q.peekLast()] <= a[i]) {
				q.removeLast();
			}
			
			while (!q.isEmpty() && i - q.peekFirst() >= k) {
				q.removeFirst();
			}
			
			q.add(i);
		}
		
		list.add(a[q.peekFirst()]);
		
		System.out.println(list.toString());
	}
	
}
