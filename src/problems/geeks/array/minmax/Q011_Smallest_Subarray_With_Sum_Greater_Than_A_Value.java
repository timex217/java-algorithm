package problems.geeks.array.minmax;

/**
 * Smallest subarray with sum greater than a given value
 * 
 * Given an array of integers and a number x, find the smallest subarray with sum greater than the given value.
 * 
 * Examples:
 * arr[] = {1, 4, 45, 6, 0, 19}
 *    x  =  51
 * Output: 3
 * Minimum length subarray is {4, 45, 6}
 * 
 * arr[] = {1, 10, 5, 2, 7}
 *    x  = 9
 * Output: 1
 * Minimum length subarray is {10}
 * 
 * arr[] = {1, 11, 100, 1, 0, 200, 3, 2, 1, 250}
 *     x = 280
 * Output: 4
 * Minimum length subarray is {100, 1, 0, 200}
 */

public class Q011_Smallest_Subarray_With_Sum_Greater_Than_A_Value {
    
    public static void main(String[] args) {
        int[] a = {1, 11, 100, 1, 0, 200, 3, 2, 1, 250};
        int res = smallestSubArrayWithSum(a, 280);
        System.out.println(res);
    }
    
    static int smallestSubArrayWithSum(int[] a, int x) {
        if (a == null) {
            return -1;
        }
        
        int n = a.length;
        int sum = 0, min_len = n + 1;
        int i = 0, j = 0;
        
        while (j < n) {
            // Keep adding array elements while current sum
            // is smaller than x
            while (sum <= x && j < n) {
                sum += a[j++];
            }
            
            // If current sum becomes greater than x.
            while (sum > x && i < n) {
                min_len = Math.min(min_len, j - i);
                // remove starting elements
                sum -= a[i++];
            }
        }
        
        return min_len;
    }

}
