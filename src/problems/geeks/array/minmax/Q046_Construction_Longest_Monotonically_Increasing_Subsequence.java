package problems.geeks.array.minmax;

/**
 * Longest Monotonically Increasing Subsequence Size (N log N)
 * 
 * http://www.geeksforgeeks.org/longest-monotonically-increasing-subsequence-size-n-log-n/
 * 
 * Given an array of random numbers. Find longest monotonically increasing subsequence (LIS) in the array.
 */

public class Q046_Construction_Longest_Monotonically_Increasing_Subsequence {
    
    public static void main(String[] args) {
        
    }

}
