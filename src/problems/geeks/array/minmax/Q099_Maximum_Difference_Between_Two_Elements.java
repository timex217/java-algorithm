package problems.geeks.array.minmax;

/**
 * Maximum difference between two elements
 * 
 * http://www.geeksforgeeks.org/maximum-difference-between-two-elements/
 * 
 * Given an array arr[] of integers, find out the max difference between any two elements such that larger element appears after the smaller number in arr[].
 * 
 * Examples: If array is [2, 3, 10, 6, 4, 8, 1] then returned value should be 8 (Diff between 10 and 2). If array is [ 7, 9, 5, 6, 3, 2 ] 
 * then returned value should be 2 (Diff between 7 and 9)
 */

import java.util.*;

public class Q099_Maximum_Difference_Between_Two_Elements {

	public static void main(String[] args) {
		int[] a = {2, 3, 10, 6, 4, 8, 1};
		int res = getMaxDiff(a, 7);
		System.out.println(res);
	}
	
	static int getMaxDiff(int[] a, int n) {
		int min_index = 0;
		int max = 0;
		
		for (int i = 0; i < n; i++) {
			if (a[i] < a[min_index]) {
				min_index = i;
			}
			
			int diff = a[i] - a[min_index];
			max = Math.max(max, diff);
		}
		
		return max;
	}
	
}
