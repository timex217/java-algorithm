package problems.geeks.array.minmax;

/**
 * Remove minimum elements from either side such that 2*min becomes more than max
 * 
 * Given an unsorted array, trim the array such that twice of minimum is greater than maximum in the trimmed array. 
 * Elements should be removed either end of the array.
 * 
 * Number of removals should be minimum.
 * 
 * Examples:
 * 
 * arr[] = {4, 5, 100, 9, 10, 11, 12, 15, 200}
 * Output: 4
 * We need to remove 4 elements (4, 5, 100, 200)
 * so that 2*min becomes more than max.
 * 
 * 
 * arr[] = {4, 7, 5, 6}
 * Output: 0
 * We don't need to remove any element as 
 * 4*2 > 7 (Note that min = 4, max = 8)
 * 
 * arr[] = {20, 7, 5, 6}
 * Output: 1
 * We need to remove 20 so that 2*min becomes
 * more than max
 * 
 * arr[] = {20, 4, 1, 3}
 * Output: 3
 * We need to remove any three elements from ends
 * like 20, 4, 1 or 4, 1, 3 or 20, 3, 1 or 20, 4, 1
 */

public class Q012_Remove_Minimum_Elements {
    
    public static void main(String[] args) {
        int[] a = {4, 5, 100, 9, 10, 11, 12, 15, 200};
        //int res = minRemovalsR(a, 0, a.length - 1);
        int res = minRemovals(a);
        System.out.println(res);
    }
    
    // ---------------
    //  Recursion
    // ---------------
    static int minRemovalsR(int[] a, int lo, int hi) {
        if (lo >= hi) {
            return 0;
        }
        
        int min = min(a, lo, hi);
        int max = max(a, lo, hi);
        
        if (2 * min > max) {
            return 0;
        }
        
        return Math.min(minRemovalsR(a, lo + 1, hi), minRemovalsR(a, lo, hi - 1)) + 1;
    }
    
    // -----------------------
    //  Dynamic Programming
    // -----------------------
    static int minRemovals(int[] a) {
        if (a == null || a.length == 0) {
            return 0;
        }
        
        // r(i, j) represents the minimum removals from i-th to j-th elements
        // r(i, j) = 0, i >= j or 2 * min(a, i, j) > max(a, i, j)
        // r(i, j) = min{ r(i+1, j), r(i, j-1) } + 1
        int n = a.length;
        int[][] r = new int[n][n];
        
        for (int i = n - 1; i >= 0; i--) {
            for (int j = i + 1; j < n; j++) {
                int min = min(a, i, j);
                int max = max(a, i, j);
                
                if (2 * min > max) {
                    r[i][j] = 0;
                } else {
                    r[i][j] = Math.min(r[i + 1][j], r[i][j - 1]) + 1;
                }
            }
        }
        
        return r[0][n - 1];
        
    }
    
    // -----------------------
    //  Helper functions
    // -----------------------
    static int min(int[] a, int lo, int hi) {
        int v = a[lo];
        for (int i = lo + 1; i <= hi; i++) {
            if (a[i] < v) v = a[i];
        }
        return v;
    }
    
    static int max(int[] a, int lo, int hi) {
        int v = a[lo];
        for (int i = lo + 1; i <= hi; i++) {
            if (a[i] > v) v = a[i];
        }
        return v;
    }

}
