package problems.geeks.array.minmax;

/**
 * Maximum and minimum of an array using minimum number of comparisons
 * 
 * http://www.geeksforgeeks.org/maximum-and-minimum-in-an-array/
 * 
 * Write a C function to return minimum and maximum in an array. You program should make minimum number of comparisons.
 * 
 * If n is odd:    3*(n-1)/2  
 * If n is even:   1 Initial comparison for initializing min and max, 
 *                 and 3(n-2)/2 comparisons for rest of the elements  
 *                 =  1 + 3*(n-2)/2 = 3n/2 -2
 */

import java.util.*;

public class Q103_Maximum_And_Minimum_Of_An_Array_Using_Minimum_Number_Of_Comparisons {
	
	static class Pair {
		int min;
		int max;
		
		@Override
		public String toString() {
			return String.format("min: %d, max: %d", min, max);
		}
	}
	
	public static void main(String[] args) {
		int[] a = {1000, 11, 445, 1, 330, 3000};
		Pair p = getMinMax(a, 6);
		System.out.println(p.toString());
	}
	
	// Compare in Pairs
	static Pair getMinMax(int[] a, int n) {
		if (n == 0) {
			return null;
		}
		
		int i = 0;
		Pair p = new Pair();
		
		if (n % 2 == 1) {
			// 如果是奇数，用第一个数来初始化pair
			p.min = a[0];
			p.max = a[0];
			
			i = 1;
		} else {
			// 如果是偶数，用前两个数初始化pair，此时比较了一次
			if (a[0] < a[1]) {
				p.min = a[0];
				p.max = a[1];
			} else {
				p.min = a[1];
				p.max = a[0];
			}
			
			i = 2;
		}
		
		while (i < n - 1) {
			// 比较一次
			if (a[i] < a[i + 1]) {
				// 比较两次
				if (a[i] < p.min)     p.min = a[i];
				if (a[i + 1] > p.max) p.max = a[i + 1];
			} else {
				if (a[i + 1] < p.min) p.min = a[i + 1];
				if (a[i] > p.max)     p.max = a[i];
			}
			
			i += 2;
		}
		
		return p;
	}
	
}
