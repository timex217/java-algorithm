package problems.geeks.array.minmax;

/**
 * Maximum Length Chain of Pairs
 * 
 * You are given n pairs of numbers. In every pair, the first number is always smaller than the second number. A pair (c, d) can follow another pair (a, b) if b < c. Chain of pairs can be formed in this fashion. Find the longest chain which can be formed from a given set of pairs.
 * Source: Amazon Interview | Set 2
 * 
 * For example, if the given pairs are {{5, 24}, {39, 60}, {15, 28}, {27, 40}, {50, 90} }, then the longest chain that can be formed is of length 3, and the chain is {{5, 24}, {27, 40}, {50, 90}}
 * 
 * This problem is a variation of standard Longest Increasing Subsequence problem. Following is a simple two step process.
 * 1) Sort given pairs in increasing order of first (or smaller) element.
 * 2) Now run a modified LIS process where we compare the second element of already finalized LIS with the first element of new LIS being constructed.
 *
 * 基本思想是LIS (Longest Increasing Sequence)
 */

import java.util.*;

public class Q056_Maximum_Length_Chain_Of_Pairs {
    
    static class Pair implements Comparable<Pair> {
        int a;
        int b;
        Pair(int x, int y) {
            this.a = x;
            this.b = y;
        }
        public int compareTo(Pair p) {
            return this.a - p.a;
        }
    }
    
    public static void main(String[] args) {
        ArrayList<Pair> pairs = new ArrayList<Pair>();
        
        pairs.add(new Pair(5, 24));
        pairs.add(new Pair(39, 60));
        pairs.add(new Pair(15, 28));
        pairs.add(new Pair(27, 40));
        pairs.add(new Pair(50, 90));
        
        int res = lcp(pairs);
        
        System.out.println(res);
    }
    
    // Longest Chain of Pairs
    static int lcp(ArrayList<Pair> pairs) {
        if (pairs == null || pairs.size() == 0) {
            return 0;
        }
        
        // sort the pairs
        Collections.sort(pairs);
        
        int n = pairs.size();
        
        int[] dp = new int[n];
        dp[0] = 1;
        
        int res = 1;
        
        for (int i = 1; i < n; i++) {
            dp[i] = 1;
            Pair pi = pairs.get(i);
            
            for (int j = 0; j < i; j++) {
                Pair pj = pairs.get(j);
                if (pj.b < pi.a) {
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                }
            }
            
            res = Math.max(dp[i], res);
        }
        
        return res;
    }

}
