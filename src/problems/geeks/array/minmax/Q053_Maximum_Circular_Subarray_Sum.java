package problems.geeks.array.minmax;

/**
 * Maximum circular subarray sum
 * 
 * http://www.geeksforgeeks.org/maximum-contiguous-circular-sum/
 * 
 * Given n numbers (both +ve and -ve), arranged in a circle, fnd the maximum sum of consecutive number.
 * 
 * Examples:
 * 
 * Input: a[] = {8, -8, 9, -9, 10, -11, 12}
 * Output: 22 (12 + 8 - 8 + 9 - 9 + 10)
 * 
 * Input: a[] = {10, -3, -4, 7, 6, 5, -4, -1} 
 * Output:  23 (7 + 6 + 5 - 4 -1 + 10) 
 * 
 * Input: a[] = {-1, 40, -14, 7, 6, 5, -4, -1}
 * Output: 52 (7 + 6 + 5 - 4 - 1 - 1 + 40)
 * There can be two cases for the maximum sum:
 * 
 * Case 1: The elements that contribute to the maximum sum are arranged such that no wrapping is there. 
 * Examples: {-10, 2, -1, 5}, {-2, 4, -1, 4, -1}. In this case, Kadane�s algorithm will produce the result.
 * 
 * Case 2: The elements which contribute to the maximum sum are arranged such that wrapping is there. 
 * Examples: {10, -12, 11}, {12, -5, 4, -8, 11}. In this case, we change wrapping to non-wrapping. Let us see how. 
 * Wrapping of contributing elements implies non wrapping of non contributing elements, 
 * so find out the sum of non contributing elements and subtract this sum from the total sum. 
 * To find out the sum of non contributing, invert sign of each element and then run Kadane�s algorithm.
 * Our array is like a ring and we have to eliminate the maximum continuous negative that implies maximum continuous positive in the inverted arrays.
 * 
 * Finally we compare the sum obtained by both cases, and return the maximum of the two sums.
 */

public class Q053_Maximum_Circular_Subarray_Sum {
    
    public static void main(String[] args) {
        int[] a = {8, -8, 9, -9, 10, -11, 12};
        int res = maxCircularSubarraySum(a);
        System.out.println(res);
    }
    
    static int maxCircularSubarraySum(int[] a) {
        int sum = 0, max1 = maxSubarraySum(a);
        
        for (int i = 0; i < a.length; i++) {
            sum += a[i];
            a[i] = -a[i];
        }
        
        int max2 = sum + maxSubarraySum(a);
        
        return Math.max(max1, max2);
    }
    
    static int maxSubarraySum(int[] a) {
        int sum = a[0], max = a[0];
        
        for (int i = 1; i < a.length; i++) {
            sum = (a[i] >= sum + a[i]) ? a[i] : sum + a[i];
            max = Math.max(max, sum);
        }
        
        return max;
    }

}
