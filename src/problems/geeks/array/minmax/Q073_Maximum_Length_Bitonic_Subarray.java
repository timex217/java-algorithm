package problems.geeks.array.minmax;

/**
 * Maximum Length Bitonic Subarray
 * 
 * http://www.geeksforgeeks.org/maximum-length-bitonic-subarray/
 * 
 * Given an array A[0 ... n-1] containing n positive integers, a subarray A[i ... j] is bitonic if there is a k with i <= k <= j 
 * such that A[i] <= A[i + 1] ... <= A[k] >= A[k + 1] >= .. A[j - 1] > = A[j]. 
 * Write a function that takes an array as argument and returns the length of the maximum length bitonic subarray.
 * Expected time complexity of the solution is O(n)
 * 
 * Simple Examples
 * 1) A[] = {12, 4, 78, 90, 45, 23}, the maximum length bitonic subarray is {4, 78, 90, 45, 23} which is of length 5.
 * 
 * 2) A[] = {20, 4, 1, 2, 3, 4, 2, 10}, the maximum length bitonic subarray is {1, 2, 3, 4, 2} which is of length 5.
 * 
 * Extreme Examples
 * 1) A[] = {10}, the single element is bitnoic, so output is 1.
 * 
 * 2) A[] = {10, 20, 30, 40}, the complete array itself is bitonic, so output is 4.
 * 
 * 3) A[] = {40, 30, 20, 10}, the complete array itself is bitonic, so output is 4.
 */

public class Q073_Maximum_Length_Bitonic_Subarray {
	
	public static void main(String[] args) {
		int[] a = {12, 4, 78, 90, 45, 23};
		int res = maxBitonicSubarray(a, 6);
		System.out.println(res);
	}
	
	static int maxBitonicSubarray(int[] a, int n) {
		// construct the lis array
		int[] lis = new int[n];
		lis[0] = 1;
		for (int i = 1; i < n; i++) {
			if (a[i] >= a[i - 1]) {
				lis[i] = lis[i - 1] + 1;
			} else {
				lis[i] = 1;
			}
		}
		
		// construct the lds array
		int[] lds = new int[n];
		lds[n - 1] = 1;
		for (int i = n - 2; i >= 0; i--) {
			if (a[i] >= a[i + 1]) {
				lds[i] = lds[i + 1] + 1;
			} else {
				lds[i] = 1;
			}
		}
		
		// go through smaller and greater and find the max lis + lds - 1
		int max = 1;
		for (int i = 0; i < n; i++) {
			max = Math.max(max, lis[i] + lds[i] - 1);
		}
		
		return max;
	}
	
}
