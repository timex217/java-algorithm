package problems.geeks.array.minmax;

/**
 * Largest subarray with equal number of 0s and 1s
 * 
 * Given an array containing only 0s and 1s, find the largest subarray which contain equal no of 0s and 1s. Expected time complexity is O(n).
 * 
 * Examples:
 * 
 * Input: arr[] = {1, 0, 1, 1, 1, 0, 0}
 * Output: 1 to 6 (Starting and Ending indexes of output subarray)
 * 
 * Input: arr[] = {1, 1, 1, 1}
 * Output: No such subarray
 * 
 * Input: arr[] = {0, 0, 1, 1, 0}
 * Output: 0 to 3 Or 1 to 4
 */

public class Q061_Largest_Subarray_With_Equal_Number_Of_0s_And_1s {
    
    public static void main(String[] args) {
        int[] a = {1, 0, 1, 1, 1, 0, 0};
        int res = findSubArray(a, 7);
        System.out.println(res);
    }
    
    static int findSubArray(int[] a, int n) {
        int max = 0;
        
        for (int i = 0; i < n - 1; i++) {
            int sum = a[i];
            
            for (int j = i + 1; j < n; j++) {
                sum += a[j];
                int count = j - i + 1;
                if (count % 2 == 0 && sum == count / 2) {
                    max = Math.max(max, count);
                }
            }
        }
        
        return max;
    }

}
