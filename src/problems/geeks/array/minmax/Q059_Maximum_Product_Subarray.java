package problems.geeks.array.minmax;

/**
 * Maximum Product Subarray
 * 
 * http://www.geeksforgeeks.org/maximum-product-subarray/
 * 
 * Given an array that contains both positive and negative integers, find the product of the maximum product subarray. Expected Time complexity is O(n) and only O(1) extra space can be used.
 * 
 * Examples:
 * 
 * Input:  arr[] = {6, -3, -10, 0, 2}
 * Output: 180  // The subarray is {6, -3, -10}
 * 
 * Input:  arr[] = {-1, -3, -10, 0, 60}
 * Output: 60  // The subarray is {60}
 * 
 * Input:  arr[] = {-2, -3, 0, -2, -40}
 * Output: 80  // The subarray is {-2, -40}
 */

public class Q059_Maximum_Product_Subarray {
    
    public static void main(String[] args) {
        //int[] a = {6, -3, -10, 0, 2};
        //int[] a = {-1, -3, -10, 0, 60};
        int[] a = {-2, -3, 0, -2, -40};
        int res = maxProductSubarray(a, 5);
        System.out.println(res);
    }
    
    static int maxProductSubarray(int[] a, int n) {
        int max = a[0];
        int curr_max = a[0];
        int curr_min = a[0];
        
        for (int i = 1; i < n; i++) {
            if (a[i] >= 0) {
                curr_max = Math.max(a[i], a[i] * curr_max);
                curr_min = Math.min(a[i], a[i] * curr_min);
            } else {
                int temp = curr_max;
                curr_max = Math.max(a[i], a[i] * curr_min);
                curr_min = Math.min(a[i], a[i] * temp);
            }
            max = Math.max(max, curr_max);
        }
        
        return max;
    }

}
