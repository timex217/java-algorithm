package problems.geeks.array.binarysearch;

/**
 * Floor and Ceiling in a sorted array
 * 
 * http://www.geeksforgeeks.org/search-floor-and-ceil-in-a-sorted-array/
 * 
 * Given a sorted array and a value x, the ceiling of x is the smallest element in array greater than or equal to x, 
 * and the floor is the greatest element smaller than or equal to x. Assume than the array is sorted in non-decreasing order. 
 * Write efficient functions to find floor and ceiling of x.
 * 
 * For example, let the input array be {1, 2, 8, 10, 10, 12, 19}
 * For x = 0:    floor doesn't exist in array,  ceil  = 1
 * For x = 1:    floor  = 1,   ceil  = 1
 * For x = 5:    floor  = 2,   ceil  = 8
 * For x = 20:   floor  = 19,  ceil doesn't exist in array
 * 
 * Solution: Binary Search
 */

public class Q097_BS_Floor_And_Ceiling_In_Sorted_Array {
	
	public static void main(String[] args) {
		int[] a = {1, 2, 8, 10, 10, 12, 19};
		int x = 9;
		int n = a.length;
		
		// get the floor
		int floor = floorSearch(a, 0, n - 1, x);
		if (floor == -1) {
			System.out.format("floor of %d doesn't exist.\n", x);
		} else {
			System.out.format("floor of %d is %d.\n", x, a[floor]);
		}
		
		// get the ceiling
		int ceil = ceilSearch(a, 0, n - 1, x);
		if (ceil == -1) {
			System.out.format("ceiling of %d doesn't exist.\n", x);
		} else {
			System.out.format("ceiling of %d is %d.\n", x, a[ceil]);
		}
	}
	
	// 找最小的一个数使它 >= x (similar to upper bound)
	static int ceilSearch(int[] a, int lo, int hi, int x) {
		/* If x is smaller than or equal to the first element,
	       then return the first element */
		if (x <= a[lo]) {
			return lo;
		}
		
		/* If x is greater than the last element, then return -1 */
		if (a[hi] < x) {
			return -1;
		}
		
		/* get the index of middle element of arr[low..high]*/
		int mid = lo + (hi - lo) / 2;
		
		/* If x is same as middle element, then return mid */
		if (a[mid] == x) {
			return mid;
		}
		
		/* If x is greater than arr[mid], then either arr[mid + 1]
	       is ceiling of x or ceiling lies in arr[mid+1...high] */
		if (a[mid] < x) {
			if (mid + 1 < a.length && x <= a[mid + 1]) {
				return mid + 1;
			} else {
				return ceilSearch(a, mid + 1, hi, x);
			}
		}
		
		/* If x is smaller than arr[mid], then either arr[mid] 
	       is ceiling of x or ceiling lies in arr[mid-1...high] */
		else {
			if (mid - 1 >= 0 && a[mid - 1] < x) {
				return mid;
			} else {
				return ceilSearch(a, lo, mid - 1, x);
			}
		}
	}
	
	// 找最大的数使它 <= x (similar to lower bound)
	static int floorSearch(int[] a, int lo, int hi, int x) {
		if (a[hi] <= x) {
			return hi;
		}
		
		if (a[lo] > x) {
			return -1;
		}
		
		int mid = lo + (hi - lo) / 2;
		
		if (a[mid] == x) {
			return mid;
		}
		
		if (x < a[mid]) {
			if (mid - 1 >= lo && a[mid - 1] <= x) {
				return mid - 1;
			} else {
				return floorSearch(a, lo, mid - 1, x);
			}
		}
		
		else {
			if (mid + 1 <= hi && x < a[mid + 1]) {
				return mid;
			} else {
				return floorSearch(a, mid + 1, hi, x);
			}
		}
	}
}
