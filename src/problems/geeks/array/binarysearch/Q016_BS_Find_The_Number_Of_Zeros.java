package problems.geeks.array.binarysearch;

/**
 * Find the number of zeroes
 * 
 * Given an array of 1s and 0s which has all 1s first followed by all 0s. 
 * Find the number of 0s. Count the number of zeroes in the given array.
 * 
 * Examples:
 * 
 * Input: arr[] = {1, 1, 1, 1, 0, 0}
 * Output: 2
 * 
 * Input: arr[] = {1, 0, 0, 0, 0}
 * Output: 4
 * 
 * Input: arr[] = {0, 0, 0}
 * Output: 3
 * 
 * Input: arr[] = {1, 1, 1, 1}
 * Output: 0
 */

public class Q016_BS_Find_The_Number_Of_Zeros {
    
    public static void main(String[] args) {
        int[] a = {1, 0, 0, 0, 0, 0};
        int res = countZeros(a);
        System.out.println(res);
    }
    
    static int countZeros(int[] a) {
        int p = firstZero(a);
        
        if (p == -1) 
            return 0;
        else
            return a.length - p;
    }
    
    // search lower bound of zero
    static int firstZero(int[] a) {
        if (a == null) {
            return -1;
        }
        
        int lo = 0, hi = a.length - 1;
        
        while (lo < hi) {
            int mid = lo + (hi - lo) / 2;
            
            if (a[mid] == 0) 
                hi = mid;
            else
                lo = mid + 1;
            
        }
        
        return (a[lo] == 0) ? lo : -1;
    }
    
    // -----------------------------
    //  Recursion
    // -----------------------------
    static int firstZeroR(int[] a, int lo, int hi) {
    	if (lo > hi) {
    		return -1;
    	}
        
        int mid = lo + (hi - lo) / 2;
        
        // Check if mid element is first 0
        if (a[mid] == 0 && (mid == 0 || a[mid - 1] == 1)) {
            return mid;
        }
        
        if (a[mid] == 0) {
            return firstZeroR(a, lo, mid - 1);
        } else {
            return firstZeroR(a, mid + 1, hi);
        }
    }

}
