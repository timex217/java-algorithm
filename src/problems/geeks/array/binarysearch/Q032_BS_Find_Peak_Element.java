package problems.geeks.array.binarysearch;

/**
 * Find a peak element
 * 
 * http://www.geeksforgeeks.org/find-a-peak-in-a-given-array/
 * 
 * Given an array of integers. Find a peak element in it. 
 * An array element is peak if it is NOT smaller than its neighbors. 
 * For corner elements, we need to consider only one neighbor. 
 * For example, for input array {5, 10, 20, 15}, 20 is the only peak element. 
 * For input array {10, 20, 15, 2, 23, 90, 67}, there are two peak elements: 20 and 90. 
 * Note that we need to return any one peak element.
 * 
 * Following corner cases give better idea about the problem.
 * 1) If input array is sorted in strictly increasing order, the last element is always a peak element. For example, 50 is peak element in {10, 20, 30, 40, 50}.
 * 2) If input array is sorted in strictly decreasing order, the first element is always a peak element. 100 is the peak element in {100, 80, 60, 50, 20}.
 * 3) If all elements of input array are same, every element is a peak element.
 * 
 * It is clear from above examples that there is always a peak element in input array in any input array.
 * 
 * 这道题因为只需要返回1个，所以可以采用Binary Search的思想，利用peak element的性质，判断是从哪边搜素。
 * 如果是要找到所有的peak elements，需要O(n)的时间
 */

public class Q032_BS_Find_Peak_Element {
    
    public static void main(String[] args) {
        int[] a = {10, 20, 15, 2, 23, 90, 67};
        int res = findPeakR(a, a.length, 0, a.length - 1);
        if (res == -1) {
            System.out.println("Not Found");
        } else {
            System.out.println(a[res]);
        }
    }
    
    static int findPeakR(int[] a, int n, int lo, int hi) {
        int mid = lo + (hi - lo) / 2;
        
        if ((mid == 0 || a[mid] >= a[mid - 1]) && (mid == n - 1 || a[mid] >= a[mid + 1])) {
            return mid;
        }
        
        if (a[mid - 1] > a[mid]) {
        	// 处于下坡路上，往左边走
            return findPeakR(a, n, lo, mid - 1);
        } else {
        	// 处于上坡路上，往右边走
            return findPeakR(a, n, mid + 1, hi);
        }
    }
    
    // ------------------------
    //  Find all the peaks
    // ------------------------
    static void findAllPeakR(int[] a, int n, int lo, int hi) {
        if (lo > hi) {
            return;
        }
        
        int mid = lo + (hi - lo) / 2;
        
        if ((mid == 0 || a[mid] >= a[mid - 1]) && (mid == n - 1 || a[mid] >= a[mid + 1])) {
        	System.out.println(a[mid]);
            return;
        }
        
        findAllPeakR(a, n, lo, mid - 1);
        findAllPeakR(a, n, mid + 1, hi);
    }

}
