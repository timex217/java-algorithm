package problems.geeks.array.binarysearch;

/**
 * Count the number of occurrences in a sorted array
 * 
 * Given a sorted array arr[] and a number x, write a function that counts the occurrences of x in arr[]. Expected time complexity is O(Logn)
 * 
 * Examples:
 * 
 *   Input: arr[] = {1, 1, 2, 2, 2, 2, 3,},   x = 2
 *   Output: 4 // x (or 2) occurs 4 times in arr[]
 * 
 *   Input: arr[] = {1, 1, 2, 2, 2, 2, 3,},   x = 3
 *   Output: 1 
 * 
 *   Input: arr[] = {1, 1, 2, 2, 2, 2, 3,},   x = 1
 *   Output: 2 
 * 
 *   Input: arr[] = {1, 1, 2, 2, 2, 2, 3,},   x = 4
 *   Output: -1 // 4 doesn't occur in arr[] 
 *   
 *   经典的Binary Search找lower bound和upper bound
 */

public class Q084_BS_Count_Occurrences_In_Sorted_Array {
	
	public static void main(String[] args) {
		int[] a = {1, 1, 2, 2, 2, 2, 3};
		int res = count(a, 7, 2);
		System.out.println(res);
	}
	
	static int count(int[] a, int n, int x) {
		int i = first(a, 0, n - 1, x);
		System.out.println(i);
		
		if (i == -1) {
			return i;
		}
		
		int j = last(a, 0, n - 1, x, n);
		System.out.println(j);
		
		return j - i + 1;
	}
	
	// ------------------
	//  Lower bound
	// ------------------
	static int first(int[] a, int lo, int hi, int x) {
		if (lo > hi) {
			return -1;
		}
		
		int mid = lo + (hi - lo) / 2;
		
		if (a[mid] == x && (mid == 0 || a[mid - 1] < x)) {
			return mid;
		}
		
		if (x <= a[mid]) {
			return first(a, lo, mid - 1, x);
		} else {
			return first(a, mid + 1, hi, x);
		}
	}
	
	// ------------------
	//  Upper bound
	// ------------------
	static int last(int[] a, int lo, int hi, int x, int n) {
		if (lo > hi) {
			return -1;
		}
		
		int mid = lo + (hi - lo) / 2;
		
		if (a[mid] == x && (mid == n - 1 || a[mid + 1] > x)) {
			return mid;
		}
		
		if (a[mid] <= x ) {
			return last(a, mid + 1, hi, x, n);
		} else {
			return last(a, lo, mid - 1, x, n);
		}
	}
	
}
