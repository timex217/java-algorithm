package problems.geeks.array.binarysearch;

/**
 * Find a Fixed Point in a given array
 * 
 * http://www.geeksforgeeks.org/find-a-fixed-point-in-a-given-array/
 * 
 * Given an array of n distinct integers sorted in ascending order, write a function that returns a Fixed Point in the array, 
 * if there is any Fixed Point present in array, else returns -1. Fixed Point in an array is an index i such that arr[i] is equal to i. 
 * Note that integers in array can be negative.
 * 
 * Examples:
 * 
 *   Input: arr[] = {-10, -5, 0, 3, 7}
 *   Output: 3  // arr[3] == 3 
 * 
 *   Input: arr[] = {0, 2, 5, 8, 17}
 *   Output: 0  // arr[0] == 0 
 * 
 * 
 *   Input: arr[] = {-10, -5, 3, 4, 7, 9}
 *   Output: -1  // No Fixed Point
 */

public class Q074_BS_Find_Fixed_Point_In_Array {
	
	public static void main(String[] args) {
		int[] a = {-10, -5, 0, 3, 7};
		int res = findFixedPoint(a);
		System.out.println(res);
	}
	
	static int findFixedPoint(int[] a) {
		if (a == null) {
			return -1;
		}
		
		int n = a.length;
		return helper(a, 0, n - 1);
	}
	
	static int helper(int[] a, int lo, int hi) {
		if (lo > hi) {
			return -1;
		}
		
		int mid = lo + (hi - lo) / 2;
		
		if (a[mid] == mid) {
			return mid;
		}
		
		if (a[mid] > mid) {
			return helper(a, lo, mid - 1);
		} else {
			return helper(a, mid + 1, hi);
		}
	}
	
}
