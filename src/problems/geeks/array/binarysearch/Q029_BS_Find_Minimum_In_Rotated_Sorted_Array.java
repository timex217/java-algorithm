package problems.geeks.array.binarysearch;

/**
 * Find the minimum element in a sorted and rotated array
 * 
 * A sorted array is rotated at some unknown point, find the minimum element in it.
 * 
 * Following solution assumes that all elements are distinct.
 * 
 * Examples
 * 
 * Input: {5, 6, 1, 2, 3, 4}
 * Output: 1
 * 
 * Input: {1, 2, 3, 4}
 * Output: 1
 * 
 * Input: {2, 1}
 * Output: 1
 * 
 * We can do it in O(Logn) using Binary Search. 
 * If we take a closer look at above examples, we can easily figure out following pattern: 
 * The minimum element is the only element whose previous element is greater than it. 
 * If there is no such element, then there is no rotation and first element is the minimum element. 
 * Therefore, we do binary search for an element which is smaller than the previous element. 
 */

public class Q029_BS_Find_Minimum_In_Rotated_Sorted_Array {
    
    public static void main(String[] args) {
        //int[] a = {6, 1, 2, 3, 4, 5};
        int[] a = {2, 2, 2, 2, 2, 2, 2, 2, -1, 1, 1, 2};
        int n = a.length;
        int min = findMin(a, n, 0, n - 1);
        System.out.println(min);
    }
    
    static int findMin(int[] a, int n, int lo, int hi) {
        // This condition is needed to handle the case when array is not
        // rotated at all
        if (lo > hi) return a[0];
        
        // If there is only one element left
        if (lo == hi) return a[lo];
        
        // Find mid
        int mid = lo + (hi - lo) / 2;
        
        // Check if element (mid+1) is minimum element. Consider
        // the cases like {3, 4, 5, 1, 2}
        if (mid < n - 1 && a[mid] > a[mid + 1])
            return a[mid + 1];
        
        // Check if mid itself is minimum element
        if (mid > 0 && a[mid - 1] > a[mid]) 
            return a[mid];
        
        // For handle duplicates
        if (a[lo] == a[mid] && a[mid] == a[hi]) 
            return Math.min(findMin(a, n, lo, mid - 1), findMin(a, n, mid + 1, hi));
        
        // Decide whether we need to go to left half or right half
        // 最小值应该在没有sort好的那一边
        if (a[mid] < a[hi])
            return findMin(a, n, lo, mid - 1);
        else
            return findMin(a, n, mid + 1, hi);
    }

}
