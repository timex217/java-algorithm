package problems.geeks.array.binarysearch;

/**
 * Find k closest elements to a given value
 * 
 * Given a sorted array arr[] and a value X, find the k closest elements to X in arr[]. 
 * Examples:
 * 
 * Input: K = 4, X = 35
 *        arr[] = {12, 16, 22, 30, 35, 39, 42, 45, 48, 50, 53, 55, 56}
 *        
 * Output: 30 39 42 45
 * 
 * An Optimized Solution is to find k elements in O(Logn + k) time. 
 * The idea is to use Binary Search to find the crossover point. 
 * Once we find index of crossover point, we can print k closest elements in O(k) time.
 */

import java.util.*;

public class Q009_BS_Find_k_Closest_Elements_To_A_Given_Value {
    
    public static void main(String[] args) {
        int[] a = {12, 16, 22, 30, 35, 39, 42, 45, 48, 50, 53, 55, 56};
        ArrayList<Integer> res = getKClosest(a, 4, 20);
        System.out.println(res.toString());
    }
    
    static int findClosest(int[] a, int lo, int hi, int x) {
    	if (lo > hi) {
    		return -1;
    	}
    	
    	if (lo == hi) {
    		return lo;
    	}
    	
    	if (lo == hi - 1) {
    		return (Math.abs(x - a[lo]) < Math.abs(x - a[hi])) ? lo : hi;
    	}
    	
    	int mid = lo + (hi - lo) / 2;
    	
    	if (a[mid] == x) {
    		return mid;
    	}
    	
    	if (a[mid] < x) {
    		return findClosest(a, mid, hi, x);
    	} else {
    		return findClosest(a, lo, mid, x);
    	}
    }
    
    // This function prints k closest elements to x in arr[].
    // n is the number of elements in arr[]
    static ArrayList<Integer> getKClosest(int[] a, int k, int x) {
        ArrayList<Integer> res = new ArrayList<Integer>();
        
        int n = a.length;
        
        int l = findClosest(a, 0, n - 1, x);
        int r = l + 1;
        int count = 0;
        
        // If x is present in arr[], then reduce left index
        // Assumption: all elements in arr[] are distinct
        if (a[l] == x) l--;
        
        while (count < k && l >= 0 && r < n) {
            if (x - a[l] < a[r] - x) {
                res.add(a[l--]);
            } else {
                res.add(a[r++]);
            }
            count++;
        }
        
        while (l >= 0 && count < k) {  
            res.add(a[l--]);
            count++;
        }
        
        while (r < n && count < k) {
            res.add(a[r++]);
            count++;
        }
        
        return res;
    }

}
