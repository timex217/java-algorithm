package problems.geeks.array.binarysearch;

/**
 * Search in an almost sorted array
 * 
 * Given an array which is sorted, but after sorting some elements are moved to either of the adjacent positions, 
 * i.e., arr[i] may be present at arr[i+1] or arr[i-1]. 
 * 
 * Write an efficient function to search an element in this array. 
 * Basically the element arr[i] can only be swapped with either arr[i+1] or arr[i-1].
 * 
 * For example consider the array {2, 3, 10, 4, 40}, 4 is moved to next position and 10 is moved to previous position.
 * 
 * Example:
 * 
 * Input: arr[] =  {10, 3, 40, 20, 50, 80, 70}, key = 40
 * Output: 2 
 * Output is index of 40 in given array
 * 
 * Input: arr[] =  {10, 3, 40, 20, 50, 80, 70}, key = 90
 * Output: -1
 * -1 is returned to indicate element is not present
 * 
 * The idea is to compare the key with middle 3 elements, if present then return the index. 
 * If not present, then compare the key with middle element to decide whether to go in left half or right half. 
 * Comparing with middle element is enough as all the elements after mid+2 must be greater than element mid 
 * and all elements before mid-2 must be smaller than mid element.
 */

import java.util.*;

public class Q005_BS_Search_In_Almost_Sorted_Array {
    
    public static void main(String[] args) {
        int[] a = {10, 3, 40, 20, 50, 80, 70};
        int idx = searchR(a, 40, 0, 7);
        System.out.println(idx);
    }
    
    static int search(int[] a, int key) {
        if (a == null) {
            return -1;
        }
        
        int n = a.length;
        int lo = 0;
        int hi = n - 1;
        
        while (lo <= hi) {
            int mid = lo + (hi - lo) / 2;
            
            if (a[mid] == key)                    return mid;
            if (mid > 1 && a[mid - 1] == key)     return mid - 1;
            if (mid < n - 1 && a[mid + 1] == key) return mid + 1;
            
            if (key < a[mid]) 
            	hi = mid - 2;
            else 
            	lo = mid + 2;
        }
        
        return -1;
    }
    
    static int searchR(int[] a, int key, int lo, int hi) {
        if (lo > hi) {
            return -1;
        }
        
        int n = a.length;
        int mid = lo + (hi - lo) / 2;
        
        if (a[mid] == key)                    return mid;
        if (mid > 0 && a[mid - 1] == key)     return mid - 1;
        if (mid < n - 1 && a[mid + 1] == key) return mid + 1;
        
        if (key < a[mid]) {
            return searchR(a, key, lo, mid - 2);
        } else {
            return searchR(a, key, mid + 2, hi);
        }
    }

}
