package problems.geeks.array.binarysearch;

/**
 * Find the maximum element in an array which is first increasing and then decreasing
 * 
 * http://www.geeksforgeeks.org/find-the-maximum-element-in-an-array-which-is-first-increasing-and-then-decreasing/
 * 
 * Given an array of integers which is initially increasing and then decreasing, find the maximum value in the array.
 * 
 * Input: arr[] = {8, 10, 20, 80, 100, 200, 400, 500, 3, 2, 1}
 * Output: 500
 * 
 * Input: arr[] = {1, 3, 50, 10, 9, 7, 6}
 * Output: 50
 * 
 * Corner case (No decreasing part)
 * Input: arr[] = {10, 20, 30, 40, 50}
 * Output: 50
 * 
 * Corner case (No increasing part)
 * Input: arr[] = {120, 100, 80, 20, 0}
 * Output: 120
 */

public class Q072_BS_Find_The_Maximum_Element_Which_Is_First_Increasing_And_Then_Decreasing {
	
	public static void main(String[] args) {
		int[] a = {8, 10, 20, 80, 100, 200, 400, 500, 3, 2, 1};
		int n = a.length;
		int res = findPeakR(a, n, 0, n - 1);
		System.out.println(a[res]);
	}
	
	static int findPeakR(int[] a, int n, int lo, int hi) {
        int mid = lo + (hi - lo) / 2;
        
        if ((mid == 0 || a[mid] >= a[mid - 1]) && (mid == n - 1 || a[mid] >= a[mid + 1])) {
            return mid;
        }
        
        if (a[mid - 1] > a[mid]) {
        	// 处于下坡路上，往左边走
            return findPeakR(a, n, lo, mid - 1);
        } else {
        	// 处于上坡路上，往右边走
            return findPeakR(a, n, mid + 1, hi);
        }
    }
	
	// ------------------------------
	//  Solution 2
	// ------------------------------
	static int findMaxElement(int[] a, int lo, int hi) {
		// only one element
		if (lo == hi) 
			return a[lo];
		
		// only two elements
		if (hi == lo + 1) 
			return Math.max(a[lo], a[hi]);
		
		// more than two elements
		int mid = lo + (hi - lo) / 2;
		
		// we are lucky
		if (a[mid - 1] < a[mid] && a[mid] > a[mid + 1])
			return a[mid];
		
		// 处在上坡的路上
		if (a[mid - 1] < a[mid] && a[mid] < a[mid + 1])
			return findMaxElement(a, mid + 1, hi);
		
		// 处在下坡的路上
		else
			return findMaxElement(a, lo, mid - 1);
	}
	
}
