package problems.geeks.array.binarysearch;

/**
 * Check for Majority Element in a sorted array
 * 
 * http://www.geeksforgeeks.org/check-for-majority-element-in-a-sorted-array/
 * 
 * Write a C function to find if a given integer x appears more than n/2 times in a sorted array of n integers.
 * 
 * Basically, we need to write a function say isMajority() that takes an array (arr[] ), 
 * array�s size (n) and a number to be searched (x) as parameters and returns true if x is a majority element (present more than n/2 times).
 * 
 * Examples:
 * 
 * Input: arr[] = {1, 2, 3, 3, 3, 3, 10}, x = 3
 * Output: True (x appears more than n/2 times in the given array)
 * 
 * Input: arr[] = {1, 1, 2, 4, 4, 4, 6, 6}, x = 4
 * Output: False (x doesn't appear more than n/2 times in the given array)
 * 
 * Input: arr[] = {1, 1, 1, 2, 2}, x = 1
 * Output: True (x appears more than n/2 times in the given array)
 */

public class Q104_BS_Check_For_Majority_Element_In_A_Sorted_Array {

	public static void main(String[] args) {
		int[] a = {1, 1, 2, 4, 4, 4, 6, 6};
		boolean res = isMajority(a, 8, 4);
		System.out.println(res);
	}
	
	static boolean isMajority(int[] a, int n, int x) {
		// find the index of first occurrence of x in a[]
		int i = getLowerBound(a, 0, n - 1, x);
		System.out.println(i);
		
		if (i == -1) {
			return false;
		}
		
		if ((i + n / 2 < n) && a[i + n / 2] == x) {
			return true;
		} else {
			return false;
		}
	}
	
	static int getLowerBound(int[] a, int lo, int hi, int x) {
		if (lo > hi) {
			return -1;
		}
		
		int mid = lo + (hi - lo) / 2;
		
		if (a[mid] == x && (mid == 0 || a[mid - 1] < x)) {
			return mid;
		}
		
		if (a[mid] < x) {
			return getLowerBound(a, mid + 1, hi, x);
		} else {
			return getLowerBound(a, lo, mid - 1, x);
		}
	}
	
}
