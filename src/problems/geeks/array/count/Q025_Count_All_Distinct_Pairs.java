package problems.geeks.array.count;

/**
 * Count all distinct pairs with difference equal to k
 * 
 * http://www.geeksforgeeks.org/count-pairs-difference-equal-k/
 * 
 * Given an integer array and a positive integer k, count all distinct pairs with difference equal to k.
 * 
 * Examples:
 * 
 * Input: arr[] = {1, 5, 3, 4, 2}, k = 3
 * Output: 2
 * There are 2 pairs with difference 3, the pairs are {1, 4} and {5, 2} 
 * 
 * Input: arr[] = {8, 12, 16, 4, 0, 20}, k = 4
 * Output: 5
 * There are 5 pairs with difference 4, the pairs are {0, 4}, {4, 8}, 
 * {8, 12}, {12, 16} and {16, 20} 
 */

import java.util.*;

public class Q025_Count_All_Distinct_Pairs {
    
    public static void main(String[] args) {
        int[] a = {8, 12, 16, 4, 0, 20};
        int res = countPairsWithDiffK(a, 6, 4);
        System.out.println(res);
    }
    
    // --------------------
    //  Naive solution
    // --------------------
    static int countPairsWithDiffKNaive(int[] a, int n, int k) {
        int count = 0;
        
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                if (Math.abs(a[i] - a[j]) == k) {
                    count++;
                }
            }
        }
        
        return count;
    }
    
    // --------------------
    //  Hashmap
    // --------------------
    static int countPairsWithDiffK(int[] a, int n, int k) {
        int count = 0;
        
        // put all numbers to hashmap
        HashMap<Integer, Boolean> map = new HashMap<Integer, Boolean>();
        for (int i = 0; i < n; i++) {
            map.put(a[i], true);
        }
        
        // go through a and try to find a match
        for (int i = 0; i < n; i++) {
            int x = a[i];
            
            if (map.containsKey(x + k) && map.get(x + k)) {
                count++;
            }
            
            if (map.containsKey(x - k) && map.get(x - k)) {
                count++;
            }
            
            map.put(x, false);
        }
        
        return count;
    }

}
