package problems.geeks.array.count;

/**
 * Count the number of possible triangles
 * 
 * http://www.geeksforgeeks.org/find-number-of-triangles-possible/
 * 
 * Given an unsorted array of positive integers. Find the number of triangles that can be formed with three different array elements as three sides of triangles. 
 * For a triangle to be possible from 3 values, the sum of any two values (or sides) must be greater than the third value (or third side).
 * 
 * For example, if the input array is {4, 6, 3, 7}, the output should be 3. There are three triangles possible {3, 4, 6}, {4, 6, 7} and {3, 6, 7}. 
 * Note that {3, 4, 7} is not a possible triangle.
 * 
 * As another example, consider the array {10, 21, 22, 100, 101, 200, 300}. 
 * There can be 6 possible triangles: {10, 21, 22}, {21, 100, 101}, {22, 100, 101}, {10, 100, 101}, {100, 101, 200} and {101, 200, 300}
 */

import java.util.*;

public class Q048_Count_The_Number_Of_Possible_Triangles {
    
    public static void main(String[] args) {
        int[] a = {10, 21, 22, 100, 101, 200, 300};
        int res = findTriangles(a, a.length);
        System.out.println(res);
    }
    
    static int findTriangles(int[] a, int n) {
        if (a == null) return 0;
        
        int count = 0;
        
        for (int i = 0; i < n - 2; i++) {
        	int k = i + 2;
        	
            for (int j = i + 1; j < n - 1; j++) {
            	
            	while (k < n && a[i] + a[j] > a[k]) {
            		k++;
            	}
            	
                count += k - j - 1;
            }
        }
        
        return count;
    }

}
