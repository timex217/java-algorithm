package problems.geeks.array.count;

/**
 * Count smaller elements on right side
 * 
 * http://www.geeksforgeeks.org/count-smaller-elements-on-right-side/
 * 
 * Write a function to count number of smaller elements on right of each element in an array. 
 * Given an unsorted array arr[] of distinct integers, construct another array countSmaller[] such that countSmaller[i] contains 
 * count of smaller elements on right side of each element arr[i] in array.
 * 
 * Examples:
 * 
 * Input:   arr[] =  {12, 1, 2, 3, 0, 11, 4}
 * Output:  countSmaller[]  =  {6, 1, 1, 1, 0, 1, 0} 
 * 
 * (Corner Cases)
 * Input:   arr[] =  {5, 4, 3, 2, 1}
 * Output:  countSmaller[]  =  {4, 3, 2, 1, 0} 
 * 
 * Input:   arr[] =  {1, 2, 3, 4, 5}
 * Output:  countSmaller[]  =  {0, 0, 0, 0, 0}
 * 
 * 正确的做法是用自平衡BST, O(n): nlogn
 */

import java.util.*;

public class Q071_Count_Smaller_Elements_On_Right_Side {
	
	public static void main(String[] args) {
		int[] a = {12, 1, 2, 3, 0, 11, 4};
		int[] countSmaller = constructLowerArray(a, 7);
		System.out.println(Arrays.toString(countSmaller));
	}
	
	static int[] constructLowerArray(int[] a, int n) {
		int[] res = new int[n];
		
		res[n - 1] = 0;
		for (int i = n - 2; i >= 0; i--) {
			for (int j = n - 1; j > i; j--) {
				if (a[i] > a[j]) {
					res[i]++;
				}
			}
		}
		
		return res;
	}
	
}
