package problems.geeks.array.search;

/**
 * Find the repeating and the missing
 * 
 * http://www.geeksforgeeks.org/find-a-repeating-and-a-missing-number/
 * 
 * Given an unsorted array of size n. Array elements are in range from 1 to n. One number from set {1, 2, ...n} is missing and one number occurs twice in array. 
 * Find these two numbers.
 * 
 * Examples:
 * 
 *   arr[] = {3, 1, 3}
 *   Output: 2, 3   // 2 is missing and 3 occurs twice 
 * 
 *   arr[] = {4, 3, 6, 2, 1, 1}
 *   Output: 1, 5  // 5 is missing and 1 occurs twice 
 *   
 * Time complexity: O(n)
 * Space complexity: O(1)
 */

public class Q078_Find_The_Repeating_And_The_Missing {
	
	public static void main(String[] args) {
		int[] a = {4, 3, 6, 2, 1, 1};
		findRepeatingAndMissing(a);
	}
	
	static void findRepeatingAndMissing(int[] a) {
		if (a == null) {
			return;
		}
		
		int n = a.length;
		
		for (int i = 0; i < n; i++) {
			int j = Math.abs(a[i]) - 1;
			if (a[j] > 0) {
				a[j] = -a[j];
			} else {
				System.out.format("Repeating: %d\n", Math.abs(a[i]));
			}
		}
		
		for (int i = 0; i < n; i++) {
			if (a[i] > 0) {
				System.out.format("Missing: %d\n", i + 1);
			}
		}
	}
	
}
