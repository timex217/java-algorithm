package problems.geeks.array.search;

/**
 * Median in a stream of integers
 * 
 * http://www.geeksforgeeks.org/median-of-stream-of-integers-running-integers/
 * 
 * Given that integers are read from a data stream. Find median of elements read so far in efficient way. 
 * For simplicity assume there are no duplicates. For example, let us consider the stream 5, 15, 1, 3...
 * 
 * After reading 1st element of stream - 5 -> median - 5
 * After reading 2nd element of stream - 5, 15 -> median - 10
 * After reading 3rd element of stream - 5, 15, 1 -> median - 5
 * After reading 4th element of stream - 5, 15, 1, 3 -> median - 4, so on...
 * Making it clear, when the input size is odd, we take the middle element of sorted data. 
 * If the input size is even, we pick average of middle two elements in sorted stream.
 * 
 * Solution:
 * 
 * Similar to balancing BST in Method 2 above, we can use a max heap on left side to represent elements that are less than effective median, 
 * and a min heap on right side to represent elements that are greater than effective median.
 * 
 * After processing an incoming element, the number of elements in heaps differ utmost by 1 element. 
 * When both heaps contain same number of elements, we pick average of heaps root data as effective median. 
 * When the heaps are not balanced, we select effective median from the root of heap containing more elements.
 */

import java.util.*;

public class Q075_Median_In_A_Stream_Of_Integers {
	
	public static void main(String[] args) {
		int[] a = {5, 15, 1, 3, 2, 8, 7, 9, 10, 6, 11, 4};
		printMedian(a);
	}
	
	static void printMedian(int[] a) {
		if (a == null) {
			return;
		}
		
		int n = a.length;
		int m = 0;
		
		// Left is max heap
		PriorityQueue<Integer> left = new PriorityQueue<Integer>(n, new Comparator<Integer>() {
			@Override
			public int compare(Integer a, Integer b) {
				return b - a;
			}
		});
		
		// Right is min heap
		PriorityQueue<Integer> right = new PriorityQueue<Integer>(n, new Comparator<Integer>() {
			@Override
			public int compare(Integer a, Integer b) {
				return a - b;
			}
		});
		
		for (int i = 0; i < n; i++) {
			m = getMedian(a[i], m, left, right);
			System.out.format("%d ", m);
		}
	}
	
	static int getMedian(int e, int m, PriorityQueue<Integer> left, PriorityQueue<Integer> right) {
		int diff = left.size() - right.size();
		// left and right are balanced
		if (diff == 0) {
			if (e < m) {
				left.offer(e);
				m = left.peek();
			} else {
				right.offer(e);
				m = right.peek();
			}
		} 
		// left has more elements than right
		else if (diff > 0) {
			if (e < m) {
				right.offer(left.poll());
				left.offer(e);
			} else {
				right.offer(e);
			}
			m = (left.peek() + right.peek()) / 2;
		}
		// right has more elements than left
		else {
			if (e < m) {
				left.offer(e);
			} else {
				left.offer(right.poll());
				right.offer(e);
			}
			m = (left.peek() + right.peek()) / 2;
		}
		
		return m;
	}
	
}
