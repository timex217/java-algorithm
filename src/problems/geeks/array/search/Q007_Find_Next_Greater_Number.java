package problems.geeks.array.search;

/**
 * Find next greater number with same set of digits
 * 
 * Given a number n, find the smallest number that has same set of digits as n and is greater than n. 
 * If x is the greatest possible number with its set of digits, then print “not possible”.
 * 
 * Examples:
 * For simplicity of implementation, we have considered input number as a string.
 * 
 * Input:  n = "218765"
 * Output: "251678"
 * 
 * Input:  n = "1234"
 * Output: "1243"
 * 
 * Input: n = "4321"
 * Output: "Not Possible"
 * 
 * Input: n = "534976"
 * Output: "536479"
 * 
 * Following is the algorithm for finding the next greater number.
 * I) Traverse the given number from rightmost digit, keep traversing till you find a digit which is smaller than the previously traversed digit. 
 * For example, if the input number is “534976″, we stop at 4 because 4 is smaller than next digit 9. 
 * If we do not find such a digit, then output is “Not Possible”.
 * 
 * II) Now search the right side of above found digit ‘d’ for the smallest digit greater than ‘d’. 
 * For “534976″, the right side of 4 contains “976″. The smallest digit greater than 4 is 6.
 * 
 * III) Swap the above found two digits, we get 536974 in above example.
 * 
 * IV) Now sort all digits from position next to ‘d’ to the end of number. 
 * The number that we get after sorting is the output. 
 * For above example, we sort digits in bold 536974. 
 * We get “536479” which is the next greater number for input 534976.
 */

import java.util.*;

public class Q007_Find_Next_Greater_Number {
    
    public static void main(String[] args) {
        char[] number = "534976".toCharArray();
        String res = findNext(number);
        System.out.println(res);
    }
    
    static String findNext(char[] number) {
        if (number == null) {
            return "Not Possible";
        }
        
        int n = number.length;
        int i = 0, j = 0;
        
        // I) Start from the right most digit and find the first digit that is
        // smaller than the digit next to it.
        for (i = n - 1; i > 0; i--) {
            if (number[i] > number[i - 1]) {
                break;
            }
        }
        
        if (i == 0) {
            return "Not Possible";
        }
        
        // II) Find the smallest digit on right side of (i-1)'th digit that is
        // greater than number[i-1]
        int x = number[i - 1], smallest = i;
        for (j = i + 1; j < n; j++) {
            if (number[j] > x && number[j] <= number[smallest]) {
                smallest = j;
            }
        }
        
        // III) Swap the above found smallest digit with number[i-1]
        swap(number, i - 1, smallest);
        
        // IV) Sort the digits after (i-1) in ascending order
        reverse(number, i, n - 1);
        
        return new String(number);
    }
    
    static void reverse(char[] chars, int from, int to) {
    	while (from < to) {
    		swap(chars, from++, to--);
    	}
    }
    
    static void swap(char[] chars, int i, int j) {
        char tmp = chars[i];
        chars[i] = chars[j];
        chars[j] = tmp;
    }

}
