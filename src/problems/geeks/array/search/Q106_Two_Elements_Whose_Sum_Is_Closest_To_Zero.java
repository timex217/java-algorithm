package problems.geeks.array.search;

/**
 * Two elements whose sum is closest to zero
 * 
 * http://www.geeksforgeeks.org/two-elements-whose-sum-is-closest-to-zero/
 * 
 * An Array of integers is given, both +ve and -ve. You need to find the two elements such that their sum is closest to zero.
 * 
 * {1, 60, -10, 70, -80, 85}
 *
 * For the below array, program should print -80 and 85.
 */

import java.util.*;

public class Q106_Two_Elements_Whose_Sum_Is_Closest_To_Zero {

	public static void main(String[] args) {
		int[] a = {1, 60, -10, 70, -80, 85};
		minAbsSumPair(a, a.length);
	}
	
	static void minAbsSumPair(int[] a, int n) {
		quickSort(a, 0, n - 1);
		
		System.out.println(Arrays.toString(a));
		
		int min = Integer.MAX_VALUE;
		int left = 0, right = n - 1;
		int i = 0, j = n - 1;
		
		while (i < j) {
			int sum = a[i] + a[j];
			
			if (Math.abs(sum) < Math.abs(min)) {
				min = sum;
				left = i;
				right = j;
			}
			
			if (sum < 0) {
				i++;
			} else {
				j--;
			}
		}
		
		System.out.format("left: %d, right: %d, min: %d\n", a[left], a[right], min);
	}
	
	static void quickSort(int[] a, int lo, int hi) {
		if (lo > hi) {
			return;
		}
		
		int p = partition(a, lo, hi);
		
		quickSort(a, lo, p - 1);
		quickSort(a, p + 1, hi);
	}
	
	static int partition(int[] a, int lo, int hi) {
		int mid = lo + (hi - lo) / 2;
		swap(a, mid, hi);
		
		int i = lo - 1;
		for (int j = lo; j < hi; j++) {
			if (a[j] < a[hi]) {
				swap(a, ++i, j);
			}
		}
		
		swap(a, i + 1, hi);
		return i + 1;
	}
	
	static void swap(int[] a, int i, int j) {
		int tmp = a[i];
		a[i] = a[j];
		a[j] = tmp;
	}
}
