package problems.geeks.array.search;

/**
 * Find duplicates
 * 
 * http://www.geeksforgeeks.org/find-duplicates-in-on-time-and-constant-extra-space/
 * 
 * Given an array of n elements which contains elements from 0 to n-1, with any of these numbers appearing any number of times. 
 * Find these repeating numbers in O(n) and using only constant memory space.
 * 
 * For example, let n be 7 and array be {1, 2, 3, 1, 3, 6, 6}, the answer should be 1, 3 and 6.
 */

import java.util.*;

public class Q091_Find_Duplicates {
	
	public static void main(String[] args) {
		int[] a = {1, 2, 3, 1, 3, 6, 6, 0, 0};
		findDuplicates(a, 9);
	}
	
	static void findDuplicates(int[] a, int n) {
		// mark the visited element as negative value
		for (int i = 0; i < n; i++) {
			int d = a[i] % n;
			a[d] += n;
		}
		
		for (int i = 0; i < n; i++) {
			if (a[i] / n > 1) {
				System.out.println(i);
			}
			
			// print the original value
			//System.out.println(a[i] % n);
		}
	}
	
}
