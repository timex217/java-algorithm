package problems.geeks.array.search;

/**
 * Find the two numbers with odd occurrences in an unsorted array
 * 
 * http://www.geeksforgeeks.org/find-the-two-numbers-with-odd-occurences-in-an-unsorted-array/
 * 
 * Given an unsorted array that contains even number of occurrences for all numbers except two numbers. 
 * Find the two numbers which have odd occurrences in O(n) time complexity and O(1) extra space.
 * 
 * Examples:
 * 
 * Input: {12, 23, 34, 12, 12, 23, 12, 45}
 * Output: 34 and 45
 * 
 * Input: {4, 4, 100, 5000, 4, 4, 4, 4, 100, 100}
 * Output: 100 and 5000
 * 
 * Input: {10, 20}
 * Output: 10 and 20
 */

public class Q065_Find_The_Two_Numbers_With_Odd_Occurrences_In_Unsorted_Array {
	
	public static void main(String[] args) {
		int[] a = {12, 23, 34, 12, 12, 23, 12, 45};
		printTwoOdd(a);
	}
	
	static void printTwoOdd(int[] a) {
		if (a == null) {
			return;
		}
		
		int n = a.length;
		int x = 0, y = 0;
		int xor2 = a[0];
		
		for (int i = 1; i < n; i++) {
			xor2 ^= a[i];
		}
		
		xor2 = xor2 & ~ (xor2 - 1);
		
		for (int i = 0; i < n; i++) {
			if ((a[i] & xor2) != 0) {
				x ^= a[i];
			} else {
				y ^= a[i];
			}
		}
		
		System.out.format("%d, %d\n", x, y);
	}
	
}
