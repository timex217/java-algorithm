package problems.geeks.array.search;

/**
 * Find a pair with the given difference
 * 
 * http://www.geeksforgeeks.org/find-a-pair-with-the-given-difference/
 * 
 * Given an unsorted array and a number n, find if there exists a pair of elements in the array whose difference is n.
 * 
 * Examples:
 * Input: arr[] = {5, 20, 3, 2, 50, 80}, n = 78
 * Output: Pair Found: (2, 80)
 * 
 * Input: arr[] = {90, 70, 20, 80, 50}, n = 45
 * Output: No Such Pair
 */

import java.util.*;

public class Q058_Find_A_Pair_With_The_Given_Difference {
    
    public static void main(String[] args) {
        int[] a = {5, 20, 3, 2, 50, 80};
        int diff = 78;
        boolean res = findPair(a, diff);
        System.out.println(res);
    }
    
    static boolean findPair(int[] a, int diff) {
        if (a == null || a.length < 2) {
            System.out.println("No Such Pair");
            return false;
        }
        
        int n = a.length;
        
        Set<Integer> set = new HashSet<Integer>();
        
        // step 1. create the set
        for (int i = 0; i < n; i++) {
            if (!set.contains(a[i])) {
                set.add(a[i]);
            }
        }
        
        // step 2. go through the array and check the set
        for (int i = 0; i < n; i++) {
            if (set.contains(a[i] + diff)) {
                System.out.format("(%d, %d)\n", a[i], a[i] + diff);
                return true;
            }
            
            if (set.contains(a[i] - diff)) {
                System.out.format("(%d, %d)\n", a[i], a[i] - diff);
                return true;
            }
        }
        
        System.out.println("No Such Pair");
        return false;
    }

}
