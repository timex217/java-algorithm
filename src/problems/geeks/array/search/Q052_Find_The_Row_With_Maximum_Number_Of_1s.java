package problems.geeks.array.search;

/**
 * Find the row with maximum number of 1s
 * 
 * http://www.geeksforgeeks.org/find-the-row-with-maximum-number-1s/
 * 
 * Given a boolean 2D array, where each row is sorted. Find the row with the maximum number of 1s.
 * 
 * Example
 * Input matrix
 * 0 1 1 1
 * 0 0 1 1
 * 1 1 1 1  // this row has maximum 1s
 * 0 0 0 0
 * 
 * Output: 2
 * 
 * 因为是sorted，所以可以对每一行采用binary search，找1的左边界，然后即可知道1的个数.
 */

public class Q052_Find_The_Row_With_Maximum_Number_Of_1s {
    
    public static void main(String[] args) {
        int[][] M = {
            {0, 1, 1, 1},
            {0, 0, 1, 1},
            {1, 1, 1, 1},
            {0, 0, 0, 0}
        };
        
        int res = rowWithMax1s(M, 4, 4);
        System.out.println(res);
    }
    
    static int lowerBound(int[] a, int lo, int hi) {
        if (lo > hi) {
            return -1;
        }
        
        int mid = lo + (hi - lo) / 2;
        
        if (a[mid] == 1 && (mid == 0 || a[mid - 1] == 0)) {
            return mid;
        }
        
        if (a[mid] == 0) {
            return lowerBound(a, mid + 1, hi);
        } else {
            return lowerBound(a, lo, mid - 1);
        }
    }
    
    static int rowWithMax1s(int[][] M, int m, int n) {
        int max = 0;
        
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                int k = lowerBound(M[i], 0, n - 1);
                if (k != -1 && n - k > max) {
                    max = n - k;
                }
            }
        }
        
        return max;
    }

}
