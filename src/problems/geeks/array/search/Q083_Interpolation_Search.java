package problems.geeks.array.search;

/**
 * Interpolation search
 * 
 * http://www.geeksforgeeks.org/g-fact-84/
 * 
 * Interpolation search works better than Binary Search for a sorted and uniformly distributed array.
 * 
 * On average the interpolation search makes about log(log(n)) comparisons (if the elements are uniformly distributed), 
 * where n is the number of elements to be searched. In the worst case (for instance where the numerical values of the keys increase exponentially) 
 * it can make up to O(n) comparisons.
 * 
 * Sources:
 * http://en.wikipedia.org/wiki/Interpolation_search
 */

public class Q083_Interpolation_Search {
	
	public static void main(String[] args) {
		
	}
	
}
