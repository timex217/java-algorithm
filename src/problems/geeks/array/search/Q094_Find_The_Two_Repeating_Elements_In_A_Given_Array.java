package problems.geeks.array.search;

/**
 * Find the two repeating elements in a given array
 * 
 * http://www.geeksforgeeks.org/find-the-two-repeating-elements-in-a-given-array/
 * 
 * You are given an array of n+2 elements. All elements of the array are in range 1 to n. 
 * And all elements occur once except two numbers which occur twice. Find the two repeating numbers.
 * 
 * For example, array = {4, 2, 4, 5, 2, 3, 1} and n = 5
 * 
 * The above array has n + 2 = 7 elements with all elements occurring once except 2 and 4 which occur twice. So the output should be 4 2.
 */

public class Q094_Find_The_Two_Repeating_Elements_In_A_Given_Array {
	
	public static void main(String[] args) {
		int[] a = {4, 2, 4, 5, 2, 3, 1};
		findRepeating(a, 7);
	}
	
	static void findRepeating(int[] a, int n) {
		if (a == null || n <= 1) {
			return;
		}
		
		for (int i = 0; i < n; i++) {
			int mod = a[i] % n;
			a[mod - 1] += n;
		}
		
		for (int i = 0; i < n; i++) {
			if (a[i] / n  > 1) {
				System.out.format("%d ", i + 1);
			}
		}
	}
	
}
