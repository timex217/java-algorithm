package problems.geeks.array.search;

/**
 * Find the first repeating element in an array of integers
 * 
 * Given an array of integers, find the first repeating element in it. 
 * We need to find the element that occurs more than once and whose index of first occurrence is smallest.
 * 
 * Examples:
 * 
 * Input:  arr[] = {10, 5, 3, 4, 3, 5, 6}
 * Output: 5 [5 is the first element that repeats]
 * 
 * Input:  arr[] = {6, 10, 5, 4, 9, 120, 4, 6, 10}
 * Output: 6 [6 is the first element that repeats]
 * 
 * Solution: We can Use Hashing to solve this in O(n) time on average. 
 * The idea is to traverse the given array from left to right and keep track of minimum index that repeats.
 */

import java.util.*;

public class Q001_Find_First_Repeating_Element {
    
    public static void main(String[] args) {
        int arr[] = {10, 5, 3, 4, 3, 5, 6};
        int idx = printFirstRepeating(arr);
        System.out.println(idx);
    }
    
    static int printFirstRepeating(int[] a) {
        if (a == null) {
            return -1;
        }
        
        int n = a.length;
        int min = n;
        
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        
        for (int i = 0; i < n; i++) {
            if (map.containsKey(a[i])) {
                min = Math.min(min, map.get(a[i]));
            } else {
                map.put(a[i], i);
            }
        }
        
        return min;
    }

}
