package problems.geeks.array.search;

/**
 * 4 Sum O(n^3) Solution
 */

import java.util.*;

public class Q055A_4Sum {
    
    public static void main(String[] args) {
        int[] a = {1, 4, 45, 6, 10, 12};
        int sum = 21;
        solution(a, sum);
    }
    
    static void solution(int[] a, int sum) {
        if (a == null) return;
        
        Arrays.sort(a);
        
        int n = a.length;
        
        for (int i = 0; i < n - 3; i++) {
            for (int j = i + 1; j < n - 2; j++) {
                int l = j + 1;
                int r = n - 1;
                
                while (l < r) {
                    int x = a[i] + a[j] + a[l] + a[r];
                    if (x == sum) {
                        System.out.format("%d, %d, %d, %d\n", a[i], a[j], a[l], a[r]);
                        l++;
                        r--;
                    } else if (x < sum) {
                        l++;
                    } else {
                        r--;
                    }
                }
            }
        }
    }

}
