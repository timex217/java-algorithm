package problems.geeks.array.search;

/**
 * Leaders in an array
 * 
 * http://www.geeksforgeeks.org/leaders-in-an-array/
 * 
 * Write a program to print all the LEADERS in the array. An element is leader if it is greater than all the elements to its right side. 
 * And the rightmost element is always a leader. For example int the array {16, 17, 4, 3, 5, 2}, leaders are 17, 5 and 2.
 * 
 * Let the input array be arr[] and size of the array be size.
 */

public class Q108_Leaders_In_An_Array {
	
	public static void main(String[] args) {
		int[] a = {16, 17, 4, 3, 5, 2};
		printLeaders(a, 6);
	}
	
	static void printLeaders(int[] a, int n) {
		if (a == null || n <= 0) {
			return;
		}
		
		int max = a[n - 1];
		System.out.println(max);
		
		for (int i = n - 2; i >= 0; i--) {
			if (a[i] > max) {
				max = a[i];
				System.out.println(max);
			}
		}
	}
	
}
