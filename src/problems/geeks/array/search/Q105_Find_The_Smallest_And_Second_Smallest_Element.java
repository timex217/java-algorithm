package problems.geeks.array.search;

/**
 * Find the smallest and second smallest element in an array
 * 
 * http://www.geeksforgeeks.org/to-find-smallest-and-second-smallest-element-in-an-array/
 * 
 * Write an efficient C program to find smallest and second smallest element in an array.
 */

public class Q105_Find_The_Smallest_And_Second_Smallest_Element {
	
	public static void main(String[] args) {
		int[] a = {12, 13, 1, 10, 34, 1};
		print2Smallest(a, 6);
	}
	
	static void print2Smallest(int[] a, int n) {
		if (a == null || n < 2) {
			return;
		}
		
		int first = Integer.MAX_VALUE;
		int second = Integer.MAX_VALUE;
		
		for (int i = 0; i < n; i++) {
			if (a[i] < first) {
				second = first;
				first = a[i];
			} else if (a[i] < second && a[i] != first) {
				second = a[i];
			}
		}
		
		System.out.format("first: %d, second: %d\n", first, second);
	}
	
}
