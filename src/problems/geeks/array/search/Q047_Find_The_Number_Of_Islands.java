package problems.geeks.array.search;

/**
 * Find the number of islands
 * 
 * http://www.geeksforgeeks.org/find-number-of-islands/
 * 
 * Given a boolean 2D matrix, find the number of islands.
 * 
 * This is an variation of the standard problem: �Counting number of connected components in a undirected graph�.
 * 
 * What is an island?
 * A group of connected 1s forms an island. For example, the below matrix contains 5 islands
 * 
 *             {1, 1, 0, 0, 0},
 *             {0, 1, 0, 0, 1},
 *             {1, 0, 0, 1, 1},
 *             {0, 0, 0, 0, 0},
 *             {1, 0, 1, 0, 1}
 *             
 * A cell in 2D matrix can be connected to 8 neighbors. 
 * So, unlike standard DFS(), where we recursively call for all adjacent vertices, here we can recursive call for 8 neighbors only. 
 * We keep track of the visited 1s so that they are not visited again.
 */

public class Q047_Find_The_Number_Of_Islands {
    
    public static void main(String[] args) {
        int[][] M = {
            {1, 1, 0, 0, 0},
            {0, 1, 0, 0, 1},
            {1, 0, 0, 1, 1},
            {0, 1, 0, 0, 0},
            {1, 0, 1, 0, 1}
        };
        
        int res = countIslands(M, 5, 5);
        System.out.println(res);
    }
    
    static int countIslands(int[][] M, int m, int n) {
        int count = 0;
        
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (M[i][j] == 1) {
                    dfs(M, m, n, i, j);
                    count++;
                }
            }
        }
        
        return count;
    }
    
    static void dfs(int[][] M, int m, int n, int i, int j) {
        if (i < 0 || i >= m || j < 0 || j >= n) {
            return;
        }
        
        if (M[i][j] == 0)  {
            return;
        }
        
        M[i][j] = 0;
        
        dfs(M, m, n, i - 1, j);
        dfs(M, m, n, i + 1, j);
        dfs(M, m, n, i, j - 1);
        dfs(M, m, n, i, j + 1);
    }

}
