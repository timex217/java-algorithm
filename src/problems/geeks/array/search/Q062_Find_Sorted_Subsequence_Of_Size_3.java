package problems.geeks.array.search;

/**
 * Find a sorted subsequence of size 3 in linear time
 * 
 * http://www.geeksforgeeks.org/find-a-sorted-subsequence-of-size-3-in-linear-time/
 * 
 * Given an array of n integers, find the 3 elements such that a[i] < a[j] < a[k] and i < j < k in 0(n) time. 
 * If there are multiple such triplets, then print any one of them.
 * 
 * Examples:
 * 
 * Input:  arr[] = {12, 11, 10, 5, 6, 2, 30}
 * Output: 5, 6, 30
 * 
 * Input:  arr[] = {1, 2, 3, 4}
 * Output: 1, 2, 3 OR 1, 2, 4 OR 2, 3, 4
 * 
 * Input:  arr[] = {4, 3, 2, 1}
 * Output: No such triplet
 */

import java.util.*;

public class Q062_Find_Sorted_Subsequence_Of_Size_3 {
	
    public static void main(String[] args) {
        int[] a = {12, 11, 10, 5, 6, 2, 30};
        find3Numbers(a);
    }
    
    static void find3Numbers(int[] a) {
    	if (a == null) {
    		return;
    	}
    	
    	int n = a.length;
    	
    	// build the smaller array
    	int min = 0;
    	int[] smaller = new int[n];
    	smaller[0] = -1;
    	
    	for (int i = 1; i < n; i++) {
    		if (a[i] > a[min]) {
    			smaller[i] = min;
    		} else {
    			smaller[i] = -1;
    			min = i;
    		}
    	}
    	
    	// build the greater array
    	int max = n - 1;
    	int[] greater = new int[n];
    	greater[n - 1] = -1;
    	
    	for (int i = n - 2; i >= 0; i--) {
    		if (a[i] < a[max]) {
    			greater[i] = max;
    		} else {
    			greater[i] = -1;
    			max = i;
    		}
    	}
    	
    	// go through both smaller and greater arrays and find an element
    	// that has both smaller and greater not equal to -1
    	boolean found = false;
    	
    	for (int i = 0; i < n; i++) {
    		if (smaller[i] != -1 && greater[i] != -1) {
    			found = true;
    			System.out.format("%d, %d, %d\n", a[smaller[i]], a[i], a[greater[i]]);
    		}
    	}
    	
    	if (!found) {
    		System.out.println("No such triplet");
    	}
    }

}
