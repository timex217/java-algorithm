package problems.geeks.array.search;

/**
 * Find number of pairs such that x^y > y^x
 * 
 * http://www.geeksforgeeks.org/find-number-pairs-xy-yx/
 * 
 * Given two arrays X[] and Y[] of positive integers, 
 * find number of pairs such that x^y > y^x where x is an element from X[] and y is an element from Y[].
 * 
 * Examples:
 * 
 *   Input: X[] = {2, 1, 6}, Y = {1, 5}
 *   Output: 3 
 *   // There are total 3 pairs where pow(x, y) is greater than pow(y, x)
 *   // Pairs are (2, 1), (2, 5) and (6, 1)
 * 
 * 
 *   Input: X[] = {10, 19, 18}, Y[] = {11, 15, 9};
 *   Output: 2
 *   // There are total 2 pairs where pow(x, y) is greater than pow(y, x)
 *   // Pairs are (10, 11) and (10, 15)
 */

public class Q026_Find_Pairs {
    
    public static void main(String[] args) {
        
    }

}
