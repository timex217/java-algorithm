package problems.geeks.array.search;

/**
 * Find if there is a subarray with 0 sum
 * 
 * Given an array of positive and negative numbers, find if there is a subarray with 0 sum.
 * 
 * Examples:
 * 
 * Input: {4, 2, -3, 1, 6}
 * Output: true 
 * There is a subarray with zero sum from index 1 to 3.
 * 
 * Input: {4, 2, 0, 1, 6}
 * Output: true 
 * There is a subarray with zero sum from index 2 to 2.
 * 
 * Input: {-3, 2, 3, 1, 6}
 * Output: false
 * There is no subarray with zero sum.
 */

import java.util.*;

public class Q017_Find_0_Sum_Subarray {
    
    public static void main(String[] args) {
        int[] a = {4, 2, -3, 1, 6};
        //int[] a = {-3, 2, 3, 1, 6};
        boolean res = findSumZeroSubArray(a);
        System.out.println(res);
    }
    
    static boolean findSumZeroSubArray(int[] a) {
        if (a == null) 
            return false;
        
        int n = a.length;
        
        // calculate the sum array
        int sum = 0;
        int[] s = new int[n];
        for (int i = 0; i < n; i++) {
            sum += a[i];
            s[i] = sum;
        }
        
        // use a hashmap to help check
        Set<Integer> set = new HashSet<Integer>();
        
        for (int i = 0; i < n; i++) {
            if (set.contains(s[i]))
                return true;
            else
                set.add(s[i]);
        }
        
        return false;
    }

}
