package problems.geeks.array.search;

/**
 * Find the first circular tour that visits all petrol pumps
 * 
 * http://www.geeksforgeeks.org/find-a-tour-that-visits-all-stations/
 * 
 * Suppose there is a circle. There are n petrol pumps on that circle. You are given two sets of data.
 * 
 * 1. The amount of petrol that petrol pump will give.
 * 2. Distance from that petrol pump to the next petrol pump.
 * 
 * Calculate the first point from where a truck will be able to complete the circle (The truck will stop at each petrol pump and it has infinite capacity). 
 * Expected time complexity is O(n). Assume for 1 litre petrol, the truck can go 1 unit of distance.
 * 
 * For example, let there be 4 petrol pumps with amount of petrol and distance to next petrol pump value pairs as {4, 6}, {6, 5}, {7, 3} and {4, 5}. 
 * The first point from where truck can make a circular tour is 2nd petrol pump. Output should be "start = 1" (index of 2nd petrol pump).
 */

import java.util.*;

public class Q045_Find_The_First_Circular_Tour {
    
    static class PetrolPump {
        int petrol;
        int distance;
        PetrolPump(int p, int d) {
            this.petrol = p;
            this.distance = d;
        }
    }
    
    public static void main(String[] args) {
        ArrayList<PetrolPump> pumps = new ArrayList<PetrolPump>();
        
        pumps.add(new PetrolPump(4, 6));
        pumps.add(new PetrolPump(6, 5));
        pumps.add(new PetrolPump(7, 3));
        pumps.add(new PetrolPump(4, 5));
        
        int start = findTourFirstCircular(pumps);
        System.out.println(start);
    }
    
    static int findTourFirstCircular(ArrayList<PetrolPump> pumps) {
        if (pumps == null || pumps.size() == 0) {
            return -1;
        }
        
        int n = pumps.size();
        int start = 0, end = 0, count = 0, petrol = 0;
        
        while (count < n) {
            PetrolPump pump = pumps.get(end);
            petrol += pump.petrol - pump.distance;
            
            end = (end + 1) % n;
            
            if (petrol >= 0) {
                count++;    
            } else {
                petrol = 0; 
                count = 0;
                start = end;
                
                if (start == 0) {
                    return -1;
                }
            }
        }
        
        return start;
    }

}
