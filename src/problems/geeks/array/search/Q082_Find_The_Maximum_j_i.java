package problems.geeks.array.search;

/**
 * Given an array arr[], find the maximum j � i such that arr[j] > arr[i]
 * 
 * http://www.geeksforgeeks.org/given-an-array-arr-find-the-maximum-j-i-such-that-arrj-arri/
 * 
 * Examples:
 * 
 *   Input: {34, 8, 10, 3, 2, 80, 30, 33, 1}
 *   Output: 6  (j = 7, i = 1)
 * 
 *   Input: {9, 2, 3, 4, 5, 6, 7, 8, 18, 0}
 *   Output: 8 ( j = 8, i = 0)
 * 
 *   Input:  {1, 2, 3, 4, 5, 6}
 *   Output: 5  (j = 5, i = 0)
 * 
 *   Input:  {6, 5, 4, 3, 2, 1}
 *   Output: -1 
 */

import java.util.*;

public class Q082_Find_The_Maximum_j_i {
	
	public static void main(String[] args) {
		int[] a = {34, 8, 10, 3, 2, 80, 30, 33, 1};
		int res = maxIndexDiff(a);
		System.out.println(res);
	}
	
	static int maxIndexDiff(int[] a) {
		if (a == null) {
			return -1;
		}
		
		int n = a.length;
		
		int[] lmin = new int[n];
		int[] rmax = new int[n];
		
		lmin[0] = a[0];
		for (int i = 1; i < n; i++) {
			lmin[i] = Math.min(a[i], lmin[i - 1]);
		}
		
		rmax[n - 1] = a[n - 1];
		for (int i = n - 2; i >= 0; i--) {
			rmax[i] = Math.max(a[i], rmax[i + 1]);
		}
		
		System.out.println(Arrays.toString(lmin));
		System.out.println(Arrays.toString(rmax));
		
		int i = 0, j = 0, maxdiff = -1;
		while (i < n && j < n) {
			if (lmin[i] < rmax[j]) {
				maxdiff = Math.max(maxdiff, j - i);
				j++;
			} else {
				i++;
			}
		}
		
		return maxdiff;
	}
	
}
