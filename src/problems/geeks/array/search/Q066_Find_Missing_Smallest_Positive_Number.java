package problems.geeks.array.search;

/**
 * Find the smallest positive number missing from an unsorted array
 * 
 * http://www.geeksforgeeks.org/find-the-smallest-positive-number-missing-from-an-unsorted-array/
 * 
 * You are given an unsorted array with both positive and negative elements. 
 * You have to find the smallest positive number missing from the array in O(n) time using constant extra space. 
 * You can modify the original array.
 * 
 * Examples
 * 
 *  Input:  {2, 3, 7, 6, 8, -1, -10, 15}
 *  Output: 1
 * 
 *  Input:  { 2, 3, -7, 6, 8, 1, -10, 15 }
 *  Output: 4
 * 
 *  Input: {1, 1, 0, -1, -2}
 *  Output: 2 
 *  
 *  Solution:
 *  1) Segregate positive numbers from others i.e., move all non-positive numbers to left side. In the following code, segregate() function does this part.
 *  2) Now we can ignore non-positive elements and consider only the part of array which contains all positive elements. 
 *  We traverse the array containing all positive numbers and to mark presence of an element x, we change the sign of value at index x to negative. 
 *  We traverse the array again and print the first index which has positive value. In the following code, findMissingPositive() function does this part. 
 *  Note that in findMissingPositive, we have subtracted 1 from the values as indexes start from 0 in C.
 *  
 *  关键是掌握解题的思想，手法是将数值对应的index上的数置为负数!
 *  
 *  这道题可以转变为：在一个没有排序好的正数数列中找最小的missing number，O(n)
 */

import java.util.*;

public class Q066_Find_Missing_Smallest_Positive_Number {
	
	public static void main(String[] args) {
		int[] a = {2, 3, 7, 6, 8, -1, -10, 15};
		int res = findMissingNumber2(a);
		System.out.println(res);
	}
	
	static int findMissingNumber2(int[] a) {
		if (a == null) {
			return 1;
		}
		
		int n = a.length;
		int m = partition(a);
		
		for (int i = 0; i < m; i++) {
			int val = Math.abs(a[i]);
			if (val <= m) {
				// 将在 1 - m 之间出现了的数标记为负数
				a[val - 1] = -Math.abs(a[val - 1]);
			}
		}
		
		System.out.println(Arrays.toString(a));
		
		for (int i = 0; i < m; i++) {
			if (a[i] > 0) {
				return i + 1;
			}
		}
		
		return m + 1;
	}
	
	static int partition(int[] a) {
		int i = 0;
		for (int j = 0; j < a.length; j++) {
			if (a[j] > 0) {
				swap(a, i++, j);
			}
		}
		return i;
	}
	
	static int findMissingNumber(int[] a) {
		if (a == null) {
			return 1;
		}
		
		int n = a.length;
		
		// put all the negative numbers on the left
		// and all the positive numbers on the right
		int p = rearrange(a, n);
		
		// number of positive numbers
		int size = n - p;
		
	    for (int i = 0; i < size; i++) {
	    	int j = Math.abs(a[p + i]) - 1;
	    	if (j < size && a[p + j] > 0) {
	    		a[p + j] = -a[p + j];
	    	}
	    }
	    
	    for (int i = 0; i < size; i++) {
	    	if (a[p + i] > 0) {
	    		return i + 1;
	    	}
	    }
		
		return n + 1;
	}
	
	static int rearrange(int[] a, int n) {
		int i = -1;
		for (int j = 0; j < n; j++) {
			if (a[j] <= 0) {
				swap(a, ++i, j);
			}
		}
		return i + 1;
	}
	
	static void swap(int[] a, int i, int j) {
		int tmp = a[i];
		a[i] = a[j];
		a[j] = tmp;
	}
	
}
