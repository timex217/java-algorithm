package problems.geeks.array.search;

/**
 * k largest(or smallest) elements in an array
 * 
 * Write an efficient program for printing k largest elements in an array. Elements in array can be in any order.
 * 
 * For example, if given array is [1, 23, 12, 9, 30, 2, 50] and you are asked for the largest 3 elements i.e., k = 3 then your program should print 50, 30 and 23.
 * 
 * Time complexity: O(k + (n-k)*logk)
 */

import java.util.*;

public class Q101_K_Largest_Elements_In_Array {
	
	public static void main(String[] args) {
		int[] a = {1, 23, 12, 9, 30, 2, 50};
		int k = 3;
		kLargestElements(a, 7, k);
	}
	
	static void kLargestElements(int[] a, int n, int k) {
		if (a == null || n <= 0 || k <= 0 || k > n) {
			return;
		}
		
		// use a min heap
		PriorityQueue<Integer> minHeap = new PriorityQueue<Integer>(k, new Comparator<Integer>() {
			@Override
			public int compare(Integer a, Integer b) {
				return a - b;
			}
		});
		
		// step 1. put first k elements to the min heap
		for (int i = 0; i < k; i++) {
			minHeap.offer(a[i]);
		}
		
		// step 2. for the rest n - k elements, compare with the top element of min heap
		for (int i = k; i < n; i++) {
			if (a[i] > minHeap.peek()) {
				// 注意这里一定要poll出去，否则minHeap的大小会增加!
				minHeap.poll();
				minHeap.offer(a[i]);
			}
		}
		
		// step 3. print the k element in the min heap
		while (!minHeap.isEmpty()) {
			System.out.println(minHeap.poll());
		}
	}
	
}
