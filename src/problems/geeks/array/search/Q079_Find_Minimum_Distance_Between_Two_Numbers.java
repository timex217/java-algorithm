package problems.geeks.array.search;

/**
 * Find the minimum distance between two numbers
 * 
 * http://www.geeksforgeeks.org/find-the-minimum-distance-between-two-numbers/
 * 
 * Given an unsorted array arr[] and two numbers x and y, find the minimum distance between x and y in arr[]. 
 * The array might also contain duplicates. You may assume that both x and y are different and present in arr[].
 * 
 * Examples:
 * Input: arr[] = {1, 2}, x = 1, y = 2
 * Output: Minimum distance between 1 and 2 is 1.
 * 
 * Input: arr[] = {3, 4, 5}, x = 3, y = 5
 * Output: Minimum distance between 3 and 5 is 2.
 * 
 * Input: arr[] = {3, 5, 4, 2, 6, 5, 6, 6, 5, 4, 8, 3}, x = 3, y = 6
 * Output: Minimum distance between 3 and 6 is 4.
 * 
 * Input: arr[] = {2, 5, 3, 5, 4, 4, 2, 3}, x = 3, y = 2
 * Output: Minimum distance between 3 and 2 is 1.
 */

public class Q079_Find_Minimum_Distance_Between_Two_Numbers {
	
	public static void main(String[] args) {
		int[] a = {3, 5, 4, 2, 6, 5, 6, 6, 5, 4, 8, 3};
		int x = 3, y = 6;
		int res = findMinDistance(a, x, y);
		System.out.println(res);
	}
	
	static int findMinDistance(int[] a, int x, int y) {
		if (a == null) {
			return -1;
		}
		
		int n = a.length;
		int xpos = -1;
		int ypos = -1;
		int dist = n;
		
		for (int i = 0; i < n; i++) {
			if (a[i] != x && a[i] != y) {
				continue;
			}
			
			if (a[i] == x) {
				xpos = i;
			} else { 
				ypos = i;
			}
			
			if (xpos != -1 && ypos != -1) {
				dist = Math.min(dist, Math.abs(xpos - ypos));
			}
		}
		
		return dist;
	}
	
}
