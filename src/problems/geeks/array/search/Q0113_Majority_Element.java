package problems.geeks.array.search;

/**
 * Majority Element
 * 
 * http://www.geeksforgeeks.org/majority-element/
 * 
 * A majority element in an array A[] of size n is an element that appears more than n/2 times (and hence there is at most one such element).
 * 
 * Write a function which takes an array and emits the majority element (if it exists), otherwise prints NONE as follows:
 * 
 *        I/P : 3 3 4 2 4 4 2 4 4
 *        O/P : 4 
 * 
 *        I/P : 3 3 4 2 4 4 2 4
 *        O/P : NONE
 *        
 * 这道题要注意：用voting algorithm求出来的只是一个candidate，并不是最终答案，还要再统计一次candidate的count才行
 */

public class Q0113_Majority_Element {

	public static void main(String[] args) {
		int[] a = {3, 3, 4, 2, 4, 4, 2, 4, 4};
		int n = 9;
		
		int cand = findCandidate(a, n);
		int count = countCandidate(a, n, cand);
		
		if (count > n / 2) {
			System.out.println(cand);
		} else {
			System.out.println("NONE");
		}
	}
	
	static int findCandidate(int[] a, int n) {
		int count = 1, maj_index = 0;
		for (int i = 1; i < n; i++) {
			if (a[i] == a[maj_index]) {
				count++;
			} else {
				count--;
			}
			
			if (count == 0) {
				maj_index = i;
				count = 1;
			}
		}
		
		// 最后剩下的就是candidate
		return a[maj_index];
	}
	
	static int countCandidate(int[] a, int n, int cand) {
		int count = 0;
		for (int i = 0; i < n; i++) {
			if (a[i] == cand) {
				count++;
			}
		}
		return count;
	}
		
}
