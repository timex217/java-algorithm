package problems.geeks.array.search;

/**
 * Find subarray with given sum
 * Given an unsorted array of nonnegative integers, find a continous subarray which adds to a given number.
 * 
 * Examples:
 * 
 * Input: arr[] = {1, 4, 20, 3, 10, 5}, sum = 33
 * Ouptut: Sum found between indexes 2 and 4
 * 
 * Input: arr[] = {1, 4, 0, 0, 3, 10, 5}, sum = 7
 * Ouptut: Sum found between indexes 1 and 4
 * 
 * Input: arr[] = {1, 4}, sum = 0
 * Output: No subarray found
 * There may be more than one subarrays with sum as the given sum. The following solutions print first such subarray.
 */

import java.util.*;

public class Q018_Find_Subarray_With_Given_Sum {
    
    public static void main(String[] args) {
        int[] a = {1, 4, 20, 3, 10, 5};
        boolean res = findSubArraySum(a, 33);
        System.out.println(res);
    }
    
    static boolean findSubArraySum(int[] a, int target) {
        if (a == null) {
            return false;
        }
        
        int n = a.length;
        
        // calculate the sum array and build the hashmap
        int[] s = new int[n];
        int sum = 0;
        
        Set<Integer> set = new HashSet<Integer>();
        
        for (int i = 0; i < n; i++) {
            sum += a[i];
            s[i] = sum;
        }
        
        // quick check
        if (set.contains(target)) {
            return true;
        }
        
        // use the hashmap to help check
        for (int i = 0; i < n; i++) {
            if (set.contains(s[i] - target)) {
                return true;
            } else if (!set.contains(s[i])) {
            	set.add(s[i]);
            }
        }
        
        return false;
    }

}
