package problems.geeks.array.search;

/**
 * Intersection Algorithm
 * 
 * Find common elements in three sorted arrays
 * 
 * Given three arrays sorted in non-decreasing order, print all common elements in these arrays.
 * 
 * Examples:
 * 
 * ar1[] = {1, 5, 10, 20, 40, 80}
 * ar2[] = {6, 7, 20, 80, 100}
 * ar3[] = {3, 4, 15, 20, 30, 70, 80, 120}
 * Output: 20, 80
 * 
 * ar1[] = {1, 5, 5}
 * ar2[] = {3, 4, 5, 5, 10}
 * ar3[] = {5, 5, 10, 20}
 * Outptu: 5, 5
 * 
 * Let the current element traversed in ar1[] be x, in ar2[] be y and in ar3[] be z. We can have following cases inside the loop.
 * 1) If x, y and z are same, we can simply print any of them as common element and move ahead in all three arrays.
 * 2) Else If x < y, we can move ahead in ar1[] as x cannot be a common element
 * 3) Else If y < z, we can move ahead in ar2[] as y cannot be a common element
 * 4) Else (We reach here when x > y and y > z), we can simply move ahead in ar3[] as z cannot be a common element.
 */

public class Q019_Find_Common_Elements_In_Three_Sorted_Arrays {
    
    public static void main(String[] args) {
        int[] a1 = {1, 5, 10, 20, 40, 80};
        int[] a2 = {6, 7, 20, 80, 100};
        int[] a3 = {3, 4, 15, 20, 30, 70, 80, 120};
        
        findCommon(a1, a2, a3);
    }
    
    static void findCommon(int[] a1, int[] a2, int[] a3) {
        int n1 = a1.length;
        int n2 = a2.length;
        int n3 = a3.length;
        
        int i = 0, j = 0, k = 0;
        
        while (i < n1 && j < n2 && k < n3) {
            if (a1[i] == a2[j] && a2[j] == a3[k]) {
                System.out.println(a1[i]);
                i++; j++; k++;
            }
            else if (a1[i] < a2[j]) {
                i++;
            }
            else if (a2[j] < a3[k]) {
                j++;
            }
            else {
                k++;
            }
        }
    }

}
