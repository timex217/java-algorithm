package problems.geeks.array.search;

/**
 * Replace every element with the next greatest
 * 
 * http://www.geeksforgeeks.org/replace-every-element-with-the-greatest-on-right-side/
 * 
 * Given an array of integers, replace every element with the next greatest element (greatest element on the right side) in the array. 
 * Since there is no element next to the last element, replace it with -1. 
 * 
 * For example, if the array is {16, 17, 4, 3, 5, 2}, then it should be modified to {17, 5, 5, 5, 2, -1}.
 */

import java.util.*;

public class Q057_Replace_Every_Element_With_The_Next_Greatest {
    
    public static void main(String[] args) {
        int[] a = {16, 17, 4, 3, 5, 2};
        nextGreatest(a);
        System.out.println(Arrays.toString(a));
    }
    
    static void nextGreatest(int[] a) {
        if (a == null) {
            return;
        }
        
        int n = a.length;
        int max = -1;
        
        for (int i = n - 1; i >= 0; i--) {
        	int tmp = a[i];
            a[i] = max;
            max = Math.max(max, tmp);
        }
    }

}
