package problems.geeks.graph;

/**
 * Depth First Traversal for a Graph
 * 
 * http://www.geeksforgeeks.org/depth-first-traversal-for-a-graph/
 * 
 * Depth First Traversal (or Search) for a graph is similar to Depth First Traversal of a tree. 
 * The only catch here is, unlike trees, graphs may contain cycles, so we may come to the same node again. 
 * To avoid processing a node more than once, we use a boolean visited array. 
 * For example, in the following graph, we start traversal from vertex 2. When we come to vertex 0, 
 * we look for all adjacent vertices of it. 2 is also an adjacent vertex of 0. If we don�t mark visited vertices, 
 * then 2 will be processed again and it will become a non-terminating process. 
 * Depth First Traversal of the following graph is 2, 0, 1, 3
 */

import java.util.*;

public class Q002A_Depth_First_Traversal_for_a_Graph {
	
	static class GraphNode {
		int index;
		ArrayList<GraphNode> neighbors;
		
		GraphNode(int x) {
			index = x;
			neighbors = new ArrayList<GraphNode>();
		}
		
		void addNeighbors(GraphNode[] nodes) {
			for (int i = 0; i < nodes.length; i++) {
				neighbors.add(nodes[i]);
			}
		}
	}
	
	static void visit(GraphNode node) {
		System.out.format("%d ", node.index);
	}
	
	// ---------------------------------
	//  DFS - no recursion
	// ---------------------------------
	static void dfs(GraphNode node) {
		if (node == null) {
			return;
		}
		
		// use a set to mark visited nodes
		Set<GraphNode> visited = new HashSet<GraphNode>();
		
		// use a stack to do depth-first traversal
		Stack<GraphNode> stack = new Stack<GraphNode>();
		stack.push(node);
		
		while (!stack.isEmpty()) {
			GraphNode curr = stack.pop();
			
			if (!visited.contains(curr)) {
				// visit the node
				visit(curr);
				
				// mark it as visited
				visited.add(curr);
			}
			
			// for all its neighbors, if they are not visited, push to the stack
			for (int i = 0; i < curr.neighbors.size(); i++) {
				GraphNode neighbor = curr.neighbors.get(i);
				if (!visited.contains(neighbor)) {
					stack.push(neighbor);
				}
			}
		}
	}
	
	// ----------------------------------
	//  DFS - recursion
	// ----------------------------------
	static void dfs(GraphNode node, Set<GraphNode> visited) {
		if (node == null) {
			return;
		}
		
		if (!visited.contains(node)) {
			// visit the node
			visit(node);
			
			// mark it as visited
			visited.add(node);
		}
		
		// for all its neighbors, if they are not visited yet, dfs
		for (int i = 0; i < node.neighbors.size(); i++) {
			GraphNode neighbor = node.neighbors.get(i);
			if (!visited.contains(neighbor)) {
				dfs(neighbor, visited);
			}
		}
	}
	
	public static void main(String[] args) {
		GraphNode n0 = new GraphNode(0);
		GraphNode n1 = new GraphNode(1);
		GraphNode n2 = new GraphNode(2);
		GraphNode n3 = new GraphNode(3);
		GraphNode n4 = new GraphNode(4);
		GraphNode n5 = new GraphNode(5);
		GraphNode n6 = new GraphNode(6);
		GraphNode n7 = new GraphNode(7);
		GraphNode n8 = new GraphNode(8);
		
		n0.addNeighbors(new GraphNode[]{n1, n7});
		n1.addNeighbors(new GraphNode[]{n0, n2, n7});
		n2.addNeighbors(new GraphNode[]{n1, n3, n5, n8});
		n3.addNeighbors(new GraphNode[]{n2, n4, n5});
		n4.addNeighbors(new GraphNode[]{n3, n5});
		n5.addNeighbors(new GraphNode[]{n2, n3, n4, n6});
		n6.addNeighbors(new GraphNode[]{n5, n7, n8});
		n7.addNeighbors(new GraphNode[]{n0, n1, n6, n8});
		n8.addNeighbors(new GraphNode[]{n2, n6, n7});
		
		
		//dfs(n0);
		//System.out.println();
		
		GraphNode[] nodes = {n0, n1, n2, n3, n4, n5, n6, n7, n8};
		
		Set<GraphNode> visited = new HashSet<GraphNode>();
		
		for (int i = 0; i < nodes.length; i++) {
			dfs(nodes[i], visited);
		}
	}
	
}
