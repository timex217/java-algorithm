package problems.geeks.graph;

/**
 * Check whether a given graph is Bipartite or not
 * 
 * http://www.geeksforgeeks.org/bipartite-graph/
 * 
 * A Bipartite Graph is a graph whose vertices can be divided into two independent sets, U and V such that every edge (u, v) either 
 * connects a vertex from U to V or a vertex from V to U. In other words, for every edge (u, v), either u belongs to U and v to V, 
 * or u belongs to V and v to U. We can also say that there is no edge that connects vertices of same set.
 * 
 * Following is a simple algorithm to find out whether a given graph is Birpartite or not using Breadth First Search (BFS).
 * 
 * 1.	Assign RED color to the source vertex (putting into set U).
 * 2.	Color all the neighbors with BLUE color (putting into set V).
 * 3.	Color all neighbor�s neighbor with RED color (putting into set U).
 * 4.	This way, assign color to all vertices such that it satisfies all the constraints of m way coloring problem where m = 2.
 * 5.   While assigning colors, if we find a neighbor which is colored with same color as current vertex, then the graph cannot be 
 *      colored with 2 vertices (or graph is not Bipartite)
 */

import java.util.*;

public class Q005_Check_whether_a_given_graph_is_Bipartite_or_not {

	static class Graph {
		// Number of vertices
		int V;
		// Adjacency matrix
		int[][] matrix;
		
		Graph(int x) {
			V = x;
			matrix = new int[V][V];
		}
		
		// Add edge
		void addEdge(int u, int v, int weight) {
			matrix[u][v] = weight;
			matrix[v][u] = weight;
		}
		
		void print() {
			for (int i = 0; i < V; i++) {
				System.out.println(Arrays.toString(matrix[i]));
			}
		}
	}
	
	public static void main(String[] args) {
		Graph graph = new Graph(4);
		graph.addEdge(0, 1, 1);
		graph.addEdge(0, 3, 1);
		graph.addEdge(1, 2, 1);
		graph.addEdge(2, 3, 1);
		
		graph.print();
		
		boolean res = isBipartite(graph, 0);
		System.out.println(res);
	}
	
	static boolean isBipartite(Graph graph, int src) {
		int V = graph.V;
		
		// Create a color array to store colors assigned to all veritces. Vertex 
	    // number is used as index in this array. The value '-1' of  colorArr[i] 
	    // is used to indicate that no color is assigned to vertex 'i'.  The value 
	    // 1 is used to indicate first color is assigned and value 0 indicates 
	    // second color is assigned.
	    int[] color = new int[V];
	    for (int i = 0; i < V; i++) {
	    	color[i] = -1;
	    }
	    
	    // Assign first color to source
	    color[src] = 1;
	    
	    // Create a queue (FIFO) of vertex numbers and enqueue source vertex
	    // for BFS traversal
	    Queue<Integer> queue = new LinkedList<Integer>();
	    queue.add(src);
	    
	    while (!queue.isEmpty()) {
	    	int u = queue.poll();
	    	
	    	// Find all adjacent vertices
	    	for (int v = 0; v < V; v++) {
	    		if (graph.matrix[u][v] > 0) {
	    			// Destination v is not colored
	    			if (color[v] == -1) {
	    				// Assign alternate color to this adjacent v of u
	    				color[v] = 1 - color[u];
	    				queue.add(v);
	    			}
	    			// An edge from u to v exists and destination v is colored with
	                // same color as u
	    			else if (color[v] == color[u]) {
	    				return false;
	    			}
	    		}
	    	}
	    }
	    
	    // If we reach here, then all adjacent vertices can be colored with 
	    // alternate color
		return true;
	}
	
}
