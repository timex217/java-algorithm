package problems.geeks.graph;

/**
 * Topological Sorting
 * 
 * http://www.geeksforgeeks.org/topological-sorting/
 * 
 * Topological sorting for Directed Acyclic Graph (DAG) is a linear ordering of vertices such that for every directed edge uv, 
 * vertex u comes before v in the ordering. Topological Sorting for a graph is not possible if the graph is not a DAG.
 * For example, a topological sorting of the following graph is “5 4 2 3 1 0″. There can be more than one topological sorting for a graph. 
 * For example, another topological sorting of the following graph is “4 5 2 3 1 0″. The first vertex in topological sorting is always 
 * a vertex with in-degree as 0 (a vertex with no in-coming edges).
 * 
 * 
 *                5     4
 *               / \   / \
 *              /   \ /   \
 *             2     0     1
 *              \         /
 *               \-->3-->/
 * 
 * 
 * Algorithm to find Topological Sorting:
 * 
 * We recommend to first see implementation of DFS here. We can modify DFS to find Topological Sorting of a graph. 
 * In DFS, we start from a vertex, we first print it and then recursively call DFS for its adjacent vertices. 
 * In topological sorting, we use a temporary stack. We don’t print the vertex immediately, we first recursively call topological 
 * sorting for all its adjacent vertices, then push it to a stack. Finally, print contents of stack. Note that a vertex is pushed 
 * to stack only when all of its adjacent vertices (and their adjacent vertices and so on) are already in stack.
 * 
 * Time Complexity: The above algorithm is simply DFS with an extra stack. So time complexity is same as DFS which is O(V+E).
 * 
 * Applications:
 * Topological Sorting is mainly used for scheduling jobs from the given dependencies among jobs. In computer science, 
 * applications of this type arise in instruction scheduling, ordering of formula cell evaluation when recomputing formula values 
 * in spreadsheets, logic synthesis, determining the order of compilation tasks to perform in makefiles, data serialization, 
 * and resolving symbol dependencies in linkers
 */

import java.util.*;

public class Q004_Topological_Sorting {

	static class Graph {
		int V;
		List[] lists;
		
		Graph(int x) {
			V = x;
			lists = new List[V];
			
			// Initialize each adjacency list as empty by making head as NULL
			for (int i = 0; i < V; i++) {
				lists[i] = new List();
			}
		}
		
		// Add edge
		void addEdge(int u, int v) {
			List list;
			// directed graph
			list = lists[u];
			list.addNode(v);
		}
		
		void print() {
			for (int i = 0; i < V; i++) {
				List list = lists[i];
				System.out.format("%d: ", i);
				list.print();
			}
		}
		
		void topologicalSort() {
			Stack<Integer> stack = new Stack();
			
			// Mark all the vertices as not visited
			boolean[] visited = new boolean[V];
			
			// Call the recursive helper function to store Topological Sort
		    // starting from all vertices one by one
			for (int i = 0; i < V; i++) {
				topologicalHelper(i, visited, stack);
			}
			
			// print contents of stack
			while (!stack.isEmpty()) {
				System.out.format("%d ", stack.pop());
			}
		}
		
		void topologicalHelper(int i, boolean[] visited, Stack<Integer> stack) {
			if (visited[i]) {
				return;
			}
			
			// Mark the current node as visited
			visited[i] = true;
			
			// Recur for all the vertices adjacent to this vertex
			ListNode p = lists[i].head;
			while (p != null) {
				if (visited[p.index] == false) {
					topologicalHelper(p.index, visited, stack);
				}
				p = p.next;
			}
			
			// Push current vertx to stack with stores result
			stack.push(i);
		}
	}
	
	// Adjacency list
	static class List {
		ListNode head;
		
		List() {
			head = null;
		}
		
		void addNode(int x) {
			ListNode node = new ListNode(x);
			node.next = head;
			head = node;
		}
		
		void print() {
			ListNode p = head;
			while (p != null) {
				System.out.format("%d", p.index);
				p = p.next;
				if (p != null) {
					System.out.format(" -> ");
				}
			}
			System.out.println();
		}
	}
	
	// Adjacency list node
	static class ListNode {
		int index;
		ListNode next;
		
		ListNode(int d) {
			index = d;
			next = null;
		}
	}
	
	public static void main(String[] args) {
		Graph graph = new Graph(6);
		graph.addEdge(5, 2);
		graph.addEdge(5, 0);
		graph.addEdge(4, 0);
		graph.addEdge(4, 1);
		graph.addEdge(2, 3);
		graph.addEdge(3, 1);
		
		graph.print();
		
		System.out.format("Following is a Topological Sort of the given graph\n");
		graph.topologicalSort();
	}
	
}
