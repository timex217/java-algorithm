package problems.geeks.graph;

/**
 * Detect cycle in an undirected graph
 * 
 * http://www.geeksforgeeks.org/detect-cycle-undirected-graph/
 * 
 * We have discussed cycle detection for directed graph. We have also discussed a union-find algorithm for cycle detection in undirected graphs. 
 * The time complexity of the union-find algorithm is O(ELogV). Like directed graphs, we can use DFS to detect cycle in an undirected graph in O(V+E) time. 
 * We do a DFS traversal of the given graph. For every visited vertex �v�, if there is an adjacent �u� such that u is already visited and u is not parent of v, 
 * then there is a cycle in graph. If we don�t find such an adjacent for any vertex, we say that there is no cycle. 
 * The assumption of this approach is that there are no parallel edges between any two vertices.
 */

import java.util.*;

public class Q006B_Detect_Cycle_in_Undirected_Graph {
	
	static class Graph {
		int V;
		int[][] matrix;
		
		Graph(int x) {
			V = x;
			matrix = new int[V][V];
		}
		
		void addEdge(int u, int v) {
			matrix[u][v] = 1;
			matrix[v][u] = 1;
		}
		
		boolean isCyclic() {
			boolean[] visited = new boolean[V];
			for (int i = 0; i < V; i++) {
				if (isCyclicHelper(i, visited, -1)) {
					return true;
				}
			}
			return false;
		}
		
		boolean isCyclicHelper(int u, boolean[] visited, int parent) {
			visited[u] = true;
			
			for (int v = 0; v < V; v++) {
				if (matrix[u][v] > 0) {
					if ((visited[v] == true && v != parent) || (visited[v] == false && isCyclicHelper(v, visited, v))) {
						return true;
					}
				}
			}
			
			return false;
		}
	}
	
	public static void main(String[] args) {
		Graph g = new Graph(5);
		g.addEdge(1, 0);
	    g.addEdge(1, 2);
	    g.addEdge(0, 2);
	    g.addEdge(0, 3);
	    g.addEdge(3, 4);
	    
	    boolean res = g.isCyclic();
	    if (res) {
	    	System.out.println("Graph contains cycle");
	    } else {
	    	System.out.println("Graph doesn't contain cycle");
	    }
	}
	
}
