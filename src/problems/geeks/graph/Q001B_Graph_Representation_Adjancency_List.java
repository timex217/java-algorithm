package problems.geeks.graph;

/**
 * Dijkstra’s Algorithm for Adjacency List Representation
 * 
 * http://www.geeksforgeeks.org/greedy-algorithms-set-7-dijkstras-algorithm-for-adjacency-list-representation/
 * 
 * As discussed in the previous post, in Dijkstra’s algorithm, two sets are maintained, one set contains list of vertices already included in SPT (Shortest Path Tree), other set contains vertices not yet included. With adjacency list representation, all vertices of a graph can be traversed in O(V+E) time using BFS. The idea is to traverse all vertices of graph using BFS and use a Min Heap to store the vertices not yet included in SPT (or the vertices for which shortest distance is not finalized yet).  Min Heap is used as a priority queue to get the minimum distance vertex from set of not yet included vertices. Time complexity of operations like extract-min and decrease-key value is O(LogV) for Min Heap.
 * 
 * Following are the detailed steps.
 * 1) Create a Min Heap of size V where V is the number of vertices in the given graph. Every node of min heap contains vertex number and distance value of the vertex.
 * 2) Initialize Min Heap with source vertex as root (the distance value assigned to source vertex is 0). The distance value assigned to all other vertices is INF (infinite).
 * 3) While Min Heap is not empty, do following
 * …..a) Extract the vertex with minimum distance value node from Min Heap. Let the extracted vertex be u.
 * …..b) For every adjacent vertex v of u, check if v is in Min Heap. If v is in Min Heap and distance value is more than weight of u-v plus distance value of u, then update the distance value of v.
 */

import java.util.*;

public class Q001B_Graph_Representation_Adjancency_List {

	static class Graph {
		int V;
		List[] lists;
		
		Graph(int x) {
			V = x;
			lists = new List[V];
			
			// Initialize each adjacency list as empty by making head as NULL
			for (int i = 0; i < V; i++) {
				lists[i] = new List();
			}
		}
		
		// Add edge
		void addEdge(int u, int v, int weight) {
			List list;
			// undirected graph
			list = lists[u];
			list.addNode(v, weight);
			
			list = lists[v];
			list.addNode(u, weight);
		}
		
		void print() {
			for (int i = 0; i < V; i++) {
				List list = lists[i];
				System.out.format("%d: ", i);
				list.print();
			}
		}
	}
	
	// Adjacency list
	static class List {
		ListNode head;
		
		List() {
			head = null;
		}
		
		void addNode(int x, int w) {
			ListNode node = new ListNode(x, w);
			node.next = head;
			head = node;
		}
		
		void print() {
			ListNode p = head;
			while (p != null) {
				System.out.format("%d (%d)", p.index, p.weight);
				p = p.next;
				if (p != null) {
					System.out.format(" -> ");
				}
			}
			System.out.println();
		}
	}
	
	// Adjacency list node
	static class ListNode {
		int index;
		int weight;
		ListNode next;
		
		ListNode(int d, int w) {
			index = d;
			weight = w;
			next = null;
		}
	}
	
	public static void main(String[] args) {
		Graph graph = new Graph(9);
		graph.addEdge(0, 1, 4);
		graph.addEdge(0, 7, 8);
		graph.addEdge(1, 7, 11);
		graph.addEdge(1, 2, 8);
		graph.addEdge(7, 8, 7);
		graph.addEdge(6, 8, 6);
		graph.addEdge(7, 6, 1);
		graph.addEdge(8, 2, 2);
		graph.addEdge(6, 5, 2);
		graph.addEdge(2, 5, 4);
		graph.addEdge(2, 3, 7);
		graph.addEdge(3, 5, 14);
		graph.addEdge(3, 4, 9);
		graph.addEdge(4, 5, 10);
		
		graph.print();
		
		dijkstra(graph, 0);
	}
	
	// --------------------------------
	//  Dijkstra - Adjacency list
	// --------------------------------
	
	static void dijkstra(Graph graph, int src) {
		int V = graph.V;
		List[] lists = graph.lists;
		
		int[] dist = new int[V];
		boolean[] visited = new boolean[V];
		
		// initialize
		for (int i = 0; i < V; i++) {
			dist[i] = Integer.MAX_VALUE;
			visited[i] = false;
		}
		
		dist[src] = 0;
		
		for (int count = 0; count < V; count++) {
			// 在还没被访问过的节点中找距离（权重）最小的一个点
			int u = minDistance(dist, visited, V);
			// 首先标记它为已访问
			visited[u] = true;
			
			// 找出与 u 相连的、还没被访问过的点，如果新权值比当前的小，将其更新
			ListNode p = lists[u].head;
			while (p != null) {
				int v = p.index;
				if (!visited[v] && dist[v] > dist[u] + p.weight) {
					dist[v] = dist[u] + p.weight;
				}
				p = p.next;
			}
		}
		
		// print the result
		printDist(dist, V);
	}
	
	static int minDistance(int[] dist, boolean[] visited, int V) {
		int min = Integer.MAX_VALUE;
		int min_index = -1;
		for (int v = 0; v < V; v++) {
			if (visited[v] == false && dist[v] < min) {
				min = dist[v];
				min_index = v;
			}
		}
		return min_index;
	}
	
	static void printDist(int[] dist, int V) {
		for (int i = 0; i < V; i++) {
			System.out.format("%d \t\t %d\n", i, dist[i]);
		}
	}
	
}
