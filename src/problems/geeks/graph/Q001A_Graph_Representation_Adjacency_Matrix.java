package problems.geeks.graph;

/**
 * Dijkstra’s shortest path algorithm
 * 
 * http://www.geeksforgeeks.org/greedy-algorithms-set-6-dijkstras-shortest-path-algorithm/
 * 
 * Below are the detailed steps used in Dijkstra’s algorithm to find the shortest path from a single source vertex to all other vertices in the given graph.
 * Algorithm
 * 
 * 1) Create a set sptSet (shortest path tree set) that keeps track of vertices included in shortest path tree, i.e., whose minimum distance from source is 
 * calculated and finalized. Initially, this set is empty.
 * 
 * 2) Assign a distance value to all vertices in the input graph. Initialize all distance values as INFINITE. Assign distance value as 0 for the source vertex 
 * so that it is picked first.
 * 
 * 3) While sptSet doesn’t include all vertices
 * ….a) Pick a vertex u which is not there in sptSetand has minimum distance value.
 * ….b) Include u to sptSet.
 * ….c) Update distance value of all adjacent vertices of u. To update the distance values, iterate through all adjacent vertices. For every adjacent vertex v, 
 * if sum of distance value of u (from source) and weight of edge u-v, is less than the distance value of v, then update the distance value of v.
 * 
 * Notes:
 * 1) The code calculates shortest distance, but doesn’t calculate the path information. We can create a parent array, update the parent array when distance is updated 
 * (like prim’s implementation) and use it show the shortest path from source to different vertices.
 * 
 * 2) The code is for undirected graph, same dijekstra function can be used for directed graphs also.
 * 
 * 3) The code finds shortest distances from source to all vertices. If we are interested only in shortest distance from source to a single target, we can break the for 
 * loop when the picked minimum distance vertex is equal to target (Step 3.a of algorithm).
 * 
 * 4) Time Complexity of the implementation is O(V^2). If the input graph is represented using adjacency list, it can be reduced to O(E log V) with the help of binary heap. 
 * We will soon be discussing O(E Log V) algorithm as a separate post.
 * 
 * 5) Dijkstra’s algorithm doesn’t work for graphs with negative weight edges. For graphs with negative weight edges, Bellman–Ford algorithm can be used, we will soon be 
 * discussing it as a separate post.
 */

import java.util.*;

public class Q001A_Graph_Representation_Adjacency_Matrix {
	
	static class Graph {
		// Number of vertices
		int V;
		// Adjacency matrix
		int[][] matrix;
		
		Graph(int x) {
			V = x;
			matrix = new int[V][V];
		}
		
		// Add edge
		void addEdge(int u, int v, int weight) {
			matrix[u][v] = weight;
			matrix[v][u] = weight;
		}
		
		void print() {
			for (int i = 0; i < V; i++) {
				System.out.println(Arrays.toString(matrix[i]));
			}
		}
	}
	
	public static void main(String[] args) {
		Graph graph = new Graph(9);
		graph.addEdge(0, 1, 4);
		graph.addEdge(0, 7, 8);
		graph.addEdge(1, 7, 11);
		graph.addEdge(1, 2, 8);
		graph.addEdge(7, 8, 7);
		graph.addEdge(6, 8, 6);
		graph.addEdge(7, 6, 1);
		graph.addEdge(8, 2, 2);
		graph.addEdge(6, 5, 2);
		graph.addEdge(2, 5, 4);
		graph.addEdge(2, 3, 7);
		graph.addEdge(3, 5, 14);
		graph.addEdge(3, 4, 9);
		graph.addEdge(4, 5, 10);
		
		//graph.print();
		
		dijkstra(graph, 0);
	}
	
	// --------------------------------
	//  Dijkstra - Adjacency matrix
	// --------------------------------
	
	static void dijkstra(Graph graph, int src) {
		int V = graph.V;
		int[][] matrix = graph.matrix;
		
		int[] dist = new int[V];
		boolean[] visited = new boolean[V];
		
		// initialize
		for (int i = 0; i < V; i++) {
			dist[i] = Integer.MAX_VALUE;
			visited[i] = false;
		}
		
		dist[src] = 0;
		
		for (int count = 0; count < V; count++) {
			// 在还没被访问过的节点中找距离（权重）最小的一个点
			int u = minDistance(dist, visited, V);
			// 首先标记它为已访问
			visited[u] = true;
			
			// 找出与 u 相连的、还没被访问过的点，如果新权值比当前的小，将其更新
			for (int v = 0; v < V; v++) {
				if (visited[v] == false && matrix[u][v] > 0 && dist[v] > dist[u] + matrix[u][v]) {
					dist[v] = dist[u] + matrix[u][v];
				}
			}
		}
		
		// print the result
		printDist(dist, V);
	}
	
	static int minDistance(int[] dist, boolean[] visited, int V) {
		int min = Integer.MAX_VALUE;
		int min_index = -1;
		for (int v = 0; v < V; v++) {
			if (visited[v] == false && dist[v] < min) {
				min = dist[v];
				min_index = v;
			}
		}
		return min_index;
	}
	
	static void printDist(int[] dist, int V) {
		for (int i = 0; i < V; i++) {
			System.out.format("%d \t\t %d\n", i, dist[i]);
		}
	}

}
