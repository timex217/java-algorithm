package problems.geeks.graph;

/**
 * Breadth First Traversal for a Graph
 * 
 * http://www.geeksforgeeks.org/breadth-first-traversal-for-a-graph/
 * 
 * Breadth First Traversal (or Search) for a graph is similar to Breadth First Traversal of a tree (See mthod 2 of this post). 
 * The only catch here is, unlike trees, graphs may contain cycles, so we may come to the same node again. 
 * To avoid processing a node more than once, we use a boolean visited array. For simplicity, it is assumed that all vertices 
 * are reachable from the starting vertex.
 * For example, in the following graph, we start traversal from vertex 2. When we come to vertex 0, we look for all adjacent 
 * vertices of it. 2 is also an adjacent vertex of 0. If we don’t mark visited vertices, then 2 will be processed again and 
 * it will become a non-terminating process. Breadth First Traversal of the following graph is 2, 0, 3, 1.
 */

import java.util.*;

import problems.geeks.graph.Q002A_Depth_First_Traversal_for_a_Graph.GraphNode;

public class Q002B_Breadth_First_Traversal_for_a_Graph {

	static class GraphNode {
		int index;
		ArrayList<GraphNode> neighbors;
		
		GraphNode(int x) {
			index = x;
			neighbors = new ArrayList<GraphNode>();
		}
		
		void addNeighbors(GraphNode[] nodes) {
			for (int i = 0; i < nodes.length; i++) {
				neighbors.add(nodes[i]);
			}
		}
	}
	
	static void visit(GraphNode node) {
		System.out.format("%d ", node.index);
	}
	
	// ---------------------------------
	//  BFS - no recursion
	//  note: BFS并不合适用recursion来实现
	// ---------------------------------
	
	static void bfs(GraphNode node) {
		if (node == null) {
			return;
		}
		
		// use a set to mark visited nodes
		Set<GraphNode> visited = new HashSet<GraphNode>();
		
		// use a queue to do breadth-first traversal
		Queue<GraphNode> queue = new LinkedList<GraphNode>();
		queue.add(node);
		
		while (!queue.isEmpty()) {
			GraphNode curr = queue.poll();
			
			if (!visited.contains(curr)) {
				// visit the node
				visit(curr);
				
				// mark it as visited
				visited.add(curr);
			}
			
			for (int i = 0; i < curr.neighbors.size(); i++) {
				GraphNode neighbor = curr.neighbors.get(i);
				if (!visited.contains(neighbor)) {
					queue.add(neighbor);
				}
			}
		}
	}
	
	public static void main(String[] args) {
		GraphNode n0 = new GraphNode(0);
		GraphNode n1 = new GraphNode(1);
		GraphNode n2 = new GraphNode(2);
		GraphNode n3 = new GraphNode(3);
		GraphNode n4 = new GraphNode(4);
		GraphNode n5 = new GraphNode(5);
		GraphNode n6 = new GraphNode(6);
		GraphNode n7 = new GraphNode(7);
		GraphNode n8 = new GraphNode(8);
		
		n0.addNeighbors(new GraphNode[]{n1, n7});
		n1.addNeighbors(new GraphNode[]{n0, n2, n7});
		n2.addNeighbors(new GraphNode[]{n1, n3, n5, n8});
		n3.addNeighbors(new GraphNode[]{n2, n4, n5});
		n4.addNeighbors(new GraphNode[]{n3, n5});
		n5.addNeighbors(new GraphNode[]{n2, n3, n4, n6});
		n6.addNeighbors(new GraphNode[]{n5, n7, n8});
		n7.addNeighbors(new GraphNode[]{n0, n1, n6, n8});
		n8.addNeighbors(new GraphNode[]{n2, n6, n7});
		
		bfs(n0);
	}
	
}
