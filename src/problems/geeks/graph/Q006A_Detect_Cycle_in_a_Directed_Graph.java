package problems.geeks.graph;

/**
 * Detect Cycle in a Directed Graph
 * 
 * http://www.geeksforgeeks.org/detect-cycle-in-a-graph/
 * 
 * Given a directed graph, check whether the graph contains a cycle or not. Your function should return true if the 
 * given graph contains at least one cycle, else return false. For example, the following graph contains three cycles 0->2->0, 
 * 0->1->2->0 and 3->3, so your function must return true.
 * 
 * Solution
 * Depth First Traversal can be used to detect cycle in a Graph. DFS for a connected graph produces a tree. There is a 
 * cycle in a graph only if there is a back edge present in the graph. A back edge is an edge that is from a node to itself 
 * (selfloop) or one of its ancestor in the tree produced by DFS. In the following graph, there are 3 back edges, marked with cross sign. 
 * We can observe that these 3 back edges indicate 3 cycles present in the graph.
 */

import java.util.*;

public class Q006A_Detect_Cycle_in_a_Directed_Graph {
	
	static class Graph {
		int V;
		int[][] matrix;
		
		Graph(int x) {
			V = x;
			matrix = new int[V][V];
		}
		
		void addEdge(int u, int v) {
			matrix[u][v] = 1;
		}
		
		boolean isCyclic() {
			boolean[] visited = new boolean[V];
			boolean[] stack = new boolean[V];
			
			// 对每个节点进行环检查，如果发现从某个节点出发dfs发现了环，则返回true
			for (int i = 0; i < V; i++) {
				if (isCyclicHelper(i, visited, stack)) {
					return true;
				}
			}
			
			return false;
		}
		
		boolean isCyclicHelper(int u, boolean[] visited, boolean[] stack) {
			// mark it as visited
			visited[u] = true;
			stack[u] = true;
			
			// check all its neighbors
			for (int v = 0; v < V; v++) {
				if (matrix[u][v] > 0) {
					if (stack[v] == true || (visited[v] == false && isCyclicHelper(v, visited, stack))) {
						return true;
					}
				}
			}
			
			// remove the vertex from recursion stack
			stack[u] = false;
			
			return false;
		}
	}

	public static void main(String[] args) {
		Graph g = new Graph(4);
		g.addEdge(0, 1);
	    g.addEdge(0, 2);
	    g.addEdge(1, 2);
	    g.addEdge(2, 0);
	    g.addEdge(2, 3);
	    g.addEdge(3, 3);
	    
	    boolean res = g.isCyclic();
	    if (res) {
	    	System.out.println("Graph contains cycle");
	    } else {
	    	System.out.println("Graph doesn't contain cycle");
	    }
	}
	
}
