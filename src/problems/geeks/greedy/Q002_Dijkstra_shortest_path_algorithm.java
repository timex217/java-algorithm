package problems.geeks.greedy;

/**
 * Dijkstra�s shortest path algorithm
 * 
 * http://www.geeksforgeeks.org/greedy-algorithms-set-6-dijkstras-shortest-path-algorithm/
 * 
 * Below are the detailed steps used in Dijkstra�s algorithm to find the shortest path from a single source vertex to all other vertices in the given graph.
 * Algorithm
 * 
 * 1) Create a set sptSet (shortest path tree set) that keeps track of vertices included in shortest path tree, i.e., whose minimum distance from source is 
 * calculated and finalized. Initially, this set is empty.
 * 
 * 2) Assign a distance value to all vertices in the input graph. Initialize all distance values as INFINITE. Assign distance value as 0 for the source vertex 
 * so that it is picked first.
 * 
 * 3) While sptSet doesn�t include all vertices
 * �.a) Pick a vertex u which is not there in sptSetand has minimum distance value.
 * �.b) Include u to sptSet.
 * �.c) Update distance value of all adjacent vertices of u. To update the distance values, iterate through all adjacent vertices. For every adjacent vertex v, 
 * if sum of distance value of u (from source) and weight of edge u-v, is less than the distance value of v, then update the distance value of v.
 * 
 * Notes:
 * 1) The code calculates shortest distance, but doesn�t calculate the path information. We can create a parent array, update the parent array when distance is updated 
 * (like prim�s implementation) and use it show the shortest path from source to different vertices.
 * 
 * 2) The code is for undirected graph, same dijekstra function can be used for directed graphs also.
 * 
 * 3) The code finds shortest distances from source to all vertices. If we are interested only in shortest distance from source to a single target, we can break the for 
 * loop when the picked minimum distance vertex is equal to target (Step 3.a of algorithm).
 * 
 * 4) Time Complexity of the implementation is O(V^2). If the input graph is represented using adjacency list, it can be reduced to O(E log V) with the help of binary heap. 
 * We will soon be discussing O(E Log V) algorithm as a separate post.
 * 
 * 5) Dijkstra�s algorithm doesn�t work for graphs with negative weight edges. For graphs with negative weight edges, Bellman�Ford algorithm can be used, we will soon be 
 * discussing it as a separate post.
 */

import java.util.*;

public class Q002_Dijkstra_shortest_path_algorithm {
	
	// Number of vertices in the graph
	static int V = 9;
	
	static int INT_MAX = Integer.MAX_VALUE;
	
	public static void main(String[] args) {
		/* Let us create the example graph discussed above */
		int[][] graph = {{0,  4,  0,  0,  0,  0,  0,  8,  0},
	                     {4,  0,  8,  0,  0,  0,  0,  11, 0},
	                     {0,  8,  0,  7,  0,  4,  0,  0,  2},
	                     {0,  0,  7,  0,  9,  14, 0,  0,  0},
	                     {0,  0,  0,  9,  0,  10, 0,  0,  0},
	                     {0,  0,  4,  0,  10, 0,  2,  0,  0},
	                     {0,  0,  0,  14, 0,  2,  0,  1,  6},
	                     {8,  11, 0,  0,  0,  0,  1,  0,  7},
	                     {0,  0,  2,  0,  0,  0,  6,  7,  0}
	                    };
		
		dijkstra(graph, 0);
	}
	
	// Funtion that implements Dijkstra's single source shortest path algorithm
	// for a graph represented using adjacency matrix representation
	static void dijkstra(int[][] graph, int src) {
		// The output array. dist[i] will hold the shortest distance from src to i
		int[] dist = new int[V];
		
		// sptSet[i] will true if vertex i is included in shortest path tree or shortest distance from src to i is finalized
		boolean[] sptSet = new boolean[V];
		
		// Initialize all distances as INFINITE and stpSet[] as false
	    for (int i = 0; i < V; i++) {
	    	dist[i] = INT_MAX;
	    	sptSet[i] = false;
	    }
	    
	    // Distance of source vertex from itself is always 0
	    dist[src] = 0;
	    
	    // Find shortest path for all vertices
	    for (int count = 0; count < V; count++) {
	    	// Pick the minimum distance vertex from the set of vertices not
	        // yet processed. u is always equal to src in first iteration.
	        int u = minDistance(dist, sptSet);
	        
	        // Mark the picked vertex as processed
	        sptSet[u] = true;
	        
	        // Update dist value of the adjacent vertices of the picked vertex.
	        for (int v = 0; v < V; v++) {
	        	// Update dist[v] only if:
	        	// (1) is not in sptSet --- sptSet[v] == false
	        	// (2) there is an edge from u to v --- graph[u][v] > 0
	        	// (3) and total weight of path from src to v through u is smaller than current value of dist[v] --- dist[u] + graph[u][v] < dist[v]
	        	if (sptSet[v] == false && graph[u][v] > 0 && dist[u] != INT_MAX && dist[u] + graph[u][v] < dist[v]) {
	        		dist[v] = dist[u] + graph[u][v];
	        	}
	        }
	    }
	    
	    // print the constructed distance array
	    printSolution(dist);
	}
	
	// A utility function to find the vertex with minimum distance value, from
	// the set of vertices not yet included in shortest path tree
	static int minDistance(int[] dist, boolean[] sptSet) {
	   // Initialize min value
	   int min = INT_MAX;
	   int min_index = -1;
	   
	   for (int v = 0; v < V; v++) {
	     if (sptSet[v] == false && dist[v] <= min) {
	         min = dist[v]; 
	         min_index = v;
	     }
	   }
	   
	   return min_index;
	}
	
	// A utility function to print the constructed distance array
	static void printSolution(int[] dist) {
	   System.out.println("Vertex Distance from Source");
	   for (int i = 0; i < V; i++) {
	      System.out.format("%d \t %d\n", i, dist[i]);
	   }
	}
	
}
