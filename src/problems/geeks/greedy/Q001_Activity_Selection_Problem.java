package problems.geeks.greedy;

/**
 * Activity Selection Problem
 * 
 * http://www.geeksforgeeks.org/greedy-algorithms-set-1-activity-selection-problem/
 * 
 * Let us consider the Activity Selection problem as our first example of Greedy algorithms. Following is the problem statement.
 * You are given n activities with their start and finish times. Select the maximum number of activities that can be performed by a single person, assuming that a person can only work on a single activity at a time.
 * Example:
 * 
 * Consider the following 6 activities. 
 * 
 *      start[]  =  {1, 3, 0, 5, 8, 5};
 *      finish[] =  {2, 4, 6, 7, 9, 9};
 *      
 * The maximum set of activities that can be executed by a single person is {0, 1, 3, 4}
 * 
 * The greedy choice is to always pick the next activity whose finish time is least among the remaining activities and the start time is more than or equal to the finish time of previously selected activity. We can sort the activities according to their finishing time so that we always consider the next activity as minimum finishing time activity.
 * 
 * 1) Sort the activities according to their finishing time
 * 2) Select the first activity from the sorted array and print it.
 * 3) Do following for remaining activities in the sorted array.
 * ��.a) If the start time of this activity is greater than the finish time of previously selected activity then select this activity and print it.
 */

import java.util.*;

public class Q001_Activity_Selection_Problem {
	
	public static void main(String[] args) {
		int[] start  = {1, 3, 0, 5, 8, 5};
		int[] finish = {2, 4, 6, 7, 9, 9};
		printMaxActivities(start, finish, 6);
	}
	
	static void printMaxActivities(int[] start, int[] finish, int n) {
		if (n <= 0) {
			return;
		}
		
		System.out.format("%d ", 0);
		int f = finish[0];
		
		for (int i = 1; i < n; i++) {
			if (start[i] >= f) {
				System.out.format("%d ", i);
				f = finish[i];
			}
		}
	}
	
}
