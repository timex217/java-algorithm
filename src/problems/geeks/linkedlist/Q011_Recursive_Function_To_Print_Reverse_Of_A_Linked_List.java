package problems.geeks.linkedlist;

/**
 * Recursive function to print reverse of a Linked List
 * 
 * http://www.geeksforgeeks.org/write-a-recursive-function-to-print-reverse-of-a-linked-list/
 * 
 * Note that the question is only about printing the reverse. 
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q011_Recursive_Function_To_Print_Reverse_Of_A_Linked_List {

	public static void main(String[] args) {
		LinkedList list = new LinkedList(10);
		reversePrint(list);
	}
	
	static void reversePrint(LinkedList list) {
		if (list == null) {
			return;
		}
		
		print(list.head);
	}
	
	static void print(ListNode head) {
		if (head == null) {
			return;
		}
		
		print(head.next);
		
		System.out.format("%d ", head.val);
	}
	
}
