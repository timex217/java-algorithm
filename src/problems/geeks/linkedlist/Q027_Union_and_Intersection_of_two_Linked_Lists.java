package problems.geeks.linkedlist;

/**
 * Union and Intersection of two Linked Lists
 * 
 * Given two Linked Lists, create union and intersection lists that contain union and intersection of the elements present in the given lists. Order of elments in output lists doesn�t matter.
 * 
 * Example:
 * 
 * Input:
 *    List1: 10->15->4->20
 *    lsit2:  8->4->2->10
 * Output:
 *    Intersection List: 4->10
 *    Union List: 2->8->20->4->15->10
 *    
 *    
 * Method 2 (Use Merge Sort)
 * In this method, algorithms for Union and Intersection are very similar. First we sort the given lists, then we traverse the sorted lists to get union and intersection.
 * Following are the steps to be followed to get union and intersection lists.
 * 
 * 1) Sort the first Linked List using merge sort. This step takes O(mLogm) time. Refer this post for details of this step (http://www.geeksforgeeks.org/merge-sort-for-linked-list/).
 * 2) Sort the second Linked List using merge sort. This step takes O(nLogn) time. Refer this post for details of this step (http://www.geeksforgeeks.org/merge-sort-for-linked-list/).
 * 3) Linearly scan both sorted lists to get the union and intersection. This step takes O(m + n) time. This step can be implemented using the same algorithm as sorted arrays algorithm discussed here.
 * 
 * Time complexity of this method is O(mLogm + nLogn) which is better than method 1�s time complexity.
 * 
 * 
 * Method 3 (Use Hashing)
 * Union (list1, list2)
 * Initialize the result list as NULL and create an empty hash table. Traverse both lists one by one, for each element being visited, look the element in hash table. If the element is not present, then insert the element to result list. If the element is present, then ignore it.
 * 
 * Intersection (list1, list2)
 * Initialize the result list as NULL and create an empty hash table. Traverse list1. For each element being visited in list1, insert the element in hash table. Traverse list2, for each element being visited in list2, look the element in hash table. If the element is present, then insert the element to result list. If the element is not present, then ignore it.
 * 
 * Both of the above methods assume that there are no duplicates.
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q027_Union_and_Intersection_of_two_Linked_Lists {

	public static void main(String[] args) {
		LinkedList a = new LinkedList(new int[]{10, 15, 4, 20});
		LinkedList b = new LinkedList(new int[]{8, 4, 2, 10});
		
		ListNode intersection = getIntersection(a.head, b.head);
		ListNode union = getUnion(a.head, b.head);
		
		intersection.print();
		union.print();
	}
	
	// ----------------------------
	//  Intersection
	// ----------------------------
	static ListNode getIntersection(ListNode l1, ListNode l2) {
		ListNode a = l1;
		ListNode b = l2;
		
		// use a hashmap to store visited nodes
		Set<Integer> set = new HashSet<Integer>();
		
		// use a fake head
		ListNode fh = new ListNode();
		ListNode p = fh;
		
		// step 1. go through list a
		while (a != null) {
			// avoid duplication
			if (!set.contains(a.val)) {
				set.add(a.val);
			}
			a = a.next;
		}
		
		// step 2. go through list b
		while (b != null) {
			if (set.contains(b.val)) {
				p.next = b;
				p = b;
				set.remove(b.val); // avoid duplication
			}
			b = b.next;
		}
		
		p.next = null;
		
		return fh.next;
	}
	
	// ----------------------------
	//  Union
	// ----------------------------
	static ListNode getUnion(ListNode l1, ListNode l2) {
		ListNode a = l1;
		ListNode b = l2;
		
		// use a hashmap to store visited nodes
		Set<Integer> set = new HashSet<Integer>();
		
		// use a fake head
		ListNode fh = new ListNode();
		ListNode p = fh;
		
		// step 1. go through list a
		while (a != null) {
			if (!set.contains(a.val)) {
				set.add(a.val);
				p.next = a;
				p = a;
			}
			a = a.next;
		}
		
		// step 2. go through list b
		while (b != null) {
			if (!set.contains(b.val)) {
				set.add(b.val);
				p.next = b;
				p = b;
			}
			b = b.next;
		}
		
		p.next = null;
		return fh.next;
	}
}
