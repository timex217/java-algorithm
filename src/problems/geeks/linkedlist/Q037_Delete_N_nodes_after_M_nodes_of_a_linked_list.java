package problems.geeks.linkedlist;

/**
 * Delete N nodes after M nodes of a linked list
 * 
 * http://www.geeksforgeeks.org/delete-n-nodes-after-m-nodes-of-a-linked-list/
 * 
 * Given a linked list and two integers M and N. Traverse the linked list such that you retain M nodes then delete next N nodes, continue the same till end of the linked list.
 * 
 * Difficulty Level: Rookie
 * 
 * Examples:
 * 
 * Input:
 * M = 2, N = 2
 * Linked List: 1->2->3->4->5->6->7->8
 * Output:
 * Linked List: 1->2->5->6
 * 
 * Input:
 * M = 3, N = 2
 * Linked List: 1->2->3->4->5->6->7->8->9->10
 * Output:
 * Linked List: 1->2->3->6->7->8
 * 
 * Input:
 * M = 1, N = 1
 * Linked List: 1->2->3->4->5->6->7->8->9->10
 * Output:
 * Linked List: 1->3->5->7->9
 * The main part of the problem is to maintain proper links between nodes, make sure that all corner cases are handled. Following is C implementation of function skipMdeleteN() that skips M nodes and delete N nodes till end of list. It is assumed that M cannot be 0.
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q037_Delete_N_nodes_after_M_nodes_of_a_linked_list {

	public static void main(String[] args) {
		LinkedList list = new LinkedList(10);
		skipMdeleteN(list.head, 3, 2);
		list.print();
	}
	
	static void skipMdeleteN(ListNode head, int M, int N) {
		if (head == null) {
			return;
		}
		
		ListNode curr = head;
		
		while (curr != null) {
			// skip M nodes
			for (int i = 1; i < M && curr != null; i++) {
				curr = curr.next;
			}
			
			// If we reached end of list, then return
			if (curr == null) {
				return;
			}
			
			// Start from next node and delete N nodes
			ListNode t = curr.next;
			for (int i = 1; i <= N && t != null; i++) {
				t = t.next;
			}
			
			// Link the previous list with remaining nodes
			curr.next = t;
			
			// Set current pointer for next iteration
	        curr = t;
		}
	}
	
}
