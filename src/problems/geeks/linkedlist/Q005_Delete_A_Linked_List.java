package problems.geeks.linkedlist;

/**
 * Delete a Linked List
 * 
 * Algorithm: Iterate through the linked list and delete all the nodes one by one. 
 * Main point here is not to access next of the current pointer if current pointer is deleted.
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q005_Delete_A_Linked_List {
	
	public static void main(String[] args) {
		LinkedList list = new LinkedList(10);
		
		deleteList(list.head);
		
		list.print();
	}
	
	static void deleteList(ListNode head) {
		if (head == null) {
			return;
		}
		
		deleteList(head.next);
		head = null;
	}
	
}
