package problems.geeks.linkedlist;

/**
 * Nth node from the end of a Linked List
 * 
 * http://www.geeksforgeeks.org/category/linked-list/page/5/
 * 
 * Given a Linked List and a number n, write a function that returns the value at the nth node from end of the Linked List.
 * 
 * Method 2 (Use two pointers) 
 * Maintain two pointers – reference pointer and main pointer. Initialize both reference and main pointers to head. 
 * First move reference pointer to n nodes from head. Now move both pointers one by one until reference pointer reaches end. 
 * Now main pointer will point to nth node from the end. Return main pointer.
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q004_Nth_Node_From_The_End_Of_A_Linked_List {
	
	public static void main(String[] args) {
		LinkedList list = new LinkedList(10);
		
		ListNode node = lastNthNode(list.head, 3);
		
		if (node != null) {
			System.out.println(node.val);
		} else {
			System.out.println("No such node");
		}
	}
	
	static ListNode lastNthNode(ListNode head, int n) {
		ListNode slow = head;
		ListNode fast = head;
		
		// fast先走n步
		int count = 0;
		while (fast != null && count < n) {
			count++;
			fast = fast.next;
		}
		
		// 此时，fast 要么为 null, 要么就是走了 n 步
		
		// slow和fast同时走
		while (fast != null) {
			slow = slow.next;
			fast = fast.next;
		}
		
		return slow;
	}
	
}
