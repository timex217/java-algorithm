package problems.geeks.linkedlist;

/**
 * Swap Kth node from beginning with Kth node from end in a Linked List
 * 
 * Given a singly linked list, swap kth node from beginning with kth node from end. Swapping of data is not allowed, only pointers should be changed. 
 * This requirement may be logical in many situations where the linked list data part is huge (For example student details line Name, RollNo, Address, ..etc). 
 * The pointers are always fixed (4 bytes for most of the compilers).
 * 
 * 1->2->3->4->5->6->7->8
 * 
 * for k = 3, the above list should be changed to following (6 and 3 are swapped)
 * 
 * 1->2->6->4->5->3->7->8
 * 
 * The problem seems simple at first look, but it has many interesting cases.
 * 
 * Let X be the kth node from beginning and Y be the kth node from end. Following are the interesting cases that must be handled.
 * 1) Y is next to X
 * 2) X is next to Y
 * 3) X and Y are same
 * 4) X and Y don�t exist (k is more than number of nodes in linked list)
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q035A_Swap_Kth_node_from_beginning_with_Kth_node_from_end_in_a_Singly_Linked_List {

	public static void main(String[] args) {
		LinkedList list = new LinkedList(8);
		ListNode head = swap(list.head, 3);
		head.print();
	}
	
	static ListNode swap(ListNode head, int k) {
		// Count nodes in linked list
	    int n = countNodes(head);
	    
	    // Check if k is valid
	    if (n < k) {
	    	return head;
	    }
	    
	    // If x (kth node from start) and y(kth node from end) are same
	    if (k == n - k + 1) {
	    	return head;
	    }
	    
	    // Find the kth node from beginning of linked list. We also find
	    // previous of kth node because we need to update next pointer of
	    // the previous.
	    ListNode x = head;
	    ListNode x_prev = null;
	    for (int i = 1; i < k; i++)
	    {
	        x_prev = x;
	        x = x.next;
	    }
	    
	    // Similarly, find the kth node from end and its previous. kth node
	    // from end is (n-k+1)th node from beginning
	    ListNode y = head;
	    ListNode y_prev = null;
	    for (int i = 1; i < n-k+1; i++)
	    {
	        y_prev = y;
	        y = y.next;
	    }
	    
	    // If x_prev exists, then new next of it will be y. Consider the case
	    // when y->next is x, in this case, x_prev and y are same. So the statement
	    // "x_prev->next = y" creates a self loop. This self loop will be broken
	    // when we change y->next.
	    //
	    //      x_prev   x           y_prev   y
	    // 1  ->  2  ->  3  ->  4  ->  5  ->  6  ->  7  ->  8
	    // 
	    //      x_prev   y   
	    // 1  ->  2  ->  6  ->  7  ->  8 
	    
	    if (x_prev != null)
	        x_prev.next = y;
	 
	    // Same thing applies to y_prev
	    //
	    // x           y_prev 
	    // 3  ->  4  ->  5
	    // |             |
	    // ------ < ------
	    
	    if (y_prev != null)
	        y_prev.next = x;
	    
	    // Swap next pointers of x and y. These statements also break self
	    // loop if x->next is y or y->next is x
	    //
	    //      x_prev   y          y_prev   x
	    // 1  ->  2  ->  6  ->  4 ->  5  ->  3  ->  7  ->  8
	    // 
	    
	    ListNode temp = x.next;
	    x.next = y.next;
	    y.next = temp;
	    
	    // Change head pointers when k is 1 or n
	    if (k == 1)
	        head = y;
	    if (k == n)
	        head = x;
	    
	    return head;
	}
	
	static int countNodes(ListNode head) {
		int n = 0;
		ListNode p = head;
		while (p != null) {
			n++;
			p = p.next;
		}
		return n;
	}
	
}
