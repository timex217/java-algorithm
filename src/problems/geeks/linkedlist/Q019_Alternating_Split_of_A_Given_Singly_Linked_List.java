package problems.geeks.linkedlist;

/**
 * Alternating split of a given Singly Linked List
 * 
 * http://www.geeksforgeeks.org/alternating-split-of-a-given-singly-linked-list/
 * 
 * Write a function AlternatingSplit() that takes one list and divides up its nodes to make two smaller lists ‘a’ and ‘b’. 
 * The sublists should be made from alternating elements in the original list. 
 * So if the original list is 0->1->0->1->0->1 then one sublist should be 0->0->0 and the other should be 1->1->1.
 * 
 * 思维：利用链表的删除操作和添加到头的操作是O(1)，一旦遇到0，将其插入到头前即可，所以复杂度是O(n)
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q019_Alternating_Split_of_A_Given_Singly_Linked_List {

	public static void main(String[] args) {
		LinkedList list = new LinkedList(new int[]{0, 1, 0, 1, 0, 1});
		splitList(list);
	}
	
	static void splitList(LinkedList list) {
		if (list == null) {
			return;
		}
		
		ListNode a = new ListNode();
		ListNode pa = a;
		ListNode b = new ListNode();
		ListNode pb = b;
		
		ListNode node = list.head;
		while (node != null) {
			if (node.val == 0) {
				pa.next = node;
				pa = node;
			} else {
				pb.next = node;
				pb = node;
			}
			
			node = node.next;
		}
		
		pa.next = null;
		pb.next = null;
		
		if (a.next != null) {
			a.next.print();
		}
		
		if (b.next != null) {
			b.next.print();
		}
	}
	
}
