package problems.geeks.linkedlist;

/**
 * Design a stack with operations on middle element
 * 
 * http://www.geeksforgeeks.org/design-a-stack-with-find-middle-operation/
 * 
 * How to implement a stack which will support following operations in O(1) time complexity?
 * 1) push() which adds an element to the top of stack.
 * 2) pop() which removes an element from top of stack.
 * 3) findMiddle() which will return middle element of the stack.
 * 4) deleteMiddle() which will delete the middle element.
 * Push and pop are standard stack operations.
 * 
 * The important question is, whether to use a linked list or array for implementation of stack?
 * 
 * Please note that, we need to find and delete middle element. Deleting an element from middle is not O(1) for array. 
 * Also, we may need to move the middle pointer up when we push an element and move down when we pop(). In singly linked list, 
 * moving middle pointer in both directions is not possible.
 * 
 * The idea is to use Doubly Linked List (DLL). We can delete middle element in O(1) time by maintaining mid pointer. 
 * We can move mid pointer in both directions using previous and next pointers.
 */

public class Q036_Design_a_stack_with_operations_on_middle_element {

}
