package problems.geeks.linkedlist;

/**
 * Reverse a Linked List in groups of given size
 * 
 * http://www.geeksforgeeks.org/reverse-a-list-in-groups-of-given-size/
 * 
 * Given a linked list, write a function to reverse every k nodes (where k is an input to the function).
 * 
 * Example:
 * Inputs:  1->2->3->4->5->6->7->8->NULL and k = 3 
 * Output:  3->2->1->6->5->4->8->7->NULL. 
 * 
 * Inputs:  1->2->3->4->5->6->7->8->NULL and k = 5
 * Output:  5->4->3->2->1->8->7->6->NULL. 
 * 
 * Algorithm: reverse(head, k)
 * 1) Reverse the first sub-list of size k. While reversing keep track of the next node and previous node. 
 *    Let the pointer to the next node be next and pointer to the previous node be prev. See this post for reversing a linked list.
 * 
 * 2) head->next = reverse(next, k) Recursively call for rest of the list and link the two sub-lists
 * 
 * 3) return prev /* prev becomes the new head of the list (see the diagrams of iterative method of this post) 
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q006C_Reverse_Linked_List_In_Groups_of_Given_Size {

	public static void main(String[] args) {
		LinkedList list = new LinkedList(8);
		list.head = reverse(list.head, 3);
		list.print();
	}
	
	static ListNode reverse(ListNode head, int k) {
		if (head == null) {
			return null;
		}
		
		ListNode curr = head;
		ListNode next = null;
		ListNode prev = null;
		
		int count = 0;
		
		// reverse first k nodes of the linked list
		while (curr != null && count < k) {
			next = curr.next;
			curr.next = prev;
			prev = curr;
			curr = next;
			
			count++;
		}
		
		// then recursion
		// head 此时已是最后一个! prev是最头一个
		head.next = reverse(curr, k);
		
		return prev;
	}
	
}
