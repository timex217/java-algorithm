package problems.geeks.linkedlist;

/**
 * Print the middle of a given linked list
 * 
 * Method 1:
 * Traverse the whole linked list and count the no. of nodes. Now traverse the list again till count/2 and return the node at count/2.
 * 
 * Method 2:
 * Traverse linked list using two pointers. Move one pointer by one and other pointer by two. 
 * When the fast pointer reaches end slow pointer will reach middle of the linked list.
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q003_Print_The_Middle_Of_A_Given_Linked_List {

	public static void main(String[] args) {
		LinkedList list = new LinkedList(9);
		ListNode middle = getMiddleNode(list.head);
		
		if (middle != null) {
			System.out.println(middle.val);
		} else {
			System.out.println("No such node");
		}
	}
	
	static ListNode getMiddleNode(ListNode head) {
		ListNode slow = head;
		ListNode fast = head;
		
		while (fast != null && fast.next != null) {
			slow = slow.next;
			fast = fast.next.next;
		}
		
		return slow;
	}
	
}
