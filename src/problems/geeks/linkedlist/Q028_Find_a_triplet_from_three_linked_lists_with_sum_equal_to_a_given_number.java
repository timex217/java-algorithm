package problems.geeks.linkedlist;

/**
 * Find a triplet from three linked lists with sum equal to a given number
 * 
 * http://www.geeksforgeeks.org/find-a-triplet-from-three-linked-lists-with-sum-equal-to-a-given-number/
 * 
 * Given three linked lists, say a, b and c, find one node from each list such that the sum of the values of the nodes is equal to a given number. 
 * For example, if the three linked lists are 12->6->29, 23->5->8 and 90->20->59, and the given number is 101, the output should be tripel “6 5 90″.
 * 
 * In the following solutions, size of all three linked lists is assumed same for simplicity of analysis. 
 * The following solutions work for linked lists of different sizes also.
 * 
 * A simple method to solve this problem is to run three nested loops. The outermost loop picks an element from list a, 
 * the middle loop picks an element from b and the innermost loop picks from c. 
 * The innermost loop also checks whether the sum of values of current nodes of a, b and c is equal to given number. 
 * 
 * The time complexity of this method will be O(n^3).
 * 
 * Sorting can be used to reduce the time complexity to O(n*n). Following are the detailed steps.
 * 1) Sort list b in ascending order (5->8->23), and list c in descending order(90->59->20).
 * 2) After the b and c are sorted, one by one pick an element from list a and find the pair by traversing both b and c. 
 * See isSumSorted() in the following code. The idea is similar to Quadratic algorithm of 3 sum problem.
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q028_Find_a_triplet_from_three_linked_lists_with_sum_equal_to_a_given_number {

	public static void main(String[] args) {
		LinkedList a = new LinkedList(new int[]{12, 6, 29});
		LinkedList b = new LinkedList(new int[]{23, 5, 8});
		LinkedList c = new LinkedList(new int[]{90, 20, 59});
		
		b.mergeSort(true);
		c.mergeSort(false);
		
		findSumTriple(a.head, b.head, c.head, 101);
	}
	
	static void findSumTriple(ListNode l1, ListNode l2, ListNode l3, int target) {
		ListNode a = l1, b, c;
		
		while (a != null) {
			b = l2;
			c = l3;
			
			while (b != null && c != null) {
				int sum = a.val + b.val + c.val;
				if (sum == target) {
					System.out.format("%d, %d, %d\n", a.val, b.val, c.val);
					return;
				}
				
				if (sum < target) {
					b = b.next;
				} else {
					c = c.next;
				}
			}
			
			a = a.next;
		}
	}
	
}
