package problems.geeks.linkedlist;

/**
 * Merge two sorted linked lists
 * 
 * http://www.geeksforgeeks.org/merge-two-sorted-linked-lists/
 * 
 * Write a SortedMerge() function that takes two lists, each of which is sorted in increasing order, and merges the two together into one 
 * list which is in increasing order. SortedMerge() should return the new list. The new list should be made by splicing
 * together the nodes of the first two lists.
 * 
 * For example if the first linked list a is 5->10->15 and the other linked list b is 2->3->20, then SortedMerge() should return a pointer 
 * to the head node of the merged list 2->3->5->10->15->20.
 * 
 * There are many cases to deal with: either ‘a’ or ‘b’ may be empty, during processing either ‘a’ or ‘b’ may run out first, and finally 
 * there’s the problem of starting the result list empty, and building it up while going through ‘a’ and ‘b’.
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q020_Merge_Two_Sorted_Linked_Lists {

	public static void main(String[] args) {
		LinkedList a = new LinkedList(new int[]{5, 10, 15});
		LinkedList b = new LinkedList(new int[]{2, 3, 20});
		ListNode c = merge(a.head, b.head);
		
		if (c != null) {
			c.print();
		}
	}
	
	// ---------------------------
	//  Iterative
	// ---------------------------
	
	static ListNode merge(ListNode l1, ListNode l2) {
		// use a fake head
		ListNode head = new ListNode();
		ListNode p = head;
		
		while (l1 != null && l2 != null) {
			if (l1.val < l2.val) {
				p.next = l1;
				p = l1;
				l1 = l1.next;
			} else {
				p.next = l2;
				p = l2;
				l2 = l2.next;
			}
		}
		
		// 断开
		p.next = null;
		// 返回
		return head.next;
	}
	
	// ---------------------------
	//  Recursion
	// ---------------------------
	
	static ListNode mergeR(ListNode l1, ListNode l2) {
		if (l1 == null) {
			return l2;
		}
		
		if (l2 == null) {
			return l1;
		}
		
		ListNode res;
		
		if (l1.val < l2.val) {
			res = l1;
			res.next = mergeR(l1.next, l2);
		} else {
			res = l2;
			res.next = mergeR(l1, l2.next);
		}
		
		return res;
	}
}
