package problems.geeks.linkedlist;

/**
 * Delete a node in a Doubly Linked List
 * 
 * http://www.geeksforgeeks.org/delete-a-node-in-a-doubly-linked-list/
 * 
 * Write a function to delete a given node in a doubly linked list.
 * 
 * Algorithm
 * Let the node to be deleted is del.
 * 1) If node to be deleted is head node, then change the head pointer to next current head.
 * 2) Set next of previous to del, if previous to del exixts.
 * 3) Set prev of next to del, if next to del exixts.
 */

import java.util.*;

public class Q016_Delete_A_Node_In_Doubly_Linked_List {
	
	static class ListNode {
		int val;
		ListNode next;
		ListNode prev;
	}
	
	static void DeleteNode(ListNode head, ListNode del) {
		if (head == null || del == null) {
			return;
		}
		
		// 若是头节点，直接砍去
		if (head == del) {
			head = head.next;
			return;
		}
		
		if (del.prev != null)
			del.prev.next = del.next;
		
		if (del.next != null)
			del.next.prev = del.prev;
		
		del = null;
	}
	
	public static void main(String[] args) {
		
	}
	
}
