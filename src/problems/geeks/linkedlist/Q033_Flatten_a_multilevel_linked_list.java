package problems.geeks.linkedlist;

/**
 * Flatten a multilevel linked list
 * 
 * http://www.geeksforgeeks.org/flatten-a-linked-list-with-next-and-child-pointers/
 * 
 * Given a linked list where in addition to the next pointer, each node has a child pointer, which may or may not point to a separate list. 
 * 
 * These child lists may have one or more children of their own, and so on, to produce a multilevel data structure, as shown in below figure.
 * 
 * You are given the head of the first level of the list. Flatten the list so that all the nodes appear in a single-level linked list. 
 * 
 * You need to flatten the list in way that all nodes at first level should come first, then nodes of second level, and so on.
 * 
 * The problem clearly say that we need to flatten level by level. The idea of solution is, we start from first level, 
 * process all nodes one by one, if a node has a child, then we append the child at the end of list, otherwise we don’t do anything. 
 * After the first level is processed, all next level nodes will be appended after first level. Same process is followed for the appended nodes.
 * 
 * 1) Take "cur" pointer, which will point to head of the fist level of the list
 * 2) Take "tail" pointer, which will point to end of the first level of the list
 * 3) Repeat the below procedure while "curr" is not NULL.
 *     I) if current node has a child then
 * 	a) append this new child list to the "tail"
 * 		tail->next = cur->child
 * 	b) find the last node of new child list and update "tail"
 * 		tmp = cur->child;
 * 		while (tmp->next != NULL)
 * 			tmp = tmp->next;
 * 		tail = tmp;
 *   II) move to the next node. i.e. cur = cur->next 
 */

import java.util.*;

public class Q033_Flatten_a_multilevel_linked_list {

	static class ListNode {
		int val;
		ListNode next;
		ListNode child;
		ListNode(int x) {
			this.val = x;
			this.next = null;
			this.child = null;
		}
	}
	
	static void flattenList(ListNode head) {
		// base case
		if (head == null) {
			return;
		}
		
		ListNode current = head;
		ListNode tail = head;
		ListNode tmp = null;
		
		// step 1. find the tail
		while (tail.next != null) {
			tail = tail.next;
		}
		
		// step 2.
		while (current != tail) {
			// 如果有小孩，就先把孩子都放到后面去
			if (current.child != null) {
				// then append the child at the end of current list
				tail.next = current.child;
				
				// and update the tail to new last node
				tmp = current.child;
				while (tmp.next != null) {
					tmp = tmp.next;
				}
				
				tail = tmp;
			}
			current = current.next;
		}
	}
	
	public static void main(String[] args) {
		
	}
	
}
