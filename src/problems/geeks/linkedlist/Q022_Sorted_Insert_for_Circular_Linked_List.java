package problems.geeks.linkedlist;

/**
 * Sorted insert for circular linked list
 * 
 * http://www.geeksforgeeks.org/sorted-insert-for-circular-linked-list/
 * 
 * Difficulty Level: Rookie
 * Write a C function to insert a new value in a sorted Circular Linked List (CLL). For example, if the input CLL is following.
 * 
 * Algorithm:
 * Allocate memory for the newly inserted node and put data in the newly allocated node. Let the pointer to the new node be new_node. 
 * After memory allocation, following are the three cases that need to be handled.
 * 
 * 1) Linked List is empty:  
 *     a)  since new_node is the only node in CLL, make a self loop.      
 *           new_node->next = new_node;  
 *     b) change the head pointer to point to new node.
 *           *head_ref = new_node;
 * 2) New node is to be inserted just before the head node:    
 *   (a) Find out the last node using a loop.
 *          while(current->next != *head_ref)
 *             current = current->next;
 *   (b) Change the next of last node. 
 *          current->next = new_node;
 *   (c) Change next of new node to point to head.
 *          new_node->next = *head_ref;
 *   (d) change the head pointer to point to new node.
 *          *head_ref = new_node;
 * 3) New node is to be  inserted somewhere after the head: 
 *    (a) Locate the node after which new node is to be inserted.
 *          while ( current->next!= *head_ref && 
 *              current->next->data < new_node->data)
 *          {   current = current->next;   }
 *    (b) Make next of new_node as next of the located pointer
 *          new_node->next = current->next;
 *    (c) Change the next of the located pointer
 *          current->next = new_node; 
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q022_Sorted_Insert_for_Circular_Linked_List {

	public static void main(String[] args) {
		
	}
	
	static ListNode insertSortedCLL(ListNode head, ListNode node) {
		ListNode current = head;
		
		// case 1, linked list is empty
		if (head == null) {
			node.next = node;
			head = node;
		}
		// case 2, head node is larger or equal to new node
		// insert to the front, link the last node to node
		else if (head.val >= node.val) {
			while (current.next != head) {
				current = current.next;
			}
			current.next = node;
			node.next = head;
			head = node;
		}
		// case 3
		else {
			while (current.next != head && current.next.val < node.val) {
				current = current.next;
			}
			node.next = current.next;
			current.next = node;
		}
		
		return head;
	}
	
}
