package problems.geeks.linkedlist;

/**
 * Detect loop in a linked list
 * 
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q007_Detect_Loop_In_A_Linked_List {

	public static void main(String[] args) {
		LinkedList list = new LinkedList();
		
		ListNode node1 = list.append(1);
		ListNode node2 = list.append(2);
		ListNode node3 = list.append(3);
		ListNode node4 = list.append(4);
		ListNode node5 = list.append(5);
		
		// make a loop
		node5.next = node2;
		
		boolean res = detectLoop(list);
		System.out.println(res);
	}
	
	/**
	 * Floyd�s Cycle-Finding Algorithm:
	 * 
	 * This is the fastest method. Traverse linked list using two pointers.  
	 * Move one pointer by one and other pointer by two.  If these pointers meet at some node then there is a loop.  
	 * If pointers do not meet then linked list doesn�t have loop.
	 */
	static boolean detectLoop(LinkedList list) {
		if (list == null || list.head == null) {
			return false;
		}
		
		ListNode head = list.head;
		ListNode fast = head;
		ListNode slow = head;
		
		while (fast != null && fast.next != null) {
			slow = slow.next;
			fast = fast.next.next;
			
			if (fast == slow) {
				return true;
			}
		}
		
		return false;
	}
	
}
