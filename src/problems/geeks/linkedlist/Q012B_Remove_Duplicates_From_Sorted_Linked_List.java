package problems.geeks.linkedlist;

/**
 * Remove duplicates from a sorted linked list (A. keep none)
 * 
 * http://www.geeksforgeeks.org/remove-duplicates-from-a-sorted-linked-list/
 * 
 * Write a removeDuplicates() function which takes a list sorted in non-decreasing order and deletes any duplicate nodes from the list. The list should only be traversed once.
 * 
 * For example if the linked list is 11->11->11->21->43->43->60 then removeDuplicates() should convert the list to 21->60.
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q012B_Remove_Duplicates_From_Sorted_Linked_List {

	public static void main(String[] args) {
		int[] arr = {11, 11, 11, 21, 43, 43, 60};
		LinkedList list = new LinkedList(arr);
		
		ListNode head = removeDups(list);
		head.print();
	}
	
	static ListNode removeDups(LinkedList list) {
		if (list == null || list.head == null) {
			return null;
		}
		
		// use a fake head
		ListNode head = new ListNode();
		ListNode prev = head;
		
		ListNode slow = list.head;
		ListNode fast = null;
		
		while (slow != null) {
			// count the occurrances of slow
			int count = 1;
			fast = slow.next;
			
			while (fast != null && fast.val == slow.val) {
				count++;
				fast = fast.next;
			}
			
			// only keep slow if the count is 1
			if (count == 1) {
				prev.next = slow;
				prev = slow;
			}
			
			// new start
			slow = fast;
		}
		
		// cut off the list
		prev.next = null;
		
		return head.next;
	}
	
}
