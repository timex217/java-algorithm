package problems.geeks.linkedlist;

/**
 * Sort a linked list of 0s, 1s and 2s
 * 
 * http://www.geeksforgeeks.org/sort-a-linked-list-of-0s-1s-or-2s/
 * 
 * Source: Microsoft Interview | Set 1
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q032_Sort_a_linked_list_of_0s_1s_and_2s {

	public static void main(String[] args) {
		LinkedList list = new LinkedList(new int[]{0, 1, 0, 2, 1, 1, 2, 1, 2});
		ListNode res = sortList(list.head);
		res.print();
	}
	
	static ListNode sortList(ListNode head) {
		if (head == null) {
			return null;
		}
		
		// list of 0s
		ListNode f0 = new ListNode();
		ListNode p0 = f0;
		
		// list of 1s
		ListNode f1 = new ListNode();
		ListNode p1 = f1;
		
		// list of 2s
		ListNode f2 = new ListNode();
		ListNode p2 = f2;
		
		ListNode p = head;
		while (p != null) {
			switch (p.val) {
			case 0:
				p0.next = p;
				p0 = p;
				break;
					
			case 1:
				p1.next = p;
				p1 = p;
				break;
					
			case 2:
				p2.next = p;
				p2 = p;
				break;
			}
			
			p = p.next;
		}
		
		p2.next = null;
		p1.next = f2.next;
		p0.next = f1.next;
		
		return f0.next;
	}
	
}
