package problems.geeks.linkedlist;

/**
 * Intersection of two Sorted Linked Lists
 * 
 * http://www.geeksforgeeks.org/intersection-of-two-sorted-linked-lists/
 * 
 * Given two lists sorted in increasing order, create and return a new list representing the intersection of the two lists. 
 * The new list should be made with its own memory � the original lists should not be changed.
 * 
 * For example, let the first linked list be 1->2->3->4->6 and second linked list be 2->4->6->8, then your function should create and return a third list as 2->4->6.
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q017_Intersection_of_Two_Sorted_Linked_Lists {

	public static void main(String[] args) {
		LinkedList l1 = new LinkedList(new int[]{1, 2, 3, 4, 6});
		LinkedList l2 = new LinkedList(new int[]{2, 4, 6, 8});
		ListNode head = getIntersection(l1.head, l2.head);
		head.print();
	}
	
	static ListNode getIntersection(ListNode h1, ListNode h2) {
		if (h1 == null || h2 == null) {
			return null;
		}
		
		ListNode head = new ListNode();
		ListNode prev = head;
		
		ListNode n1 = h1, n2 = h2;
		
		while (n1 != null && n2 != null) {
			if (n1.val == n2.val) {
				ListNode node = new ListNode(n1.val);
				prev.next = node;
				prev = node;
				
				n1 = n1.next;
				n2 = n2.next;
			}
			else if (n1.val < n2.val) {
				n1 = n1.next;
			}
			else {
				n2 = n2.next;
			}
		}
		
		prev.next = null;
		return head.next;
	}
	
}
