package problems.geeks.linkedlist;

/**
 * Delete alternate nodes of a Linked List
 * 
 * http://www.geeksforgeeks.org/delete-alternate-nodes-of-a-linked-list/
 * 
 * Given a Singly Linked List, starting from the second node delete all alternate nodes of it. 
 * For example, if the given linked list is 1->2->3->4->5 then your function should convert it to 1->3->5, 
 * and if the given linked list is 1->2->3->4 then convert it to 1->3.
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q018_Delete_Alternate_Nodes_of_Linked_List {

	public static void main(String[] args) {
		LinkedList list = new LinkedList(5);
		deleteAlternate(list);
		list.print();
	}
	
	static void deleteAlternate(LinkedList list) {
		if (list == null) {
			return;
		}
		
		ListNode node = list.head;
		
		while (node != null && node.next != null) {
			node.next = node.next.next;
			node = node.next;
		}
	}
}
