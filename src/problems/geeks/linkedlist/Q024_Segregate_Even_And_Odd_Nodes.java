package problems.geeks.linkedlist;

/**
 * Segregate even and odd nodes in a Linked List
 * 
 * http://www.geeksforgeeks.org/segregate-even-and-odd-elements-in-a-linked-list/
 * 
 * Given a Linked List of integers, write a function to modify the linked list such that all even numbers appear before all the odd numbers 
 * in the modified linked list. Also, keep the order of even and odd numbers same.
 * 
 * Examples:
 * Input: 17->15->8->12->10->5->4->1->7->6->NULL
 * Output: 8->12->10->4->6->17->15->5->1->7->NULL
 * 
 * Input: 8->12->10->5->4->1->6->NULL
 * Output: 8->12->10->4->6->5->1->NULL
 * 
 * // If all numbers are even then do not change the list
 * Input: 8->12->10->NULL
 * Output: 8->12->10->NULL
 * 
 * // If all numbers are odd then do not change the list
 * Input: 1->3->5->7->NULL
 * Output: 1->3->5->7->NULL
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q024_Segregate_Even_And_Odd_Nodes {

	public static void main(String[] args) {
		LinkedList list = new LinkedList(new int[]{17, 15, 8, 12, 10, 5, 4, 1, 7, 6});
		ListNode head = segregate(list.head);
		head.print();
	}
	
	static ListNode segregate(ListNode head) {
		// create fake heads
		ListNode fa = new ListNode();
		ListNode pa = fa;
		
		ListNode fb = new ListNode();
		ListNode pb = fb;
		
		ListNode node = head;
		
		while (node != null) {
			if (node.val % 2 == 0) {
				pa.next = node;
				pa = node;
			} else {
				pb.next = node;
				pb = node;
			}
			node = node.next;
		}
		
		pa.next = fb.next;
		pb.next = null;
		
		return fa.next;
	}
	
}
