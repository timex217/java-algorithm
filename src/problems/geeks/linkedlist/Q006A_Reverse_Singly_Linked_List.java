package problems.geeks.linkedlist;

/**
 * Reverse a linked list
 * 
 * http://www.geeksforgeeks.org/write-a-function-to-reverse-the-nodes-of-a-linked-list/
 * 
 * Iterative Method
 * Iterate trough the linked list. In loop, change next to prev, prev to current and current to next.
 * 
 * Recursive Method:
 * 
 *    1) Divide the list in two parts - first node and rest of the linked list.
 *    2) Call reverse for the rest of the linked list.
 *    3) Link rest to first.
 *    4) Fix head pointer
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q006A_Reverse_Singly_Linked_List {

	public static void main(String[] args) {
		LinkedList list = new LinkedList(10);
		// 1. reverse iteratively
		reverse(list);
		
		// 2. reverse recursively
		list.head = reverseR(list.head);
		
		list.print();
	}
	
	// ----------------------
	//  Iterative
	// ----------------------
	
	static void reverse(LinkedList list) {
		ListNode head = list.head;
		
		ListNode prev = null;
		ListNode curr = head;
		ListNode next = null;
		
		while (curr != null) {
			next = curr.next;
			curr.next = prev;
			prev = curr;
			curr = next;
		}
		
		list.head = prev;
	}
	
	// ----------------------
	//  Recursion
	// ----------------------
	
	static ListNode reverseR(ListNode head) {
		if (head == null || head.next == null) {
			return head;
		}
		
		ListNode first = head;
		ListNode rest = head.next;
		
		// reverse recursively
		head = reverseR(rest);
		
		// fix the first node after recursion
		first.next.next = first;
		first.next = null;
		
		return head;
	}
}
