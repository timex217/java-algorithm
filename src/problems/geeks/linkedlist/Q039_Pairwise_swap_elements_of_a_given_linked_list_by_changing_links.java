package problems.geeks.linkedlist;

/**
 * Pairwise swap elements of a given linked list by changing links
 * 
 * http://www.geeksforgeeks.org/pairwise-swap-elements-of-a-given-linked-list-by-changing-links/
 * 
 * Given a singly linked list, write a function to swap elements pairwise. 
 * For example, if the linked list is 1->2->3->4->5->6->7 then the function should change it to 2->1->4->3->6->5->7, 
 * and if the linked list is 1->2->3->4->5->6 then the function should change it to 2->1->4->3->6->5
 * 
 * This problem has been discussed here. The solution provided there swaps data of nodes. If data contains many fields, there will be many swap operations. 
 * So changing links is a better idea in general. 
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q039_Pairwise_swap_elements_of_a_given_linked_list_by_changing_links {

	public static void main(String[] args) {
		LinkedList list = new LinkedList(7);
		pairWiseSwap(list.head);
	}
	
	// ------------------------------
	//  Iterative
	// ------------------------------
	static void pairWiseSwap(ListNode head) 
	{
		// If linked list is empty or there is only one node in list
	    if (head == null || head.next == null)
	        return;
	    
	    // Initialize previous and current pointers
	    ListNode prev = head;
	    ListNode curr = prev.next;
	    
	    head = curr;  // Change head before proceeeding
	    
	    // Traverse the list
	    while (true)
	    {
	        ListNode next = curr.next;
	        curr.next = prev; // Change next of current as previous node
	 
	        // If next NULL or next is the last node
	        if (next == null || next.next == null)
	        {
	            prev.next = next;
	            break;
	        }
	 
	        // Change next of previous to next next
	        prev.next = next.next;
	 
	        // Update previous and curr
	        prev = next;
	        curr = prev.next;
	    }
	    
	    head.print();
	}
	
	// ------------------------------
	//  Recursion
	// ------------------------------
	/* Function to pairwise swap elements of a linked list.
	   It returns head of the modified list, so return value
	   of this node must be assigned */
	static ListNode pairWiseSwapR(ListNode head)
	{
	    // Base Case: The list is empty or has only one node
	    if (head == null || head.next == null)
	        return head;
	 
	    // Store head of list after two nodes
	    ListNode rest = head.next.next;
	 
	    // Change head
	    ListNode newhead = head.next;
	 
	    // Change next of second node
	    head.next.next = head;
	 
	    // Recur for remaining list and change next of head
	    head.next = pairWiseSwapR(rest);
	 
	    // Return new head of modified list
	    return newhead;
	}
	
}
