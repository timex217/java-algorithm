package problems.geeks.linkedlist;

/**
 * Given a linked list, reverse alternate nodes and append at the end
 * 
 * http://www.geeksforgeeks.org/given-linked-list-reverse-alternate-nodes-append-end/
 * 
 * Given a linked list, reverse alternate nodes and append them to end of list. Extra allowed space is O(1) 
 * Examples
 * 
 * Input List:  1->2->3->4->5->6
 * Output List: 1->3->5->6->4->2
 * 
 * Input List:  12->14->16->18->20
 * Output List: 12->16->20->18->14
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q040_Reverse_alternate_nodes_and_append_at_the_end {

	public static void main(String[] args) {
		LinkedList list = new LinkedList(6);
		ListNode head = rearrange(list.head);
		head.print();
	}
	
	static ListNode rearrange(ListNode head) {
		if (head == null) {
			return null;
		}
		
		ListNode fa = new ListNode();
		ListNode pa = fa, pb = null;
		
		ListNode curr = head, next = null;
		
		int count = 1;
		while (curr != null) {
			next = curr.next;
			
			// odd
			if ((count & 1) == 1) {
				pa.next = curr;
				pa = curr;
			}
			// even
			else {
				curr.next = pb;
				pb = curr;
			}
			
			count++;
			curr = next;
		}
		
		pa.next = pb;
		
		return fa.next;
	}
	
}
