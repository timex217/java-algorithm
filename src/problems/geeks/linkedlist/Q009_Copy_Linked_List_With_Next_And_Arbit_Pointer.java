package problems.geeks.linkedlist;

/**
 * Copy a linked list with next and arbit pointer
 * 
 * http://www.geeksforgeeks.org/a-linked-list-with-next-and-arbit-pointer/
 * 
 * You are given a Double Link List with one pointer of each node pointing to the next node just like in a single link list. 
 * The second pointer however CAN point to any node in the list and not just the previous node. 
 * Now write a program in O(n) time to duplicate this list. That is, write a program which will create a copy of this list.
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q009_Copy_Linked_List_With_Next_And_Arbit_Pointer {
	
	public static void main(String[] args) {
		LinkedList list = new LinkedList(10);
		copyLinkedList(list);
	}
	
	static void copyLinkedList(LinkedList list) {
		if (list == null || list.head == null) {
			return;
		}
		
		ListNode node = list.head;
		ListNode copy = null;
		ListNode next = null;
		
		// step 1. insert copy nodes after each original node
		while (node != null) {
			next = node.next;
			copy = new ListNode(node.val);
			copy.next = next;
			node.next = copy;
			node = next;
		}
		
		list.print();
		
		// step 2. copy arbit pointer
		node = list.head;
		
		while (node != null && node.next != null) {
			next = node.next.next;
			//node.next.arbit = node.arbit.next;
			node = next;
		}
		
		// step 3. cut the linked list
		node = list.head;
		ListNode copy_head = node.next;
		
		while (node != null && node.next != null) {
			copy = node.next;
			next = node.next.next;
			
			node.next = next;
			copy.next = (copy.next == null) ? null : copy.next.next;
			
			node = next;
		}
		
		copy_head.print();
	}
	
}
