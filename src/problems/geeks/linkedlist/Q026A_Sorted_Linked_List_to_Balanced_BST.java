package problems.geeks.linkedlist;

/**
 * Sorted Linked List to Balanced BST
 * 
 * http://www.geeksforgeeks.org/sorted-linked-list-to-balanced-bst/
 * 
 * Given a Singly Linked List which has data members sorted in ascending order. 
 * Construct a Balanced Binary Search Tree which has same data members as the given Linked List.
 * 
 * Examples:
 * 
 * Input:  Linked List 1->2->3
 * Output: A Balanced BST 
 *      2   
 *    /  \  
 *   1    3 
 * 
 * 
 * Input: Linked List 1->2->3->4->5->6->7
 * Output: A Balanced BST
 *         4
 *       /   \
 *      2     6
 *    /  \   / \
 *   1   3  4   7  
 * 
 * Input: Linked List 1->2->3->4
 * Output: A Balanced BST
 *       3   
 *     /  \  
 *    2    4 
 *  / 
 * 1
 * 
 * Input:  Linked List 1->2->3->4->5->6
 * Output: A Balanced BST
 *       4   
 *     /   \  
 *    2     6 
 *  /  \   / 
 * 1   3  5   
 * 
 * Method 1 (Simple)
 * Following is a simple algorithm where we first find the middle node of list and make it root of the tree to be constructed.
 * 
 * 1) Get the Middle of the linked list and make it root.
 * 2) Recursively do same for left half and right half.
 *        a) Get the middle of left half and make it left child of the root
 *           created in step 1.
 *        b) Get the middle of right half and make it right child of the
 *           root created in step 1.
 * 
 * Time complexity: O(nLogn) where n is the number of nodes in Linked List.
 */

import java.util.*;

import adt.ListNode;
import adt.LinkedList;
import adt.tree.TreeNode;

public class Q026A_Sorted_Linked_List_to_Balanced_BST {

	public static void main(String[] args) {
		LinkedList list = new LinkedList(new int[]{1, 2, 3, 4, 5, 6, 7});
		TreeNode root = sortedListToBST(list.head);
		root.print();
	}
	
	static TreeNode sortedListToBST(ListNode head) {
		if (head == null) {
			return null;
		}
		
		// 务必单独考虑只剩下一个节点的时候, 否则会死循环!
		if (head.next == null) {
			return new TreeNode(head.val);
		}
		
		// step 1. find the middle node
		ListNode prev = null;
		ListNode slow = head;
		ListNode fast = head;
		
		while (fast != null && fast.next != null) {
			prev = slow;
			slow = slow.next;
			fast = fast.next.next;
		}
		
		// step 2. create the root node
		TreeNode root = new TreeNode(slow.val);
		
		// step 3. build left and right trees recursively
		// 先把前面的链表切断
		prev.next = null;
		
		root.left = sortedListToBST(head);
		root.right = sortedListToBST(slow.next);
		
		return root;
	}
	
}
