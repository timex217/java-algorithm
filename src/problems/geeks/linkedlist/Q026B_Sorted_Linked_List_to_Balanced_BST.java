package problems.geeks.linkedlist;

/**
 * Sorted Linked List to Balanced BST
 * 
 * http://www.geeksforgeeks.org/sorted-linked-list-to-balanced-bst/
 * 
 * Given a Singly Linked List which has data members sorted in ascending order. 
 * Construct a Balanced Binary Search Tree which has same data members as the given Linked List.
 * 
 * Examples:
 * 
 * Input:  Linked List 1->2->3
 * Output: A Balanced BST 
 *      2   
 *    /  \  
 *   1    3 
 * 
 * 
 * Input: Linked List 1->2->3->4->5->6->7
 * Output: A Balanced BST
 *         4
 *       /   \
 *      2     6
 *    /  \   / \
 *   1   3  4   7  
 * 
 * Input: Linked List 1->2->3->4
 * Output: A Balanced BST
 *       3   
 *     /  \  
 *    2    4 
 *  / 
 * 1
 * 
 * Input:  Linked List 1->2->3->4->5->6
 * Output: A Balanced BST
 *       4   
 *     /   \  
 *    2     6 
 *  /  \   / 
 * 1   3  5   
 * 
 * Method 2 (Tricky) 
 * The method 1 constructs the tree from root to leaves. In this method, we construct from leaves to root. 
 * 
 * The idea is to insert nodes in BST in the same order as the appear in Linked List, so that the tree can be constructed in O(n) time complexity. 
 * 
 * We first count the number of nodes in the given Linked List. Let the count be n. After counting nodes, 
 * 
 * we take left n/2 nodes and recursively construct the left subtree. After left subtree is constructed, we allocate memory for root and link the left subtree with root. 
 * 
 * Finally, we recursively construct the right subtree and link it with root.
 * 
 * While constructing the BST, we also keep moving the list head pointer to next so that we have the appropriate pointer in each recursive call.
 */

import java.util.*;

import adt.ListNode;
import adt.LinkedList;
import adt.tree.TreeNode;

public class Q026B_Sorted_Linked_List_to_Balanced_BST {
	
	static ListNode head;
	
	public static void main(String[] args) {
		LinkedList list = new LinkedList(new int[]{1, 2, 3, 4, 5, 6, 7});
		head = list.head;
		TreeNode root = sortedListToBST(7);
		root.print();
	}
	
	static TreeNode sortedListToBST(int n) {
		if (n <= 0) {
			return null;
		}
		
		TreeNode left = sortedListToBST(n/2);
		
		TreeNode root = new TreeNode(head.val);
		
		root.left = left;
		
		head = head.next;  // 这个是最巧妙的地方！
		
		root.right = sortedListToBST(n - n/2 - 1);
		
		return root;
	}
}
