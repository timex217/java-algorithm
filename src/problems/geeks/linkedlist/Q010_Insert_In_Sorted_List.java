package problems.geeks.linkedlist;

/**
 * Given a linked list which is sorted, how will you insert in sorted way
 * 
 * http://www.geeksforgeeks.org/given-a-linked-list-which-is-sorted-how-will-you-insert-in-sorted-way/
 * 
 * Algorithm: 
 * Let input linked list is sorted in increasing order.
 * 
 * 1) If Linked list is empty then make the node as head and return it.
 * 2) If value of the node to be inserted is smaller than value of head node
 *     then insert the node at start and make it head.
 * 3) In a loop, find the appropriate node after which the input node (let 9) is
 *     to be inserted. To find the appropriate node start from head, keep moving 
 *     until you reach a node GN (10 in the below diagram) who's value is 
 *     greater than the input node. The node just before GN is the appropriate
 *     node (7).
 * 4) Insert the node (9) after the appropriate node (7) found in step 3.
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q010_Insert_In_Sorted_List {
	
	public static void main(String[] args) {
		int[] arr = {1, 2, 3, 5, 7, 9};
		LinkedList list = new LinkedList(arr);
		insert(list, new ListNode(6));
	}
	
	static void insert(LinkedList list, ListNode node) {
		if (list == null || node == null) {
			return;
		}
		
		ListNode head = list.head;
		
		if (head == null || head.val >= node.val ) {
			node.next = head;
			head = node;
		} else {
			ListNode current = head;
			while (current.next != null && current.next.val < node.val) {
				current = current.next;
			}
			node.next = current.next;
			current.next = node;
		}
		
		list.print();
	}
	
}
