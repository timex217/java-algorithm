package problems.geeks.linkedlist;

/**
 * Check if a singly linked list is palindrome
 * 
 * Given a singly linked list of characters, write a function that returns true if the given list is palindrome, else false.
 * 
 * 1 -> 2 -> 3 -> 4 -> 3 -> 2 -> 1
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q008_Check_If_A_Singly_Linked_List_Is_Palindrome {
	
	public static void main(String[] args) {
		// initialize the list
		LinkedList list = new LinkedList();
		int[] arr = {1, 2, 3, 4, 3, 2, 1};
		list.append(arr);
		list.print();
		
		// check if it's palindrome
		boolean res = isPalindrome(list);
		
		System.out.println(res);
	}
	
	static boolean isPalindrome(LinkedList list) {
		if (list == null || list.head == null) {
			return false;
		}
		
		// get the middle node
		ListNode fast = list.head;
		ListNode slow = list.head;
		ListNode prev = null;
		
		while (fast != null && fast.next != null) {
			prev = slow;
			slow = slow.next;
			fast = fast.next.next;
		}
		
		// 奇数长度, 记录下正中间的点
		// 1  2  3  2  1
		//    p  s     f
		if (fast != null) {
			slow = slow.next;
		}
		
		// 分割成两条链表
		prev.next = null;
		
		// 反转右边一半
		ListNode second_half = reverse(slow);
		
		return compareLists(list.head, second_half);
		
	}
	
	static ListNode reverse(ListNode head) {
		if (head == null || head.next == null) {
			return head;
		}
		
		ListNode first = head;
		ListNode rest = head.next;
		
		head = reverse(rest);
		
		first.next.next = first;
		first.next = null;
		
		return head;
	}
	
	static boolean compareLists(ListNode l1, ListNode l2) {
		if (l1 == null && l2 == null) {
			return true;
		}
		
		if (l1 == null || l2 == null) {
			return false;
		}
		
		ListNode p1 = l1;
		ListNode p2 = l2;
		
		while (p1 != null && p2 != null) {
			if (p1.val == p2.val) {
				p1 = p1.next;
				p2 = p2.next;
			} else {
				return false;
			}
		}
		
		if (p1 == null && p2 == null) {
			return true;
		}
		
		return false;
	}
	
}
