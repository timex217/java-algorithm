package problems.geeks.linkedlist;

/**
 * Rotate a Linked List
 * 
 * http://www.geeksforgeeks.org/rotate-a-linked-list/
 * 
 * Given a singly linked list, rotate the linked list counter-clockwise by k nodes. Where k is a given positive integer. 
 * 
 * For example, if the given linked list is 10->20->30->40->50->60 and k is 4, the list should be modified to 50->60->10->20->30->40. 
 * 
 * Assume that k is smaller than the count of nodes in linked list.
 * 
 * To rotate the linked list, we need to change next of kth node to NULL, next of last node to previous head node, and finally change head to (k+1)th node. 
 * So we need to get hold of three nodes: kth node, (k+1)th node and last node.
 * Traverse the list from beginning and stop at kth node. Store pointer to kth node. We can get (k+1)th node using kthNode->next. 
 * Keep traversing till end and store pointer to last node also. Finally, change pointers as stated above.
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q029_Rotate_a_Linked_List {

	public static void main(String[] args) {
		LinkedList list = new LinkedList(new int[]{10, 20, 30, 40, 50, 60});
		leftRotate(list, 4);
		list.print();
	}
	
	static void leftRotate(LinkedList list, int k) {
		if (list == null) {
			return;
		}
		
		ListNode head = list.head;
		ListNode current = head;
		
		int count = 1;
		while (current != null && count < k) {
			count++;
			current = current.next;
		}
		
		// If current is NULL, k is greater than or equal to count
	    // of nodes in linked list. Don't change the list in this case
		if (current == null) {
			return;
		}
		
		// current points to kth node. Store it in a variable.
	    // kthNode points to node 40 in the above example
		ListNode kthNode = current;
		
		// current will point to last node after this loop
	    // current will point to node 60 in the above example
		while (current.next != null) {
			current = current.next;
		}
		
		// Change next of last node to previous head
	    // Next of 60 is now changed to node 10
		current.next = head;
		
		// Change head to (k+1)th node
	    // head is now changed to node 50
		list.head = kthNode.next;
		
		// change next of kth node to NULL
	    // next of 40 is now NULL
		kthNode.next = null;
	}
	
}
