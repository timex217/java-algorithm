package problems.geeks.linkedlist;

/**
 * Move last element to front of a given Linked List
 * 
 * http://www.geeksforgeeks.org/move-last-element-to-front-of-a-given-linked-list/
 * 
 * Write a C function that moves last element to front in a given Singly Linked List. For example, if the given Linked List is 1->2->3->4->5, 
 * then the function should change the list to 5->1->2->3->4.
 * 
 * Algorithm:
 * Traverse the list till last node. Use two pointers: one to store the address of last node and other for address of second last node. 
 * After the end of loop do following operations.
 * i) Make second last as last (secLast->next = NULL).
 * ii) Set next of last as head (last->next = *head_ref).
 * iii) Make last as head ( *head_ref = last)
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q014_Move_Last_Element_To_Front_Of_A_Given_Linked_List {
	
	public static void main(String[] args) {
		LinkedList list = new LinkedList(5);
		moveLastToFront(list);
		list.print();
	}
	
	static void moveLastToFront(LinkedList list) {
		if (list == null || list.head == null) {
			return;
		}
		
		ListNode prev = null;
		ListNode node = list.head;
		
		while (node.next != null) {
			prev = node;
			node = node.next;
		}
		
		prev.next = null;
		node.next = list.head;
		list.head = node;
	}
	
}
