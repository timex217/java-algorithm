package problems.geeks.linkedlist;

/**
 * Split a Circular Linked List into two halves
 * 
 * http://www.geeksforgeeks.org/split-a-circular-linked-list-into-two-halves/
 * 
 * 这道题目比较简单
 * 
 * 1) Store the mid and last pointers of the circular linked list using tortoise and hare algorithm.
 * 2) Make the second half circular.
 * 3) Make the first half circular.
 * 4) Set head (or start) pointers of the two linked lists.
 * 
 * In the below implementation, if there are odd nodes in the given circular linked list then the first result list has 1 more node than the second result list.
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q013_Split_A_Circular_Linked_List_Into_Two_Halves {

	public static void main(String[] args) {}
	
}
