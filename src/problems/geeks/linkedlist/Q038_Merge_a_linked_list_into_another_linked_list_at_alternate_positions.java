package problems.geeks.linkedlist;

/**
 * Merge a linked list into another linked list at alternate positions
 * 
 * http://www.geeksforgeeks.org/merge-a-linked-list-into-another-linked-list-at-alternate-positions/
 * 
 * Given two linked lists, insert nodes of second list into first list at alternate positions of first list.
 * For example, if first list is 5->7->17->13->11 and second is 12->10->2->4->6, the first list should become 5->12->7->10->17->2->13->4->11->6 and 
 * second list should become empty. The nodes of second list should only be inserted when there are positions available. 
 * 
 * For example, if the first list is 1->2->3 and second list is 4->5->6->7->8, then first list should become 1->4->2->5->3->6 and second list to 7->8.
 * 
 * Use of extra space is not allowed (Not allowed to create additional nodes), i.e., insertion must be done in-place. 
 * Expected time complexity is O(n) where n is number of nodes in first list.
 * 
 * The idea is to run a loop while there are available positions in first loop and insert nodes of second list by changing pointers.
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q038_Merge_a_linked_list_into_another_linked_list_at_alternate_positions {

	public static void main(String[] args) {
		LinkedList l1 = new LinkedList(new int[]{5, 7, 17, 13, 11});
		LinkedList l2 = new LinkedList(new int[]{12, 10, 2, 4, 6, 7, 8});
		merge(l1.head, l2.head);
	}
	
	// Main function that inserts nodes of linked list q into p at alternate
	// positions. Since head of first list never changes and head of second list 
	// may change, we need single pointer for first list and double pointer for 
	// second list.
	static void merge(ListNode a, ListNode b) {
		ListNode a_curr = a, b_curr = b;
		ListNode a_next = null, b_next = null;
		
		while (a_curr != null && b_curr != null) {
			// Save next pointers
			a_next = a_curr.next;
			b_next = b_curr.next;
			
			// Make b_curr as next of a_curr
	        b_curr.next = a_next;  // Change next pointer of b_curr
	        a_curr.next = b_curr;  // Change next pointer of a_curr
	        
	        // Update current pointers for next iteration
	        a_curr = a_next;
	        b_curr = b_next;
		}
		
		b = b_curr;
		
		if (a != null) {
			a.print();
		}
		
		if (b != null) {
			b.print();
		}
	}
	
}
