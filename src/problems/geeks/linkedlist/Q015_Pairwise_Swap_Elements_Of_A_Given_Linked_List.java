package problems.geeks.linkedlist;

/**
 * Pairwise swap elements of a given linked list
 * 
 * http://www.geeksforgeeks.org/pairwise-swap-elements-of-a-given-linked-list/
 * 
 * Given a singly linked list, write a function to swap elements pairwise. 
 * For example, if the linked list is 1->2->3->4->5 then the function should change it to 2->1->4->3->5, 
 * and if the linked list is 1->2->3->4->5->6 then the function should change it to 2->1->4->3->6->5.
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q015_Pairwise_Swap_Elements_Of_A_Given_Linked_List {

	public static void main(String[] args) {
		LinkedList list = new LinkedList(5);
		pairSwap(list);
		
		//pairSwapR(list.head);
		list.print();
	}
	
	static void swap(ListNode node1, ListNode node2) {
		int tmp = node1.val;
		node1.val = node2.val;
		node2.val = tmp;
	}
	
	// ---------------------------
	//  Iterative
	// ---------------------------
	static void pairSwap(LinkedList list) {
		if (list == null) {
			return;
		}
		
		ListNode node = list.head;
		
		while (node != null && node.next != null) {
			swap(node, node.next);
			node = node.next.next;
		}
	}
	
	// ---------------------------
	//  Recursion
	// ---------------------------
	static void pairSwapR(ListNode head) {
		// 至少要有连续两个才能交换
		if (head == null || head.next == null) {
			return;
		}
		
		// 两两交换
		swap(head, head.next);
		
		pairSwapR(head.next.next);
	}
	
}
