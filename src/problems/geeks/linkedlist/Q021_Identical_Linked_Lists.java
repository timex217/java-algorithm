package problems.geeks.linkedlist;

/**
 * Identical Linked Lists
 * 
 * http://www.geeksforgeeks.org/identical-linked-lists/
 * 
 * Two Linked Lists are identical when they have same data and arrangement of data is also same. 
 * For example Linked lists a (1->2->3) and b(1->2->3) are identical.
 * 
 * Write a function to check if the given two linked lists are identical.
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q021_Identical_Linked_Lists {

	public static void main(String[] args) {
		LinkedList a = new LinkedList(new int[]{1, 2, 3});
		LinkedList b = new LinkedList(new int[]{1, 2, 3});
		
		boolean res = isIdentical(a.head, b.head);
		System.out.println(res);
	}
	
	// ----------------------------
	//  Iterative
	// ----------------------------
	static boolean isIdentical(ListNode a, ListNode b) {
		while (a != null && b != null) {
			if (a.val != b.val) {
				return false;
			}
			
			a = a.next;
			b = b.next;
		}
		
		if (a != null || b != null) {
			return false;
		}
		
		return true;
	}
	
	// -----------------------------
	//  Recursion
	// -----------------------------
	static boolean isIdenticalR(ListNode a, ListNode b) {
		if (a == null && b == null) {
			return true;
		}
		
		if (a == null || b == null) {
			return false;
		}
		
		if (a.val != b.val) {
			return false;
		}
		
		return isIdenticalR(a.next, b.next);
	}
}
