package problems.geeks.linkedlist;

/**
 * Remove duplicates from an unsorted linked list
 * 
 * http://www.geeksforgeeks.org/remove-duplicates-from-an-unsorted-linked-list/
 * 
 * Write a removeDuplicates() function which takes a list and deletes any duplicate nodes from the list. The list is not sorted.
 * 
 * For example if the linked list is 12->11->12->21->41->43->21 then removeDuplicates() should convert the list to 12->11->21->41->43.
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q012C_Remove_Duplicates_From_Unsorted_Linked_List {
	
	public static void main(String[] args) {
		int[] arr = {12, 11, 12, 21, 41, 43, 21};
		LinkedList list = new LinkedList(arr);
		
		ListNode head = removeDups(list);
		head.print();
	}
	
	static ListNode removeDups(LinkedList list) {
		if (list == null) {
			return null;
		}
		
		// use a hash set to track pre-existed node
		Set<Integer> set = new HashSet<Integer>();
		
		// use a fake head
		ListNode head = new ListNode();
		ListNode prev = head;
		
		ListNode node = list.head;
		while (node != null) {
			if (!set.contains(node.val)) {
				prev.next = node;
				prev = node;
				
				set.add(node.val);
			}
			
			node = node.next;
		}
		
		prev.next = null;
		return head.next;
	}
	
}
