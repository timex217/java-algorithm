package problems.geeks.linkedlist;

/**
 * Add two numbers represented by linked lists 
 * 
 * http://www.geeksforgeeks.org/add-two-numbers-represented-by-linked-lists/
 * 
 * Given two numbers represented by two lists, write a function that returns sum list. The sum list is list representation of addition of two input numbers.
 * 
 * Example 1
 * 
 * Input:
 *   First List: 5->6->3  // represents number 365
 *   Second List: 8->4->2 //  represents number 248
 * Output
 *   Resultant list: 3->1->6  // represents number 613
 * Example 2
 * 
 * Input:
 *   First List: 7->5->9->4->6  // represents number 64957
 *   Second List: 8->4 //  represents number 48
 * Output
 *   Resultant list: 5->0->0->5->6  // represents number 65005
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q025_Add_Two_Numbers_Represented_By_Linked_Lists {
	
	public static void main(String[] args) {
		LinkedList a = new LinkedList(new int[]{5, 6, 3, 4});
		LinkedList b = new LinkedList(new int[]{8, 4, 2, 9});
		ListNode c = add(a.head, b.head);
		c.print();
	}
	
	static ListNode add(ListNode a, ListNode b) {
		// create a fake head
		ListNode fc = new ListNode();
		ListNode pc = fc;
		
		int carry = 0, sum = 0;
		
		ListNode node = null;
		
		while (a != null || b != null) {
			sum = ((a == null) ? 0 : a.val) + ((b == null) ? 0 : b.val) + carry;
			carry = sum / 10;
			sum = sum % 10;
			
			node = new ListNode(sum);
			pc.next = node;
			pc = node;
			
			if (a != null) {
				a = a.next;
			}
			
			if (b != null) {
				b = b.next;
			}
		}
		
		if (carry > 0) {
			node = new ListNode(carry);
			pc.next = node;
			pc = node;
		}
		
		pc.next = null;
		
		return fc.next;
	}
	
}
