package problems.geeks.linkedlist;

/**
 * Remove duplicates from a sorted linked list (A. keep one)
 * 
 * Write a removeDuplicates() function which takes a list sorted in non-decreasing order and deletes any duplicate nodes from the list. The list should only be traversed once.
 * 
 * For example if the linked list is 11->11->11->21->43->43->60 then removeDuplicates() should convert the list to 11->21->43->60.
 * 
 * 思路：两个指针，一快一慢，当快指针的值不等于慢指针的值时，将慢指针的next指向快指针，最后切断慢指针的next.
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q012A_Remove_Duplicates_From_Sorted_Linked_List_A {

	public static void main(String[] args) {
		int[] arr = {11, 11, 11, 21, 43, 43, 60};
		LinkedList list = new LinkedList(arr);
		
		removeDups(list);
	}
	
	static void removeDups(LinkedList list) {
		if (list == null || list.head == null) {
			return;
		}
		
		ListNode slow = list.head;
		ListNode fast = slow.next;
		
		while (fast != null) {
			if (fast.val != slow.val) {
				slow.next = fast;
				slow = fast;
			}
			
			fast = fast.next;
		}
		
		slow.next = null;
		
		list.print();
	}
	
}
