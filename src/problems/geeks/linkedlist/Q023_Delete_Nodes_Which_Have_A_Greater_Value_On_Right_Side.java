package problems.geeks.linkedlist;

/**
 * Delete nodes which have a greater value on right side
 * 
 * http://www.geeksforgeeks.org/delete-nodes-which-have-a-greater-value-on-right-side/
 * 
 * Given a singly linked list, remove all the nodes which have a greater value on right side.
 * 
 * Examples:
 * a) The list 12->15->10->11->5->6->2->3->NULL should be changed to 15->11->6->3->NULL. Note that 12, 10, 5 and 2 have been deleted 
 * because there is a greater value on the right side.
 * 
 * When we examine 12, we see that after 12 there is one node with value greater than 12 (i.e. 15), so we delete 12.
 * When we examine 15, we find no node after 15 that has value greater than 15 so we keep this node.
 * When we go like this, we get 15->6->3
 * 
 * b) The list 10->20->30->40->50->60->NULL should be changed to 60->NULL. Note that 10, 20, 30, 40 and 50 have been deleted 
 * because they all have a greater value on the right side.
 * 
 * c) The list 60->50->40->30->20->10->NULL should not be changed.
 */

import java.util.*;
import adt.ListNode;
import adt.LinkedList;

public class Q023_Delete_Nodes_Which_Have_A_Greater_Value_On_Right_Side {

	public static void main(String[] args) {
		LinkedList list = new LinkedList(new int[]{12, 15, 10, 11, 5, 6, 2, 3});
		deleteGreaterNodes(list.head);
	}
	
	static void deleteGreaterNodes(ListNode head) {
		if (head == null) {
			return;
		}
		
		// step 1. reverse the linked list
		head = reverse(head);
		head.print();
		
		// step 2. go through list, if next node is smaller than current max, delete it
		ListNode current = head;
		int max = current.val;
		
		while (current != null && current.next != null) {  // 1. 注意：循环条件
			if (current.next.val >= max) {
				// update max
				max = current.next.val;
				// continue
				current = current.next;
			} else {
				// delete the current.next
				current.next = current.next.next; // 2. 注意：delete掉current.next后，current保持不动!
			}
		}
		
		// step 3. reverse back
		head = reverse(head);
		head.print();
	}
	
	static ListNode reverse(ListNode head) {
		if (head == null || head.next == null) {
			return head;
		}
		
		ListNode first = head;
		ListNode rest = head.next;
		
		head = reverse(rest);
		
		first.next.next = first;
		first.next = null;
		
		return head;
	}
	
}
