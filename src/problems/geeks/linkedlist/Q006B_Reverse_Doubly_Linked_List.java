package problems.geeks.linkedlist;

/**
 * Reverse a Doubly Linked List
 * 
 * http://www.geeksforgeeks.org/reverse-a-doubly-linked-list/
 * 
 * Here is a simple method for reversing a Doubly Linked List. 
 * 
 * All we need to do is swap prev and next pointers for all nodes, change prev of the head (or start) and change the head pointer in the end.
 * 
 * 记住，利用一个curr和prev搞定
 */

public class Q006B_Reverse_Doubly_Linked_List {
	
	static class ListNode {
		int val;
		ListNode next;
		ListNode prev;
		ListNode(int x) {
			this.next = null;
			this.prev = null;
		}
	}
	
	static void reverse(ListNode head) {
		if (head == null) {
			return;
		}
		
		ListNode curr = head;
		ListNode prev = null;
		
		while (curr != null) {
			// swap prev and next
			prev = curr.prev;
			curr.prev = curr.next;
			curr.next = prev;
			curr = curr.prev;
		}
		
		// 因为最后 curr 为null，所以只能依靠 prev 来处理head
		// 这点类似于单链表的处理，只是在单链表中，prev直接就是head
		// 这里的 prev 慢一拍
		if (prev != null) {
			head = prev.prev;
		}
	}
	
	public static void main(String[] args) {
		
	}
}
