package problems.geeks.string;

/**
 * Remove all duplicates from the input string
 * 
 * http://www.geeksforgeeks.org/remove-all-duplicates-from-the-input-string/
 * 
 * In-place removal
 */

import java.util.*;

public class Q002_Remove_All_Duplicates {
	
	public static void main(String[] args) {
		String s = "geeksforgeeks";
		s = removeDups(s);
		System.out.println(s);
	}
	
	static String removeDups(String s) {
		if (s == null) {
			return null;
		}
		
		StringBuilder sb = new StringBuilder();
		Set<Character> set = new HashSet<Character>();
		
		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			
			if (!set.contains(ch)) {
				sb.append(ch);
				set.add(ch);
			}
		}
		
		return sb.toString();
	}
	
}
