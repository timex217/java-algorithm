package problems.geeks.string;

/**
 * Longest Common Substring
 * 
 * http://www.geeksforgeeks.org/longest-common-substring/
 * 
 * Given two strings �X� and �Y�, find the length of the longest common substring. For example, if the given strings are �GeeksforGeeks� and �GeeksQuiz�, the output should be 5 as longest common substring is �Geeks�
 * 
 * Let m and n be the lengths of first and second strings respectively.
 * 
 * A simple solution is to one by one consider all substrings of first string and for every substring check if it is a substring in second string. Keep track of the maximum length substring. There will be O(m^2) substrings and we can find whether a string is subsring on another string in O(n) time (See this). So overall time complexity of this method would be O(n * m2)
 * 
 * Dynamic Programming can be used to find the longest common substring in O(m*n) time. The idea is to find length of the longest common suffix for all substrings of both strings and store these lengths in a table.
 * 
 * The longest common suffix has following optimal substructure property
 *    LCSuff(X, Y, m, n) = LCSuff(X, Y, m-1, n-1) + 1 if X[m-1] = Y[n-1]
 *                         0  Otherwise (if X[m-1] != Y[n-1])
 * 
 * The maximum length Longest Common Suffix is the longest common substring.
 *    LCSubStr(X, Y, m, n)  = Max(LCSuff(X, Y, i, j)) where 1 <= i <= m
 *                                                      and 1 <= j <= n
 */

import java.util.*;

public class Q027_Longest_Common_Substring {
	
	public static void main(String[] args) {
		String x = "OldSite:GeeksforGeeks.org";
		String y = "NewSite:GeeksQuiz.com";
		
		String lcs = LCSubStr(x, y);
		if (lcs == null) {
			System.out.println("NONE");
		} else {
			System.out.println(lcs);
		}
	}
	
	static String LCSubStr(String x, String y) {
		if (x == null || y == null) {
			return null;
		}
		
		int m = x.length();
		int n = y.length();
		int max = 0;
		int max_i = 0;
		int max_j = 0;
		
		int[][] lcs = new int[m + 1][n + 1];
		
		for (int i = 0; i <= m; i++) {
			for (int j = 0; j <= n; j++) {
				if (i == 0 || j == 0) {
					lcs[i][j] = 0;
				} else {
					if (x.charAt(i - 1) == y.charAt(j - 1)) {
						lcs[i][j] = lcs[i - 1][j - 1] + 1;
						if (lcs[i][j] > max) {
							max = lcs[i][j];
							max_i = i - 1;
							max_j = j - 1;
						}
					}
				}
			}
		}
		
		if (max == 0) {
			return null;
		} else {
			return x.substring(max_i - max + 1, max_i + 1);
		}
	}
	
}
