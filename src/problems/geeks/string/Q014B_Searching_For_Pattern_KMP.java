package problems.geeks.string;

/**
 * Searching for Patterns | Set 2 (KMP Algorithm)
 * 
 * http://www.geeksforgeeks.org/searching-for-patterns-set-2-kmp-algorithm/
 * 
 * KMP (Knuth Morris Pratt) Pattern Searching
 * The Naive pattern searching algorithm doesn�t work well in cases where we see many matching characters followed by a mismatching character. Following are some examples.
 * 
 *    txt[] = "AAAAAAAAAAAAAAAAAB"
 *    pat[] = "AAAAB"
 * 
 *    txt[] = "ABABABCABABABCABABABC"
 *    pat[] =  "ABABAC" (not a worst case, but a bad case for Naive)
 * The KMP matching algorithm uses degenerating property (pattern having same sub-patterns appearing more than once in the pattern) of the pattern and improves the worst case complexity to O(n). The basic idea behind KMP�s algorithm is: whenever we detect a mismatch (after some matches), we already know some of the characters in the text (since they matched the pattern characters prior to the mismatch). We take advantage of this information to avoid matching the characters that we know will anyway match.
 * KMP algorithm does some preprocessing over the pattern pat[] and constructs an auxiliary array lps[] of size m (same as size of pattern). Here name lps indicates longest proper prefix which is also suffix.. For each sub-pattern pat[0�i] where i = 0 to m-1, lps[i] stores length of the maximum matching proper prefix which is also a suffix of the sub-pattern pat[0..i].
 * 
 *    lps[i] = the longest proper prefix of pat[0..i] 
 *               which is also a suffix of pat[0..i]. 
 * Examples:
 * For the pattern �AABAACAABAA�, lps[] is [0, 1, 0, 1, 2, 0, 1, 2, 3, 4, 5]
 * For the pattern �ABCDE�, lps[] is [0, 0, 0, 0, 0]
 * For the pattern �AAAAA�, lps[] is [0, 1, 2, 3, 4]
 * For the pattern �AAABAAA�, lps[] is [0, 1, 2, 0, 1, 2, 3]
 * For the pattern �AAACAAAAAC�, lps[] is [0, 1, 2, 0, 1, 2, 3, 3, 3, 4]
 * 
 * Searching Algorithm:
 * Unlike the Naive algo where we slide the pattern by one, we use a value from lps[] to decide the next sliding position. Let us see how we do that. When we compare pat[j] with txt[i] and see a mismatch, we know that characters pat[0..j-1] match with txt[i-j+1...i-1], and we also know that lps[j-1] characters of pat[0...j-1] are both proper prefix and suffix which means we do not need to match these lps[j-1] characters with txt[i-j...i-1] because we know that these characters will anyway match. See KMPSearch() in the below code for details.
 * 
 * Preprocessing Algorithm:
 * In the preprocessing part, we calculate values in lps[]. To do that, we keep track of the length of the longest prefix suffix value (we use len variable for this purpose) for the previous index. We initialize lps[0] and len as 0. If pat[len] and pat[i] match, we increment len by 1 and assign the incremented value to lps[i]. If pat[i] and pat[len] do not match and len is not 0, we update len to lps[len-1]. See computeLPSArray () in the below code for details.
 */

import java.util.*;

public class Q014B_Searching_For_Pattern_KMP {

	public static void main(String[] args) {
		String txt = "AABAACAADAABAAABAA";
		String pat = "AABA";
		
		KMPSearch(txt, pat);
	}
	
	static void KMPSearch(String txt, String pat) {
		int n = txt.length();
		int m = pat.length();
		
		// create lps[] that will hold the longest prefix suffix values for pattern
		int[] lps = new int[m];
		int i = 0, j = 0;
		
		// Preprocess the pattern (calculate lps[] array)
	    computeLPS(pat, lps);
	    
	    // Perform the search
	    while (i < n) {
	    	if (txt.charAt(i) == pat.charAt(j)) {
	    		i++; j++;
	    		
	    		if (j == m) {
	    			System.out.format("Pattern found at index %d\n", i - m);
	    			j = lps[j - 1];
	    		}
	    	} else if (j > 0) {
	    		j = lps[j - 1];
	    	} else {
	    		i++;
	    	}
	    }
	}
	
	// _______/////////_______/////////_______
	//          len    [len]    len    [i]
	//         /   \
	// _______///__///________///__///________
	//         x    x          x    x
	
	static void computeLPS(String pat, int[] lps) {
		int m = pat.length();
		int i = 1, len = 0;
		
		while (i < m) {
			if (pat.charAt(i) == pat.charAt(len)) {
				lps[i++] = ++len;
			} else if (len == 0) {
				lps[i++] = 0;
			} else {
				len = lps[len - 1];
			}
		}
	}
	
}
