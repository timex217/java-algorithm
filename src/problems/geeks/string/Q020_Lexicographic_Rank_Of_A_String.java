package problems.geeks.string;

/**
 * Lexicographic rank of a string
 * 
 * http://www.geeksforgeeks.org/lexicographic-rank-of-a-string/
 * 
 * Given a string, find its rank among all its permutations sorted lexicographically. For example, rank of �abc� is 1, rank of �acb� is 2, and rank of �cba� is 6.
 * 
 * For simplicity, let us assume that the string does not contain any duplicated characters.
 * 
 * One simple solution is to initialize rank as 1, generate all permutations in lexicographic order. 
 * After generating a permutation, check if the generated permutation is same as given string, if same, then return rank, 
 * if not, then increment the rank by 1. The time complexity of this solution will be exponential in worst case. Following is an efficient solution.
 * 
 * Let the given string be �STRING�. In the input string, �S� is the first character. 
 * There are total 6 characters and 4 of them are smaller than �S�. So there can be 4 * 5! smaller strings where first character is smaller than �S�, like following
 * 
 * R X X X X X
 * I X X X X X
 * N X X X X X
 * G X X X X X
 * 
 * Now let us Fix S� and find the smaller strings staring with �S�.
 * 
 * Repeat the same process for T, rank is 4*5! + 4*4! +�
 * 
 * Now fix T and repeat the same process for R, rank is 4*5! + 4*4! + 3*3! +�
 * 
 * Now fix R and repeat the same process for I, rank is 4*5! + 4*4! + 3*3! + 1*2! +�
 * 
 * Now fix I and repeat the same process for N, rank is 4*5! + 4*4! + 3*3! + 1*2! + 1*1! +�
 * 
 * Now fix N and repeat the same process for G, rank is 4*5! + 4*4 + 3*3! + 1*2! + 1*1! + 0*0!
 * 
 * Rank = 4*5! + 4*4! + 3*3! + 1*2! + 1*1! + 0*0! = 597
 * 
 * Since the value of rank starts from 1, the final rank = 1 + 597 = 598
 */

import java.util.*;

public class Q020_Lexicographic_Rank_Of_A_String {
	static class Rank {
		int val = 0;
		boolean found = false;
	}
	
	public static void main(String[] args) {
		String s = "AABCD";
		String w = "BADCA";
		int r = getLexicographicRank(s, w);
		System.out.format("The ranking is: %d", r);
	}
	
	static int getLexicographicRank(String s, String w) {
		Rank rank = new Rank();
		
		Set<String> set = new HashSet<String>();
		Set<String> map = new HashSet<String>();
		
		helper(s, w, "", rank, set, map);
		
		if (rank.found) {
			return rank.val;
		} else {
			return -1;
		}
	}
	
	// dfs solution
	static void helper(String s, String w, String sol, Rank rank, Set<String> set, Set<String> map) {
		if (sol.length() == s.length()) {
			// avoid duplication output
			if (!map.contains(sol)) {
				System.out.println(sol);
				map.add(sol);
				rank.val++;
				// found the result
				if (sol.equals(w)) {
					rank.found = true;
				}
			}
			
			return;
		}
		
		// No more solution
		if (rank.found) {
			return;
		}
		
		
		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			// format the key for hash set
			String key = String.format("%c-%d", ch, i);
			
			if (!set.contains(key)) {
				set.add(key);
				helper(s, w, sol + String.valueOf(ch), rank, set, map);
				set.remove(key);
			}
		}
	}
	
}
