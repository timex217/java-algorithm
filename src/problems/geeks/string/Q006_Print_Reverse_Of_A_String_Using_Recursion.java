package problems.geeks.string;

/**
 * Print reverse of a string using recursion
 * 
 * http://www.geeksforgeeks.org/reverse-a-string-using-recursion/
 * 
 * Write a recursive function to print reverse of a given string.
 */

public class Q006_Print_Reverse_Of_A_String_Using_Recursion {
	
	public static void main(String[] args) {
		String s = "I love Google!";
		reversePrint(s, 0);
	}
	
	static void reversePrint(String s, int index) {
		if (s == null || index == s.length()) {
			return;
		}
		
		reversePrint(s, index + 1);
		
		System.out.format("%c", s.charAt(index));
	}
	
}
