package problems.geeks.string;

/**
 * Count LoL
 * 
 * Given a string "lollooolololol", count how many different ways to form "lol", the letters you pick to form "lol" must keep their relative order in the input string.
 */

import java.util.*;

public class Q036_Count_LoL {

	static int count = 0;
	
	public static void main(String[] args) {
		String str = "lollooolololol";
		countLoL(str, 0, "");
		System.out.println(count);
	}
	
	static void countLoL(String str, int index, String sol) {
		if (sol.equals("lol")) {
			count++;
			return;
		}
		
		for (int i = index; i < str.length(); i++) {
			countLoL(str, i + 1, sol + str.charAt(i));
		}
	}
	
}
