package problems.geeks.string;

/**
 * Print all permutations with repetition of characters
 * 
 * http://www.geeksforgeeks.org/print-all-permutations-with-repetition-of-characters/
 * 
 * Given a string of length n, print all permutation of the given string. Repetition of characters is allowed. 
 * Print these permutations in lexicographically sorted order 
 * Examples:
 * 
 * Input: AB
 * Ouput: All permutations of AB with repetition are:
 *       AA
 *       AB
 *       BA
 *       BB
 * 
 * Input: ABC
 * Output: All permutations of ABC with repetition are:
 *       AAA
 *       AAB
 *       AAC
 *       ABA
 *       ...
 *       ...
 *       CCB
 *       CCC
 * For an input string of size n, there will be n^n permutations with repetition allowed. 
 * The idea is to fix the first character at first index and recursively call for other subsequent indexes. 
 * Once all permutations starting with the first character are printed, fix the second character at first index. Continue these steps till last character.
 */

import java.util.*;

public class Q016_DFS_Print_All_Permutations_With_Repetition_Of_Characters {
	
	public static void main(String[] args) {
		String s = "ABC";
		ArrayList<String> res = new ArrayList<String>();
		dfs(s, "", res);
		System.out.println(res.toString());
	}
	
	static void dfs(String s, String sol, ArrayList<String> res) {
		if (sol.length() == s.length()) {
			res.add(sol);
			return;
		}
		
		for (int i = 0; i < s.length(); i++) {
			String ch = s.substring(i, i + 1);
			dfs(s, sol + ch, res);
		}
	}
	
}
