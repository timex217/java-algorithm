package problems.geeks.string;

/**
 * Return maximum occurring character in the input string
 * 
 * http://www.geeksforgeeks.org/return-maximum-occurring-character-in-the-input-string/
 * 
 * Write an efficient function to return maximum occurring character in the input string e.g., if input string is �test string� then function should return �t�.
 */

import java.util.*;

public class Q001_Find_Maximum_Occurring_Character {
	
	public static void main(String[] args) {
		String s = "test string";
		findMaxOccurringChar(s);
	}
	
	static void findMaxOccurringChar(String s) {
		if (s == null || s.length() == 0) {
			return;
		}
		
		int n = s.length();
		int max = 0;
		char max_char = 0;
		
		// use a hashmap to count the number of occurrance for a character
		Map<Character, Integer> map = new HashMap<Character, Integer>();
		
		for (int i = 0; i < n; i++) {
			char ch = s.charAt(i);
			if (map.containsKey(ch)) {
				map.put(ch, map.get(ch) + 1);
			} else {
				map.put(ch, 1);
			}
			
			if (map.get(ch) > max) {
				max = map.get(ch);
				max_char = ch;
			}
		}
		
		System.out.println(max_char);
	}
	
}
