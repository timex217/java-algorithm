package problems.geeks.string;

/**
 * Remove �b� and �ac� from a given string
 * 
 * http://www.geeksforgeeks.org/remove-a-and-bc-from-a-given-string/
 * 
 * Given a string, eliminate all �b� and �ac� in the string, you have to replace them in-place, and you are only allowed to iterate over the string once. (Source Google Interview Question)
 * 
 * Examples:
 * 
 * acbac   ==>  ""
 * aaac    ==>  aa
 * ababac  ==>  aa
 * bbbbd   ==>  d
 */

public class Q028_Remove_b_ac_From_String {
	
	public static void main(String[] args) {
		String s = "ababaac";
		s = stringFilter(s);
		System.out.println(s);
	}
	
	static String stringFilter(String s) {
		if (s == null) {
			return null;
		}
		
		char[] chars = s.toCharArray();
		
		int i = 0, j = 0, n = chars.length;
		
		while (j < n) {
			if (s.charAt(j) == 'b') {
				j++;
			} else if (s.charAt(j) == 'c') {
				if (j > 0 && s.charAt(j - 1) == 'a') {
					i--;
					j++;
				} else {
					chars[i++] = chars[j++];
				}
			} else {
				chars[i++] = chars[j++];
			}
		}
		
		s = new String(chars);
		return s.substring(0, i);
	}
	
}
