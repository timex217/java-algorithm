package problems.geeks.string;

/**
 * Reverse words in a given string
 * 
 * http://www.geeksforgeeks.org/reverse-words-in-a-given-string/
 * 
 * Example: Let the input string be �i like this program very much�. The function should change the string to �much very program this like i�
 * 
 * Algorithm:
 * 
 * 1) Reverse the individual words, we get the below string.
 *      "i ekil siht margorp yrev hcum"
 * 2) Reverse the whole string from start to end and you get the desired output.
 *      "much very program this like i"
 */

import java.util.*;

public class Q010_Reverse_Words_In_A_Given_String {
	
	public static void main(String[] args) {
		String s = " i  like  this program    very much  ";
		reverseWords(s);
	}
	
	static void reverseWords(String s) {
		if (s == null) {
			return;
		}
		
		char[] chars = s.toCharArray();
		int n = chars.length;
		
		// step 1. reverse the whole string
		reverse(chars, 0, n - 1);
		
		System.out.println(Arrays.toString(chars));
		
		// step 2. reverse each word
		int i = 0;
		while (i < n && chars[i] == ' ') i++;
		int j = i;
		
		while (j < n) {
			while (j < n && chars[j] != ' ') j++;
			reverse(chars, i, j - 1);
			while (j < n && chars[j] == ' ') j++;
			i = j;
		}
		
		s = new String(chars);
		System.out.println(s);
	}
	
	static void reverse(char[] chars, int start, int end) {
		int i = start, j = end;
		while (i < j) {
			swap(chars, i, j);
			i++; j--;
		}
	}
	
	static void swap(char[] chars, int i, int j) {
		char tmp = chars[i];
		chars[i] = chars[j];
		chars[j] = tmp;
	}
	
}
