package problems.geeks.string;

/**
 * Find if a string is interleaved of two other strings
 * 
 * http://www.geeksforgeeks.org/check-whether-a-given-string-is-an-interleaving-of-two-other-given-strings-set-2/
 * 
 * Given three strings A, B and C. Write a function that checks whether C is an interleaving of A and B. 
 * C is said to be interleaving A and B, if it contains all characters of A and B and order of all characters in individual strings is preserved.
 * 
 * We have discussed a simple solution of this problem here. The simple solution doesn’t work if strings A and B have some common characters. 
 * For example A = “XXY”, string B = “XXZ” and string C = “XXZXXXY”. To handle all cases, two possibilities need to be considered.
 * 
 * a) If first character of C matches with first character of A, we move one character ahead in A and C and recursively check.
 * 
 * b) If first character of C matches with first character of B, we move one character ahead in B and C and recursively check.
 * 
 * If any of the above two cases is true, we return true, else false. Following is simple recursive implementation of this approach
 */

public class Q029_Find_If_A_String_Is_Interleaved_Of_Two_Other_Strings {

	public static void main(String[] args) {
		String A = "XXY";
		String B = "XXZ";
		String C = "XXZXXY";
		boolean res = isInterleavedDP(A, B, C);
		System.out.println(res);
	}
	
	static boolean isInterleaved(String A, String B, String C) {
		return isInterleaved(A, 0, B, 0, C, 0);
	}
	
	static boolean isInterleaved(String A, int i, String B, int j, String C, int k) {
		int m = A.length(), n = B.length(), p = C.length();
		
		if (i == m && j == n) {
			return k == p;
		}
		
		if ((i < m && A.charAt(i) == C.charAt(k)) && (j < n && B.charAt(j) == C.charAt(k))) {
			return isInterleaved(A, i + 1, B, j, C, k + 1) || isInterleaved(A, i, B, j + 1, C, k + 1);
		}
		else if (i < m && A.charAt(i) == C.charAt(k)) {
			return isInterleaved(A, i + 1, B, j, C, k + 1);
		}
		else if (j < n && B.charAt(j) == C.charAt(k)) {
			return isInterleaved(A, i, B, j + 1, C, k + 1);
		}
		else {
			return false;
		}
	}
	
	static boolean isInterleavedDP(String A, String B, String C) {
		int m = A.length(), n = B.length(), p = C.length();
		if (m + n != p) {
			return false;
		}
		
		boolean[][] dp = new boolean[m + 1][n + 1];
		
		for (int i = 0; i <= m; i++) {
			for (int j = 0; j <= n; j++) {
				// base case 十分特殊
				if (i == 0 && j == 0) {
					dp[i][j] = true;
				}
				else if (i == 0) {
					dp[i][j] = dp[i][j - 1] && (B.charAt(j - 1) == C.charAt(j - 1));
				}
				else if (j == 0) {
					dp[i][j] = dp[i - 1][j] && (A.charAt(i - 1) == C.charAt(i - 1));
				}
				else {
					dp[i][j] = (dp[i][j - 1] && (B.charAt(j - 1) == C.charAt(i + j - 1))) ||
							   (dp[i - 1][j] && (A.charAt(i - 1) == C.charAt(i + j - 1)));
				}
			}
		}
		
		return dp[m][n];
		
	}
	
}
