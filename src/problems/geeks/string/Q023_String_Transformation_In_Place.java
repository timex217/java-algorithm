package problems.geeks.string;

/**
 * An in-place algorithm for String Transformation
 * 
 * Given a string, move all even positioned elements to end of string. 
 * While moving elements, keep the relative order of all even positioned and odd positioned elements same. 
 * For example, if the given string is "a1b2c3d4e5f6g7h8i9j1k2l3m4", convert it to "abcdefghijklm1234567891234" in-place and in O(n) time complexity.
 */

public class Q023_String_Transformation_In_Place {

	public static void main(String[] args) {
		
	}
	
}
