package problems.geeks.string;

/**
 * Possible Decodings
 * 
 * http://www.geeksforgeeks.org/count-possible-decodings-given-digit-sequence/
 * 
 * Let 1 represent ‘A’, 2 represents ‘B’, etc. Given a digit sequence, count the number of possible decodings of the given digit sequence.
 * 
 * Examples:
 * 
 * Input:  digits[] = "121"
 * Output: 3
 * // The possible decodings are "ABA", "AU", "LA"
 * 
 * Input: digits[] = "1234"
 * Output: 3
 * // The possible decodings are "ABCD", "LCD", "AWD"
 * An empty digit sequence is considered to have one decoding. It may be assumed that the input contains valid digits from 0 to 9 and there are no leading 0’s, 
 * no extra trailing 0’s and no two or more consecutive 0’s.
 */

import java.util.*;

public class Q035_Possible_Decodings {

	public static void main(String[] args) {
		int[] digits = {1, 2, 3, 4};
		int res = countDecodingDP(digits, 4);
		System.out.println(res);
	}
	
	// ---------------------------
	//  Recursion
	// ---------------------------
	
	static int countDecoding(int[] digits, int n) {
		// 当digits的个数只有0个或者1个时，有且只有一种解码形式
		if (n == 0 || n == 1) {
			return 1;
		}
		
		int count = 0;
		
		// 只单独考虑最后一个数字时，解码方式就是前 n-1 个数的解码方式
		if (digits[n - 1] > 0) {
			count = countDecoding(digits, n - 1);
		}
		
		// 同时考虑当前数字和前一个数字时，如果这个两位数在 1-26 之间，那么解码方式就是前 n-2 个数的解码方式
		if (digits[n - 2] < 2 || (digits[n - 2] == 2 && digits[n - 1] <= 6)) {
			count += countDecoding(digits, n - 2);
		}
		
		return count;
	}
	
	// ---------------------------
	//  Dynamic programming
	// ---------------------------
		
	static int countDecodingDP(int[] digits, int n) {
		// count(n) 表示长度为 n 的数字串的解码方式总数
		int[] count = new int[n + 1];
		count[0] = 1;
		count[1] = 1;
		
		for (int i = 2; i <= n; i++) {
			count[i] = 0;
			
			// 只单独考虑最后一个数字时，解码方式就是前 n-1 个数的解码方式
			if (digits[i - 1] > 0) {
				count[i] = count[i - 1];
			}
			
			// 同时考虑当前数字和前一个数字时，如果这个两位数在 1-26 之间，那么解码方式就是前 n-2 个数的解码方式
			if (digits[i - 2] < 2 || (digits[i - 2] == 2 && digits[i - 1] <= 6)) {
				count[i] += count[i - 2];
			}
		}
		
		return count[n];
	}
	
}
