package problems.geeks.string;

/**
 * Print list items containing all characters of a given word
 * 
 * http://www.geeksforgeeks.org/print-list-items-containing-all-characters-of-a-given-word/
 * 
 * There is a list of items. Given a specific word, e.g., �sun�, print out all the items in list which contain all the characters of �sun�
 * 
 * For example if the given word is �sun� and the items are �sunday�, �geeksforgeeks�, �utensils�, �just� and �sss�, 
 * then the program should print �sunday� and �utensils�.
 */

import java.util.*;

public class Q009_Print_List_Items_Containing_All_Characters_Of_A_Given_Word {

	public static void main(String[] args) {
		String word = "sun";
		String[] list = {"sunday", "geeksforgeeks", "utensils", "just", "sss"};
		printWords(word, list);
	}
	
	static void printWords(String word, String[] list) {
		if (word == null || list == null) {
			return;
		}
		
		for (int i = 0; i < list.length; i++) {
			// build the map
			Map<Character, Integer> map = new HashMap<Character, Integer>();
			for (int j = 0; j < word.length(); j++) {
				char ch = word.charAt(j);
				if (map.containsKey(ch)) {
					map.put(ch, map.get(ch) + 1);
				} else {
					map.put(ch, 1);
				}
			}
			
			String s = list[i];
			for (int j = 0; j < s.length(); j++) {
				char ch = s.charAt(j);
				if (map.containsKey(ch)) {
					map.put(ch, map.get(ch) - 1);
					
					if (map.get(ch) == 0) {
						map.remove(ch);
					}
					
					if (map.isEmpty()) {
						System.out.println(s);
						continue;
					}
				}
			}
		}
	}

}
