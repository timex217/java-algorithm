package problems.geeks.string;

/**
 * String Permutation
 * 
 * http://www.geeksforgeeks.org/write-a-c-program-to-print-all-permutations-of-a-given-string/
 * 
 * A permutation, also called an �arrangement number� or �order,� is a rearrangement of the elements of an ordered list S into a one-to-one correspondence with S itself. A string of length n has n! permutation.
 * Source: Mathword(http://mathworld.wolfram.com/Permutation.html)
 * 
 * Below are the permutations of string ABC.
 * ABC, ACB, BAC, BCA, CAB, CBA
 */

import java.util.*;

public class Q007_DFS_String_Permutation {

	public static void main(String[] args) {
		String s = "ABC";
		ArrayList<String> perm = getPermutation(s);
		System.out.println(perm.toString());
	}
	
	static ArrayList<String> getPermutation(String s) {
		ArrayList<String> perm = new ArrayList<String>();
		
		if (s == null) {
			return perm;
		}
		
		char[] chars = s.toCharArray();
		
		dfs(chars, 0, perm);
		
		return perm;
	}
	
	static void dfs(char[] chars, int index, ArrayList<String> res) {
		if (index == chars.length) {
			res.add(new String(chars));
			return;
		}
		
		for (int i = index; i < chars.length; i++) {
			swap(chars, index, i);
			dfs(chars, index + 1, res);
			swap(chars, index, i);
		}
	}
	
	static void swap(char[] chars, int i, int j) {
		char tmp = chars[i];
		chars[i] = chars[j];
		chars[j] = tmp;
	}
	
}
