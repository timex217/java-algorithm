package problems.geeks.string;

/**
 * Print all the duplicates
 * 
 * http://www.geeksforgeeks.org/print-all-the-duplicates-in-the-input-string/
 */

import java.util.*;

public class Q003_Print_All_Duplicates {
	
	public static void main(String[] args) {
		String s = "geeksforgeeks";
		printAllDups(s);
	}
	
	static void printAllDups(String s) {
		if (s == null)
			return;
		
		Map<Character, Integer> map = new HashMap<Character, Integer>();
		
		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			if (map.containsKey(ch)) {
				map.put(ch, map.get(ch) + 1);
			} else {
				map.put(ch, 1);
			}
		}
		
		for (Map.Entry<Character, Integer> entry : map.entrySet()) {
			System.out.format("%c: %d\n", entry.getKey(), entry.getValue());
		}
	}
	
}
