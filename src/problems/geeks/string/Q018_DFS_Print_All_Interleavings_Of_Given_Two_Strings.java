package problems.geeks.string;

/**
 * Print all interleavings of given two strings
 * 
 * http://www.geeksforgeeks.org/print-all-interleavings-of-given-two-strings/
 * 
 * Given two strings str1 and str2, write a function that prints all interleavings of the given two strings. You may assume that all characters in both strings are different
 * 
 * Example:
 * 
 * Input: str1 = "AB",  str2 = "CD"
 * Output:
 *     ABCD
 *     ACBD
 *     ACDB
 *     CABD
 *     CADB
 *     CDAB
 * 
 * Input: str1 = "AB",  str2 = "C"
 * Output:
 *     ABC
 *     ACB
 *     CAB
 *     
 * 这道题的解法类似与添加合法括号的解法
 */

import java.util.*;

public class Q018_DFS_Print_All_Interleavings_Of_Given_Two_Strings {

	public static void main(String[] args) {
		String s1 = "AB";
		String s2 = "CD";
		ArrayList<String> res = new ArrayList<String>();
		dfs(s1, 0, s2, 0, "", res);
		System.out.println(res.toString());
	}
	
	static void dfs(String s1, int i1, String s2, int i2, String sol, ArrayList<String> res) {
		if (i1 == s1.length() && i2 == s2.length()) {
			res.add(sol);
			return;
		}
		
		if (i1 < s1.length()) {
			dfs(s1, i1 + 1, s2, i2, sol + s1.substring(i1, i1 + 1), res);
		}
		
		if (i2 < s2.length()) {
			dfs(s1, i1, s2, i2 + 1, sol + s2.substring(i2, i2 + 1), res);
		}
	}
}
