package problems.geeks.string;

/**
 * Write your own atoi()
 * 
 * http://www.geeksforgeeks.org/write-your-own-atoi/
 * 
 * The atoi() function takes a string (which represents an integer) as an argument and returns its value.
 */

public class Q026A_String_To_Integer {
	
	public static void main(String[] args) {
		String str = "-134";
		int val = myAtoi(str);
		System.out.println(val);
	}
	
	static int myAtoi(String str) {
		int sign = 1;
		int i = 0;
		if (str.charAt(0) == '-') {
			sign = -1;
			i++;
		}
		
		int val = 0;
		
		for (; i < str.length(); i++) {
			char ch = str.charAt(i);
			val = val * 10 + (ch - '0');
		}
		
		return sign * val;
	}
	
}
