package problems.geeks.string;

/**
 * Given a sequence of words, print all anagrams together 
 * 
 * http://www.geeksforgeeks.org/given-a-sequence-of-words-print-all-anagrams-together/
 * 
 * Given an array of words, print all anagrams together. 
 * 
 * For example, if the given array is {�cat�, �dog�, �tac�, �god�, �act�}, then output may be �cat tac act dog god�.
 */

import java.util.*;

public class Q024_Print_All_Anagrams_Together {

	static class Word implements Comparable<Word> {
		String str;
		String sortedStr;
		
		Word(String s) {
			this.str = s;
			this.sortedStr = sortStr(s);
		}
		
		public int compareTo(Word w) {
			return this.sortedStr.compareTo(w.sortedStr);
		}
		
		// -----------------------
		//  Sort string
		// -----------------------
		String sortStr(String s) {
			if (s == null) {
				return null;
			}
			
			char[] chars = s.toCharArray();
			qsort(chars, 0, chars.length - 1);
			return new String(chars);
		}
		
		void qsort(char[] chars, int lo, int hi) {
			if (lo >= hi) {
				return;
			}
			
			int p = partition(chars, lo, hi);
			
			qsort(chars, lo, p - 1);
			qsort(chars, p + 1, hi);
		}
		
		int partition(char[] chars, int lo, int hi) {
			int mid = lo + (hi - lo) / 2;
			
			swap(chars, mid, hi);
			
			int i = lo - 1;
			for (int j = lo; j <= hi; j++) {
				if (chars[j] < chars[hi]) {
					swap(chars, ++i, j);
				}
			}
			
			swap(chars, i + 1, hi);
			
			return i + 1;
		}
		
		void swap(char[] chars, int i, int j) {
			char tmp = chars[i];
			chars[i] = chars[j];
			chars[j] = tmp;
		}
	}
	
	public static void main(String[] args) {
		String[] arr = {"cat", "dog", "tac", "god", "act"};
		printAnagramsTogether(arr);
	}
	
	static void printAnagramsTogether(String[] arr) {
		int n = arr.length;
		
		Word[] words = new Word[n];
		
		for (int i = 0; i < n; i++) {
			words[i] = new Word(arr[i]);
		}
		
		Arrays.sort(words);
		
		for (int i = 0; i < n; i++) {
			Word w = words[i];
			System.out.format("%s ", w.str);
		}
	}
	
}
