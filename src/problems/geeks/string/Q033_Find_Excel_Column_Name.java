package problems.geeks.string;

/**
 * Find Excel column name from a given column number
 * 
 * http://www.geeksforgeeks.org/find-excel-column-name-given-number/
 * 
 * MS Excel columns has a pattern like A, B, C, ... ,Z, AA, AB, AC ,..., AZ, BA, BB, ... ZZ, AAA, AAB... etc. 
 * In other words, column 1 is named as �A�, column 2 as �B�, column 27 as �AA�.
 * 
 * Given a column number, find its corresponding Excel column name. Following are more examples.
 * 
 * Input          Output
 *  26             Z
 *  51             AY
 *  52             AZ
 *  80             CB
 *  676            YZ
 *  702            ZZ
 *  705            AAC
 */

import java.util.*;

public class Q033_Find_Excel_Column_Name {

	public static void main(String[] args) {
		int n = 705;
		String col = getColName(n);
		System.out.println(col);
	}
	
	static String getColName(int n) {
		StringBuilder sb = new StringBuilder();
		
		while (n > 0) {
			int mod = (n - 1) % 26;
			sb.insert(0, getChar(mod));
			n = (n - 1) / 26;
		}
		
		return sb.toString();
	}
	
	static char getChar(int n) {
		return (char)(n + 65);
	}
	
}
