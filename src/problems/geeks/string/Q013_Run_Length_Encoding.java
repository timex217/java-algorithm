package problems.geeks.string;

/**
 * Run Length Encoding
 * 
 * http://www.geeksforgeeks.org/run-length-encoding/
 * 
 * Given an input string, write a function that returns the Run Length Encoded string for the input string.
 * 
 * For example, if the input string is “wwwwaaadexxxxxx”, then the function should return “w4a3d1e1x6″.
 * 
 * Algorithm:
 * a) Pick the first character from source string.
 * b) Append the picked character to the destination string.
 * c) Count the number of subsequent occurrences of the picked character and append the count to destination string.
 * d) Pick the next character and repeat steps b) c) and d) if end of string is NOT reached.
 */

import java.util.*;

public class Q013_Run_Length_Encoding {

	public static void main(String[] args) {
		String s = "wwwwaaadexxxxxx";
		String res = encode(s);
		System.out.println(res);
	}
	
	static String encode(String s) {
		if (s == null) {
			return null;
		}
		
		if (s.length() == 0) {
			return "";
		}
		
		StringBuilder sb = new StringBuilder();
		
		char ch = s.charAt(0);
		int count = 1;
		
		for (int i = 1; i < s.length(); i++) {
			char curr = s.charAt(i);
			
			if (curr == ch) {
				count++;
			} else {
				sb.append(ch).append(count);
				ch = curr;
				count = 1;
			}
		}
		
		sb.append(ch).append(count);
		
		return sb.toString();
	}
	
}
