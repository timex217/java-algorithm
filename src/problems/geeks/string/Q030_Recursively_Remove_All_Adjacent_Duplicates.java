package problems.geeks.string;

/**
 * Recursively remove all adjacent duplicates
 * 
 * http://www.geeksforgeeks.org/recursively-remove-adjacent-duplicates-given-string/
 * 
 * Given a string, recursively remove adjacent duplicate characters from string. The output string should not have any adjacent duplicates. See following examples.
 * 
 * Input:  azxxzy
 * Output: ay
 * First "azxxzy" is reduced to "azzy". The string "azzy" contains duplicates, 
 * so it is further reduced to "ay".
 * 
 * Input: geeksforgeeg
 * Output: gksfor
 * First "geeksforgeeg" is reduced to "gksforgg". The string "gksforgg" contains 
 * duplicates, so it is further reduced to "gksfor".
 * 
 * Input: caaabbbaacdddd
 * Output: Empty String
 * 
 * Input: acaaabbbacdddd
 * Output: acac
 */

import java.util.*;

public class Q030_Recursively_Remove_All_Adjacent_Duplicates {

	public static void main(String[] args) {
		String str = "acaaabbbacdddd";
		String res = removeUtil(str);
		System.out.println(res);
	}
	
	static String removeUtil(String str) {
		if (str == null) {
			return null;
		}
		
		Stack<Character> stack = new Stack<Character>();

		int i = 0, n = str.length();
		
		while (i < n) {
			char ch = str.charAt(i);
			
			if (stack.isEmpty() || stack.peek() != ch) {
				stack.push(ch);
				i++;
			} else {
				stack.pop();
				while (i < n && str.charAt(i) == ch) {
					i++;
				}
			}
		}
		
		StringBuilder sb = new StringBuilder();
		
		while (!stack.isEmpty()) {
			sb.insert(0, stack.pop());
		}
		
		return sb.toString();
	}
	
}
