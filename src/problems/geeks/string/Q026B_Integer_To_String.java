package problems.geeks.string;

/**
 * Convert integer to string
 */

import java.util.*;

public class Q026B_Integer_To_String {

	public static void main(String[] args) {
		int n = -134;
		String str = itoa(n);
		System.out.println(str);
	}
	
	static String itoa(int n) {
		boolean sign = n > 0;
		n = Math.abs(n);
		
		StringBuilder sb = new StringBuilder();
		
		while (n > 0) {
			int mod = n % 10;
			sb.insert(0, mod);
			n /= 10;
		}
		
		if (!sign) {
			sb.insert(0, '-');
		}
		
		return sb.toString();
	}
	
}
