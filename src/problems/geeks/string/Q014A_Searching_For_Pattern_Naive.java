package problems.geeks.string;

/**
 * Searching For Pattern
 * 
 * http://www.geeksforgeeks.org/searching-for-patterns-set-1-naive-pattern-searching/
 * 
 * Given a text txt[0..n-1] and a pattern pat[0..m-1], write a function search(char pat[], char txt[]) that prints all occurrences of pat[] in txt[]. 
 * You may assume that n > m.
 * 
 * Examples:
 * 1) Input:
 * 
 *   txt[] = "THIS IS A TEST TEXT"
 *   pat[] = "TEST"
 * Output:
 * 
 * Pattern found at index 10
 * 2) Input:
 * 
 *   txt[] = "AABAACAADAABAAABAA"
 *   pat[] = "AABA"
 * Output:
 * 
 *   Pattern found at index 0
 *   Pattern found at index 9
 *   Pattern found at index 13
 *   
 * Pattern searching is an important problem in computer science. When we do search for a string in notepad/word file or browser or database, 
 * pattern searching algorithms are used to show the search results.
 * 
 * Naive Pattern Searching:
 * Slide the pattern over text one by one and check for a match. If a match is found, then slides by 1 again to check for subsequent matches.
 */

import java.util.*;

public class Q014A_Searching_For_Pattern_Naive {
	
	public static void main(String[] args) {
		String txt = "AABAACAADAABAAABAA";
		String pat = "AABA";
		
		search(txt, pat);
	}
	
	static void search(String txt, String pat) {
		int n = txt.length();
		int m = pat.length();
		int i, j;
		
		for (i = 0; i <= n - m; i++) {
			for (j = 0; j < m; j++) {
				if (txt.charAt(i + j) != pat.charAt(j)) {
					break;
				}
			}
			if (j == m) {
				System.out.format("Pattern found at index %d\n", i);
			}
		}
	}
	
}
