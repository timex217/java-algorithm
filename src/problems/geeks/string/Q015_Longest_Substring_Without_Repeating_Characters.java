package problems.geeks.string;

/**
 * Length of the longest substring without repeating characters
 * 
 * http://www.geeksforgeeks.org/length-of-the-longest-substring-without-repeating-characters/
 * 
 * Given a string, find the length of the longest substring without repeating characters. 
 * For example, the longest substrings without repeating characters 
 * 
 * for “ABDEFGABEF” are “BDEFGA” and “DEFGAB”, with length 6. 
 * For “BBBB” the longest substring is “B”, with length 1. 
 * For “GEEKSFORGEEKS”, there are two longest substrings shown in the below diagrams, with length 7.
 */

import java.util.*;

public class Q015_Longest_Substring_Without_Repeating_Characters {

	public static void main(String[] args) {
		String s = "GEEKSFORGEEKS";
		int res = longestUniqueSubsttr(s);
		System.out.println(res);
	}
	
	static int longestUniqueSubsttr(String s) {
		if (s == null) {
			return 0;
		}
		
		Map<Character, Integer> map = new HashMap<Character, Integer>();
		
		int i = 0, j = 0;
		int n = s.length();
		int max = 0;
		
		while(j < n) {
			char ch = s.charAt(j);
			
			// 如果map不包含当前的character, 将它与它的index记录下来
			if (!map.containsKey(ch)) {
				map.put(ch, j);
				j++;
			}
			// 如果包含
			else {
				// 有可能会是最长的子序列
				max = Math.max(max, map.size());
				System.out.format("%d, %d\n", i, j);
				
				int k = map.get(ch);
				
				// 移动 i 到ch对应index的下一个
				// 同时将 i 对应的字符从map中删除
				while (i <= k) {
					map.remove(s.charAt(i));
					i++;
				}
			}
		}
		
		return max;
	}
	
}
