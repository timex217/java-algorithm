package problems.geeks.string;

/**
 * Given a string, find its first non-repeating character
 * 
 * http://www.geeksforgeeks.org/given-a-string-find-its-first-non-repeating-character/
 * 
 * Given a string, find the first non-repeating character in it. For example, if the input string is �GeeksforGeeks�, then output should be �f� 
 * and if input string is �GeeksQuiz�, then output should be �G�.
 * 
 * We can use string characters as index and build a count array. Following is the algorithm.
 * 
 * 1) Scan the string from left to right and construct the count array.
 * 2) Again, scan the string from left to right and check for count of each
 *  character, if you find an element who's count is 1, return it.
 * Example:
 * 
 * Input string: str = geeksforgeeks
 * 1: Construct character count array from the input string.
 *    ....
 *   count['e'] = 4
 *   count['f'] = 1
 *   count['g'] = 2
 *   count['k'] = 2
 *   ��
 * 2: Get the first character who's count is 1 ('f').
 */

import java.util.*;

public class Q008_Find_First_Non_Repeating_Character {
	
	static class Item {
		int count;
		int index;
		Item(int x) {
			this.count = 0;
			this.index = x;
		}
	}

	public static void main(String[] args) {
		String s = "geeksforgeeks";
		int res = firstNonRepeating(s);
		if (res != -1) {
			System.out.println(s.charAt(res));
		} else {
			System.out.println("NONE");
		}
	}
	
	static int firstNonRepeating(String s) {
		if (s == null || s.length() == 0) {
			return -1;
		}
		
		int n = s.length();
		
		// use a hashmap to track the unique character
		Map<Character, Item> map = new HashMap<Character, Item>();
		
		for (int i = 0; i < n; i++) {
			char ch = s.charAt(i);
			Item item = null;
			
			if (map.containsKey(ch)) {
				item = map.get(ch);
			} else {
				item = new Item(i);
				map.put(ch, item);
			}
			
			item.count++;
		}
		
		// go through the map and find the minimum index
		int min_index = n;
		
		for (Map.Entry<Character, Item> entry : map.entrySet()) {
			Item item = entry.getValue();
			if (item.count == 1) {
				min_index = Math.min(min_index, item.index);
			}
		}
		
		return (min_index == n) ? -1 : min_index;
	}

}
