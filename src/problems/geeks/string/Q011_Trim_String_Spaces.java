package problems.geeks.string;

/**
 * Trim spaces
 * 
 * Example:
 * Input: "    I like    Google    !  "
 * Output: "I like Google!"
 */

import java.util.*;

public class Q011_Trim_String_Spaces {

	public static void main(String[] args) {
		String s = "    I like    Google    !  ";
		s = trim(s);
		System.out.println(s);
	}
	
	static String trim(String str) {
		if (str == null) {
			return null;
		}
		
		char[] chars = str.toCharArray();
		int n = chars.length;
		
		// step 1. find the start and end place
		int s = 0, e = n - 1;
		while (s < n && chars[s] == ' ')  s++;
		while (e >= 0 && chars[e] == ' ') e--;
		
		// step 2. replace multiple spaces to single space
		int i = s, j = s;
		while (j <= e) {
			while (j <= e && chars[j] != ' ') chars[i++] = chars[j++];
			if (j < e)                        chars[i++] = ' ';
			while (j <= e && chars[j] == ' ') j++;
		}
		
		str = new String(chars);
		return str.substring(s, i);
	}
	
}
