package problems.geeks.string;

/**
 * Rearrange a string so that all same characters become d distance away
 * 
 * http://www.geeksforgeeks.org/rearrange-a-string-so-that-all-same-characters-become-at-least-d-distance-away/
 * 
 * Given a string and a positive integer d. Some characters may be repeated in the given string. 
 * Rearrange characters of the given string such that the same characters become d distance away from each other. 
 * Note that there can be many possible rearrangements, the output should be one of the possible rearrangements. 
 * If no such arrangement is possible, that should also be reported.
 * Expected time complexity is O(n) where n is length of input string.
 * 
 * Examples:
 * Input:  "abb", d = 2
 * Output: "bab"
 * 
 * Input:  "aacbbc", d = 3
 * Output: "abcabc"
 * 
 * Input: "geeksforgeeks", d = 3
 * Output: egkegkesfesor
 * 
 * Input:  "aaa",  d = 2
 * Output: Cannot be rearranged
 * 
 * Solution: The idea is to count frequencies of all characters and consider the most frequent character first and place all occurrences of it as close as possible. 
 * After the most frequent character is placed, repeat the same process for remaining characters.
 * 
 * 1) Let the given string be str and size of string be n
 * 
 * 2) Traverse str, store all characters and their frequencies in a Max Heap MH. The value of frequency decides the order in MH, 
 * i.e., the most frequent character is at the root of MH.
 * 
 * 3) Make all characters of str as ‘\0′.
 * 
 * 4) Do following while MH is not empty.
 * …a) Extract the Most frequent character. Let the extracted character be x and its frequency be f.
 * …b) Find the first available position in str, i.e., find the first ‘\0′ in str.
 * …c) Let the first position be p. Fill x at p, p+d,.. p+(f-1)d
 */

import java.util.*;

public class Q031_Rearrange_A_String_So_That_All_Same_Characters_Become_d_Distance_Away {
	
	static class Letter {
		char ch;
		int frequency;
		Letter(char c, int f) {
			this.ch = c;
			this.frequency = f;
		}
	}

	public static void main(String[] args) {
		String str = "geeksforgeeks";
		int d = 3;
		
		String res = rearrange(str, d);
		System.out.println(res);
	}
	
	static String rearrange(String str, int d) {
		if (str == null || d < 1) {
			return "Not Possible";
		}
		
		int n = str.length();
		
		// step 1. go through the string and count letter frequency
		Map<Character, Integer> map = new HashMap<Character, Integer>();
		for (int i = 0; i < n; i++) {
			char ch = str.charAt(i);
			
			if (map.containsKey(ch)) {
				map.put(ch, map.get(ch) + 1);
			} else {
				map.put(ch, 1);
			}
		}
		
		// step 2. use a max heap to sort the letters by frequency
		PriorityQueue<Letter> heap = new PriorityQueue(n, new Comparator<Letter>() {
			@Override
			public int compare(Letter a, Letter b) {
				return b.frequency - a.frequency;
			}
		});
		
		for (Map.Entry<Character, Integer> entry : map.entrySet()) {
			Letter l = new Letter(entry.getKey(), entry.getValue());
			heap.offer(l);
		}
		
		// step 3. organize the letters and put them in place by d distance
		char[] chars = new char[n];
		
		while (!heap.isEmpty()) {
			Letter l = heap.poll();
			
			int i = 0;
			while (i < n && chars[i] != '\0') i++;
			
			while (i < n && l.frequency > 0) {
				chars[i] = l.ch;
				i += d;
				l.frequency--;
			}
			
			if (l.frequency > 0) {
				return "Not Possible";
			}
		}
		
		return new String(chars);
	}
	
}
