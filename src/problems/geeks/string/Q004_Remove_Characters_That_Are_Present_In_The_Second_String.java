package problems.geeks.string;

/**
 * Remove characters from the first string which are present in the second string
 * 
 * http://www.geeksforgeeks.org/remove-characters-from-the-first-string-which-are-present-in-the-second-string/
 * 
 * Write an efficient C function that takes two strings as arguments and removes the characters from first string which are present in second string (mask string).
 * 
 * Time Complexity: O(m+n) Where m is the length of mask string and n is the length of the input string.
 */

import java.util.*;

public class Q004_Remove_Characters_That_Are_Present_In_The_Second_String {

	public static void main(String[] args) {
		String s1 = "geeksforgeeks";
		String s2 = "gfs";
		
		String res = removeChars(s1, s2);
		System.out.println(res);
	}
	
	static String removeChars(String s1, String s2) {
		if (s2 == null || s2.length() == 0) {
			return s1;
		}
		
		StringBuilder sb = new StringBuilder();
		
		// step 1. hash characters of s2
		Set<Character> set = new HashSet<Character>();
		
		for (int i = 0; i < s2.length(); i++) {
			char ch = s2.charAt(i);
			if (!set.contains(ch)) {
				set.add(ch);
			}
		}
		
		// step 2. go through s1 and ignore the characters in s2
		for (int i = 0; i < s1.length(); i++) {
			char ch = s1.charAt(i);
			if (!set.contains(ch)) {
				sb.append(ch);
			}
		}
		
		return sb.toString();
	}

}
