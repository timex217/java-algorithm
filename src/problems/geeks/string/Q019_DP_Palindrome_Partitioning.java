package problems.geeks.string;

/**
 * Palindrome Partitioning
 * 
 * http://www.geeksforgeeks.org/dynamic-programming-set-17-palindrome-partitioning/
 * 
 * Given a string, a partitioning of the string is a palindrome partitioning if every substring of the partition is a palindrome. 
 * For example, “aba|b|bbabb|a|b|aba” is a palindrome partitioning of “ababbbabbababa”. 
 * Determine the fewest cuts needed for palindrome partitioning of a given string. 
 * 
 * For example, minimum 3 cuts are needed for “ababbbabbababa”. The three cuts are “a|babbbab|b|ababa”. 
 * If a string is palindrome, then minimum 0 cuts are needed. 
 * If a string of length n containing all different characters, then minimum n-1 cuts are needed.
 * 
 * Solution
 * This problem is a variation of Matrix Chain Multiplication problem. If the string is palindrome, then we simply return 0. Else, 
 * like the Matrix Chain Multiplication problem, we try making cuts at all possible places, recursively calculate the cost for each cut and return the minimum value.
 * 
 * Let the given string be str and minPalPartion() be the function that returns the fewest cuts needed for palindrome partitioning. 
 * following is the optimal substructure property.
 * 
 * // i is the starting index and j is the ending index. i must be passed as 0 and j as n-1
 * minPalPartion(str, i, j) = 0 if i == j. // When string is of length 1.
 * minPalPartion(str, i, j) = 0 if str[i..j] is palindrome.
 * 
 * // If none of the above conditions is true, then minPalPartion(str, i, j) can be 
 * // calculated recursively using the following formula.
 * minPalPartion(str, i, j) = Min { minPalPartion(str, i, k) + 1 +
 *                                  minPalPartion(str, k+1, j) } 
 *                            where k varies from i to j-1
 *                            
 * c[i] 表示对str[0...i]进行palindrome partitioning cut所需的最少切割次数
 * c[i] = 1, i = 0;
 * c[i] = min{c(j)} + 1, 0 <= j <= i - 1 && str[j + 1, i] 是palindrome
 * 最后返回c[n - 1]
 */

public class Q019_DP_Palindrome_Partitioning {
	
	public static void main(String[] args) {
		String s = "ababbbabbababa";
		int res = minPalPartion(s);
		System.out.println(res);
	}
	
	static int minPalPartion(String s) {
		if (s == null || s.length() == 0) {
			return 0;
		}
		
		int n = s.length();
		int[] c = new int[n];
		
		for (int i = 0; i < n; i++) {
			if (isPalindrome(s, 0, i)) {
				continue;
			}
			
			c[i] = c[i - 1] + 1;
			for (int j = 0; j < i; j++) {
				if (isPalindrome(s, j + 1, i)) {
					c[i] = Math.min(c[i], c[j] + 1);
				}
			}
		}
		
		return c[n - 1];
	}
	
	static boolean isPalindrome(String s, int i, int j) {
		if (s == null || i > j) {
			return false;
		}
		
		while (i < j) {
			if (s.charAt(i++) != s.charAt(j--)) {
				return false;
			}
		}
		
		return true;
	}
	
}
