package problems.geeks.string;

/**
 * Check whether two strings are anagram of each other
 * 
 * http://www.geeksforgeeks.org/check-whether-two-strings-are-anagram-of-each-other/
 * 
 * Write a function to check whether two given strings are anagram of each other or not. 
 * 
 * An anagram of a string is another string that contains same characters, only the order of characters can be different. 
 * For example, “abcd” and “dabc” are anagram of each other.
 * 
 * 方法是数字符个数
 */

import java.util.*;

public class Q017_Check_Anagram {
	
	public static void main(String[] args) {
		String s1 = "test";
		String s2 = "ttew";
		
		boolean res = isAnagram(s1, s2);
		System.out.println(res);
	}
	
	static boolean isAnagram(String s1, String s2) {
		if (s1 == null || s2 == null) {
			return false;
		}
		
		int n1 = s1.length();
		int n2 = s2.length();
		
		if (n1 != n2) {
			return false;
		}
		
		// hashmap s1
		Map<Character, Integer> map = new HashMap<Character, Integer>();
		
		for (int i = 0; i < n1; i++) {
			char ch = s1.charAt(i);
			if (map.containsKey(ch)) {
				map.put(ch, map.get(ch) + 1);
			} else {
				map.put(ch, 1);
			}
		}
		
		// go through s2 and compare the character size
		for (int i = 0; i < n2; i++) {
			char ch = s2.charAt(i);
			if (map.containsKey(ch)) {
				map.put(ch, map.get(ch) - 1);
				
				if (map.get(ch) == 0) {
					map.remove(ch);
				}
			} else {
				return false;
			}
		}
		
		if (map.size() > 0) {
			return false;
		} else {
			return true;
		}
	}
	
}
