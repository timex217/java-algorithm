package problems.geeks.string;

/**
 * Word Count
 * 
 * Count how many words are there in a given string
 * 
 * Example
 * Input: "I love Google!"
 * Output: 3
 */

public class Q012_Word_Count {

	public static void main(String[] args) {
		String s = "  I   love  Google!  ";
		int res = countWord(s);
		System.out.println(res);
	}
	
	static int countWord(String s) {
		if (s == null) {
			return 0;
		}
		
		int count = 0;
		boolean in_word = false;
		
		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			
			if (isWhiteSpace(ch)) {
				in_word = false;
			} else if (!in_word) {
				in_word = true;
				count++;
			}
		}
		
		return count;
	}
	
	static boolean isWhiteSpace(char ch) {
		return ch == ' ' || ch == '\n' || ch == '\t';
	}
	
}
