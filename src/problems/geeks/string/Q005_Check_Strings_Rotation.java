package problems.geeks.string;

/**
 * A Program to check if strings are rotations of each other or not
 * 
 * http://www.geeksforgeeks.org/a-program-to-check-if-strings-are-rotations-of-each-other-or-not/
 * 
 * Given a string s1 and a string s2, write a snippet to say whether s2 is a rotation of s1 using only one call to strstr routine?
 * (eg given s1 = ABCD and s2 = CDAB, return true, given s1 = ABCD, and s2 = ACBD , return false)
 */

public class Q005_Check_Strings_Rotation {

	public static void main(String[] args) {
		String s1 = "ABCD";
		String s2 = "CDAB";
		
		boolean res = areRotations(s1, s2);
		System.out.println(res);
	}
	
	static boolean areRotations(String s1, String s2) {
		return strstr(s1 + s1, s2);
	}
	
	// the implementation should be KMP matcher
	static boolean strstr(String s1, String s2) {
		return s1.indexOf(s2) > -1;
	}

}
