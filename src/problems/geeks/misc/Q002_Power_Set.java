package problems.geeks.misc;

/**
 * Power Set
 * 
 * http://www.geeksforgeeks.org/power-set/
 * 
 * Power Set Power set P(S) of a set S is the set of all subsets of S. For example S = {a, b, c} then P(s) = {{}, {a}, {b}, {c}, {a,b}, {a, c}, {b, c}, {a, b, c}}.
 * 
 * If S has n elements in it then P(s) will have 2^n elements
 */

import java.util.*;

public class Q002_Power_Set {

	public static void main(String[] args) {
		char[] set = {'a','b','c'};
		printPowerSet(set, 3);
	}
	
	static void printPowerSet(char[] set, int n) {
		ArrayList<ArrayList<Character>> res = new ArrayList<ArrayList<Character>>();
		ArrayList<Character> sol = new ArrayList<Character>();
		
		helper(set, 0, new ArrayList<Character>(), res);
		printSet(res);
	}
	
	static void helper(char[] set, int index, ArrayList<Character> sol, ArrayList<ArrayList<Character>> res) {
		if (index == set.length) {
			res.add(new ArrayList<Character>(sol));
			return;
		}
		
		// do not add current character
		helper(set, index + 1, sol, res);
		
		// add current character
		sol.add(set[index]);
		helper(set, index + 1, sol, res);
		sol.remove(sol.size() - 1);
	}
	
	static void printSet(ArrayList<ArrayList<Character>> res) {
		for (int i = 0; i < res.size(); i++) {
			ArrayList<Character> set = res.get(i);
			System.out.println(set.toString());
		}
	}
	
}
