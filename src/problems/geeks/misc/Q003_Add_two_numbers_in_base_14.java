package problems.geeks.misc;

/**
 * Add two numbers in base 14
 * 
 * 
 * 
 * Method 2
 * Just add the numbers in base 14 in same way we add in base 10. Add numerals of both numbers one by one from right to left. If there is a carry while adding two numerals, consider the carry for adding next numerals.
 * 
 * Let us consider the presentation of base 14 numbers same as hexadecimal numbers
 * 
 *    A --> 10
 *    B --> 11
 *    C --> 12
 *    D --> 13
 *    
 * Example:
 * 
 *    num1 = 1  2  A
 *    num2 = C  D  3   
 * 
 *    1. Add A and 3, we get 13(D). Since 13 is smaller than 
 * 14, carry becomes 0 and resultant numeral becomes D         
 * 
 *   2. Add 2, D and carry(0). we get 15. Since 15 is greater 
 * than 13, carry becomes 1 and resultant numeral is 15 - 14 = 1
 * 
 *   3. Add 1, C and carry(1). we get 14. Since 14 is greater 
 * than 13, carry becomes 1 and resultant numeral is 14 - 14 = 0
 * 
 * Finally, there is a carry, so 1 is added as leftmost numeral and the result becomes 
 * 101D 
 */

import java.util.*;
import java.lang.Exception;

public class Q003_Add_two_numbers_in_base_14 {
	
	public static void main(String[] args) {
		char[] num1 = {'A', '2', '1'};
		char[] num2 = {'3', 'D', 'C'};
		
		ArrayList<Character> res = add(num1, 3, num2, 3);
		System.out.println(res.toString());
	}
	
	static ArrayList<Character> add(char[] num1, int m, char[] num2, int n) {
		ArrayList<Character> res = new ArrayList<Character>();
		
		int i = 0, j= 0, carry = 0;
		
		while (i < m && j < n) {
			int n1 = getNum(num1[i++]);
			int n2 = getNum(num2[j++]);
			
			int s = n1 + n2 + carry;
			
			if (s >= 14) {
				carry = 1;
				s = s - 14;
			} else {
				carry = 0;
			}
			
			res.add(getChar(s));
		}
		
		while (i < m) {
			int n1 = getNum(num1[i++]);
			int s = n1 + carry;
			if (s >= 14) {
				carry = 1;
				s = s - 14;
			} else {
				carry = 0;
			}
			res.add(getChar(s));
		}
		
		while (j < n) {
			int n2 = getNum(num2[j++]);
			int s = n2 + carry;
			if (s >= 14) {
				carry = 1;
				s = s - 14;
			} else {
				carry = 0;
			}
			res.add(getChar(s));
		}
		
		if (carry == 1) {
			res.add(getChar(1));
		}
		
		return res;
	}
	
	static int getNum(char ch) {
		if (ch >= '0' && ch <= '9') {
			return (int)(ch - '0');
		} else if (ch >= 'A' && ch <= 'D') {
			return (int)(ch - 'A') + 10;
		}
		return -1;
	}
	
	static char getChar(int n) {
		if (n >= 0 && n <= 9) {
			return (char)(n + 48);
		} else if (n >= 10 && n <= 13) {
			return (char)(n - 10 + 65);
		}
		return ' ';
	}
	
}
