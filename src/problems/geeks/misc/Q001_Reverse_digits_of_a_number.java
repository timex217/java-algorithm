package problems.geeks.misc;

/**
 * Reverse digits of a number
 * 
 * http://www.geeksforgeeks.org/write-a-c-program-to-reverse-digits-of-a-number/
 * 
 * Time Complexity: O(Log(n)) where n is the input number.
 */

import java.util.*;

public class Q001_Reverse_digits_of_a_number {

	public static void main(String[] args) {
		int n = 2117;
		int res = reverse(n);
		System.out.println(res);
	}
	
	static int reverse(int n) {
		int res = 0;
		
		while (n > 0) {
			res = 10 * res + n % 10;
			n = n / 10;
		}
		
		return res;
	}
	
}
