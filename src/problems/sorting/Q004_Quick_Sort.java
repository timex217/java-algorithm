package problems.sorting;

import java.util.*;

public class Q004_Quick_Sort {
	
	public static void main(String[] args) {
		int[] A = {8, 6, 5, 1, 0, -2, 9, 3, 4, 7};
		sort(A, 0, A.length - 1);
		print(A);
	}
	
	public static void sort(int[] A, int lo, int hi) {
		if (A == null || A.length == 0) {
			return;
		}
		
		// base case
		if (lo >= hi) {
			return;
		}
		
		int p = partition(A, lo, hi);
		
		sort(A, lo, p - 1);
		sort(A, p + 1, hi);
	}
	
	public static int partition(int[] A, int lo, int hi) {
		// randomly picked a number index
		int idx = randRange(lo, hi);
		
		// put the randomly picked number to the end
		swap(A, idx, hi);
		
		int left = lo;
		int right = hi;
		
		while (left < right) {
			if (A[left] <= A[hi]) {
				left++;
			}
			else {
				swap(A, left, --right);
			}
		}
		
		swap(A, left, hi);
		
		return left;
	}
	
	public static int randRange(int lo, int hi) {
		Random rand = new Random();
		return rand.nextInt(hi - lo) + lo;
	}
	
	public static void swap(int[] A, int i, int j) {
		int tmp = A[i];
		A[i] = A[j];
		A[j] = tmp;
	}
	
	public static void print(int[] A) {
		for (int i = 0; i < A.length; i++) {
			System.out.format("%d, ", A[i]);
		}
	}

}
