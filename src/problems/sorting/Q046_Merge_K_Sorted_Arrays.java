package problems.sorting;

import java.util.*;

public class Q046_Merge_K_Sorted_Arrays {
	
    static class Node {
        int val;
        int index;
        int list_index;
        Node(int x, int idx, int list_idx) {
            this.val = x;
            this.index = idx;
            this.list_index = list_idx;
        }
    }
    
	public static void main(String[] args) {
		
	}

	static List<Integer> mergeKArrays(List<List<Integer>> lists) {
	    if (lists == null || lists.size() == 0) return null;
	    
	    int k = lists.size();
	    
	    // create a min heap
	    PriorityQueue<Node> heap = new PriorityQueue<Node>(k, new Comparator<Node>() {
	        @Override
	        public int compare(Node p1, Node p2) {
	            return p1.val - p2.val;
	        }
	    });
	    
	    // insert the k lists head to the heap
	    for (int i = 0; i < k; i++) {
	        List<Integer> list = lists.get(i);
	        Node p = new Node(list.get(0), 0, i);
	        heap.offer(p);
	    }
	    
	    ArrayList<Integer> res = new ArrayList<Integer>();
	    
	    while (heap.size() > 0) {
	        Node p = heap.poll();
	        List<Integer> list = lists.get(p.list_index);
	        res.add(list.get(p.index));
	        
	        if (p.index < p.list_index - 1) {
	            Node t = new Node(list.get(p.index + 1), p.index + 1, p.list_index);
	            heap.offer(t);
	        }
	    }
	    
	    return res;
	}
}
