package problems.sorting;

/**
 * 选择排序: 从头到尾循环一次，每次将最小的值放在当前位置
 */

public class Q001_Selection_Sort {
	
	public static void main(String[] args) {
		int[] A = {8, 6, 5, 1, 0, -2, 9, 3, 4, 7};
		sort(A);
		print(A);
	}
	
	public static void sort(int[] A) {
		if (A == null || A.length == 0) {
			return;
		}
		
		int n = A.length;
		
		for (int i = 0; i < n - 1; i++) {
			int min_index = i;
			
			for (int j = i + 1; j < n; j++) {
				if (A[j] < A[min_index]) {
					min_index = j;
				}
			}
			
			if (min_index != i) {
				swap(A, i, min_index);
			}
		}
	}
	
	public static void swap(int[] A, int i, int j) {
		int tmp = A[i];
		A[i] = A[j];
		A[j] = tmp;
	}
	
	public static void print(int[] A) {
		for (int i = 0; i < A.length; i++) {
			System.out.format("%d, ", A[i]);
		}
	}

}
