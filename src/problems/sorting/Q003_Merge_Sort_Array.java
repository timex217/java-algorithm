package problems.sorting;

import java.util.*;

public class Q003_Merge_Sort_Array {
	
	public static void main(String[] args) {
		int[] A = {8, 6, 5, 1, 0, -2, 9, 3, 4, 7};
		A = sort(A, 0, A.length - 1);
		print(A);
	}
	
	public static int[] sort(int[] A, int lo, int hi) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		
		// sanity check
		if (A == null) {
			return null;
		}
		
		int n = A.length;
		
		if (n == 0) {
			return to_array(result);
		}
		
		// base case
		if (lo >= hi) {
			result.add(A[lo]);
			return to_array(result);
		}
		
		int mid = lo + (hi - lo) / 2;
		
		int[] left = sort(A, lo, mid);
		int[] right = sort(A, mid + 1, hi);
		
		return merge(left, right);
	}
	
	public static int[] merge(int[] left, int[] right) {
		int n_left = left.length;
		int n_right = right.length;
		
		int[] result = new int[n_left + n_right];
		
		int i = 0, j = 0, k = 0;
		
		while (i < n_left && j < n_right) {
			if (left[i] < right[j]) {
				result[k++] = left[i++];
			}
			else {
				result[k++] = right[j++];
			}
		}
		
		while (i < n_left) {
			result[k++] = left[i++];
		}
		
		while (j < n_right) {
			result[k++] = right[j++];
		}
		
		return result;
	}
	
	public static int[] to_array(ArrayList<Integer> list) {
		if (list == null) {
			return null;
		}
		
		int n = list.size();
		
		int[] result = new int[n];
		
		for (int i = 0; i < n; i++) {
			result[i] = list.get(i);
		}
		
		return result;
	}
	
	public static void print(int[] A) {
		for (int i = 0; i < A.length; i++) {
			System.out.format("%d, ", A[i]);
		}
	}

}
