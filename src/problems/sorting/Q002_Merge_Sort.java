package problems.sorting;

public class Q002_Merge_Sort {
	
	public static void main(String[] args) {
		int[] A = {8, 6, 5, 1, 0, -2, 9, 3, 4, 7};
		sort(A, 0, A.length - 1);
		print(A);
	}
	
	public static void sort(int[] A, int lo, int hi) {
		// sanity check
		if (A == null || A.length == 0) {
			return;
		}
		
		// base case
		if (lo >= hi) {
			return;
		}
		
		int mid = lo + (hi - lo) / 2;
		
		sort(A, lo, mid);
		sort(A, mid + 1, hi);
		
		merge(A, lo, mid, hi);
	}
	
	public static void merge(int[] A, int lo, int mid, int hi) {
		if (A == null || A.length == 0) {
			return;
		}
		
		// make a copy of A
		int i, j, k;
		int n = A.length;
		int[] aux = new int[n];
		
		for (i = 0; i < n; i++) {
			aux[i] = A[i];
		}
		
		k = lo;
		i = lo;
		j = mid + 1;
		
		while (k <= hi) {
			if (i > mid) {
				A[k++] = aux[j++];
			}
			else if (j > hi) {
				A[k++] = aux[i++];
			}
			else if (aux[j] < aux[i]) {
				A[k++] = aux[j++];
			}
			else {
				A[k++] = aux[i++];
			}
		}
	}
	
	public static void print(int[] A) {
		for (int i = 0; i < A.length; i++) {
			System.out.format("%d, ", A[i]);
		}
	}

}
