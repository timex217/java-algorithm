package problems.array;

/**
 * Given two arrays A and B, rearrange A so that all elements indecies match B.
 * 
 * For example, A = {0, 1, 2, 3}, B = {0, 3, 1, 2}, after the rearrangement, A should be {0, 2, 3, 1}
 * Requirement: constant space, no recursion
 */

import java.util.*;

public class RearrangeArray {
    
    public static void main(String[] args) {
        int[] a = {0, 1, 2, 3};
        int[] b = {0, 3, 1, 2};
        
        rearrange(a, b);
        System.out.println(Arrays.toString(a));
    }
    
    static void rearrange(int[] a, int[] b) {
        if (a == null || b == null) {
            return;
        }
        
        if (a.length != b.length) {
            return;
        }
        
        for (int i = 0; i < a.length; i++) {
            while (i != b[i]) {
                swap(a, i, b[i]);
                swap(b, i, b[i]);
            }
        }
    }
    
    static void swap(int[] a, int i, int j) {
        int tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }

}
