package problems.array;
/**
 * Q44 Given an unsorted array that contains only one unique number, and all the rest numbers
 * are duplicated once. How to find the unique number?
 */

public class Q44 {
	
	public static void main(String[] args) {
		int[] A = {1, 1, 2, 3, 3, 4, 4, 5, 5};
		int num = find(A);
		System.out.format("The number is: %d", num);
	}
	
	public static int find(int[] A) {
		int sum = A[0];
		
		for (int i = 1; i < A.length; i++) {
			sum ^= A[i];
		}
		
		return sum;
	}

}
