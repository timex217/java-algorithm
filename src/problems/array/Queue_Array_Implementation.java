package problems.array;

import java.util.NoSuchElementException;

public class Queue_Array_Implementation {
	
	private class Queue {
		int[] arr;
		int total;
		int first;
		int last;
		
		public Queue() {
			total = 0;
			first = 0;
			last = 0;
			arr = new int[1];
		}
		
		public void enqueue(int x) {
			if (total == arr.length) {
				resize(arr.length * 2);
			}
			
			arr[last++] = x;
			
			if (last == arr.length) {
				last = 0;
			}
			
			total++;
		}
		
		public int dequeue() {
			if (total == 0) {
				throw new NoSuchElementException("Queue underflow");
			}
			
			int x = arr[first++];
			total--;
			
			if (first == arr.length) {
				first = 0;
			}
			
			if (total > 0 && total == arr.length / 4) {
				resize(arr.length / 2);
			}
			
			return x;
		}
		
		private void resize(int cap) {
			int[] tmp = new int[cap];
			for (int i = 0; i < total; i++) {
				tmp[i] = arr[(first + i) % arr.length];
			}
			arr = tmp;
			first = 0;
			last = total;
		}
	}
	
	public static void main(String[] args) {
		
	}

}
