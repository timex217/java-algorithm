package problems.array;

import java.util.*;

public class Sum_234 {
	
	public static int[] A = {};
	
	public static void main(String[] args) {
		
	}
	
	public static void sum2(int target) {
		if (A == null || A.length < 2) return;
		
		int i = 0, j = A.length - 1;
		
		while (i < j) {
			int sum = A[i] + A[j];
			
			if (sum == target) {
				System.out.format("Find %d, %d in sum 2.\n", A[i], A[j]);
				return;
			} else if (sum < target) {
				i++;
			} else {
				j--;
			}
		}
		
		System.out.println("Can not find in sum 2.");
	}
	
	public static void sum3(int target) {
		if (A == null || A.length < 3) return;
		
		int n = A.length;
		int i = 0, j = 1, k = n - 1;
		
		for (i = 0; i < n - 2; i++) {
			j = i + 1;
			k = n - 1;
			while (j < k) {
				int sum = A[i] + A[j] + A[k];
				
				if (sum == target) {
					System.out.format("Find %d, %d, %d in sum 3.\n", A[i], A[j], A[k]);
					return;
				} else if (sum < target) {
					j++;
				} else {
					k--;
				}
			}
		}
		
		System.out.println("Can not find in sum 3.");
	}
	
	public static void sum4(int target) {
		if (A == null || A.length < 4) return;
		
		int n = A.length;
		int i = 0, j = 1, k = 2, m = n - 1;
		
		for (i = 0; i < n - 3; i++) {
			for (j = i + 1; j < n - 2; j++) {
				k = j + 1; 
				m = n - 1;
				while (k < m) {
					int sum = A[i] + A[j] + A[k] + A[m];
					
					if (sum == target) {
						System.out.format("Find %d, %d, %d, %d in sum 4.\n", A[i], A[j], A[k], A[m]);
						return;
					} else if (sum < target) {
						k++;
					} else {
						m--;
					}
				}
			}
		}
		
		System.out.println("Can not find in sum 4.");
	}
	
	/**
	 * Pair stores index
	 */
	private static class Pair implements Comparable<Pair> {
		int i;
		int j;
		int sum;
		
		Pair(int i, int j) {
			this.i = i; 
			this.j = j;
			this.sum = A[i] + A[j];
		}
		
		public int compareTo(Pair p) {
			return this.sum - p.sum;
		}
	}
	
	public static void sum4Better(int target) {
		if (A == null || A.length < 4) return;
		
		int i, j, n = A.length;
		
		ArrayList<Pair> list = new ArrayList<Pair>();
		
		for (i = 0; i < n; i++) {
			for (j = 0; j < n; j++) {
				if (i != j) {
					list.add(new Pair(i, j));
				}
			}
		}
		
		Collections.sort(list);
		
		i = 0; j = list.size() - 1;
		
		while (i < j) {
			Pair p1 = list.get(i);
			Pair p2 = list.get(j);
			
			int sum = p1.sum + p2.sum;
			
			if (sum == target) {
				if (p1.i != p2.i && p1.j != p2.j && p1.i != p2.j && p1.j != p2.i) {
					System.out.format("Find %d, %d, %d, %d in sum 4.\n", A[p1.i], A[p1.j], A[p2.i], A[p2.j]);
					return;
				}
				i++;
			}
			else if (sum < target) {
				i++;
			}
			else {
				j--;
			}
		}
		
		System.out.println("Can not find in sum 4.");
	}

}
