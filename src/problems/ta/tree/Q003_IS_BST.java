package problems.ta.tree;

/**
 * 判断一颗二叉树是不是二叉搜索树
 */

public class Q003_IS_BST {
	
	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		
		TreeNode(int x) {this.val = x;}
	}
	
	public static void main(String[] args) {
		
	}
	
	static boolean isBST(TreeNode root) {
		return isBST(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
	}
	
	static boolean isBST(TreeNode root, int min, int max) {
		if (root == null) {
			return true;
		}
		
		if (root.val <= min || root.val >= max) {
			return false;
		}
		
		return isBST(root.left, min, root.val) && isBST(root.right, root.val, max);
	}

}
