package problems.ta.tree;

public class Q004B_BST_Insert_New_Node {
	
	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		
		TreeNode(int x) {this.val = x;}
	}
	
	public static void main(String[] args) {
		
	}
	
	/**
	 * 二叉搜索树的插入很简单，将要插入的新节点作为叶子节点插入
	 * 通过二叉搜索树的性质找到要插入的父节点，然后将其插入
	 */
	static TreeNode insert(TreeNode root, int target) {
		if (root == null) {
			return new TreeNode(target);
		}
		
		if (root.val == target) {
			return root;
		}
		
		TreeNode node = null;
		
		if (target < root.val) {
			node = insert(root.left, target);
			
			// 插入操作
			if (root.left == null) {
				root.left = node;
			}
		}
		else {
			node = insert(root.right, target);
			
			// 插入操作
			if (root.right == null) {
				root.right = node;
			}
		}
		
		return node;
	}
}
