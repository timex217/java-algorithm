package problems.ta.tree;

import java.util.*;

public class Q002B_ZigZag_Traversal {
	
	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		
		TreeNode(int x) {this.val = x;}
	}
	
	public static void main(String[] args) {
		
	}
	
	static void zigzagTraversal(TreeNode root) {
		if (root == null) {
			return;
		}
		
		Stack<TreeNode> curr = new Stack<TreeNode>();
		Stack<TreeNode> next = new Stack<TreeNode>();
		
		curr.push(root);
		
		boolean leftToRight = true;
		
		while (!curr.isEmpty()) {
			while (!curr.isEmpty()) {
				TreeNode node = curr.pop();
				print(node);
				
				if (leftToRight) {
					if (node.left != null) next.push(node.left);
					if (node.right != null) next.push(node.right);
				}
				else{
					if (node.right != null) next.push(node.right);
					if (node.left != null) next.push(node.left);
				}
			}
			
			// new line
			System.out.println();
			
			leftToRight = !leftToRight;
			
			// swap curr and next
			swap(curr, next);
		}
	}
	
	static void swap(Stack<TreeNode> curr, Stack<TreeNode> next) {
		Stack<TreeNode> tmp = curr;
		curr = next;
		next = tmp;
	}
	
	static void print(TreeNode node) {
		System.out.print(node.val);
	}

}
