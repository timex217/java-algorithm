package problems.ta.tree;

public class Q011_BST_Closest_Target {
	
	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		
		TreeNode(int x) {this.val = x;}
	}
	
	public static void main(String[] args) {
		
	}
	
	static TreeNode getClosestNode(TreeNode root, int target) {
		TreeNode node = root;
		TreeNode res = null;
		
		int min = Integer.MAX_VALUE;
		
		while (node != null) {
			if (node.val == target) {
				return node;
			}
			
			// update result
			int diff = Math.abs(node.val - target);
			
			if (diff < min) {
				res = node;
				min = diff;
			}
			
			// continue the search
			if (target < node.val) {
				node = node.left;
			}
			else {
				node = node.right;
			}
		}
		
		
		return res;
	}

}
