package problems.ta.tree;

public class Q004C_BST_Delete_A_Node {
	
	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		
		TreeNode(int x) {this.val = x;}
	}
	
	public static void main(String[] args) {
		
	}
	
	/**
	 * 删除二叉搜索树的节点要分两种情况:
	 * 
	 * 1. 被删除节点左孩子为空
	 * 解决办法: 让父节点的右指针指向右孩子
	 * 
	 * 2. 被删除节点左孩子不为空
	 * 解决办法: 从左子树里找最大的节点A, 用A的值替换被删除节点的值, 最后将A的父节点的右指针指向A的左孩子
	 * 
	 * 提示: 利用一个指针来辅助记录父节点
	 */
	static boolean deleteNode(TreeNode root, int target) {
		if (root == null) {
			return false;
		}
		
		// root 没有父节点
		TreeNode parent = null;
		
		// step 1. 寻找该节点 (利用BST性质)
		TreeNode node = root;
		
		while (node != null && node.val != target) {
			// 记录父节点
			parent = node;
			
			if (target < node.val) {
				node = node.left;
			} else {
				node = node.right;
			}
		}
		
		// 若找不到直接返回
		if (node == null) {
			return false;
		}
		
		// step 2. 开始删除
		
		// 被删除节点左孩子为空
		if (node.left == null) {
			if (parent == null) {
				root = node.right;
			} else {
				parent.right = node.right;
			}
		}
		// 被删除节点左孩子不为空
		else {
			// 从左子树里找最大的节点和其父节点
			TreeNode max_node = node.left;
			TreeNode max_parent = node;
			
			while (max_node.right != null) {
				max_parent = max_node;
				max_node = max_node.right;
			}
			
			// 用max_node的值替换被删除节点的值
			node.val = max_node.val;
			
			// 最后将max_node的父节点的右指针指向max_node的左孩子
			max_parent.right = max_node.left;
		}
		
		return true;
	}

}
