package problems.ta.tree;

public class Q009_Lowest_Common_Ancestor {
	
	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		
		TreeNode(int x) {this.val = x;}
	}
	
	public static void main(String[] args) {
		
	}
	
	static TreeNode lca(TreeNode root, TreeNode a, TreeNode b) {
		if (root == null) {
			return null;
		}
		
		if (root == a || root == b) {
			return root;
		}
		
		// 分别从左边和右边找最近公共父节点
		TreeNode left = lca(root.left, a, b);
		TreeNode right = lca(root.right, a, b);
		
		// a and b are in different subtrees
		if (left != null && right != null) {
			return root;
		}
		// a and b are in the same subtree
		else {
			return left == null ? right : left;
		}
	}

}
