package problems.ta.tree;

public class Q005_Is_Same_Tree {
	
	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		
		TreeNode(int x) {this.val = x;}
	}
	
	public static void main(String[] args) {
		
	}
	
	static boolean isSameTree(TreeNode root1, TreeNode root2) {
		if (root1 == null && root2 == null) {
			return true;
		}
		
		if (root1 == null || root2 == null) {
			return false;
		}
		
		if (root1.val != root2.val) {
			return false;
		}
		
		return isSameTree(root1.left, root2.left) &&
			   isSameTree(root1.right, root2.right);
	}

}
