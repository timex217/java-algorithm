package problems.ta.tree;

/**
 * 树的最小深度，解法和最大深度类似，求左右子树最小深度的最小值并加1
 * 唯一不同的地方在于，要单独考虑只有左子树或只有右子树的情况
 */

public class Q007A_Minimum_Depth {
	
	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		
		TreeNode(int x) {this.val = x;}
	}
	
	public static void main(String[] args) {
		
	}
	
	/**
	 * 
	 */
	static int minDepth(TreeNode root) {
		if (root == null) {
			return 0;
		}
		
		// 如果只有右子树, 返回右子树最小深度 + 1
		if (root.left == null && root.right != null) {
			return minDepth(root.right) + 1;
		}
		
		// 如果只有左子树，返回左子数最小深度 + 1
		if (root.left != null && root.right == null) {
			return minDepth(root.left) + 1;
		}
		
		// 否则，返回左子树以及右子树的最小深度 + 1
		return Math.min(minDepth(root.left), minDepth(root.right)) + 1;
	}

}
