package problems.ta.tree;

/**
 * 二叉搜索树的查找利用了其递归定义的性质，采用递归法求解
 */

public class Q004A_BST_Find_A_Node {
	
	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		
		TreeNode(int x) {this.val = x;}
	}
	
	public static void main(String[] args) {
		
	}
	
	/**
	 * 在一棵二叉搜索树中查找val为target的节点
	 */
	static TreeNode search(TreeNode root, int target) {
		if (root == null || root.val == target) {
			return root;
		}
		
		if (target < root.val) {
			return search(root.left, target);
		}
		else {
			return search(root.right, target);
		}
	}

}
