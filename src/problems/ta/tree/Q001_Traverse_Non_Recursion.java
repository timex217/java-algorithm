package problems.ta.tree;

import java.util.*;

public class Q001_Traverse_Non_Recursion {
	
	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		
		TreeNode(int x) {
			this.val = x;
		}
	}
	
	/**
	 * Preorder
	 */
	static void preorder(TreeNode root) {
		if (root == null) {
			return;
		}
		
		Stack<TreeNode> stack = new Stack<TreeNode>();
		TreeNode current = root;
		
		while (!stack.isEmpty() || current != null) {
			if (current != null) {
				visit(current);
				stack.push(current);
				current = current.left;
			}
			else {
				current = stack.pop();
				current = current.right;
			}
		}
	}
	
	/**
	 * Inorder
	 */
	static void inorder(TreeNode root) {
		if (root == null) {
			return;
		}
		
		Stack<TreeNode> stack = new Stack<TreeNode>();
		TreeNode current = root;
		
		while (!stack.isEmpty() || current != null) {
			// push all the way left
			if (current != null) {
				stack.push(current);
				current = current.left;
			}
			else {
				current = stack.pop();
				visit(current);
				current = current.right;
			}
		}
	}
	
	/**
	 * Postorder
	 */
	static void postorder(TreeNode root) {
		if (root == null) {
			return;
		}
		
		Stack<TreeNode> stack = new Stack<TreeNode>();
		Stack<TreeNode> output = new Stack<TreeNode>();
		
		stack.push(root);
		
		while (!stack.isEmpty()) {
			TreeNode node = stack.pop();
			output.push(node);
			
			if (node.left != null) {
				stack.push(node.left);
			}
			
			if (node.right != null) {
				stack.push(node.right);
			}
		}
		
		while (!output.isEmpty()) {
			TreeNode node = output.pop();
			visit(node);
		}
	}
	
	static void visit(TreeNode node) {
		if (node != null) {
			// process the node
		}
	}
	
	public static void main(String[] args) {
		
	}

}
