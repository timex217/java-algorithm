package problems.ta.tree;

/**
 * 给定一个区间，在BST中删除所有value在区间之外的节点
 * 
 * 比如删除所有小于3, 大于10的节点
 */

public class Q012_BST_Delete_Nodes_Outside_Range {
	
	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		
		TreeNode(int x) {this.val = x;}
	}
	
	public static void main(String[] args) {
		
	}
	
	/**
	 * postorder遍历保证，如果有一个节点要被删除，它最多只有一个子树，因为另一个子树已经因为postorder遍历删除了 
	 */
	static TreeNode deleteNode(TreeNode root, int min, int max) {
		if (root == null) {
			return null;
		}
		
		root.left = deleteNode(root.left, min, max);
		root.right = deleteNode(root.right, min, max);
		
		// 处理当前node
		if (root.val < min) {
			return root.right;
		}
		else if (root.val > max) {
			return root.left;
		}
		else {
			return root;
		}
	}

}
