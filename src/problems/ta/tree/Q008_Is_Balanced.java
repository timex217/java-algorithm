package problems.ta.tree;

public class Q008_Is_Balanced {
	
	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		
		TreeNode(int x) {this.val = x;}
	}
	
	public static void main(String[] args) {
		
	}
	
	static boolean isBalanced(TreeNode root) {
		return balancedHeight(root) != -1;
	}
	
	static int balancedHeight(TreeNode root) {
		if (root == null) {
			return 0;
		}
		
		int left_height = balancedHeight(root.left);
		int right_height = balancedHeight(root.right);
		
		if (left_height < 0 || right_height < 0 || Math.abs(left_height - right_height) > 1) {
			return -1;
		}
		
		return Math.max(left_height, right_height) + 1;
	}

}
