package problems.ta.tree;

/**
 * 最大深度也就是树的高度
 */

public class Q007B_Maximum_Depth {
	
	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) {this.val = x;}
	}
	
	public static void main(String[] args) {
		
	}
	
	/**
	 * 
	 */
	static int maxDepth(TreeNode root) {
		if (root == null) {
			return 0;
		}
		
		int left_depth = maxDepth(root.left);
		int right_depth = maxDepth(root.right);
		
		return Math.max(left_depth, right_depth) + 1;
	}

}
