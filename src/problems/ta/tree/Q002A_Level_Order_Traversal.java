package problems.ta.tree;

/**
 * 
 */

import java.util.*;

public class Q002A_Level_Order_Traversal {
	
	static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		
		TreeNode(int x) {this.val = x;}
	}
	
	public static void main(String[] args) {
		
	}
	
	/**
	 * Use null as line separator
	 */
	static void levelOrder1(TreeNode root) {
		if (root == null) {
			return;
		}
		
		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		queue.add(root);
		queue.add(null);
		
		while (!queue.isEmpty()) {
			TreeNode node = queue.poll();
			
			if (node != null) {
				print(node);
				
				if (node.left != null) {
					queue.add(node.left);
				}
				
				if (node.right != null) {
					queue.add(node.right);
				}
			}
			else {
				// new line
				System.out.println();
				
				if (!queue.isEmpty()) {
					queue.add(null);
				}
			}
		}
	}
	
	/**
	 * Use two queue
	 */
	static void levelOrder2(TreeNode root) {
		if (root == null) {
			return;
		}
		
		Queue<TreeNode> curr = new LinkedList<TreeNode>();
		Queue<TreeNode> next = new LinkedList<TreeNode>();
		
		curr.add(root);
		
		while (!curr.isEmpty()) {
			
			while (!curr.isEmpty()) {
				TreeNode node = curr.poll();
				print(node);
				
				if (node.left != null) {
					next.add(node.left);
				}
				
				if (node.right != null) {
					next.add(node.right);
				}
			}
			
			// if current queue is empty
			// swap curr and next
			System.out.println();
			
			Queue<TreeNode> tmp = curr;
			curr = next;
			next = tmp;
		}
	}
	
	/**
	 * Print node
	 */
	static void print(TreeNode node) {
		System.out.print(node.val);
	}

}
