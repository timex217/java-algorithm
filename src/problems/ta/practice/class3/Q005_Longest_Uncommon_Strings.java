package problems.ta.practice.class3;

/**
 * 一个字典有给一系列strings，要求找两个string，使得它们没有共同字符，并且长度乘积最大.
 * (Assumption: all letters in the word is from 'a-z' in ASCII)
 */

import java.util.*;

public class Q005_Longest_Uncommon_Strings {
    
    static class Pair {
        int i1;
        int i2;
        String s1;
        String s2;
        int production;
        
        Pair(int i1, int i2, String s1, String s2) {
            this.i1 = i1;
            this.i2 = i2;
            this.s1 = s1;
            this.s2 = s2;
            this.production = s1.length() * s2.length();
        }
    }
    
    public static void main(String[] args) {
        Set<String> dict = new HashSet<String>();
        dict.add("adzz");
        dict.add("abd");
        dict.add("abcde");
        dict.add("fgz");
        
        Pair pair = solution(dict);
        
        if (pair == null) {
            System.out.println("No pair is found.");
        } 
        else {
            System.out.println(pair.s1);
            System.out.println(pair.s2);
        }
    }
    
    static Pair solution(Set<String> dict) {
        if (dict == null || dict.size() < 2) {
            return null;
        }
        
        // convert dictionary to array
        int n = dict.size();
        String[] a = new String[n];
        dict.toArray(a);
        
        // sort by string length in reverse order
        Arrays.sort(a, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s2.length() - s1.length();
            }
        });
        
        //System.out.println(Arrays.toString(a));
        
        // use a max heap
        PriorityQueue<Pair> heap = new PriorityQueue<Pair>(dict.size(), new Comparator<Pair>(){
            @Override
            public int compare(Pair p1, Pair p2) {
                return p2.production - p1.production;
            }
        });
        
        // use a set to avoid duplicate calculation
        Set<String> map = new HashSet<String>();
        
        // initialize the heap
        heap.offer(new Pair(0, 1, a[0], a[1]));
        
        while (!heap.isEmpty()) {
            Pair pair = heap.poll();
            
            if (!hasCommonChars(pair.s1, pair.s2)) {
                return pair;
            }
            
            // generate two states
            int i1 = pair.i1;
            int i2 = pair.i2;
            
            if (i1 < n - 1) {
                // check duplication
                heap.offer(new Pair(i1 + 1, i2, a[i1 + 1], a[i2]));
            }
            
            if (i2 < n - 1) {
                // check duplication
                heap.offer(new Pair(i1, i2 + 1, a[i1], a[i2 + 1]));
            }
        }
        
        return null;
    }
    
    static boolean hasCommonChars(String s1, String s2) {
        if (s1 == null && s2 == null) {
            return true;
        }
        
        if (s1 == null && s2 != null) {
            return true;
        }
        
        if (s1 != null && s2 == null) {
            return true;
        }
        
        int i1 = 0;
        for (int i = 0; i < s1.length(); i++) {
            i1 |= 1 << (s1.charAt(i) - 97);
        }
        
        int i2 = 0;
        for (int i = 0; i < s2.length(); i++) {
            i2 |= 1 << (s2.charAt(i) - 97);
        }
        
        return (i1 & i2) != 0;
    }

}
