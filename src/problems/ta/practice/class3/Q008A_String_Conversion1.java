package problems.ta.practice.class3;

public class Q008A_String_Conversion1 {
    
    public static void main(String[] args) {
        char[] chars = {'A', 'B', 'C', 'D', 'E', '1', '2', '3', '4', '5'};
        convert(chars);
        System.out.println(chars);
    }
    
    static void convert(char[] chars) {
        if (chars == null || chars.length == 0) {
            return;
        }
        helper(chars, 0, chars.length - 1);
    }
    
    static void helper(char[] chars, int lo, int hi) {
        int n = hi - lo + 1;
        
        // base case, 少于或等于2个字符时不需要处理了
        if (n <= 2) {
            return;
        }
        
        // 需要处理的字符个数
        int c = n / 2;
        // 需要旋转的字符个数
        int k = c / 2;
        
        // A B C D E 1 2 3 4 5
        //       l   m   r
        int mid = lo + c;
        int left = mid - k;
        int right = left + c  - 1;
        
        // rotate
        rotateLeft(chars, left, right, k);
        
        System.out.println(chars);
        
        helper(chars, lo, right - k);
        helper(chars, right - k + 1, hi);
    }
    
    static void rotateLeft(char[] chars, int lo, int hi, int k) {
        reverse(chars, lo, lo + k - 1);
        reverse(chars, lo + k, hi);
        reverse(chars, lo, hi);
    }
    
    static void reverse(char[] chars, int lo, int hi) {
        int i = lo;
        int j = hi;
        
        while (i < j) {
            char tmp = chars[i];
            chars[i] = chars[j];
            chars[j] = tmp;
            i++;
            j--;
        }
    }

}
