package problems.ta.practice.class3;

import java.util.*;

public class Q002_Integer_To_Hex {
    
    public static void main(String[] args) {
        int n = 29;
        String hex = int2hex(n);
        System.out.println(hex);
    }
    
    static String int2hex(int n) {
        char[] table = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        
        StringBuilder sb = new StringBuilder();
        
        while (n > 0) {
            int mod = n % 16;
            sb.insert(0, table[mod]);
            n = n / 16;
        }
        
        sb.insert(0, "0x");
        
        return sb.toString();
    }

}
