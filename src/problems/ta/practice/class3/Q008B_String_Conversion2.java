package problems.ta.practice.class3;

import java.util.*;

public class Q008B_String_Conversion2 {
    
    public static void main(String[] args) {
        char[] chars = {'A', '1', 'B', '2', 'C', '3', 'D', '4'};
        convert(chars);
        System.out.println(chars);
    }
    
    static void convert(char[] chars) {
        if (chars == null || chars.length == 0) {
            return;
        }
        helper(chars, 0, chars.length - 1);
    }
    
    static void helper(char[] chars, int lo, int hi) {
        int n = hi - lo + 1;
        
        // base case
        if (n <= 2) {
            return;
        }
        
        // 需要处理的字符个数
        int c = n / 2;
        // 需要旋转的字符个数
        int k = c / 2;
        
        int mid = lo + c;
        int left = mid - k;
        int right = left + c - 1;
        
        helper(chars, lo, right - k);
        helper(chars, right - k + 1, hi);
        
        // rotate
        rotateLeft(chars, left, right, c - k);
        
        System.out.println(chars);
    }
    
    static void rotateLeft(char[] chars, int lo, int hi, int k) {
        reverse(chars, lo, lo + k - 1);
        reverse(chars, lo + k, hi);
        reverse(chars, lo, hi);
    }
    
    static void reverse(char[] chars, int lo, int hi) {
        int i = lo;
        int j = hi;
        
        while (i < j) {
            char tmp = chars[i];
            chars[i] = chars[j];
            chars[j] = tmp;
            i++;
            j--;
        }
    }

}
