package problems.ta.practice.class3;

import java.util.*;

public class Q007_Find_Closest_To_3D_Origin_Point {
    
    static int[] x = {1, 3, 5, 7, 9};
    static int[] y = {2, 3, 4, 5, 8};
    static int[] z = {1, 3, 4, 5, 6};
    
    static class Point {
        int i;
        int j;
        int k;
        
        Point(int i, int j, int k) {
            this.i = i;
            this.j = j;
            this.k = k;
        }
    }
    
    public static void main(String[] args) {
        Point p = getKthPoint(10);
        if (p == null) {
            System.out.println("No such point");
        }
        else {
            System.out.format("\nResult is: x = %d, y = %d, z = %d\n", x[p.i], y[p.j], z[p.k]);
        }
    }
    
    static Point getKthPoint(int k) {
        // use a min heap
        PriorityQueue<Point> heap = new PriorityQueue<Point>(x.length * 3, new Comparator<Point>(){
            @Override
            public int compare(Point p1, Point p2) {
                int d1 = x[p1.i] * x[p1.i] + y[p1.j] * y[p1.j] + z[p1.k] * z[p1.k];
                int d2 = x[p2.i] * x[p2.i] + y[p2.j] * y[p2.j] + z[p2.k] * z[p2.k];
                return d1 - d2;
            }
        });
        
        // use a set to avoid duplicate calculation
        Set<String> set = new HashSet<String>();
        
        // initialize state
        heap.offer(new Point(0, 0, 0));
        
        int count = 1;
        while (!heap.isEmpty()) {
            Point p = heap.poll();
            
            System.out.format("x = %d, y = %d, z = %d\n", x[p.i], y[p.j], z[p.k]);
            
            if (count == k) {
                return p;
            }
            
            // generate three states
            if (p.i + 1 < x.length) {
                String key = getKey(p.i + 1, p.j, p.k);
                if (!set.contains(key)) {
                    heap.offer(new Point(p.i + 1, p.j, p.k));
                    set.add(key);
                }
            }
            
            if (p.j + 1 < y.length) {
                String key = getKey(p.i, p.j + 1, p.k);
                if (!set.contains(key)) {
                    heap.offer(new Point(p.i, p.j + 1, p.k));
                    set.add(key);
                }
            }
            
            if (p.k + 1 < z.length) {
                String key = getKey(p.i, p.j + 1, p.k);
                if (!set.contains(key)) {
                    heap.offer(new Point(p.i, p.j, p.k + 1));
                    set.add(key);
                }
            }
            
            count++;
        }
        
        return null;
    }
    
    static String getKey(int i, int j, int k) {
        return String.format("%d-%d-%d", i, j, k);
    }

}
