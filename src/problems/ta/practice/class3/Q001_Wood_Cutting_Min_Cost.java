package problems.ta.practice.class3;

import java.util.*;

public class Q001_Wood_Cutting_Min_Cost {
    
    public static void main(String[] args) {
        int L = 10;
        int[] a = {2, 4, 7};
        int cost = getMinCuttingCost(a, L);
        System.out.println(cost);
    }
    
    static int getMinCuttingCost(int[] a, int L) {
        // sanity check
        // ...
        
        int n = a.length + 2;
        
        int[] p = new int[n];
        p[0] = 0;
        p[n - 1] = L;
        
        for (int i = 0; i < a.length; i++) {
            p[i + 1] = a[i];
        }
        
        // 注意前提是必须都要切, 只是切的顺序会影响到cost
        // 0                       L
        // |___|___|___|___|___|___|
        // 0   i       k       j  n-1
        //
        // c(i, j) represents the minimum cutting cost from i-th cutting position to j-th cutting position
        // c(i, j) = 0, j <= i + 1 (j = i + 1, 相当于没切)
        // c(i, j) = min{ c(i, k) + c(k, j) } + p[j] - p[i], i < k < j
        //
        // return c(0, n - 1)
        
        int[][] c = new int[n][n];
        for (int i = n - 1; i >= 0; i--) {
            for (int j = i + 1; j < n; j++) {
                int min = Integer.MAX_VALUE;
                
                for (int k = i + 1; k < j; k++) {
                    min = Math.min(min, c[i][k] + c[k][j]);
                }
                
                c[i][j] = min + p[j] - p[i];
            }
        }
        
        for (int i = 0; i < n; i++) {
            System.out.println(Arrays.toString(c[i]));
        }
        
        return c[0][n - 1];
    }

}
