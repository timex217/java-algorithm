package problems.ta.practice.class3;

/**
 * 给一个integer array，允许duplicates，而且其中某个未知的integer的duplicates的个数占了整个array的一大半。
 * 如何有效的找出这个integer？
 */

import java.util.*;

public class Q009_Majority_Algorithm {
    
    public static void main(String[] args) {
        int[] a = {1, 2, 2, 1, 5, 1, 4, 5, 1, 7, 8, 1, 1, 9, 3, 1, 0, 1, 1, 1};
        Integer number = find(a);
        if (number == null) {
            System.out.println("No such number");
        }
        else {
            System.out.format("The number is: %d\n", number);
        }
    }
    
    static Integer find(int[] a) {
        if (a == null || a.length == 0) {
            return -1;
        }
        
        Integer number = null;
        int count = 0;
        
        for (int i = 0; i < a.length; i++) {
            if (number == null) {
                number = a[i];
                count = 1;
            }
            else if (number == a[i]) {
                count++;
            }
            else {
                count--;
                if (count == 0) {
                    number = null;
                }
            }
        }
        
        return number;
    }

}
