package problems.ta.practice.class3;

import java.util.*;

public class Q004_K_Nodes_Lowest_Common_Ancestor {
    
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) {
            this.val = x;
        }
    }
    
    public static void main(String[] args) {
        
    }
    
    static TreeNode kLCA(TreeNode root, List<TreeNode> nodes) {
        if (root == null || nodes == null || nodes.size() == 0) {
            return null;
        }
        
        return helper(root, nodes, 0, nodes.size() - 1);
    }
    
    static TreeNode helper(TreeNode root, List<TreeNode> nodes, int lo, int hi) {
        if (lo < hi) {
            int mid = lo + (hi - lo) / 2;
            
            TreeNode left = helper(root, nodes, lo, mid);
            TreeNode right = helper(root, nodes, mid + 1, hi);
            
            return LCA(root, left, right);
        }
        else {
            // if there's only one node, then it's lca is itself
            return nodes.get(lo);
        }
    }
    
    static TreeNode LCA(TreeNode root, TreeNode a, TreeNode b) {
        if (root == null) {
            return null;
        }
        
        if (root == a || root == b) {
            return root;
        }
        
        TreeNode left = LCA(root.left, a, b);
        TreeNode right = LCA(root.right, a, b);
        
        if (left != null && right != null) {
            return root;
        }
        
        return left != null ? left : right;
    }

}
