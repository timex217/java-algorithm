package problems.ta.practice.class3;

import java.util.*;

public class Q010A_Binary_Tree_Reconstruction_Pre_In {
    
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) {this.val = x;}
    }
    
    public static void main(String[] args) {
        int[] pre = {10, 5, 2, 7,  15, 12, 20};
        int[] in  = {2,  5, 7, 10, 12, 15, 20};
        
        TreeNode tree = reconstruct(pre, in);
    }
    
    static TreeNode reconstruct(int[] pre, int[] in) {
        // use a hashmap to help find the index of a certain value
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int i = 0; i < in.length; i++) {
            map.put(in[i], i);
        }
        
        return helper(pre, 0, pre.length - 1, in, 0, in.length - 1, map);
    }
    
    static TreeNode helper(int[] pre, int pre_left, int pre_right, int[] in, int in_left, int in_right, HashMap<Integer, Integer> map) {
        // base case
        if (pre_left > pre_right || in_left > in_right) {
            return null;
        }
        
        // create the root node
        int val = pre[pre_left];
        TreeNode root = new TreeNode(val);
        
        // get the index of root node in inorder
        int idx = map.get(val);
        
        // now, all items on root_idx left are left subtree, all items on root_idx right are right subtree
        // also, we know the number of items in the subtree and right tree, what we need to do is to separate the pre-order array
        // and solve the problem recursively
        root.left = helper(pre, pre_left + 1, pre_left + (idx - in_left), in, in_left, idx - 1, map);
        root.right = helper(pre, pre_left + (idx - in_left) + 1, pre_right, in, idx + 1, in_right, map);
        
        return root;
    }

}
