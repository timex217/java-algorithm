package problems.ta.practice.class3;

import java.util.*;

public class Q010B_Binary_Tree_Reconstruction_Post_in {
    
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) {this.val = x;}
    }
    
    public static void main(String[] args) {
        int[] post = {2, 7, 5, 12, 20, 15, 10};
        int[] in = {2, 5, 7, 10, 12, 15, 20};
        
        TreeNode tree = reconstruct(post, in);
    }
    
    static TreeNode reconstruct(int[] post, int[] in) {
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int i = 0; i < in.length; i++) {
            map.put(in[i], i);
        }
        
        return helper(post, 0, post.length - 1, in, 0, in.length - 1, map);
    }
    
    static TreeNode helper(int[] post, int post_left, int post_right, int[] in, int in_left, int in_right, HashMap<Integer, Integer> map) {
        if (post_left > post_right || in_left > in_right) {
            return null;
        }
        
        // create the root node
        int val = post[post_right];
        TreeNode root = new TreeNode(val);
        
        // get the root node index in inorder
        int idx = map.get(val);
        
        root.left = helper(post, post_left, post_left + (idx - in_left) - 1, in, in_left, idx - 1, map);
        root.right = helper(post, post_left + (idx - in_left), post_right - 1, in, idx + 1, in_right, map);
        
        return root;
    }

}
