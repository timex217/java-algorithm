package problems.ta.practice.class3;

/**
 * Sliding window of size k, always return the max element in the window size.
 * 
 * see http://leetcode.com/2011/01/sliding-window-maximum.html
 */

import java.util.*;

public class Q003_Sliding_Window {
    
    public static void main(String[] args) {
        int[] A = {1, 3, 2, 5, 8, 9, 4, 7, 3};
        ArrayList<Integer> B = maxSlidingWindow(A, 3);
        
        System.out.println(B.toString());
    }
    
    static ArrayList<Integer> maxSlidingWindow(int[] A, int w) {
        if (A == null || A.length == 0 || w < 1) {
            return null;
        }
        
        int n = A.length;
        
        ArrayList<Integer> res = new ArrayList<Integer>();
        
        // use a deque to control the window
        Deque<Integer> q = new ArrayDeque<Integer>();
        
        // initialize the window
        for (int i = 0; i < w; i++) {
            while (!q.isEmpty() && A[i] >= A[q.peekLast()]) {
                q.removeLast();
            }
            q.add(i);
        }
        
        for (int i = w; i < n; i++) {
            res.add(A[q.peekFirst()]);
            
            // pop those smaller ones from behind
            while (!q.isEmpty() && A[i] >= A[q.peekLast()]) {
                q.removeLast();
            }
            
            // exceeds window size
            while (!q.isEmpty() && (i >= q.peekFirst() + w)) {
                q.removeFirst();
            }
            
            q.add(i);
        }
        
        res.add(A[q.peekFirst()]);
        
        return res;
    }

}
