package problems.ta.practice.class2;

/**
 * how to find the largest and smallest numbers from an unsorted array with least number of comparison.
 */

import java.util.*;

public class Q005_Find_Largetst_and_Smallest_In_Unsorted_Array {
    
    public static void main(String[] args) {
        int[] a = {8, 6, 7, 5, 3, 1, 2, 9, 4, 0};
        ArrayList<Integer> res = findMinMax(a);
        System.out.println(res.toString());
    }
    
    /**
     * (n - 2) / 2 * 3 + 1 = 1.5n - 2
     */
    static ArrayList<Integer> findMinMax(int[] a) {
        if (a == null) {
            return null;
        }
        
        ArrayList<Integer> res = new ArrayList<Integer>();
        
        if (a.length == 0) {
            return res;
        }
        
        // only has one item
        if (a.length == 1) {
            res.add(a[0]);
            return res;
        }
        
        // more than 1 item
        // 1st comparison
        // initialize the res
        if (a[0] < a[1]) {
            res.add(a[0]);
            res.add(a[1]);
        }
        else {
            res.add(a[1]);
            res.add(a[0]);
        }
        
        int n = a.length;
        
        // (n - 2) / 2 comparison
        for (int i = 2; i < n - 1; i+=2) {
            int j = i + 1;  
            int min, max;
            
            // 1st comparison
            if (a[i] < a[j]) {
                min = a[i];
                max = a[j];
            }
            else {
                min = a[j];
                max = a[i];
            }
            
            // 2nd comparison
            if (min < res.get(0)) {
                res.set(0, min);
            }
            
            // 3rd comparison
            if (max > res.get(1)) {
                res.set(1, max);
            }
        }
        
        // final check for the last item
        if (n % 2 != 0) {
            // last two comparison
            if (a[n - 1] < res.get(0)) {
                res.set(0, a[n - 1]);
            }
            
            if (a[n - 1] > res.get(1)) {
                res.set(1, a[n - 1]);
            }
        }
        
        return res;
    }

}
