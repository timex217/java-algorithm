package problems.ta.practice.class2;

/**
 * Find out the length of longest ones from an unsorted array
 */

import java.util.*;

public class Q002_Longest_Ones_In_Unsorted_Array {
    
    public static void main(String[] args) {
        int[] a = {0, 1, 1, 2, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 2, 0};
        int len = getLongestOnes(a);
        
        System.out.println(len);
    }

    static int getLongestOnes(int[] a) {
        if (a == null || a.length == 0) {
            return 0;
        }
        
        int max = 0;
        int n = a.length;
        
        // c(i) represents the count of consecutive ones till a[i]
        // c(i) = 0, a[i] != 1
        // c(i) = c(i - 1) + 1, a[i] = 1
        
        int[] c = new int[n];
        
        for (int i = 0; i < n; i++) {
            c[i] = 0;
            
            if (a[i] == 1) {
                c[i] = (i == 0) ? 1 : c[i - 1] + 1;
            }
            
            max = Math.max(max, c[i]);
        }
        
        return max;
    }
}
