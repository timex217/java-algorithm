package problems.ta.practice.class2;

import java.util.*;

public class Q006_Find_Largest_and_2nd_Largest_In_Unsorted_Array {
    
    static class Node {
        int val;
        ArrayList<Integer> list;
        Node(int x) {
            this.val = x;
            this.list = new ArrayList<Integer>();
        }
    }
    
    public static void main(String[] args) {
        int[] a = {8, 6, 7, 5, 3, 1, 2, 9, 4, 0};
        ArrayList<Integer> res = findTheTwoLargest(a);
        System.out.println(res.toString());
    }
    
    static ArrayList<Integer> findTheTwoLargest(int[] a) {
        if (a == null) {
            return null;
        }
        
        ArrayList<Integer> res = new ArrayList<Integer>();
        
        if (a.length == 0) {
            return res;
        }
        
        if (a.length == 1) {
            res.add(a[0]);
            return res;
        }
        
        // 采用锦标赛策略
        //
        //     6(5, 1)
        //       / \
        //    6(1)  5(3)
        //    / \   / \
        //   1   6 5   3
        // 
        // 经过n-1次比较找到最大值6，与6比较过的数一共有logn个，所以找出第二大的数需要logn-1次比较
        // 算法复杂度就是 n + logn - 2
        // 普通算法复杂度是 2n - 3
        
        // initialize arraylist
        ArrayList<Node> nodes = new ArrayList<Node>();
        for (int i = 0; i < a.length; i++) {
            Node node = new Node(a[i]);
            nodes.add(node);
        }
        
        // compare two nodes each time until there is only one node
        while (nodes.size() > 1) {
            int n = nodes.size();
            
            ArrayList<Node> tmp = new ArrayList<Node>();
            
            //
            for (int i = 0; i < nodes.size() - 1; i+=2) {
                Node p1 = nodes.get(i);
                Node p2 = nodes.get(i + 1);
                if (p1.val > p2.val) {
                    p1.list.add(p2.val);
                    tmp.add(p1);
                }
                else {
                    p2.list.add(p1.val);
                    tmp.add(p2);
                }
            }
            
            //
            if (n % 2 != 0) {
                tmp.add(nodes.get(n - 1));
            }
            
            nodes = tmp;
        }
        
        Node maxNode = nodes.get(0);
        
        res.add(maxNode.val);
        res.add(getMax(maxNode.list));
        
        //System.out.println(maxNode.val);
        //System.out.println(maxNode.list.toString());
        
        return res;
    }
    
    static int getMax(ArrayList<Integer> list) {
        if (list == null || list.size() == 0) {
            return Integer.MIN_VALUE;
        }
        
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < list.size(); i++) {
            max = Math.max(max, list.get(i));
        }
        
        return max;
    }

}
