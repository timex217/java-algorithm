package problems.ta.practice.class2;

/**
 * Given a dictionary. How to find the longest word made of two other words.
 * 
 * example:
 * input: test, tester, testertest, testing, testingtester
 * output (longest word): testingtester
 */

import java.util.*;

public class Q004_Longest_Word_Made_of_Two_Other_Words {
    
    public static void main(String[] args) {
        // initialize the dictionary
        Set<String> dict = new HashSet<String>();
        dict.add("test");
        dict.add("tester");
        dict.add("testertest");
        dict.add("testing");
        dict.add("testingtester");
        
        String res = find(dict, "testingtester");
        System.out.println(res);
    }

    static String find(Set<String> dict, String word) {
        String[] a = sortDictionary(dict);
        if (a == null) {
            return null;
        }
        
        // use 2-sum's idea to do the search, start from the longest string
        int n = a.length;
        for (int i = 0; i < n - 2; i++) {
            String si = a[i];
            
            int j = i + 1, k = n - 1;
            while (j < k) {
                String sj = a[j];
                String sk = a[k];
                
                if (si.equals(sj + sk) || si.equals(sk + sj)) {
                    return si;
                }
                
                if (sj.length() + sk.length() > si.length()) {
                    j++;
                }
                else {
                    k--;
                }
            }
        }
        
        return null;
    }
    
    static String[] sortDictionary(Set<String> dict) {
        if (dict == null) {
            return null;
        }
        
        // convert dictionary to array
        String[] a = new String[dict.size()];
        dict.toArray(a);
        
        // sort by string length in reverse order
        Arrays.sort(a, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s2.length() - s1.length();
            }
        });
        
        return a;
    }
}
