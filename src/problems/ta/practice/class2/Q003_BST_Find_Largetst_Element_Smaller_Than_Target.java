package problems.ta.practice.class2;

/**
 * (BST) Given a binary search tree, how to find the largest element in the tree that is
 * smaller than a target number x.
 */

import java.util.*;

public class Q003_BST_Find_Largetst_Element_Smaller_Than_Target {
    
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) {this.val = x;}
    }
    
    public static void main(String[] args) {
        
    }
    
    // ---------------------------
    //  Method 0: BST nature
    //  如果当前节点值大于等于x, 就向左走curr = curr->left
    //  否则一旦向右走就更新result值, result = curr->val, 因为当前节点左边的节点值更小
    // ---------------------------
    static TreeNode find(TreeNode root, int target) {
        TreeNode current = root;
        TreeNode res = null;
        
        while (current != null) {
            if (current.val >= target) {
                current = current.left;
            } else {
                res = current;
                current = current.right;
            }
        }
        
        return res;
    }
    
    // ---------------------------
    //  Method 2: inorder -> binary search upper bound
    // ---------------------------
    static TreeNode find1(TreeNode root, int target) {
        if (root == null) {
            return null;
        }
        
        // inorder to get a sorted array
        ArrayList<TreeNode> arr = inorder(root);
        
        // binary search to find the largetst element that is smaller than the target
        return find(arr, target);
    }
    
    static ArrayList<TreeNode> inorder(TreeNode root) {
        if (root == null) {
            return null;
        }
        
        ArrayList<TreeNode> res = new ArrayList<TreeNode>();
        
        Stack<TreeNode> stack = new Stack<TreeNode>();
        TreeNode current = root;
        
        while (!stack.isEmpty() || current != null) {
            if (current != null) {
                stack.push(current);
                current = current.left;
            }
            else {
                current = stack.pop();
                res.add(current);
                current = current.right;
            }
        }
        
        return res;
    }
    
    static TreeNode find(ArrayList<TreeNode> a, int target) {
        if (a == null || a.size() == 0) {
            return null;
        }
        
        int lo = 0;
        int hi = a.size() - 1;
        
        while (lo < hi) {
            int mid = lo + (hi - lo + 1) / 2;
            TreeNode midNode = a.get(mid);
            
            if (midNode.val < target) {
                lo = mid;
            }
            else {
                hi = mid - 1;
            }
        }
        
        TreeNode hiNode = a.get(hi);
        if (hiNode.val < target) {
            return hiNode;
        }
        else {
            return null;
        }
    }

}
