package problems.ta.practice.class2;

/**
 * Given a word, can it be composed by concatenating words from a given dictionary?
 * Example: Dictionary:
 * bob
 * cat
 * rob
 * 
 * Word: bcoabt
 * Solution: False
 * 
 * Word: bobcatrob
 * Solution: True
 */

import java.util.*;

public class Q001_Dictionary_Word_Problem {
    
    public static void main(String[] args) {
        // initialize the dictionary
        Set<String> dict = new HashSet<String>();
        dict.add("bob");
        dict.add("cat");
        dict.add("rob");
        
        boolean res = check(dict, "bobcatrob");
        //boolean res = check(dict, "bcoabt");
        System.out.println(res);
    }
    
    static boolean check(Set<String> dict, String word) {
        if (dict == null || word == null || word.length() == 0) {
            return false;
        }
        
        // c(i) represents whether w[0...i] can be composed by concatenating words
        // from dictionary
        // c(i) = w[j...i] in dictionary && c(j-1), 0 <= j <= i
        int n = word.length();
        boolean[] c = new boolean[n];
        
        for (int i = 0; i < n; i++) {
            boolean can = false;
            
            for (int j = i; j >= 0; j--) {
                String s = word.substring(j, i + 1);
                
                if (j == 0) {
                    if (dict.contains(s)) {
                      can = true;
                      break;
                    }
                }
                else {
                    if (dict.contains(s) && c[j - 1]) {
                        can = true;
                        break;
                    }
                }
            }
            
            c[i] = can;
        }
        
        System.out.println(Arrays.toString(c));
        
        return c[n - 1];
    }

}
