package problems.ta.practice.class2;

import java.util.*;

public class Q007_Middle_Node_of_Linked_List {
    
    static class ListNode {
        int val;
        ListNode next;
        ListNode(int x) {this.val = x;}
    }
    
    public static void main(String[] args) {
        
    }
    
    static ListNode getMiddleNode(ListNode head) {
        if (head == null) {
            return null;
        }
        
        ListNode fast = head;
        ListNode slow = head;
        ListNode prev = null;
        
        while (fast != null) {
            prev = slow;
            slow = slow.next;
            fast = fast.next == null ? null : fast.next.next;
        }
        
        return prev;
    }
}
