package problems.ta.practice.class1;

import java.util.*;

public class Q005_Tree_Check_Same_Structure {
    
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) {this.val = x;}
    }
    
    public static void main(String[] args) {
        
    }
    
    static boolean isTweak(TreeNode r1, TreeNode r2) {
        if (r1 == null && r2 == null) return true;
        if (r1 == null && r2 != null) return false;
        if (r1 != null && r2 == null) return false;
        if (r1.val != r2.val) return false;
        
        return (isTweak(r1.left, r2.left) && isTweak(r1.right, r2.right)) ||
               (isTweak(r1.left, r2.right) && isTweak(r1.right, r2.left));
    }

}
