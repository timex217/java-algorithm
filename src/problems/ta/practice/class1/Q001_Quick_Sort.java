package problems.ta.practice.class1;

import java.util.*;

public class Q001_Quick_Sort {
    
    public static void main(String[] args) {
        int[] a = {8, 6, 5, 1, 0, -2, 9, 0, 4, 7};
        sort(a, 0, a.length - 1);
        System.out.println(Arrays.toString(a));
    }
    
    static void sort(int[] a, int lo, int hi) {
        if (a == null || a.length == 0) {
            return;
        }
        
        // base case
        if (lo >= hi) {
            return;
        }
        
        // get the pivot
        int p = partition(a, lo, hi);
        
        // sort pivot left and right
        sort(a, lo, p - 1);
        sort(a, p + 1, hi);
    }
    
    static int partition(int[] a, int lo, int hi) {
        // randomly pick an index
        int mid = lo + (hi - lo) / 2;
        int pivot = a[mid];
        
        // put it to the end
        swap(a, mid, hi);
        
        // 隔板思想
        int i = lo, j = hi;
        
        while (i < j) {
            if (a[i] <= pivot) {
                i++;
            } else {
                swap(a, i, --j);
            }
        }
        
        swap(a, i, hi);
        
        return i;
    }
    
    static void swap(int[] a, int i, int j) {
        int tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }
    
    static int randRange(int lo, int hi) {
        Random rand = new Random();
        return rand.nextInt(hi - lo) + lo;
    }

}
