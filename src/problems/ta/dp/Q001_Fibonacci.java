package problems.ta.dp;

public class Q001_Fibonacci {
	
	public static void main(String[] args) {
		int res = fibonacci(5);
		System.out.println(res);
		
		// n: 0 1 2 3 4 5 6
		// f: 0 1 1 2 3 5 8 13
	}
	
	static int fibonacci(int n) {
		if (n < 2) {
			return n;
		}
		
		int[] dp = new int[n + 1];
		dp[0] = 0;
		dp[1] = 1;
		
		for (int i = 2; i <= n; i++) {
			dp[i] = dp[i - 1] + dp[i - 2];
		}
		
		return dp[n];
	}

}
