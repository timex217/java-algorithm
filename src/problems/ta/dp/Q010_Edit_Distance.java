package problems.ta.dp;

public class Q010_Edit_Distance {
	
	public static void main(String[] args) {
		String s1 = "Yong Su";
		String s2 = "Jing Jin";
		int res = editDistance(s1, s2);
		System.out.println(res);
	}
	
	static int editDistance(String s1, String s2) {
		if (s1 == null && s2 == null) {
			return 0;
		}
		
		if (s1 == null || s1.length() == 0) {
			return s2 == null ? 0 : s2.length();
		}
		
		if (s2 == null || s2.length() == 0) {
			return s1 == null ? 0 : s1.length();
		}
		
		int n1 = s1.length();
		int n2 = s2.length();
		
		// e(i, j) represents the edit distance from s1[0...i] to s2[0...j]
		//                 e(i-1, j) + 1        (deletion)
		// e(i, j) = min { e(i-1, j-1) + 1 or 0 (replacement)
		//                 e(i, j-1) + 1        (insertion)
		
		int[][] e = new int[n1 + 1][n2 + 1];
		
		for (int i = 0; i <= n1; i++) {
			for (int j = 0; j <= n2; j++) {
				// 由空变s2, 只需添加操作就好
				if (i == 0) {
					e[i][j] = j;
				}
				// 由s1变为空, 只需删除操作就好
				else if (j == 0) {
					e[i][j] = i;
				}
				else {
					int diff = s1.charAt(i-1) == s2.charAt(j-1) ? 0 : 1;
					e[i][j] = Math.min(e[i-1][j] + 1, e[i][j-1] + 1);
					e[i][j] = Math.min(e[i][j], e[i-1][j-1] + diff);
				}
			}
		}
		
		return e[n1][n2];
	}

}
