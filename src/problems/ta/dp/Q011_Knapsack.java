package problems.ta.dp;

public class Q011_Knapsack {
	
	public static void main(String[] args) {
		int[] v = {4, 5, 3};
		int[] w = {1, 3, 2};
		
		int max = knapsack(v, w, 5);
		System.out.println(max);
	}
	
	static int knapsack(int[] v, int[] w, int c) {
		// s(i, j) represents the max value we can get by taking 前 i 个 items when weight cap is j
		// s(i, j) = max{
		//     s(i-1, j), do not take item i
		//     s(i-1, j - w[i]) + v[i], take item i
		// }
		// k(i, j) represents whether we take item i or not
		
		// item count
		int n = v.length;
		
		int[][] s = new int[n + 1][c + 1];
		boolean[][] k = new boolean[n + 1][c + 1];
		
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= c; j++) {
				// if we don't take it
				int opt = s[i - 1][j];
				
				// we can only take item i when its weight is less than or equal to j
				if (w[i - 1] <= j) {
					// if we take it
					int opt2 = s[i - 1][j - w[i - 1]] + v[i - 1];
					
					// and the value increase
					if (opt2 >= opt) {
						opt = opt2;
						k[i][j] = true;
					}
				}
				
				s[i][j] = opt;
			}
		}
		
		// trace back
		int i = n;
		int j = c;
		
		while (i > 0) {
			if (k[i][j]) {
				System.out.format("%d:%d\n", v[i - 1], w[i - 1]);
				j -= w[i-1];
			}
			i--;
		}
		
		return s[n][c];
	}

}
