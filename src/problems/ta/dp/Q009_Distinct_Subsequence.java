package problems.ta.dp;

/**
 * 给定字符串 S 和 T, 问 S 中可以有多少种出现 T 的方式, 比如 S = "rabbbit", T = "rabbit"
 */

import java.util.*;

public class Q009_Distinct_Subsequence {
	
	public static void main(String[] args) {
		String S = "rabbbit";
		String T = "rabbit";
		int res = distinctSubsequence(S, T);
		System.out.println(res);
	}
	
	static int distinctSubsequence(String S, String T) {
		if (S == null || T == null) {
			return 0;
		}
		
		int ns = S.length();
		int nt = T.length();
		
		// ns 不能小于 nt
		if (ns < nt) {
			return 0;
		}
		
		// d(i, j) represents how many ways to include T[0...j] in S[0...i]
		// d(i, j) = d(i-1, j), S[i] != T[j]
		//         = d(i-1, j) + d(i-1, j-1), S[i] == T[j]
		//
		
		int[][] dp = new int[ns + 1][nt + 1];
		
		for (int i = 1; i <= ns; i++) {
			for (int j = 1; j <= nt; j++) {
			    if (S.charAt(i-1) != T.charAt(j-1)) {
					dp[i][j] = dp[i-1][j];
				}
				else {
				    // 这道题最特殊的地方在于初始化必须单独计算 S 与 T[0]的包含关系
            	    if (j == 1) {
				        dp[i][j] = dp[i-1][j] + 1;
				    }
				    else {
				        dp[i][j] = dp[i-1][j] + dp[i-1][j-1];
				    }
				}
			}
		}
		
		return dp[ns][nt];
	}

}
