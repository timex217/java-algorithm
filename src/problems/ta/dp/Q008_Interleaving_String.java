package problems.ta.dp;

/**
 * 给定三个字符串S1, S2, S3, 问S3是否能有S1和S2中的字母构成. S1和S2必须全用上
 * 
 * 比如: s1 = "aabcc", s2 = "dbbca",
 * 如果 s3 = "aadbbcbcac", 返回 true.
 * 如果 s3 = "aadbbbaccc", 返回 false.
 */

public class Q008_Interleaving_String {
	
	public static void main(String[] args) {
		String s1 = "aabcc";
		String s2 = "dbbca";
		String s3 = "aadbbcbcac";
		//String s3 = "aadbbbaccc";
		
		boolean res = interleavingString(s1, s2, s3);
		System.out.println(res);
	}
	
	static boolean interleavingString(String s1, String s2, String s3) {
		if (s3 == null || s3.length() == 0) {
			return false;
		}
		
		if (s1 == null || s1.length() == 0) {
			return s3.equals(s2);
		}
		
		if (s2 == null || s2.length() == 0) {
			return s3.equals(s1);
		}
		
		int n1 = s1.length();
		int n2 = s2.length();
		int n3 = s3.length();
		
		if (n1 + n2 != n3) {
			return false;
		}
		
		// b(i, j) represents whether we can form s3[0...(i+j)] with s1[0...i] and s2[0...j]
		// b(i, j) = (s1[i] == s3[i+j] && b(i-1, j)) 取s1里第i个字符
		//        || (s2[j] == s3[i+j] && b(i, j-1)) 取s2里第j个字符
		// 最后返回b(n1, n2)
		
		boolean[][] b = new boolean[n1 + 1][n2 + 1];
		
		for (int i = 0; i <= n1; i++) {
			for (int j = 0; j <= n2; j++) {
				// s1, s2, s3大家都不出, ok!
				if (i == 0 && j == 0) {
					b[i][j] = true;
				}
				// s1一个都不出
				else if (i == 0) {
					b[i][j] = b[i][j-1] && (s2.charAt(j-1) == s3.charAt(j-1));
				}
				// s2一个都不出
				else if (j == 0) {
					b[i][j] = b[i-1][j] && (s1.charAt(i-1) == s3.charAt(i-1));
				}
				// 两个都可能出
				else {
					b[i][j] = (b[i][j-1] && (s2.charAt(j-1) == s3.charAt(i+j-1))) ||
							  (b[i-1][j] && (s1.charAt(i-1) == s3.charAt(i+j-1)));
				}
			}
		}
		
		return b[n1][n2];
	}

}
