package problems.ta.dp;

/**
 * 最长递增子序列
 */

import java.util.*;

public class Q004_Longest_Increasing_Subsequence {
	
	public static void main(String[] args) {
		int[] a = {5, 6, 2, 3, 4, 1, 9, 9, 8, 9, 5};
		ArrayList<Integer> res = lis(a);
		System.out.println(res.toString());
	}
	
	static ArrayList<Integer> lis(int[] a) {
		ArrayList<Integer> res = new ArrayList<Integer>();
		
		if (a == null || a.length == 0) {
			return res;
		}
		
		int n = a.length;
		
		// l(i) represents the length of longest increasing subsequence including a[i]
		// l(i) = 1 or max{l(k) + 1, (k < i, a[k] < a[i])}
		// b(i) tracks from which the current sequence ending at a[i] was extended
		int[] l = new int[n];
		int[] b = new int[n];
		
		int max = 0;
		int max_index = 0;
		
		for (int i = 0; i < n; i++) {
			int len = 1;
			for (int k = 0; k < i; k++) {
				if (a[i] > a[k] && l[k] + 1 > len) {
					len = l[k] + 1;
					b[i] = k;
				}
			}
			l[i] = len;
			
			if (len > max) {
				max = len;
				max_index = i;
			}
		}
		
		System.out.println(Arrays.toString(l));
		System.out.println(Arrays.toString(b));
		
		int i = max_index;
		for (int k = 0; k < max; k++) {
			res.add(a[i]);
			i = b[i];
		}
		
		Collections.reverse(res);
		
		return res;
	}

}
