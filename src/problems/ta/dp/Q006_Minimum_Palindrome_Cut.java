package problems.ta.dp;

/**
 * 最小palindrome cut，给定字符串，问使得各个子串都是palindrom的最小切割数是多少，比如 aab的最小切割是1， 分成aa，b
 */

import java.util.*;

public class Q006_Minimum_Palindrome_Cut {
    
    public static void main(String[] args) {
        String s = "aaabcb";
        int res = getMinPalindromeCut(s);
        System.out.println(res);
    }
    
    static int getMinPalindromeCut(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        
        int n = s.length();
        
        // c(i) represents the minimum cut till s[i]
        // 
        // |___|___|___|___|
        // 0   j       i  n-1
        //
        // c(i) = 0, s[0...i] is palindrome
        // c(i) = min { c(j) + 1 }, s[j...i] is palindrome
        
        int[] c = new int[n];
        
        for (int i = 1; i < n; i++) {
            if (isPalindrome(s.substring(0, i + 1))) {
                c[i] = 0;
                continue;
            }
            
            // 最多的切法就是每个字符都分开
            int min = i;
            
            for (int j = i; j > 0; j--) {
                if (isPalindrome(s.substring(j, i + 1))) {
                    min = Math.min(min, c[j - 1] + 1);
                }
            }
            
            c[i] = min;
        }
        
        return c[n - 1];
    }
    
    static boolean isPalindrome(String s) {
        if (s == null || s.length() == 0) {
            return false;
        }
        
        int i = 0;
        int j = s.length() - 1;
        
        while (i < j) {
            if (s.charAt(i++) != s.charAt(j--)) {
                return false;
            }
        }
        
        return true;
    }

}
