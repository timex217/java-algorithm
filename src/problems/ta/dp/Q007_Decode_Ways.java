package problems.ta.dp;

/**
 * 假设字母和数字的对应码如下
 * A: 1 B: 2 ... Z: 26, 问一串数字对应的字母表达方式有多少，比如 12 对应 AB，也可以是 L
 * 
 * 两个数字一起看，从11到26除去10，20的数字是: dp[i] = dp[i - 1] + dp[i - 2]
 * (比如当前两位是26，如果我们认为当前的为6那么它对应的个数是和dp[i - 1]也就是2那位对应的是一样的，如果认为当前是26那么它对应的个数是和dp[i - 2]那位一样
 * 即新加入的数字（一位也好，两位也好）对之前的排列组合数不产生影响)
 * 
 * 对于10和20，要单独处理，因为编码里没有0，所以要两位一起考虑: dp[i] = dp[i - 2]
 * 
 * 对于非法情况，00 30 40 50 60 70 80 90, 程序直接返回0，因为无法decode
 * 
 * 其他情况, dp[i] = dp[i - 1]
 * 
 */

import java.util.*;

public class Q007_Decode_Ways {
    
    public static void main(String[] args) {
        int[] code = {1, 2, 3, 4, 2, 0};
        int res = getDecodeWays(code);
        System.out.println(res);
    }
    
    static int getDecodeWays(int[] code) {
        if (code == null || code.length == 0 || code[0] == 0) {
            return 0;
        }
        
        // d(i) represents the number of ways to decode
        // we look at two digits, there are multiple scenarios:
        //
        // case 1: 11, 12, 13, 14, 15, 16, 17, 18, 19, 21, 22, 23, 24, 25, 26
        // d(i) = d(i-1) + d(i-2)
        //
        // case 2: 10, 20
        // d(i) = d(i-2)
        //
        // case 3: 00, 30, 40, 50, 60, 70, 80, 90
        // illegal, return 0, can not decode
        //
        // case 4: any other combination
        // d(i) = d(i-1)
        //
        // base case
        // d(0) = 1, d(1) = 1
        
        int n = code.length;
        int[] d = new int[n + 1];
        d[0] = 1;
        d[1] = 1;
        
        for (int i = 1; i < n; i++) {
            int j = i + 1;
            int prev = code[i - 1];
            int curr = code[i];
            
            // illegal
            if (curr == 0 && (prev == 0 || prev > 2)) {
                return 0;
            }
            
            // 11 12 13 14 15 16, 21 22 23 24 25 26
            else if (curr >= 1 && curr <= 6 && (prev == 1 || prev == 2)) {
                d[j] = d[j - 1] + d[j - 2];
            }
            
            // 17 18 19
            else if (curr >= 7 && curr <= 9 && prev == 1) {
                d[j] = d[j - 1] + d[j - 2];
            }
            
            // 10 20
            else if (curr == 0) {
                d[j] = d[j - 2];
            }
            
            // other case
            else {
                d[j] = d[j - 1];
            }
        }
        
        return d[n];
    }

}
