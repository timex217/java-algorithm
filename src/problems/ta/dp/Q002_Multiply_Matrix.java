package problems.ta.dp;

/**
 * 矩阵相乘，关键是理解相乘的cost怎么计算以及如何动态规划
 */

public class Q002_Multiply_Matrix {
	
	public static void main(String[] args) {
		// 9 matrix
		int[] p = {1, 2, 3, 4, 5, 6};
		int cost = getMultiplyMinCost(p, 5);
		System.out.println(cost);
	}
	
	static int getMultiplyMinCost(int[] p, int n) {
		if (p == null || n < 2) {
			return 0;
		}
		
		// m(i, j) represents the minimum multiply cost of (Ai...Ak) x (Ak+1...Aj), (0 <= i, j < n)
		// m(i, j) = {
		//   0, (i = j) 只有一个矩阵，不需要相乘
		//   min{ m(i, k) + m(k + 1, j) + pi x p(k+1) x p(j+1) }  i <= k < j
		// }
		
		int[][] m = new int[n][n];
		
		for (int i = n - 1; i >= 0; i--) {
			for (int j = i + 1; j < n; j++) {
				int min = Integer.MAX_VALUE;
				
				for (int k = i; k < j; k++) {
					min = Math.min(min, m[i][k] + m[k + 1][j] + p[i] * p[k + 1] * p[j + 1]);
				}
				
				if (min != Integer.MAX_VALUE) {
					m[i][j] = min;
				}
			}
		}
		
		return m[0][n-1];
	}

}
