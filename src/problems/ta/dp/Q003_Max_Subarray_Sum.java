package problems.ta.dp;

/**
 * 最大连续子序列和
 * 
 * 比如输入是­ {-2, 11, -4, 13, -5, 2} 输出是 11 - 4 + 13 = 20
 */

public class Q003_Max_Subarray_Sum {
	
	public static void main(String[] args) {
		int[] a = {-2, 11, -4, 13, -5, 2};
		int max = maxSubarraySum(a);
		System.out.println(max);
	}
	
	static int maxSubarraySum(int[] a) {
		if (a == null || a.length == 0) {
			return Integer.MIN_VALUE;
		}
		
		int n = a.length;
		
		// s(i) represents the max subsequence sum till a[i]
		// return max of s(i)
		int[] s = new int[n];
		s[0] = a[0];
		int max = s[0];
		
		for (int i = 1; i < n; i++) {
			s[i] = Math.max(a[i], a[i] + s[i - 1]);
			max = Math.max(max, s[i]);
		}
		
		return max;
	}
}
