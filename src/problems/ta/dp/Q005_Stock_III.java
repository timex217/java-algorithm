package problems.ta.dp;

/**
 * 买股票问题，一个一维数组，每个数组元素代表股票在一天价格，如果最多买卖两次（最多2买2卖）,假设买之前必须卖光，问股票的最大收益
 */

public class Q005_Stock_III {
    
    public static void main(String[] args) {
        int[] prices = {3, 5, 1, 4, 9, 8, 2, 7, 10, 6};
        int res = maxProfit(prices);
        System.out.println(res);
    }
    
    static int maxProfit(int[] prices) {
        if (prices == null || prices.length == 0) {
            return 0;
        }
        
        int n = prices.length;
        
        // p(i) represents the max profit till the i-th day
        // f represents the max profit before the j-th day
        // g represents the max profit after the j-th day
        // p(i) = max { f + g }, 0 <= j <= i
        
        int[] p = new int[n];
        
        for (int i = 0; i < n; i++) {
            int max = 0;
            
            for (int j = 0; j <= i; j++) {
                int f = helper(prices, 0, j);
                int g = helper(prices, j, i);
                max = Math.max(max, f + g);
            }
            
            p[i] = max;
        }
        
        return p[n - 1];
    }
    
    static int helper(int[] prices, int start, int end) {
        if (prices == null || prices.length == 0 || start >= end) {
            return 0;
        }
        
        int max = 0;
        int min_index = start;
        
        for (int i = start; i <= end; i++) {
            if (prices[i] < prices[min_index]) {
                min_index = i;
            }
            
            int profit = prices[i] - prices[min_index];
            max = Math.max(max, profit);
        }
        
        return max;
    }

}
