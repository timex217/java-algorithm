package problems.ta.dp;

public class Q012_Longest_Common_Subsequence {
	
	public static void main(String[] args) {
		String s1 = "ABCBDAB";
		String s2 = "BDCABA";
		
		String res = lcs(s1, s2);
		System.out.println(res);
	}
	
	static String lcs(String s1, String s2) {
		if (s1 == null || s2 == null) {
			return null;
		}
		
		int n1 = s1.length();
		int n2 = s2.length();
		
		// l(i, j) represents the longest common subsequence of s1[0...i] and s2[0...j]
		// l(i, j) = 0, (i = 0 or j = 0)
		// l(i, j) = l(i-1, j-1) + 1, (s1[i] = s2[j])
		// l(i, j) = max{l(i-1, j), l(i, j-1)}, (s1[i] != s2[j])
		int[][] l = new int[n1 + 1][n2 + 1];
		
		for (int i = 0; i <= n1; i++) {
			for (int j = 0; j <= n2; j++) {
				if (i == 0 || j == 0) {
					l[i][j] = 0;
				}
				else {
					if (s1.charAt(i-1) == s2.charAt(j-1)) {
						l[i][j] = l[i-1][j-1] + 1;
					}
					else {
						l[i][j] = Math.max(l[i-1][j], l[i][j-1]);
					}
				}
			}
		}
		
		// trace back the string
		int i = n1;
		int j = n2;
		String s = "";
		
		while (i > 0 && j > 0) {
			if (s1.charAt(i-1) == s2.charAt(j-1)) {
				s = s1.charAt(i-1) + s;
				i--;
				j--;
			}
			else if (l[i-1][j] >= l[i][j-1]) {
				i--;
			}
			else {
				j--;
			}
		}
		
		return s;
	}

}
