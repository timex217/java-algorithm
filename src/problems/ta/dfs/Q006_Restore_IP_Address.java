package problems.ta.dfs;

import java.util.*;

public class Q006_Restore_IP_Address {

	public static void main(String[] args) {
		String s = "127001";
		ArrayList<String> res = new ArrayList<String>();
		dfs(s, 0, 0, "", res);
		
		System.out.println(res.toString());
	}
	
	public static void dfs(String s, int start, int seg, String sol, ArrayList<String> res) {
		// 遍历完字符串并且当前是ip的最后一段，结束
		if (start == s.length() && seg == 4) {
			res.add(sol);
			return;
		}
		
		if (s.length() - start > (4 - seg) * 3) {
			return; // 剩下的数字太多
		}
		
		if (s.length() - start < (4 - seg)) {
			return; // 剩下的数字太少
		}
		
		for (int i = start; i < start + 3 && i < s.length(); i++) {
		    String sub = s.substring(start, i + 1);
		    
			int num = Integer.parseInt(sub);
			
			if (s.charAt(start) == '0' && i > start) {
                break; // 不允许以0开头的多位数这种情况出现，例如00, 01, 001等等
            }
			
			if (num < 256) {
				dfs(s, i + 1, seg + 1, sol + sub + ((seg == 3) ? "" : "."), res);
			}
		}
	}
	
	public static ArrayList<String> restoreIPAddress(String s) {
		ArrayList<String> res = new ArrayList<String>();
		
		if (s == null || s.length() == 0) {
			return res;
		}
		
		helper(s, 0, 1, "", res);
		return res;
	}
	
	private static void helper(String s, int index, int segment, String ip, ArrayList<String> res) {
		if (index >= s.length()) {
			return;
		}
		
		if (segment == 4) {
			String str = s.substring(index);
			if (isValid(str)) {
				res.add(ip + "." + str);
			}
			return;
		}
		
		for (int i = 1; i < 4 && (i + index <= s.length()); i++) {
			String str = s.substring(index, index + i);
			if (isValid(str)) {
				if (segment == 1) {
					helper(s, index + i, segment + 1, str, res);
				}
				else {
					helper(s, index + i, segment + 1, ip + "." + str, res);
				}
			}
		}
	}
	
	private static boolean isValid(String s) {
		if (s == null || s.length() > 3) {
			return false;
		}
		
		if (s.charAt(0) == '0' && s.length() > 1) {
			return false;
		}
		
		int num = Integer.parseInt(s);
		if (num < 0 || num > 255) {
			return false;
		}
		
		return true;
	}

}
