package problems.ta.dfs;

/**
 * 给定一个电话键盘，给定一串数字返回该数字对应的键盘上所有字母组合
 * 
 * 例如 Input: "23"
 * Output: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"]
 */

import java.util.*;

public class Q010_Telephone_Keyboard {
	
	public static void main(String[] args) {
		String[] keyboard = {" ", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
		ArrayList<String> res = new ArrayList<String>();
		dfs(keyboard, "234", 0, "", res);
	}
	
	static void dfs(String[] keyboard, String input, int start, String sol, ArrayList<String> res) {
		if (sol.length() == input.length()) {
			System.out.println(sol);
			res.add(sol);
			return;
		}
		
		int index = input.charAt(start) - '0';
		String key = keyboard[index];
		
		// 对当前层的所有节点进行dfs，递归到下一层
		for (int i = 0; i < key.length(); i++) {
			dfs(keyboard, input, start + 1, sol + key.charAt(i), res);
		}
	}

}
