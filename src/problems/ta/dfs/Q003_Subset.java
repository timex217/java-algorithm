package problems.ta.dfs;

import java.util.*;

public class Q003_Subset {
	
	public static void main(String[] args) {
		int[] S = {1, 2, 3};
		ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
		dfs(S, 0, new ArrayList<Integer>(), result);
		
		// output the result
		for (int i = 0; i < result.size(); i++) {
		    ArrayList<Integer> sol = result.get(i);
		    System.out.println(sol.toString());
		}
	}
	
	// --------------
	//  DFS
	// --------------
	
	public static void dfs(int[] S, int index, ArrayList<Integer> sol, ArrayList<ArrayList<Integer>> res) {
		if (index == S.length) {
			res.add(new ArrayList<Integer>(sol));
			return;
		}
		
		// 当前层的所有可能性
		
		// 1. 不包含这个数
		dfs(S, index + 1, sol, res);
		
		// 2. 包含这个数
		// 处理当前解
		sol.add(S[index]);
		// 递归下一层
		dfs(S, index + 1, sol, res);
		// 恢复当前解
		sol.remove(sol.size() - 1);
	}
	
	public static ArrayList<ArrayList<Integer>> subsets(int[] S) {
		if (S == null) {
			return null;
		}
		
		return helper(S, 0);
	}
	
	/**
	 * Recursion
	 */
	private static ArrayList<ArrayList<Integer>> helper(int[] S, int index) {
		if (index == S.length) {
			ArrayList<ArrayList<Integer>> res = new ArrayList<ArrayList<Integer>>();
			// at last, we add an empty set
			ArrayList<Integer> elem = new ArrayList<Integer>();
			res.add(elem);
			return res;
		}
		
		ArrayList<ArrayList<Integer>> res = helper(S, index + 1);
		
		int size = res.size();
		
		for (int i = 0; i < size; i++) {
			// 基于目前已经存在的子集，创建新的子集，然后加入当前元素S[i]
			ArrayList<Integer> elem = new ArrayList<Integer>(res.get(i));
			elem.add(S[index]);
			res.add(elem);
		}
		
		return res;
	}
	
	/**
	 * Non-recursion
	 */
	private static ArrayList<ArrayList<Integer>> subsets2(int[] S) {
		ArrayList<ArrayList<Integer>> res = new ArrayList<ArrayList<Integer>>();
		// add an empty sub set
		res.add(new ArrayList<Integer>());
		
		if (S == null || S.length == 0) {
			return res;
		}
		
		for (int i = 0; i < S.length; i++) {
			int size = res.size();
			
			for (int j = 0; j < size; j++) {
				// 基于目前已经存在的子集，创建新的子集，然后加入当前元素S[i]
				ArrayList<Integer> elem = new ArrayList<Integer>(res.get(j));
				elem.add(S[i]);
				res.add(elem);
			}
		}
		
		return res;
	}

}
