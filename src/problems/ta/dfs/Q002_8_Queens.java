package problems.ta.dfs;

import java.util.*;

public class Q002_8_Queens {
	
	public static void main(String[] args) {
		int[] cols = new int[8];
		dfs(8, 0, cols);
		//solve(8, 0, cols);
	}
	
	public static void dfs(int n, int row, int[] cols) {
		// 减枝或终止
		if (n == row) {
			System.out.println(Arrays.toString(cols));
			return;
		}
		
		// 对当前行的每一列我们都要测试一遍
		for (int j = 0; j < n; j++) {
			// 处理当前解，在j列放置皇后
			cols[row] = j;
			
			if (check(row, j, cols)) {
				dfs(n, row + 1, cols);
			}
			
			// 递归完毕或不满足条件，恢复当前解
			cols[row] = 0;
		}
	}
	
	public static void solve(int n, int row, int[] cols) {
		// if we have checked all the rows
		// print the result
		if (row == n) {
			System.out.println(Arrays.toString(cols));
			return;
		}
		
		for (int j = 0; j < n; j++) {
			// for each column in the row
			// we try different position
			// if it passes the check for current row
			if (check(row, j, cols)) {
				// put the queen at j-th column in row
				cols[row] = j;
				
				// then we solve the next row
				solve(n, row + 1, cols);
			}
		}
	}
	
	public static boolean check(int row, int j, int[] cols) {
		for (int i = 0; i < row; i++) {
			// same column, failed
			if (cols[i] == j) return false;
			// same diagnol, failed
			if (Math.abs(row - i) == Math.abs(j - cols[i])) return false;
		}
		
		return true;
	}

}
