/**
 * 假设每个位置的数字只能用一次呢, 而且允许有重复的数字在集合中
 */

package problems.ta.dfs;

import java.util.*;

public class Q005B_Combination_Sum_No_Repeat_Number {
	
	public static void main(String[] args) {
		int[] nums = {10, 1, 2, 7, 6, 1, 5};
		Arrays.sort(nums);
		
		//System.out.println(Arrays.toString(nums));
		
		ArrayList<ArrayList<Integer>> res = new ArrayList<ArrayList<Integer>>();
		dfs(nums, 8, 0, new ArrayList<Integer>(), res);
	}
	
	public static void dfs(int[] nums, int target, int start, ArrayList<Integer> sol, ArrayList<ArrayList<Integer>> res) {
		if (target < 0) {
			return;
		}
		
		if (target == 0) {
			System.out.println(sol.toString());
			res.add(sol);
			return;
		}
		
		for (int i = start; i < nums.length; i++) {
			if (i > 0 && nums[i - 1] == nums[i]) {
				continue; // 保证在同一层里面相同的数只能出现一次
			}
			
			sol.add(nums[i]);
			dfs(nums, target - nums[i], i + 1, sol, res);
			sol.remove(sol.size() - 1);
		}
	}

}
