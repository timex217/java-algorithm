/**
 * DFS解题思路
 * 
 * dfs() {
 *     1. 判断是否出解
 *     2. 判断是否可以减枝或终止，也就是不满足要求的搜索提前终止
 *     3. 递归主体
 *     
 *     处理当前解
 *         递归函数（当前层或者下一层的所有可能节点)
 *     恢复当前解
 *     
 *     1, 2, 3的顺序可能因题而变
 * }
 * 
 * 深度优先遍历二叉树 DFS
 */

package problems.ta.dfs;

import java.util.*;

public class Q001A_Binary_Tree_Traversal {

	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) { val = x; }
	}
	
	private static class Node {
		TreeNode treeNode;
		boolean isFirst;
		Node(TreeNode node, boolean first) { 
			treeNode = node; 
			isFirst = first;
		}
	}
	
	// ----------
	//  DFS
	// ----------
	public static void dfs(TreeNode root) {
		// 减枝或终止
		if (root == null) {
			return;
		}
		
		// 处理当前解
		visit(root);
		
		// dfs 当前点的所有子节点
		dfs(root.left);
		dfs(root.right);
	}
	
	
	/**
	 * Preorder
	 */
	private static void preorder(TreeNode root) {
		Stack<TreeNode> stack = new Stack<TreeNode>();
		
		TreeNode current = root;
		
		while (!stack.isEmpty() || current != null) {
			// 一路向左
			while (current != null) {
				// visit it
				visit(current);
				// stack it
				stack.push(current);
				// continue to the left
				current = current.left;
			}
			
			if (!stack.isEmpty()) {
				current = stack.pop();
				// to the right
				current = current.right;
			}
		}
	}
	
	/**
	 * Inorder
	 */
	private static void inorder(TreeNode root) {
		Stack<TreeNode> stack = new Stack<TreeNode>();
		
		TreeNode current = root;
		
		while (!stack.isEmpty() && current != null) {
			// 一路向左
			while (current != null) {
				// stack it
				stack.push(current);
				current = current.left;
			}
			
			if (!stack.isEmpty()) {
				current = stack.pop();
				// visit it
				visit(current);
				// to the right
				current = current.right;
			}
		}
	}
	
	/**
	 * Postorder
	 */
	private static void postorder(TreeNode root) {
		Stack<Node> stack = new Stack<Node>();
		
		TreeNode current = root;
		Node temp;
		
		while (!stack.isEmpty() || current != null) {
			// 一路向左
			if (current != null) {
				temp = new Node(current, true);
				stack.push(temp);
				current = current.left;
			}
			
			if (!stack.isEmpty()) {
				temp = stack.pop();
				
				if (temp.isFirst) {
					temp.isFirst = false;
					stack.push(temp);
					current = temp.treeNode.right;
				}
				else {
					visit(temp.treeNode);
					current = null;
				}
			}
		}
	}
	
	private static void visit(TreeNode node) {
		
	}
	
	public static void main(String[] args) {
		
	}

}
