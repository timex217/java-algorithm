package problems.ta.dfs;

import java.util.*;

public class Q007_Parenthesis {

	public static void main(String[] args) {
		dfs(3, 0, 0, "");
	}

	static void dfs(int n, int open, int close, String sol) {
		if (close == n) {
			System.out.println(sol);
			return;
		}

		if (open > close) {
			dfs(n, open, close + 1, sol + ")");
		}

		if (open < n) {
			dfs(n, open + 1, close, sol + "(");
		}
	}
}
