package problems.ta.dfs;

import java.util.*;

public class Q001B_K_Tree_Traversal {
	
	private static class TreeNode {
		int val;
		TreeNode[] nodes;
		TreeNode(int x) { val = x; }
	}
	
	// ---------------
	//  DFS
	// ---------------
	public static void dfs(TreeNode root) {
		if (root == null) {
			return;
		}
		
		visit(root);
		
		for (int i = 0; i < root.nodes.length; i++) {
			TreeNode node = root.nodes[i];
			dfs(node);
		}
	}
	
	private static void traverse(TreeNode root) {
		if (root == null) {
			return;
		}
		
		Stack<TreeNode> stack = new Stack<TreeNode>();
		stack.push(root);
		
		while (!stack.isEmpty()) {
			TreeNode current = stack.pop();
			
			// visit it
			visit(current);
			
			for (int i = 0; i < current.nodes.length; i++) {
				TreeNode child = current.nodes[i];
				stack.push(child);
			}
		}
	}
	
	private static void visit(TreeNode node) {
		
	}
	
	public static void main(String[] args) {
		
	}

}
