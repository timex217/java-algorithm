package problems.ta.dfs;

import java.util.*;

public class Q004_Combinations {

	public static void main(String[] args) {
		int[] S = {1, 2, 3, 4};
		combine(S, 2);
	}
	
	public static ArrayList<ArrayList<Integer>> combine(int[] S, int k) {
		ArrayList<ArrayList<Integer>> res = new ArrayList<ArrayList<Integer>>();
		if (S == null || S.length == 0 || S.length < k) {
			return res;
		}
		
		dfs(S, k, 0, new ArrayList<Integer>(), res);
		
		return res;
	}
	
	private static void dfs(int[] S, int k, int start, ArrayList<Integer> sol, ArrayList<ArrayList<Integer>> res) {
		if (sol.size() == k) {
			System.out.println(sol.toString());
			res.add(new ArrayList<Integer>(sol));
			return;
		}
		
		for (int i = start; i < S.length; i++) {
			sol.add(S[i]);
			dfs(S, k, i + 1, sol, res);
			sol.remove(sol.size() - 1);
		}
	}

}
