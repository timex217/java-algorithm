package problems.ta.dfs;

import java.util.*;

public class Q005A_Combination_Sum {
	
	public static void main(String[] args) {
		int[] nums = {2, 3, 6, 7};
		ArrayList<ArrayList<Integer>> res = new ArrayList<ArrayList<Integer>>();
		dfs(nums, 7, 0, new ArrayList<Integer>(), res);
	}
	
	public static void dfs(int[] nums, int target, int start, ArrayList<Integer> sol, ArrayList<ArrayList<Integer>> res) {
		if (target < 0) {
			return;
		}
		
		if (target == 0) {
			System.out.println(sol.toString());
			res.add(sol);
			return;
		}
		
		// all possibilities
		for (int i = start; i < nums.length; i++) {
			sol.add(nums[i]);
			dfs(nums, target - nums[i], i, sol, res);
			sol.remove(sol.size() - 1);
		}
	}
	
	public static ArrayList<ArrayList<Integer>> combinationSum(int[] candidates, int target) {
		ArrayList<ArrayList<Integer>> res = new ArrayList<ArrayList<Integer>>();
		
		if (candidates == null || candidates.length == 0) {
			return res;
		}
		
		Arrays.sort(candidates);
		
		helper(candidates, target, 0, new ArrayList<Integer>(), res);
		
		return res;
	}
	
	private static void helper(int[] candidates, int target, int start, ArrayList<Integer> elem, ArrayList<ArrayList<Integer>> res) {
		if (target == 0) {
			res.add(new ArrayList<Integer>(elem));
			return;
		}
		
		if (target < 0 || start >= candidates.length) {
			return;
		}
		
		for (int i = start; i < candidates.length; i++) {
			/* do not allow duplicates
			if (i > 0 && candiates[i] == candidates[i - 1]) {
				continue;
			}
			*/
			elem.add(candidates[i]);
			helper(candidates, target - candidates[i], i, elem, res);
			elem.remove(elem.size() - 1);
		}
	}

}
