package problems.ta.dfs;

/** 
 * 给定一个字母组成的二维矩阵，查找一个word是不是在这个矩阵中，字母的连接方式是水平或者垂直相邻的，比如:
 * ABCE
 * SFCS
 * ADEE
 * 
 * 输入: ABCCED, 返回: true
 * 输入: ABD,    返回: false
 */

public class Q009_Word_Search {
	
	public static void main(String[] args) {
		char[][] board = {
		    {'A', 'B', 'C', 'E'},
		    {'S', 'F', 'C', 'S'},
		    {'A', 'D', 'E', 'E'}
		};
		
		String word = "ABCCED";
		
		boolean found = search(board, word);
		System.out.println(found);
	}
	
	public static boolean search(char[][] board, String word) {
		if (word == null || word.length() == 0) {
			return false;
		}
		
		if (board == null || board.length == 0 || board[0].length == 0) {
			return false;
		}
		
		boolean[][] used = new boolean[board.length][board[0].length];
		
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[0].length; j++) {
				if (dfs(board, word, 0, i, j, used)) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	private static boolean dfs(char[][] board, String word, int index, int i, int j, boolean[][] used) {
		if (index == word.length()) {
			return true;
		}
		
		// 超出边界
		if (i < 0 || j < 0 || i >= board.length || j >= board[0].length) {
			return false;
		}
		
		// 已经使用过
		if (used[i][j]) {
			return false;
		}
		
		// 不相等
		if (board[i][j] != word.charAt(index)) {
			return false;
		}
		
		// 处理当前解
		used[i][j] = true;
		
		// 四个方向dfs
		boolean res = dfs(board, word, index + 1, i - 1, j, used) ||
				      dfs(board, word, index + 1, i + 1, j, used) ||
				      dfs(board, word, index + 1, i, j - 1, used) ||
				      dfs(board, word, index + 1, i, j + 1, used);
		
		// 恢复当前解
		used[i][j] = false;
		
		return res;
	}

}
