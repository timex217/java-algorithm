package problems.ta.linkedlist;

public class Q003_Delete_Last_Kth_Node {
	
	private static class ListNode {
		int val;
		ListNode next;
		
		ListNode(int x) {
			this.val = x;
			this.next = null;
		}
	}
	
	public static ListNode remove(ListNode head, int k) {
		// sanity check
		if (head == null || k < 1) {
			return head;
		}
		
		ListNode prev = null;
		ListNode slow = head;
		ListNode fast = head;
		
		while (k > 0 && fast != null) {
			fast = fast.next;
			k--;
		}
		
		// list length is shorter than k
		if (k > 0) {
			return head;
		}
		
		// remove the head
		if (fast == null) {
			head = head.next;
			return head;
		}
		
		while (fast != null) {
			prev = slow;
			slow = slow.next;
			fast = fast.next;
		}
		
		prev.next = slow.next;
		
		return head;
	}
	
	public static void main(String[] args) {
		
	}

}
