package problems.ta.linkedlist;

/**
 * 证明：如果有一个快指针（一次走两步），一个慢指针（一次走一步），当快指针和慢指针相遇的时候，相遇的位置离环口的距离等于环口到头节点的距离
 * 
 * |---- h ----|-------------->
 * o - o - o - # - o - o
 *            /         \
 *      d    o           o    s
 *            \         /
 *             # - o - o
 *             |-------------->
 * 设头节点到环口的距离为 h, 环口到相遇点的位置为 s, 相遇点离环口的距离为 d
 * 慢指针走了 h + s 的距离，那么快指针走了 2 x (h + s) 的距离
 * 假设快指针在环里绕了 n 圈，那么快指针所走的距离也等于 h + n x r + s，其中 r = s + d
 * 即：2 x (h + s) = h + n x r + s
 * 消去 s, 两边得：h = (n - 1) x r + d
 * 也就是说，头离环口的距离等于在环里走 n - 1 圈后再走 d 步!
 * 所以，当相遇的时候，慢指针从头开始走，快指针从相遇点开始一步步走，最后相遇的位置就是环口.
 */

public class Q004B_Find_Ring_Entrance {
	
	private static class ListNode {
		int val;
		ListNode next;
		
		ListNode(int x) {
			this.val = x;
			this.next = null;
		}
	}
	
	public static ListNode findRingEntrance(ListNode head) {
		if (head == null) {
			return null;
		}
		
		ListNode slow = head;
		ListNode fast = head;
		
		while (fast != null) {
			slow = slow.next;
			fast = fast.next == null ? null : fast.next.next;
			
			// find the ring
			if (fast == slow) {
				break;
			}
		}
		
		// there's no ring
		if (fast == null) {
			return null;
		}
		
		// search for the ring entrance
		slow = head;
		while (slow != fast) {
			slow = slow.next;
			fast = fast.next;
		}
		
		// return the entrance
		return slow;
	}
	
	public static void main(String[] args) {
		
	}

}
