package problems.ta.linkedlist;

public class Q007A_Detect_Cross {

	private static class ListNode {
		int val;
		ListNode next;
		
		ListNode(int x) {
			this.val = x;
			this.next = null;
		}
	}
	
	// method 0: Ring
	public static boolean detectCross(ListNode l1, ListNode l2) {
		if (l1 == null || l2 == null) {
			return false;
		}
		
		// step 1. circle l1
		ListNode n1 = l1;
		while (n1.next != null) {
			n1 = n1.next;
		}
		n1.next = l1;
		
		// step 2. detect ring in l2
		boolean result = detectRing(l2);
		
		// step 3. open the ring
		n1.next = null;
		
		return result;
	}
	
	public static boolean detectRing(ListNode head) {
		if (head == null) {
			return false;
		}
		
		ListNode slow = head;
		ListNode fast = head;
		
		while (fast != null) {
			slow = slow.next;
			fast = (fast.next == null) ? null : fast.next.next;
			
			if (slow == fast) {
				return true;
			}
		}
		
		return false;
	}
	
	// method 1: go together
	public static boolean detectCross1(ListNode l1, ListNode l2) {
		if (l1 == null || l2 == null) {
			return false;
		}
		
		int len1 = getLength(l1);
		int len2 = getLength(l2);
		int diff = Math.abs(len1 - len2);
		
		ListNode n1 = l1;
		ListNode n2 = l2;
		
		if (len1 > len2) {
			while (diff-- > 0) {
				n1 = n1.next;
			}
		}
		else {
			while (diff-- > 0) {
				n2 = n2.next;
			}
		}
		
		// go together
		while (n1 != null && n2 != null) {
			if (n1 == n2) {
				return true;
			}
			
			n1 = n1.next;
			n2 = n2.next;
		}
		
		return false;
	}
	
	public static int getLength(ListNode head) {
		if (head == null) {
			return 0;
		}
		
		return 1 + getLength(head.next);
	}
	
	public static void main(String[] args) {
		
	}

}
