package problems.ta.linkedlist;

/**
 * Given a linked list, swap every two adjacent nodes and return its head.
 * 
 * Given 1->2->3->4, you should return the list as 2->1->4->3.
 * 
 * Your algorithm should use only constant space. You may not modify the values in the list, only nodes itself can be changed.
 */

public class Q002B_Reverse_Pair_Recursion {
	
	private static class ListNode {
		int val;
		ListNode next;
		
		ListNode(int x) {
			this.val = x;
			this.next = null;
		}
	}
	
	public static ListNode reversePair(ListNode head) {
		if (head == null || head.next == null) {
			return head;
		}

		ListNode new_head = head.next;
		ListNode next_head = new_head.next;

		new_head.next = head;
		head.next = reversePair(next_head);

		return new_head;
	}
	
	public static void main(String[] args) {
		
	}

}
