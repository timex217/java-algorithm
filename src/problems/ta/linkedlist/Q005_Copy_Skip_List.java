package problems.ta.linkedlist;

import java.util.*;

public class Q005_Copy_Skip_List {
	
	private static class ListNode {
		int val;
		ListNode next;
		ListNode forward;
		
		ListNode(int x) {
			this.val = x;
			this.next = null;
			this.forward = null;
		}
	}
	
	// method 1: use a hashmap and iterate twice
	public static ListNode copySkipList(ListNode head) {
		if (head == null) {
			return null;
		}
		
		// use a hashmap to store the copied nodes
		HashMap<ListNode, ListNode> map = new HashMap<ListNode, ListNode>();
		
		ListNode node = head;
		
		// step 1. copy nodes and build hash map
		while (node != null) {
			ListNode copy = new ListNode(node.val);
			map.put(node, copy);
			node = node.next;
		}
		
		// step 2. rebuild the relationship
		node = head;
		while (node != null) {
			ListNode copy = map.get(node);
			copy.next = map.get(node.next);
			copy.forward = map.get(node.forward);
		}
		
		return map.get(head);
	}
	
	public static void main(String[] args) {
		
	}

}
