package problems.ta.linkedlist;

import java.util.*;

public class Q008_Reorder_List {
	
	private static class ListNode {
		int val;
		ListNode next;
		ListNode(int x) {
			this.val = x;
			this.next = null;
		}
	}
	
	public static void reorderList(ListNode head) {
        if (head == null) {
            return;
        }
        
        ListNode prev = null;
        ListNode slow = head;
        ListNode fast = head;
        
        while (fast != null) {
            prev = slow;
            slow = slow.next;
            fast = (fast.next == null) ? null : fast.next.next;
        }
        
        // cut it to two linked lists
        ListNode l1 = head;
        ListNode l2 = reverse(slow);
        prev.next = null;
        
        // insert l2 to l1 one by one
        while (l1 != null && l2 != null) {
            ListNode n1 = l1.next;
            ListNode n2 = l2.next;
            
            l1.next = l2;
            l2.next = n1;
            
            l1 = n1;
            l2 = n2;
        }
    }
    
    private static ListNode reverse(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        
        ListNode next = head.next;
        ListNode new_head = reverse(next);
        
        next.next = head;
        head.next = null;
        
        return new_head;
    }
	
	public static void main(String[] args) {
		
	}

}
