package problems.ta.linkedlist;

public class Q002A_Reverse_Pair_Non_Recursive {
	
	private static class ListNode {
		int val;
		ListNode next;
		
		ListNode(int x) {
			this.val = x;
			this.next = null;
		}
	}
	
	public static ListNode reversePair(ListNode head) {
		if (head == null || head.next == null) {
			return head;
		}
		
		ListNode new_head = head.next;
		
		ListNode curr = head;
		ListNode next = null;
		ListNode tmp = null;
		
		while (curr != null && curr.next != null) {
			next = curr.next;
			tmp = next.next;
			
			next.next = curr;
			curr.next = (tmp != null && tmp.next != null) ? tmp.next : tmp;
			curr = tmp;
		}
		
		return new_head;
	}
	
	public static void main(String[] args) {
		
	}

}
