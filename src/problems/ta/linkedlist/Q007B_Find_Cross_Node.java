package problems.ta.linkedlist;

public class Q007B_Find_Cross_Node {
	
	private static class ListNode {
		int val;
		ListNode next;
		
		ListNode(int x) {
			this.val = x;
			this.next = null;
		}
	}
	
	// method 0: ring
	public static ListNode findCrossNode(ListNode l1, ListNode l2) {
		if (l1 == null || l2 == null) {
			return null;
		}
		
		// step 1. circle l1
		ListNode n1 = l1;
		while (n1.next != null) {
			n1 = n1.next;
		}
		n1.next = l1;
		
		// step 2. find the ring in l2
		ListNode result = findRingEntrance(l2);
		
		// step 3. open the ring
		n1.next = null;
		
		return result;
	}
	
	public static ListNode findRingEntrance(ListNode head) {
		if (head == null) {
			return null;
		}
		
		ListNode slow = head;
		ListNode fast = head;
		
		// search for the ring
		while (fast != null) {
			slow = slow.next;
			fast = (fast.next == null) ? null : fast.next.next;
			
			if (fast == slow) {
				break;
			}
		}
		
		// no ring is found
		if (fast == null) {
			return null;
		}
		
		slow = head;
		while (fast != slow) {
			slow = slow.next;
			fast = fast.next;
		}
		
		return slow;
	}
	
	// method 1: go together
	public static ListNode findCrossNode1(ListNode l1, ListNode l2) {
		if (l1 == null || l2 == null) {
			return null;
		}
		
		int len1 = getLength(l1);
		int len2 = getLength(l2);
		int diff = Math.abs(len1 - len2);
		
		ListNode n1 = l1;
		ListNode n2 = l2;
		
		if (len1 > len2) {
			while (diff-- > 0) {
				n1 = n1.next;
			}
		}
		else {
			while (diff-- > 0) {
				n2 = n2.next;
			}
		}
		
		// go together
		while (n1 != null && n2 != null) {
			if (n1 == n2) {
				return n1;
			}
			n1 = n1.next;
			n2 = n2.next;
		}
		
		// otherwise, no cross node
		return null;
	}
	
	public static int getLength(ListNode head) {
		if (head == null) {
			return 0;
		}
		return 1 + getLength(head.next);
	}
	
	public static void main(String[] args) {
		
	}

}
