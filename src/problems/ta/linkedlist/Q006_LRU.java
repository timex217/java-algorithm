package problems.ta.linkedlist;

/**
 * Design and implement a data structure for Least Recently Used (LRU) cache. It should support the following operations: get and set.
 * 
 * - get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
 * - set(key, value) - Set or insert the value if the key is not already present. When the cache reached its capacity, 
 * it should invalidate the least recently used item before inserting a new item.
 */

import java.util.*;

public class Q006_LRU {
	
	private static class DoublyListNode {
	    int key;
	    int val;
	    
	    DoublyListNode prev;
	    DoublyListNode next;
	    
	    DoublyListNode(int key, int value) {
	        this.key = key;
	        this.val = value;
	        this.prev = null;
	        this.next = null;
	    }
	}

	private static class LRUCache {
	    
	    private int total;
	    private int capacity;
	    
	    private DoublyListNode head;
	    private DoublyListNode tail;
	    
	    private HashMap<Integer, DoublyListNode> map;
	    
	    public LRUCache(int capacity) 
	    {
	        this.total = 0;
	        this.capacity = capacity;
	        map = new HashMap<Integer, DoublyListNode>();
	    }
	    
	    public int get(int key) 
	    {
	        if (map.containsKey(key)) 
	        {
	            DoublyListNode node = map.get(key);
	            
	            removeNode(node);
	            setHead(node);
	            
	            return node.val;
	        }
	        else 
	        {
	            return -1;
	        }
	    }
	    
	    public void set(int key, int value) 
	    {
	        if (map.containsKey(key)) 
	        {
	            DoublyListNode node = map.get(key);
	            
	            node.val = value;
	            
	            removeNode(node);
	            setHead(node);
	        } 
	        else 
	        {
	            DoublyListNode node = new DoublyListNode(key, value);
	            map.put(key, node);
	            
	            setHead(node);
	            
	            if (total < capacity) 
	            {
	                total++;
	            } 
	            else 
	            {
	                map.remove(tail.key);
	                removeNode(tail);
	            }
	        }
	    }
	    
	    private void removeNode(DoublyListNode node) 
	    {
	        DoublyListNode curr = node;
	        DoublyListNode prev = node.prev;
	        DoublyListNode next = node.next;
	        
	        if (prev == null) 
	        {
	            head = next;
	        }
	        else 
	        {
	            prev.next = next;
	        }
	        
	        if (next == null) 
	        {
	            tail = prev;
	        }
	        else 
	        {
	            next.prev = prev;
	        }
	    }
	    
	    private void setHead(DoublyListNode node) 
	    {
	        node.next = head;
	        node.prev = null;
	        
	        if (head != null) 
	        {
	            head.prev = node;
	        }
	        
	        head = node;
	        
	        if (tail == null) 
	        {
	            tail = node;
	        }
	    }
	}
	
	public static void main(String[] args) {
		
	}

}
