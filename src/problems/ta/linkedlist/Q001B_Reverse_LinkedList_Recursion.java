package problems.ta.linkedlist;

public class Q001B_Reverse_LinkedList_Recursion {
	
	private static class ListNode {
		int val;
		ListNode next;
		
		ListNode(int x) {
			this.val = x;
			this.next = null;
		}
	}
	
	// 递归函数，输入一个头节点，返回颠倒顺序后的新头
	// 无论从哪个节点递归调用，最后返回的都是最后的一个作为新头!
	public static ListNode reverse(ListNode head) {
	    // base case
		if (head == null || head.next == null) {
			return head;
		}
		
		ListNode first = head;
		ListNode rest = head.next;
		
		head = reverse(rest);

		rest.next = first;
		first.next = null;

		return head;
	}
	
	public static void main(String[] args) {
		
	}

}
