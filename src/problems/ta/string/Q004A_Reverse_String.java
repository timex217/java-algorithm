package problems.ta.string;

public class Q004A_Reverse_String {
	
	public static void main(String[] args) {
		String s = "1234567890";
		String result = reverse(s);
		System.out.println(result);
	}
	
	public static String reverse(String s) {
		if (s == null) {
			return null;
		}
		
		char[] chars = s.toCharArray();
		
		int i = 0, j = chars.length - 1;
		
		while (i < j) {
			swap(chars, i++, j--);
		}
		
		return new String(chars);
	}
	
	public static void swap(char[] chars, int i, int j) {
		char tmp = chars[i];
		chars[i] = chars[j];
		chars[j] = tmp;
	}

}
