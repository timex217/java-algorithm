package problems.ta.string;

import java.util.*;

public class Q008_Minimum_Window_Substring {
	
	public static void main(String[] args) {
		String S = "ADOBECODEBANC";
		String T = "ABC";
		String result = minWinSubstr(S, T);
		System.out.println(result);
	}
	
	public static String minWinSubstr(String S, String T) {
		if (S == null || T == null) {
			return null;
		}
		
		// 
		int min = Integer.MAX_VALUE;
		int start = -1;
		int end = -1;
		int count = 0;
		
		// stores the characters we need to find and already found
		HashMap<Character, Integer> needsToFind = new HashMap<Character, Integer>();
		HashMap<Character, Integer> hasFound = new HashMap<Character, Integer>();
		
		// step 1. build the needsToFind map and hasFound map
		for (int i = 0; i < T.length(); i++) {
			char ch = T.charAt(i);
			
			if (!needsToFind.containsKey(ch)) {
				needsToFind.put(ch, 1);
			}
			else {
				needsToFind.put(ch, needsToFind.get(ch) + 1);
			}
			
			hasFound.put(ch, 0);
		}
		
		for (int i = 0, j = 0; j < S.length(); j++) {
			char ch_j = S.charAt(j);
			
			// not what we are looking for
			if (!needsToFind.containsKey(ch_j)) {
				continue;
			}
			
			hasFound.put(ch_j, hasFound.get(ch_j) + 1);
			
			if (hasFound.get(ch_j) <= needsToFind.get(ch_j)) {
				count++;
			}
			
			// we have found all the characters
			if (count == T.length()) {
				
				// advance begin index as far right as possible,
				// stop when it breaks the window contrains
				char ch_i = S.charAt(i);
				
				while (!needsToFind.containsKey(ch_i) || hasFound.get(ch_i) > needsToFind.get(ch_i)) {
					
					if (needsToFind.containsKey(ch_i) && hasFound.get(ch_i) > needsToFind.get(ch_i)) {
						hasFound.put(ch_i, hasFound.get(ch_i) - 1);
					}
					
					ch_i = S.charAt(++i);
				}
				
				int len = j - i + 1;
				if (len < min) {
					start = i;
					end = j;
				}
				
			}
			
		}
		
		if (count == T.length()) {
			return S.substring(start, end + 1);
		}
		else {
			return null;
		}
	}

}
