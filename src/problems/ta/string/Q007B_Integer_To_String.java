package problems.ta.string;

import java.util.*;

public class Q007B_Integer_To_String {
	
	public static void main(String[] args) {
		int n = -1024;
		String result = intToStr(n);
		System.out.println(result);
	}
	
	public static String intToStr(int n) {
		if (n == 0) {
			return "0";
		}
		
		boolean isPositive = n > 0;
		
		n = Math.abs(n);
		
		StringBuilder sb = new StringBuilder();
		
		while (n > 0) {
			int m = n % 10;
			sb.append(m);
			n /= 10;
		}
		
		if (!isPositive) {
			sb.append("-");
		}
		
		return sb.reverse().toString();
	}

}
