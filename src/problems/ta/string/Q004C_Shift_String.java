package problems.ta.string;

public class Q004C_Shift_String {
	
	public static void main(String[] args) {
		String s = "123456789";
		String result = shift(s, 2, true);
		System.out.println(result);
	}
	
	public static String shift(String s, int k, boolean leftOrRight) {
		if (s == null) {
			return s;
		}
		
		char[] chars = s.toCharArray();
		
		int n = chars.length;
		k = k % n;
		
		// step 1. reverse the whole string
		reverse(chars, 0, n - 1);
		
		// step 2. find the pivot to cut the string
		if (leftOrRight) {
			reverse(chars, 0, n - k - 1);
			reverse(chars, n - k, n - 1);
		}
		else {
			reverse(chars, 0, k - 1);
			reverse(chars, k, n - 1);
		}
		
		return new String(chars);
	}
	
	public static void reverse(char[] chars, int start, int end) {
		while (start < end) {
			char tmp = chars[start];
			chars[start] = chars[end];
			chars[end] = tmp;
			
			start++;
			end--;
		}
	}

}
