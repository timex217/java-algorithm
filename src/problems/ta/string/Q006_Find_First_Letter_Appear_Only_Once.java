package problems.ta.string;

/**
 * 在一个字符串中找到第一个只出现一次的字符. 如输入abaccdeff, 则输出b
 */

import java.util.*;

public class Q006_Find_First_Letter_Appear_Only_Once {
	
	public static void main(String[] args) {
		String s = "abaccdeff";
		char result = findFirstCharAppearOnlyOnce(s);
		System.out.println(result);
	}
	
	public static Character findFirstCharAppearOnlyOnce(String s) {
		HashMap<Character, Integer> map = new HashMap<Character, Integer>();
		
		// step 1. count each character
		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			if (!map.containsKey(ch)) {
				map.put(ch, 1);
			}
			else {
				map.put(ch, map.get(ch) + 1);
			}
		}
		
		// step 2. find out the first character that has count equals to 1
		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			if (map.get(ch) == 1) {
				return ch;
			}
		}
		
		return null;
	}

}
