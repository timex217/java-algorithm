package problems.ta.string;

import java.util.*;

public class Q002A_Roman_To_Int {
	
	public static char[] roman = {'I', 'V', 'X', 'L', 'C', 'D', 'M'};
	public static int[] decimal = {1, 5, 10, 50, 100, 500, 1000};
	
	public static void main(String[] args) {
		int result = romanToInt("MMXIV");
		System.out.println(result);
	}
	
	public static int romanToInt(String s) {
		int sum = 0;
		int i, j = s.length() - 1;
		
		sum += getDecimal(s.charAt(j));
		
		i = j - 1;
		
		while (i >= 0) {
			int vi = getDecimal(s.charAt(i));
			int vj = getDecimal(s.charAt(j));
			
			if (vi < vj) {
				sum -= vi;
			}
			else {
				sum += vi;
			}
			
			i--;
			j--;
		}
		
		return sum;
	}
	
	public static int getDecimal(char ch) {
		for (int i = 0; i < roman.length; i++) {
			if (roman[i] == ch) {
				return decimal[i];
			}
		}
		return -1;
	}

}
