package problems.ta.string;

/**
 * Check Anagram
 */

import java.util.*;

public class Q011_Check_Anagram {
	
	public static void main(String[] args) {
		String s1 = "abcde";
		String s2 = "debac";
		
		boolean result = checkAnagram(s1, s2);
		System.out.println(result);
	}
	
	public static boolean checkAnagram(String s1, String s2) {
		if (s1 == null || s2 == null) {
			return false;
		}
		
		if (s1.length() != s2.length()) {
			return false;
		}
		
		// step 1. build a map to store character count for s1
		HashMap<Character, Integer> map = new HashMap<Character, Integer>();
		
		for (int i = 0; i < s1.length(); i++) {
			char ch = s1.charAt(i);
			
			if (!map.containsKey(ch)) {
				map.put(ch, 1);
			}
			else {
				map.put(ch, map.get(ch) + 1);
			}
		}
		
		// step 2. go through s2 and minus the count
		for (int i = 0; i < s2.length(); i++) {
			char ch = s2.charAt(i);
			
			if (!map.containsKey(ch)) {
				return false;
			}
			else {
				int count = map.get(ch);
				count--;
				if (count < 0) {
					return false;
				}
				map.put(ch, count);
			}
		}
		
		return true;
	}

}
