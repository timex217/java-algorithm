package problems.ta.string;

public class Q010_Check_Rotation {
	
	public static void main(String[] args) {
		String s1 = "abcde";
		String s2 = "cdeab";
		
		boolean result = checkRotation(s1, s2);
		System.out.println(result);
	}
	
	public static boolean checkRotation(String s1, String s2) {
		return strStr(s1 + s1, s2) != null;
	}
	
	public static String strStr(String haystack, String needle) {
		if (haystack == null || needle == null) {
			return null;
		}
		
		int len1 = haystack.length();
		int len2 = needle.length();
		
		if (len1 < len2) {
			return null;
		}
		
		int i, j;
		
		for (i = 0; i <= len1 - len2; i++) {
			for (j = 0; j < len2; j++) {
				if (haystack.charAt(i+j) != needle.charAt(j)) {
					break;
				}
			}
			
			if (j == len2) {
				return haystack.substring(i);
			}
		}
		
		return null;
	}

}
