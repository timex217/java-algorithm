package problems.ta.string;

import java.util.*;

public class Q002B_Int_To_Roman {
	
	public static char[] roman = {'I', 'V', 'X', 'L', 'C', 'D', 'M'};
	public static int[] decimal = {1, 5, 10, 50, 100, 500, 1000};
	
	public static void main(String[] args) {
		int num = 2014;
		String result = intToRoman(num);
		System.out.println(result);
	}
	
	public static String intToRoman(int num) {
		if (num <= 0 || num >= 4000) {
			return "";
		}
		
		ArrayList<Integer> digits = new ArrayList<Integer>();
		
		int digit = 1000;
		
		while (digit > 0) {
			digits.add(num / digit);
			num %= digit;
			digit /= 10;
		}
		
		StringBuilder res = new StringBuilder();
		
		res.append(intToRoman(digits.get(0), 'M', ' ', ' '));
		res.append(intToRoman(digits.get(1), 'C', 'D', 'M'));
		res.append(intToRoman(digits.get(2), 'X', 'L', 'C'));
		res.append(intToRoman(digits.get(3), 'I', 'V', 'X'));
		
		return res.toString();
	}
	
	public static String intToRoman(int digit, char one, char five, char ten) {
		StringBuilder sb = new StringBuilder();
		
		if (digit == 9) {
			sb.append(one).append(ten);
		}
		else if (digit >= 5) {
			sb.append(five);
			for (int i = 0; i < digit - 5; i++) {
				sb.append(one);
			}
		}
		else if (digit == 4) {
			sb.append(one).append(five);
		}
		else {
			for (int i = 0; i < digit; i++) {
				sb.append(one);
			}
		}
		
		return sb.toString();
	}

}
