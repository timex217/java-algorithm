package problems.ta.string;

public class Q004B_Reverse_Words {
	
	public static void main(String[] args) {
		String s = "I love RetailNext!";
		String result = reverseWords(s);
		System.out.println(result);
	}
	
	public static String reverseWords(String s) {
		if (s == null) {
			return null;
		}
		
		char[] chars = s.toCharArray();
		
		int n = chars.length;
		
		// step 1. reverse the whole string
		reverse(chars, 0, n - 1);
		
		// step 2. reverse each word
		int i = 0, j = 0;
		
		while (i < n) {
			// trim the space in front
			while (i < n && chars[i] == ' ') { i++; }
			j = i;
			
			// find the border
			while (j < n && chars[j] != ' ') { j++; }
			
			// reverse from i to j - 1
			reverse(chars, i, j - 1);
			
			// continue
			i = j;
		}
		
		return new String(chars);
	}
	
	public static void reverse(char[] chars, int start, int end) {
		while (start < end) {
			char tmp = chars[start];
			chars[start] = chars[end];
			chars[end] = tmp;
			
			start++;
			end--;
		}
	}

}
