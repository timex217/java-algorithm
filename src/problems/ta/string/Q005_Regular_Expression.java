package problems.ta.string;

/**
 * '.' Matches any single character.
 * '*' Matches zero or more of the preceding element.
 * 
 * The matching should cover the entire input string (not partial).
 * 
 * The function prototype should be:
 * bool isMatch(String s, String p)
 * 
 * Some examples:
 * isMatch("aa","a") → false
 * isMatch("aa","aa") → true
 * isMatch("aaa","aa") → false
 * isMatch("aa", "a*") → true
 * isMatch("aa", ".*") → true
 * isMatch("ab", ".*") → true
 * isMatch("aab", "c*a*b") → true
 */

public class Q005_Regular_Expression {
	
	public static void main(String[] args) {
		String s = "abcbcd";
		String p = "a.*c.*d";
		
		boolean result = isMatch(s, p);
		System.out.println(result);
	}
	
	public static boolean isMatch(String s, String p) {
		if (s == null || p == null) {
			return false;
		}
		
		return isMatch(s, 0, p, 0);
	}
	
	public static boolean isMatch(String s, int i, String p, int j) {
		int slen = s.length();
		int plen = p.length();
		
		if (j >= plen) {
			return i >= slen;
		}
		
		// next char is not '*': must match current character
		if (j >= plen - 1 || p.charAt(j + 1) != '*') {
			return (i < slen) && (p.charAt(j) == '.' || s.charAt(i) == p.charAt(j)) && isMatch(s, i + 1, p, j + 1);
		}
		
		// next char is '*'
		if (j < plen - 1 && p.charAt(j + 1) == '*') {
			while ((i < slen) && (p.charAt(j) == '.' || s.charAt(i) == p.charAt(j))) {
				if (isMatch(s, i, p, j + 2)) return true;
				i++;
			}
		}
		
		return isMatch(s, i, p, j + 2);
	}

}
