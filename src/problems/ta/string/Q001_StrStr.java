package problems.ta.string;

/**
 * Implement strStr()
 * 
 * Returns a pointer to the first occurrence of needle in haystack, or null if needle is not part of haystack.
 */

public class Q001_StrStr {

	public static void main(String[] args) {
		String result = strStr("Hello World!", "World");
		System.out.println(result);
	}
	
	public static String strStr(String haystack, String needle) {
		if (haystack == null || needle == null) {
			return null;
		}
		
		int len1 = haystack.length();
		int len2 = needle.length();
		
		if (len1 < len2) {
			return null;
		}
		
		int i, j;
		
		for (i = 0; i <= len1 - len2; i++) {
			for (j = 0; j < len2; j++) {
				if (haystack.charAt(i+j) != needle.charAt(j)) {
					break;
				}
			}
			
			if (j == len2) {
				return haystack.substring(i);
			}
		}
		
		return null;
	}

}
