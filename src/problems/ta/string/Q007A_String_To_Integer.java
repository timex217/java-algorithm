package problems.ta.string;

public class Q007A_String_To_Integer {
	
	public static void main(String[] args) {
		String s = "-1024";
		int result = strToInt(s);
		System.out.println(result);
	}
	
	public static int strToInt(String s) {
		int num = 0;
		int sign = 1;
		
		int i = 0;
		
		if (s.charAt(i) == '+') {
			i++;
		}
		
		if (s.charAt(i) == '-') {
			sign = -1;
			i++;
		}
		
		while (i < s.length()) {
			char ch = s.charAt(i++);
			
			// valid character
			if (ch < '0' || ch > '9') {
				break;
			}
			
			int digit = ch - '0';
			
			int MIN = Integer.MIN_VALUE;
			int MAX = Integer.MAX_VALUE;
			
			// check overflow
			if (num > MAX / 10 || (num == MAX / 10 && digit > MAX % 10)) {
				return sign == -1 ? MIN : MAX;
			}
			
			num = num * 10 + digit;
		}
		
		return num * sign;
	}

}
