package problems.ta.string;

/**
 * Space Replacement
 */

public class Q009_Space_Replacement {
	
	public static void main(String[] args) {
		String s = "a b c";
		String result = replaceSpace(s);
		System.out.println(result);
	}
	
	public static String replaceSpace(String s) {
		if (s == null) {
			return null;
		}
		
		int n = s.length();
		int count = 0;
		int i, j;
		
		for (i = 0; i < n; i++) {
			char ch = s.charAt(i);
			if (ch == ' ') {
				count++;
			}
		}
		
		char[] chars = new char[n + count * 2];
		
		i = n - 1;
		j = chars.length - 1;
		
		while (i >= 0) {
			char ch = s.charAt(i--);
			
			if (ch == ' ') {
				chars[j--] = '0';
				chars[j--] = '2';
				chars[j--] = '%';
			}
			else {
				chars[j--] = ch;
			}
		}
		
		return new String(chars);
	}

}
