package problems.ta.string;

/**
 * Check Palindrome
 */

public class Q012_Check_Palindrome {
	
	public static void main(String[] args) {
		String s = "abcba";
		boolean result = checkPalindrome(s);
		System.out.println(result);
	}
	
	public static boolean checkPalindrome(String s) {
		if (s == null) {
			return false;
		}
		
		return checkPalindrome(s, 0, s.length() - 1);
	}
	
	public static boolean checkPalindrome(String s, int start, int end) {
		if (start >= end) {
			return true;
		}
		
		if (s.charAt(start) == s.charAt(end)) {
			return checkPalindrome(s, start + 1, end - 1);
		}
		
		return false;
	}

}
