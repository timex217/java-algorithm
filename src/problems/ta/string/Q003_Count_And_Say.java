package problems.ta.string;

/**
 * The count-and-say sequence is the sequence of integers beginning as follows:
 * 
 * 1, 11, 21, 1211, 111221, ...
 * 
 * 1 is read off as "one 1" or 11.
 * 11 is read off as "two 1s" or 21.
 * 21 is read off as "one 2, then one 1" or 1211.
 * 
 * Given an integer n, generate the n-th sequence.
 * Note: The sequence of integers will be represented as a string.
 */

import java.util.*;

public class Q003_Count_And_Say {
	
	public static void main(String[] args) {
		String result = countAndSay(5);
		System.out.println(result);
	}
	
	public static String countAndSay(int n) {
		if (n == 0) {
			return "";
		}
		
		// initialize
		int c = 1;
		String s = "1";
		
		while (c++ < n) {
			s = countAndSay(s);
		}
        
        return s;
    }
	
	public static String countAndSay(String s) {
		StringBuilder sb = new StringBuilder();
		
		char last = s.charAt(0);
		int count = 1;
		
		for (int i = 1; i < s.length(); i++) {
			char curr = s.charAt(i);
			
			if (curr == last) {
				count++;
			} else {
				sb.append(count).append(last);
				last = curr;
				count = 1;
			}
		}
		
		sb.append(count).append(last);
		
		return sb.toString();
	}

}
