package problems.ta.matrix;

import java.util.*;

public class Q005A_Max_Rectangle_In_Array {
	
	public static void main(String[] args) {
		int[] a = {2, 1, 3, 4, 5, 2, 6};
		int max = findMaxRectangleInHistogram(a);
		System.out.format("max rectangle is: %d\n", max);
	}
	
	static int findMaxRectangleInHistogram(int[] a) {
		if (a == null || a.length == 0) {
			return 0;
		}
		
		int max = 0;
		int n = a.length;
		Stack<Integer> stack = new Stack<Integer>();
		
		// 遍历一次
		for (int i = 0; i < n; i++) {
			// 只要当前（还没被压入栈）的元素小于栈顶元素，将栈顶元素弹出并计算矩阵面积
			// 计算方法是: 被弹出的元素高度 x (右边界 - 左边界)
			// 右边界: i
			// 左边界: 下一个要被弹出元素的index + 1 或者 0
			// (注意: stack里不允许有重复元素, 固遇到当前元素和栈顶相同的情况，一并处理)
			while (!stack.isEmpty() && a[i] <= a[stack.peek()]) {
				int index = stack.pop();
				int right = i;
				int left = stack.isEmpty() ? 0 : stack.peek() + 1;
				
				int area = a[index] * (right - left);
				max = Math.max(max, area);
			}
			
			stack.push(i);
		}
		
		// 最后肯定还剩下一些元素（至少一个）
		// 右边界: n
		// 左边界: 下一个要被弹出元素的index + 1 或者 0
		while (!stack.isEmpty()) {
			int index = stack.pop();
			int right = n;
			int left = stack.isEmpty() ? 0 : stack.peek() + 1;
			
			int area = a[index] * (right - left);
			max = Math.max(max, area);
		}
		
		return max;
	}
}
