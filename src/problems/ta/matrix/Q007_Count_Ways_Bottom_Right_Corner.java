package problems.ta.matrix;

/**
 * 一个m x n的grid, 从左上角出发到右下角有多少种走法?
 * 前提: 每次只能朝右或者下走
 */

public class Q007_Count_Ways_Bottom_Right_Corner {
	
	public static void main(String[] args) {
		//int res = dfs(5, 5);
		int res = walk(5, 5);
		System.out.println(res);
	}
	
	/**
	 * 只能从两个方向来到某个位置，要么从上面来，要么从左边来
	 */
	static int dfs(int m, int n) {
		// base case, 左上角只有一种走法
		if (m == 1 && n == 1) {
			return 1;
		}
		
		// 没有办法走到边界外面
		if (m < 1 || n < 1) {
			return 0;
		}
		
		return dfs(m - 1, n) + dfs(m, n - 1);
	}
	
	/**
	 * 用DP来做
	 */
	static int walk(int m, int n) {
		int[][] dp = new int[m + 1][n + 1];
		dp[1][1] = 1;
		
		for (int i = 1; i <= m; i++) {
			for (int j = 1; j <= n; j++) {
				dp[i][j] += dp[i - 1][j] + dp[i][j - 1];
			}
		}
		
		return dp[m][n];
	}

}
