package problems.ta.matrix;

public class Q001C_ZigZag_Print {
	
	public static void main(String[] args) {
		int[][] M = {
			{1, 2, 3},
			{4, 5, 6},
			{7, 8, 9}
		};
		
		zigzagPrint(M, 3, 3);
		
		// 1 2 4 7 5 3 6 8 9
	}
	
	static void zigzagPrint(int[][] M, int m, int n) {
		if (m <= 0 || n <= 0) {
			return;
		}
		
		int i = 1;
		int j = -1;
		
		int delta_i = -1;
		int delta_j = 1;
		
		for (int k = 0; k < m * n; k++) {
			i += delta_i;
			j += delta_j;
			
			// 下边界
			if (i >= m) {
				i = m - 1;
				j += 2;
				
				delta_i = -1;
				delta_j = 1;
			}
			// 右边界
			else if (j >= n) {
				i += 2;
				j = n - 1;
				
				delta_i = 1;
				delta_j = -1;
			}
			// 超出左边界
			else if (j < 0) {
				j = 0;
				
				delta_i = -1;
				delta_j = 1;
			}
			// 超出上边界
			else if (i < 0) {
				i = 0;
				
				delta_i = 1;
				delta_j = -1;
			}
			
			print(M[i][j]);
		}
	}
	
	static void print(int v) {
		System.out.format("%d ", v);
	}

}
