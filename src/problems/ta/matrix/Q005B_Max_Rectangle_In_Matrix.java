package problems.ta.matrix;

import java.util.*;

public class Q005B_Max_Rectangle_In_Matrix {
	
	public static void main(String[] args) {
		int[][] M = {
			{0, 0, 0, 1, 1, 1},
			{1, 1, 1, 1, 1, 0},
			{0, 1, 1, 1, 1, 0},
			{0, 0, 0, 1, 0, 1}
		};
		
		int res = findMaxRectangleInMatrix(M);
		
		System.out.println(res);
	}
	
	/**
	 * 方法是: 计算矩阵每一行的累加和, 然后每层计算直方图最大面积
	 */
	static int findMaxRectangleInMatrix(int[][] M) {
		if (M == null || M.length == 0 || M[0].length == 0) {
			return 0;
		}
		
		int max = 0;
		int m = M.length;
		int n = M[0].length;
		int[][] H = new int[m][n];
		
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				int v = M[i][j];
				
				if (i == 0 || v == 0) {
					H[i][j] = v;
				}
				else {
					H[i][j] = v + H[i - 1][j];
				}
			}
			
			int res = findMaxRectangleInHistogram(H[i]);
			max = Math.max(max, res);
		}
		
		return max;
	}
	
	static int findMaxRectangleInHistogram(int[] a) {
		if (a == null || a.length == 0) {
			return 0;
		}
		
		int max = 0;
		int n = a.length;
		Stack<Integer> stack = new Stack<Integer>();
		
		// 遍历一次
		for (int i = 0; i < n; i++) {
			// 只要当前（还没被压入栈）的元素小于栈顶元素，将栈顶元素弹出并计算矩阵面积
			// 计算方法是: 被弹出的元素高度 x (右边界 - 左边界)
			// 右边界: i
			// 左边界: 下一个要被弹出元素的index + 1 或者 0
			// (注意: stack里不允许有重复元素, 固遇到当前元素和栈顶相同的情况，一并处理)
			while (!stack.isEmpty() && a[i] <= a[stack.peek()]) {
				int index = stack.pop();
				int right = i;
				int left = stack.isEmpty() ? 0 : stack.peek() + 1;
				
				int area = a[index] * (right - left);
				max = Math.max(max, area);
			}
			
			stack.push(i);
		}
		
		// 最后肯定还剩下一些元素（至少一个）
		// 右边界: n
		// 左边界: 下一个要被弹出元素的index + 1 或者 0
		while (!stack.isEmpty()) {
			int index = stack.pop();
			int right = n;
			int left = stack.isEmpty() ? 0 : stack.peek() + 1;
			
			int area = a[index] * (right - left);
			max = Math.max(max, area);
		}
		
		return max;
	}

}
