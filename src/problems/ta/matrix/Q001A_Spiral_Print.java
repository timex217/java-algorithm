package problems.ta.matrix;

public class Q001A_Spiral_Print {
	
	public static void main(String[] args) {
		int[][] M = {
			{ 1,  2,  3,  4, 5},
			{16, 17, 18, 19, 6},
			{15, 24, 25, 20, 7},
			{14, 23, 22, 21, 8},
			{13, 12, 11, 10, 9}
		};
		
		spiralPrint(M);
	}
	
	static void spiralPrint(int[][] M) {
		helper(M, 5, 5, 0);
	}
	
	static void helper(int[][] M, int m, int n, int k) {
		if (m < 0 || n < 0) {
			return;
		}
		
		// 只剩一行
		if (m == 1) {
			for (int j = 0; j < n; j++) {
				print(M[k][k + j]);
			}
			return;
		}
		
		// 只剩一列
		if (n == 1) {
			for (int i = 0; i < m; i++) {
				print(M[k + i][k]);
			}
		}
		
		// 上面一行
		for (int j = 0; j < n - 1; j++) {
			print(M[k][k + j]);
		}
		
		// 右边一列
		for (int i = 0; i < m - 1; i++) {
			print(M[k + i][k + n - 1]);
		}
		
		// 下面一行
		for (int j = n - 1; j > 0; j--) {
			print(M[k + m - 1][k + j]);
		}
		
		// 左边一列
		for (int i = m - 1; i > 0; i--) {
			print(M[k + i][k]);
		}
		
		helper(M, m - 2, n - 2, k + 1);
	}
	
	static void print(int v) {
		System.out.format("%d ", v);
	}

}
