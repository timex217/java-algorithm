package problems.ta.matrix;

import java.util.*;

public class Q002_Rotate_Matrix {
	
	public static void main(String[] args) {
		int[][] M = {
			{ 1,  2,  3,  4, 5},
			{16, 17, 18, 19, 6},
			{15, 24, 25, 20, 7},
			{14, 23, 22, 21, 8},
			{13, 12, 11, 10, 9}
		};
		
		//rotate(M, 5, 0);
		rotate(M);
		
		print(M, 5);
	}
	
	// ----------------------------
	//  Recursion
	// ----------------------------
	static void rotate(int[][] M, int n, int k) {
		if (n <= 1) {
			return;
		}
		
		for (int m = 0; m < n - 1; m++) {
			// save top
			int tmp = M[k][k + m];
			// copy left to top
			M[k][k + m] = M[k + n - 1 - m][k];
			// copy bottom to left
			M[k + n - 1 - m][k] = M[k + n - 1][k + n - 1 - m];
			// copy right to bottom
			M[k + n - 1][k + n - 1 - m] = M[k + m][k + n - 1];
			// copy top to right
			M[k + m][k + n - 1] = tmp;
		}
		
		rotate(M, n - 2, k + 1);
	}
	
	// -----------------------------
	//  Iterative
	// -----------------------------
	static void rotate(int[][] M) {
		int n = M.length;
		int layers = n / 2;
		
		for (int k = 0; k < layers; k++) {
			for (int m = 0; m < n - 1; m++) {
				// save top
				int tmp = M[k][k + m];
				// copy left to top
				M[k][k + m] = M[k + n - 1 - m][k];
				// copy bottom to left
				M[k + n - 1 - m][k] = M[k + n - 1][k + n - 1 - m];
				// copy right to bottom
				M[k + n - 1][k + n - 1 - m] = M[k + m][k + n - 1];
				// copy top to right
				M[k + m][k + n - 1] = tmp;
			}
			n -= 2;
		}
	}
	
	static void print(int[][] M, int n) {
		for (int i = 0; i < n; i++) {
			System.out.println(Arrays.toString(M[i]));
		}
	}

}
