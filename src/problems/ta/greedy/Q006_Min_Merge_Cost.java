package problems.ta.greedy;

/**
 * 有一个数组每个元素代表一个文件的长度, 我们想要把所有的文件merge成一个文件, 
 * 假设merge一个长度为 5 和一个长度为 10 的文件的cost是 15, 问怎样merge的cost最小
 * 
 * 把数组放入一个priority queue里面, 也就是说一直保持排序状态
 * 总是merge两个最小的元素, 并且把它插入回priority queue, 直到只有一个元素
 */

import java.util.*;

public class Q006_Min_Merge_Cost {
	
	public static void main(String[] args) {
		int[] a = {5, 10, 10, 20};
		int cost = minMergeCost(a);
		System.out.println(cost);
	}
	
	static int minMergeCost(int[] a) {
		if (a == null || a.length == 0) {
			return 0;
		}
		
		int total_cost = 0;
		
		// initialize the priority queue
		PriorityQueue<Integer> heap = new PriorityQueue<Integer>(a.length);
		
		for (int i = 0; i < a.length; i++) {
			heap.offer(a[i]);
		}
		
		while (heap.size() >= 2) {
			int l1 = heap.poll();
			int l2 = heap.poll();
			int cost = l1 + l2;
			
			heap.offer(cost);
			total_cost += cost;
		}
		
		return total_cost;
	}

}
