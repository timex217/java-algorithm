/**
 * Given a string, find the length of the longest substring without repeating characters. 
 * 
 * For example, the longest substring without repeating letters for "abcabcbb" is "abc", 
 * which the length is 3. For "bbbbb" the longest substring is "b", with the length of 1.
 */

package problems.ta.greedy;

import java.util.*;

public class Q005_Longest_Without_Repeating_Characters {
	
	public static void main(String[] args) {
		String s = "abcabcbb";
		int len = solution(s);
		System.out.println(len);
	}
	
	static int solution(String s) {
		if (s == null || s.length() == 0) {
			return 0;
		}
		
		int i = 0; // slow pointer
		int j = 0; // fast pointer
		int max = 0;
		
		// use a set to track whether there's a duplication
		Set<Character> set = new HashSet<Character>();
		
		while (j < s.length()) {
			// current character
			char ch = s.charAt(j);
			
			// new character, add to the set
			if (!set.contains(ch)) {
				set.add(ch);
			}
			// found duplicate
			else {
				// update max
				max = Math.max(max, j - i);
				// find the duplicate in front and skip it
				while (s.charAt(i) != ch) {
					set.remove(s.charAt(i));
					i++;
				}
				i++;
			}
			j++;
		}
		
		return Math.max(max, j - i);
	}

}
