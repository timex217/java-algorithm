package problems.ta.greedy;

public class Q001_Power_N {
	
	public static void main(String[] args) {
		double res = pow(2.0, 3);
		System.out.println(res);
	}
	
	static double pow(double x, int n) {
		if (n == 0) {
			return 1.0;
		}
		
		double v = pow(x, Math.abs(n) / 2);
		
		v *= v;
		
		if (Math.abs(n) % 2 == 1) {
			v *= x;
		}
		
		return (n > 0) ? v : 1.0 / v;
	}

}
