package problems.ta.greedy;

/**
 * fractional knapsack问题，有物品t1, t2, ... tn, 对应的重量w1, w2, ... wn,价值v1, v2, ... vn,
 * 如果我们的重量限制是C, 怎样最大化装载物品的价值
 * 求一个比率 v/w, 然后按照这个比率给物品排序, 然后用greedy策略从高向低选
 */

import java.util.*;

public class Q009_Fractional_Knapsack {
	
	static class Item implements Comparable<Item> {
		double value;
		double weight;
		
		double getRatio() {
			return value / weight;
		}
		
		Item(double v, double w) {
			value = v;
			weight = w;
		}
		
		public int compareTo(Item item) {
			if (getRatio() < item.getRatio()) return -1;
			if (getRatio() > item.getRatio()) return 1;
			return 0;
		}
	}
	
	public static void main(String[] args) {
		double[] v = {5, 3, 4};
		double[] w = {3, 2, 1};
		
		ArrayList<String> res = knapsack(v, w, 5);
		
		System.out.println(res.toString());
	}
	
	static ArrayList<String> knapsack(double[] v, double[] w, double capacity) {
		ArrayList<String> res = new ArrayList<String>();
		
		int n = v.length;
		
		// 将物品按照价值重量比率按照从大到小的顺序排列
		Item[] items = new Item[n];
		for (int i = 0; i < n; i++) {
			Item item = new Item(v[i], w[i]);
			items[i] = item;
		}
		
		Arrays.sort(items, Collections.reverseOrder());
		
		// 然后用greedy的办法能装多少就装多少
		for (int i = 0; i < n; i++) {
			Item item = items[i];
			if (item.weight < capacity) {
				res.add(String.format("v=%.1f:w=%.1f", item.value, item.weight));
				capacity -= item.weight;
			}
		}
		
		return res;
	}

}
