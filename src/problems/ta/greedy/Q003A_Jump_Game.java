/**
 * We need to minimize something, that is jump. How can we do that? 
 * We take longest jump possible from each index. 
 * 
 * While taking jump we also take care that which among the possible landing indexes can give us maximum jump ahead. 
 * So every time we land, we have selected that index because it will give us maximum jump. 
 * Just greedy algorithm.
 * 
 * Mathematically,
 * for index i, scan through all indices e from i+1 till e = i + a[i] and calculate a value v = e + a[e]. 
 * Take e with maximum v.
 *
 * let's work out an example:
 * In above example, for i = 0, we check:
 * 
 * e = 1 and v = 1(e) + 3(a[e]) = 4 
 * e = 2 and v = 2 + 1 = 3
 *
 * So we select e = 1.
 */

package problems.ta.greedy;

public class Q003A_Jump_Game {
	
	public static void main(String[] args) {
		int[] a = {1, 3, 5, 8, 9, 2, 6, 7, 6, 8, 9};
		//int[] a = {3, 2, 1, 0, 4};
		boolean res = canJumpDP(a);
		System.out.println(res ? "Yes" : "No");
	}
	
	// method 1: O(n)
	static boolean canJump(int[] a) {
		int n = a.length;
		
		// 当前能跳到的最远位置
		int reach = 0;
		
		// 在每个位置上更新一下最远能跳到的位置
		for (int i = 0; i <= reach && i < n; i++) {
			reach = Math.max(reach, i + a[i]);
		}
		
		// 最后如果最远能跳出数组，那么就返回true
		if (reach >= a.length - 1) {
			return true;
		}
		
		return false;
	}
	
	// method 2: dp
	static boolean canJumpDP(int[] a) {
		if (a == null || a.length == 0) {
			return false;
		}
		
		int n = a.length - 1;
		
		// dp(i) represents whether we can jump to the end from i-th position
		boolean[] dp = new boolean[n];
		dp[n-1] = true;
		
		for (int i = n - 2; i >= 0; i--) {
			for (int j = 1; j <= a[i]; j++) {
				if (dp[i + j]) {
					dp[i] = true;
					break;
				}
			}
		}
		
		return dp[0];
	}

}
