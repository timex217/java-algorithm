package problems.ta.greedy;

import java.util.*;

/**
 * Say you have an array for which the ith element is the price of a given stock on day i.
 * 
 * If you were only permitted to complete at most one transaction (ie, buy one and sell one share of the stock), 
 * design an algorithm to find the maximum profit.
 */

public class Q004A_Stock {
	
	public static void main(String[] args) {
		int[] s = {3, 5, 1, 4, 9, 8, 2, 7, 10, 6};
		int res = maxProfit(s);
		System.out.println(res);
	}
	
	static int maxProfit(int[] prices) {
		if (prices == null || prices.length == 0) {
            return 0;
        }
        
        int buy = 0;
        int sell = 0;
        int min_index = 0;
        int max_profit = 0;
        
        for (int i = 0; i < prices.length; i++) {
            // track the index of minimum price
            if (prices[i] < prices[min_index]) {
                min_index = i;
            }
            
            // see if current profit is the max
            int diff = prices[i] - prices[min_index];
            if (diff > max_profit) {
                buy = min_index;
                sell = i;
                max_profit = diff;
            }
        }
        
        return max_profit;
	}

}
