package problems.ta.greedy;

import java.util.*;

public class Q003B_Min_Jump {
	
	public static void main(String[] args) {
		//int[] a = {1, 30, 5, 8, 9, 2, 6, 7, 6, 8, 9};
		int[] a = {2, 3, 1, 1, 4};
		int steps = minJumpDP(a);
		
		System.out.println(steps);
	}
	
	// method 1: Greedy O(n)
	// 无法输出具体在哪一步跳，但是能快速得出一共要跳多少步
	// 基本思想：每次都尽量跳，看能跳几次
	// 如果要知道怎么跳，就得用DP
	static int minJump(int[] a) {
		if (a == null || a.length == 0) {
			return 0;
		}
		
		int n = a.length;
		
		int reach = 0;
		int step = 0;
		int last_reach = 0;
		
		// step 1. (i <= reach), 判断当前还能不能继续往前走
		for (int i = 0; i <= reach && i < n; i++) {
			// step 2. 如果能往前走，看看是不是已经超过了上一次能跳到的最远位置
			if (i > last_reach) {
				// 如果是，说明我们无论是否能一跳到终点都需要全新的一跳
				step++;
				// 至于这次能跳多远，就看之前经历过的位置生成的最远位置reach
				// 注意，不包括这个点所生成的reach，因为这个点的值有可能是0，而其实本来还可以往前继续走的，就被你返回了
				// 所以，得要用这个点之前的所有点所生成的最大位置
				last_reach = reach;
			}
			
			// 每次更新从该位置最远能跳到的位置reach
			reach = Math.max(reach, i + a[i]);
		}
		
		if (reach >= n - 1) {
			return step;
		}
		
		return 0;
	}
	
	static int minJumpDP(int[] a) {
		if (a == null || a.length == 0) {
			return 0;
		}
		
		int n = a.length;
		
		// dp[i] represents the minimum steps from i-th position to the end
		int[] dp = new int[n];
		dp[n - 1] = 0;
		
		for (int i = n - 2; i >= 0; i--) {
			// 如果当前位置为0， 表示无论如何都跳不到最后
			if (a[i] == 0) {
				dp[i] = Integer.MAX_VALUE;
			}
			// 如果能一步跳到最后
			else if (i + a[i] >= n - 1) {
				dp[i] = 1;
			}
			else {
				int min = Integer.MAX_VALUE;
				// 从当前位置找后面能跳到最后的最小值
				for (int j = 1; j <= a[i] && (i + j) < n; j++) {
					min = Math.min(min, dp[i + j]);
				}
				
				if (min != Integer.MAX_VALUE) {
					dp[i] = min + 1;
				}
				else {
					dp[i] = Integer.MAX_VALUE;
				}
			}
		}
		
		return dp[0];
	}

}
