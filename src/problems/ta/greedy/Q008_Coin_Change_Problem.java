package problems.ta.greedy;

/**
 * 假设有一个钱数要把它换成硬币, 怎样才能得到minimum number of coins的换法.
 * 
 * 从最大的币种开始选, 依次向最小的币种. 有一个前提, 美元的系统(1, 5, 10, 25)可以这么做,
 * 其它的一些系统greedy方法是不对的, 比如(1,3,4) 6 greedy 4 11 optimal 3 3
 * greedy的话前提是上一币种面额是下一币种的至少两倍
 */

import java.util.*;

public class Q008_Coin_Change_Problem {
	
	public static void main(String[] args) {
		int[] coins = {25, 10, 5, 1};
		//int[] coins = {4, 3, 1};
		ArrayList<Integer> changes = getChanges(99, coins);
		System.out.println(changes.toString());
	}
	
	static ArrayList<Integer> getChanges(int target, int[] coins) {
		ArrayList<Integer> res = new ArrayList<Integer>();
		
		if (target == 0) {
			return res;
		}
		
		int i = 0;
		
		while (target > 0) {
			// current coin
			int coin = coins[i];
			
			if (target >= coin) {
				res.add(coin);
				target -= coin;
			}
			else {
				// find the next proper coin
				while (target < coins[i] && i < coins.length) i++;
			}
		}
		
		return res;
	}

}
