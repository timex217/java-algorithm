package problems.ta.greedy;

/**
 * Say you have an array for which the ith element is the price of a given stock on day i.
 *
 * Design an algorithm to find the maximum profit. You may complete at most two transactions.
 *
 * Note:
 * You may not engage in multiple transactions at the same time (ie, you must sell the stock before you buy again).
 * 
 * 这里我们先解释最多可以进行k次交易的算法, 然后最多进行两次我们只需要把k取成2即可. 我们还是使用"局部最优和全局最优解法".
 * 
 * 我们维护两种量:
 * 
 * 1. 当前到达第i天, 最多可以进行j次交易, 最好的利润是多少 (global[i][j])
 * 2. 当前到达第i天, 最多可以进行j次交易, 最后一次交易在当天卖出, 最好的利润是多少 (local[i][j])
 * 
 * 下面我们来看递推式, 全局的比较简单:
 * 
 * global[i][j] = max(global[i-1][j], local[i][j])
 * 
 * 也就是取当前局部最好的, 和过往全局最好的那个作比较 (因为最后一次交易如果包含当前天, 一定在局部最好的里面, 否则一定在过往全局最优的里面)
 * 
 * 对于局部变量的维护, 递推式是:
 * 
 * local[i][j] = max(global[i-1][j-1] + max(diff, 0), local[i-1][j] + diff)
 * 
 * 也就是看两个量:
 * 
 * 第一个是: global[i-1][j-1] + max(diff, 0)
 * 全局到i-1天进行j-1次交易, 然后加上今天的交易, 如果今天是赚钱的话 (也就是前面只要j-1次交易, 最后一次交易取当前天)
 * 
 * 第二个是: local[i-1][j] + diff
 * 取local第i-1天j次交易, 然后加上今天的差值 (这里因为local[i-1][j]比如包含第i-1天卖出的交易, 所以现在变成第i天卖出, 并不会增加交易次数,
 * 而且这里无论diff是不是大于0都一定要加上，因为否则就不满足local[i][j]必须在最后一天卖出的条件了)
 * 
 * 上面的算法中对于天数需要一次扫描, 而每次要对交易次数进行递推式求解, 所以时间复杂度是O(n*k), 如果是最多进行两次交易, 那么复杂度还是O(n)
 * 空间上只需要维护当天数据皆可以, 所以是O(k), 当k = 2, 则是O(1)
 */

public class Q004C_Stock_III {
	
	public static void main(String[] args) {
		int[] s = {3, 5, 1, 4, 9, 8, 2, 7, 10, 6};
		int res = maxProfit(s, 2);
		System.out.println(res);
	}
	
	// 最多k次交易
	static int maxProfit(int[] prices, int k) {
	    if (prices == null || prices.length == 0) {
	        return 0;
	    }
	    
	    int n = prices.length; 
	    
	    int[][] local  = new int[n][k + 1];
	    int[][] global = new int[n][k + 1];
	    
	    for (int i = 1; i < n; i++) {
	    	int diff = prices[i] - prices[i - 1];
	    	
	    	for (int j = 1; j <= k; j++) {
	    		local[i][j] = Math.max(global[i-1][j-1] + Math.max(diff, 0), local[i-1][j] + diff);
	    		global[i][j] = Math.max(global[i-1][j], local[i][j]);
	    	}
	    }
	    
	    return global[n-1][k];
	}

}
