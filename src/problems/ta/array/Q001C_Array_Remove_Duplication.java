package problems.ta.array;

/**
 * 给定一个排好序的数组, 消除里面重复的元素.
 * 
 * A: 对于重复元素只保留一个怎么做
 * B: 对于重复元素只保留2个怎么做
 * C: 对于重复的元素一个都不保留怎么做
 */

import java.util.*;

public class Q001C_Array_Remove_Duplication {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] A = {0, 0, 1, 1, 1, 1, 2, 2, 2, 3, 3, 4, 5, 5, 5, 5, 5, 6, 6, 7, 8, 9};
		int[] B = removeDuplication(A);
		System.out.println(Arrays.toString(B));
		// output: [4, 7, 8, 9]
	}
	
	// method 0
	public static int[] removeDuplication(int[] A) {
		// 隔板思想
		int i = 0;
		int val = A[0];
		int count = 1;
		
		for (int j = 1; j < A.length; j++) {
			if (A[j] == val) {
				count++;
			} else {
				if (count == 1) {
					A[i++] = val;
				}
				
				val = A[j];
				count = 1;
			}
		}
		
		if (count == 1) {
			A[i++] = val;
		}
		
		int[] B = new int[i];
		for (int k = 0; k < i; k++) {
			B[k] = A[k];
		}
		
		return B;
	}
	
	
	// method 1
	public static int[] removeDuplication1(int[] A) {
		if (A == null) {
			return null;
		}
		
		ArrayList<Integer> list = new ArrayList<Integer>();
		
		int buffer = Integer.MIN_VALUE;
		int buffer_count = 0;
		
		for (int i = 0; i < A.length; i++) {
			if (A[i] == buffer) {
				buffer_count++;
			}
			else {
				if (buffer_count == 1) {
					list.add(buffer);
				}
				buffer = A[i];
				buffer_count = 1;
			}
		}
		
		// for the last item
		if (buffer_count == 1) {
			list.add(buffer);
		}
		
		// return result
		int[] B = new int[list.size()];
		for (int i = 0; i < list.size(); i++) {
			B[i] = list.get(i);
		}
		
		return B;
	}

}
