package problems.ta.array;

/**
 * 两个数组代表数字
 * 
 * a 求他们的和
 * b 求他们的乘积
 */

import java.util.*;

public class Q009B_Array_Multiply_Two_Array {
	
	public static void main(String[] args) {
		Integer[] A = {7, 8, 9};
		Integer[] B = {9, 5, 9, 6};
		Integer[] C = multiply(A, B);
		System.out.println(Arrays.toString(C));
		// output: {7, 5, 7, 1, 2, 4, 4}
	}
	
	/**
	 * 
	 */
	public static ArrayList<Integer> add(ArrayList<Integer> A, ArrayList<Integer> B) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		
		int i = A.size() - 1;
		int j = B.size() - 1;
		int carry = 0;
		
		while (i >= 0 && j >= 0) {
			int sum = A.get(i--) + B.get(j--) + carry;
			int mod = sum % 10;
			carry = sum / 10;
			
			result.add(0, mod);
		}
		
		while (i >= 0) {
			int sum = A.get(i--) + carry;
			int mod = sum % 10;
			carry = sum / 10;
			
			result.add(0, mod);
		}
		
		while (j >= 0) {
			int sum = B.get(j--) + carry;
			int mod = sum % 10;
			carry = sum / 10;
			
			result.add(0, mod);
		}
		
		if (carry > 0) {
			result.add(0, carry);
		}
		
		return result;
	}
	
	/**
	 * 
	 */
	public static ArrayList<Integer> multiply(Integer[] A, int d) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		
		int i = A.length - 1;
		int carry = 0;
		
		while (i >= 0) {
			int product = A[i--] * d + carry;
			int mod = product % 10;
			carry = product / 10;
			
			result.add(0, mod);
		}
		
		if (carry > 0) {
			result.add(0, carry);
		}
		
		return result;
	}
	
	/**
	 * 
	 */
	public static Integer[] multiply(Integer[] A, Integer[] B) {
		ArrayList<ArrayList<Integer>> list = new ArrayList<ArrayList<Integer>>();
		
		for (int i = 0; i < B.length; i++) {
			ArrayList<Integer> r = multiply(A, B[i]);
			for (int j = i + 1; j < B.length; j++) {
				r.add(0);
			}
			list.add(r);
		}
		
		ArrayList<Integer> result = add(list, 0, list.size() - 1);
		
		int n = result.size();
		Integer[] C = new Integer[n];
		for (int i = 0; i < n; i++) {
			C[i] = result.get(i);
		}
		
		return C;
	}
	
	public static ArrayList<Integer> add(ArrayList<ArrayList<Integer>> list, int lo, int hi) {
		if (lo < hi) {
			int mid = lo + (hi - lo) / 2;
			
			ArrayList<Integer> left = add(list, lo, mid);
			ArrayList<Integer> right = add(list, mid + 1, hi);
			
			return add(left, right);
		}
		else {
			return list.get(lo);
		}
	}

}
