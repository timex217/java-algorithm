package problems.ta.array;

/**
 * 一个数组里面所有元素重复两次只有一个是一次
 * 
 * a 求出这个元素
 * b 所有元素重复三次只有一个意外呢
 */

public class Q008A_Array_Find_Number_Repeat_Twice {
	
	public static void main(String[] args) {
		int[] A = {0, 2, 9, 0, 6, 8, 3, 7, 2, 1, 3, 8, 1, 6, 9};
		int d = find(A);
		System.out.println(d);
	}
	
	public static int find(int[] A) {
		int sum = A[0];
		for (int i = 1; i < A.length; i++) {
			sum ^= A[i];
		}
		return sum;
	}

}
