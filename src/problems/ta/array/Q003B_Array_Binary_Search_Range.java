package problems.ta.array;

/**
 * 一个排好序的数组, 寻找元素
 * 
 * a 用binary search找到其中一个元素的位置
 * b 如果数组元素可以重复, 返回它的序号range
 */

import java.util.*;

public class Q003B_Array_Binary_Search_Range {
	
	public static void main(String[] args) {
		// index:  0  1  2  3  4  5  6  7  8  9  10 11
		int[] A = {0, 0, 1, 1, 2, 2, 2, 2, 2, 2, 3, 4};
		int[] B = findRange(A, 2);
		System.out.println(Arrays.toString(B));
		// output: [4, 9]
	}
	
	public static int[] findRange(int[] A, int target) {
		if (A == null) {
			return new int[]{-1, -1};
		}
		
		int lower = getLowerBound(A, target);
		int upper = getUpperBound(A, target);
		
		return new int[]{lower, upper};
	}
	
	public static int getLowerBound(int[] A, int target) {
		if (A == null) {
			return -1;
		}
		
		int lo = 0;
		int hi = A.length - 1;
		
		while (lo < hi) {
			int mid = lo + (hi - lo) / 2;
			
			if (A[mid] < target) {
				lo = mid + 1;
			}
			else {
				hi = mid;
			}
		}
		
		return (A[lo] == target) ? lo : -1;
	}
	
	public static int getUpperBound(int[] A, int target) {
		if (A == null) {
			return -1;
		}
		
		int lo = 0;
		int hi = A.length - 1;
		
		while (lo < hi) {
			int mid = lo + (hi - lo + 1) / 2;
			
			if (A[mid] > target) {
				hi = mid - 1;
			}
			else {
				lo = mid;
			}
		}
		
		return (A[hi] == target) ? hi : -1;
	}

}
