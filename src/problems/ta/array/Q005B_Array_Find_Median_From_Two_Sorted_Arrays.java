package problems.ta.array;

public class Q005B_Array_Find_Median_From_Two_Sorted_Arrays {
	
	public static void main(String[] args) {
		int[] A = {1, 2, 3, 4};
		int[] B = {1, 2, 3, 4};
		
		double d = findMedianSortedArrays(A, B);
		System.out.println(d);
		// output: 
	}
	
	public static double findMedianSortedArrays(int A[], int B[]) {
		int m = A.length;
		int n = B.length;
		int k = (m + n) / 2;
		
	    if ((m + n) % 2 == 1) {
	        return helper(A, B, 0, m - 1, 0, n - 1, k + 1);
	    }
	    else {
	        return (helper(A, B, 0, m - 1, 0, n - 1, k)  
	              + helper(A, B, 0, m - 1, 0, n - 1, k + 1))
	              / 2.0;
	    }
	}
	
	private static int helper(int A[], int B[], int i, int i2, int j, int j2, int k)
	{
		// count the elements
	    int m = i2 - i + 1;
	    int n = j2 - j + 1;
	    
	    // make sure A has less elements than B
	    if (m > n) {
	        return helper(B, A, j, j2, i, i2, k);
	    }
	    // base case 1: if A has nothing, simply return B's k-th element
	    if (m == 0) {
	        return B[j + k - 1];
	    }
	    // base case 2
	    if (k == 1) {
	        return Math.min(A[i], B[j]);
	    }
	    
	    int posA = Math.min(k / 2, m);
	    int posB = k - posA;
	    
	    if (A[i + posA - 1] == B[j + posB - 1]) {
	        return A[i + posA - 1];
	    }
	    else if (A[i + posA - 1] < B[j + posB - 1]) {
	        return helper(A, B, i + posA, i2, j, j + posB - 1, k - posA);
	    }
	    else {
	        return helper(A, B, i, i + posA - 1, j + posB, j2, k - posB);
	    }
	}

}
