package problems.ta.array;

/**
 * 假设一个数组里面有三种数字0, 1, 2杂乱排列
 * 要求把这三种数字分类排好, 也就是0放在一起, 1在一起, 2在一起
 */

import java.util.*;

public class Q002_Array_Rearrange_012 {
	
	public static void main(String[] args) {
		int[] A = {0, 2, 1, 0, 1, 0, 2, 2, 0, 0, 1, 1, 1, 0, 2};
		rearrange(A);
		System.out.println(Arrays.toString(A));
	}
	
	public static void rearrange(int[] A) {
		if (A == null || A.length == 0) {
			return;
		}
		
		int i = 0;				// right border of 0
		int j = A.length - 1;	// left border of 2
		int k = 0; 				// right border of 1
		
		while (k <= j) {
			if (A[k] == 2) {
				swap(A, k, j--);
			}
			else if (A[k] == 0) {
				swap(A, k++, i++);
			}
			else {
				k++;
			}
		}
	}
	
	public static void swap(int[] A, int i, int j) {
		int tmp = A[i];
		A[i] = A[j];
		A[j] = tmp;
	}

}
