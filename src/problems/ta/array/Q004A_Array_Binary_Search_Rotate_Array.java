package problems.ta.array;

/**
 * 一个rotate的排序数组, 寻找元素
 * 
 * a 寻找一个元素的位置
 * b 如果这个排序数组有duplicate, 怎么找到这个元素
 */

public class Q004A_Array_Binary_Search_Rotate_Array {
	
	public static void main(String[] args) {
		// index:  0  1   2   3   4   5  6  7  8  9  10 11
		int[] A = {9, 10, 17, 20, 21, 0, 1, 3, 4, 5, 7, 8};
		int index = search(A, 17);
		System.out.println(index == -1 ? "Not found" : index);
		// output: 2
	}
	
	public static int search(int[] A, int target) {
		if (A == null) {
			return -1;
		}
		
		int lo = 0;
		int hi = A.length - 1;
		
		while (lo <= hi) {
			int mid = lo + (hi - lo) / 2;
			
			if (A[mid] == target) {
				return mid;
			}
			else if (A[lo] <= A[mid]) {
				if (A[lo] <= target && target < A[mid]) {
					hi = mid - 1;
				}
				else {
					lo = mid + 1;
				}
			}
			else {
				if (A[mid] < target && target <= A[hi]) {
					lo = mid + 1;
				}
				else {
					hi = mid - 1;
				}
			}
		}
		
		return -1;
	}

}
