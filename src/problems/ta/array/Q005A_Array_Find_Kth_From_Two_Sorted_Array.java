package problems.ta.array;

/**
 * 两个排好序的数组, 找到第k大的元素
 * 
 * a 寻找第k大的元素
 * b 寻找两个数组的median
 */

public class Q005A_Array_Find_Kth_From_Two_Sorted_Array {
	
	public static void main(String[] args) {
		int[] A = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		int[] B = {2, 4, 6, 7, 9, 10, 11, 12, 13};
		
		int d = find(A, B, 15);
		System.out.println(d);
		// output: 10
	}
	
	public static int find(int[] A, int[] B, int k) {
		int result = Integer.MIN_VALUE;
		int i = 0, j = 0, m = 0;
		
		while (m++ < k) {
			if (i >= A.length)      result = B[j++];
			else if (j >= B.length) result = A[i++];
			else if (A[i] < B[j])   result = A[i++];
			else                    result = B[j++];
		}
		
		return result;
	}

}
