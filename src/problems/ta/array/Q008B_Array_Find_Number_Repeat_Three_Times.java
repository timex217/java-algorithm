package problems.ta.array;

/**
 * 一个数组里面所有元素重复两次只有一个是一次
 * 
 * a 求出这个元素
 * b 所有元素重复三次只有一个意外呢
 */

public class Q008B_Array_Find_Number_Repeat_Three_Times {
	
	public static void main(String[] args) {
		//int[] A = {0, 0, 2, 2, 9, 9, 0, 6, 6, 8, 8, 3, 3, 7, 2, 1, 1, 3, 8, 1, 6, 9};
		int[] A = {1, 4, 5, 1, 1, 4, 4};
		int d = find(A);
		System.out.println(d);
	}
	
	public static int find(int[] A) {
	    int ones = 0;
	    int twos = 0;
	    int not_threes;
	    
	    for (int i = 0; i < A.length; i++) {
	        int x = A[i];
	        twos |= ones & x; // twos holds the number that appears twice 
	        ones ^= x;        // ones holds the number that appears only once
	        
	        // below is used to clear the ones and twos if both ones and twos have the number
	        not_threes = ~(ones & twos);
	        ones &= not_threes;
	        twos &= not_threes;
	    }
	    
		return ones;
	}
}
