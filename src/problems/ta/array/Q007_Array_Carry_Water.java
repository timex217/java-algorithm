package problems.ta.array;

/**
 * 有一个数组代表bar宽度为1的高度
 * 
 * a 这个数组可以存多少水
 */

import java.util.*;

public class Q007_Array_Carry_Water {
	
	public static void main(String[] args) {
		int[] A = {0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1};
		int result = carryWater2(A);
		System.out.println(result);
		// output: 6
	}
	
	public static int carryWater2(int[] A) {
		if (A == null || A.length == 0) {
	        return 0;
		}
		
		int n = A.length;
	    int max = 0;
	    int res = 0;
	    
	    int[] container = new int[n];
	    
	    for (int i = 0; i < n; i++) {
	        container[i] = max;
	        max = Math.max(max, A[i]);
	    }
	    
	    max = 0;
	    for (int i = n - 1; i >= 0; i--) {
	        container[i] = Math.min(max, container[i]);
	        max = Math.max(max, A[i]);
	        int water = container[i] - A[i];
	        res += water > 0 ? water : 0;
	    }
	    
	    return res;
	}
	
	// method 0
	public static int carryWater(int[] A) {
		if (A == null) {
			return 0;
		}
		
		int n = A.length, sum = 0; 
		int left = 0, right = 0;
		int height = 0;
		boolean climb_up = false;
		
		Stack<Integer> stack = new Stack<Integer>();
		stack.push(0);
		
		for (int i = 1; i < n; i++) {
			right = stack.peek();
			
			if (A[i] < A[right] && climb_up) {
				while (!stack.isEmpty()) {
					height = Math.min(A[left], A[right]);
					sum += Math.max(height - A[stack.pop()], 0);
				}
				
				stack.push(right);
				
				left = right;
				climb_up = false;
			}
			else if (A[i] > A[right] && !climb_up) {
				climb_up = true;
			}
			
			stack.push(i);
		}
		
		// at last
		while (!stack.isEmpty()) {
			height = Math.min(A[left], A[n - 1]);
			sum += Math.max(height - A[stack.pop()], 0);
		}
		
		return sum;
	}
	
	// method 1
	// from a point find its left and right borders
	// that can carry water
	public static int carryWater1(int[] A) {
		if (A == null) {
			return 0;
		}
		
		int n = A.length, sum = 0;
		
		for (int i = 0; i < n; i++) {
			// find the left border
			int left = i;
			while (left > 0 && A[left] < A[left - 1]) {
				left--;
			}
			
			// find the right border
			int right = i;
			while (right < n - 1 && A[right] < A[right + 1]) {
				right++;
			}
			
			if (left < i && right > i) {
				int max_height = Math.min(A[left], A[right]);
				for (int j = left + 1; j <= right - 1; j++) {
					sum += max_height - A[j];
				}
			}
		}
		
		return sum;
	}

}
