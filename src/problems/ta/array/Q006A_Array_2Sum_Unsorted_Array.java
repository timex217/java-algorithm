package problems.ta.array;

import java.util.*;

public class Q006A_Array_2Sum_Unsorted_Array {
	
	public static void main(String[] args) {
		int[] A = {3, 2, 4};
		int[] result = twoSum(A, 6);
		System.out.println(Arrays.toString(result));
	}
	
	public static int[] twoSum(int[] numbers, int target) {
        int[] result = new int[2];
        
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        
        for (int i = 0; i < numbers.length; i++) {
            int num = numbers[i];
            int diff = target - num;
            
            if (map.containsKey(diff)) {
                int index = map.get(diff);
                if (index != i) {
                    result[0] = index + 1;
                    result[1] = i + 1;
                    break;
                }
            }
            
            if (!map.containsKey(num)) {
                map.put(num, i);
            }
        }
        
        return result;
    }

}
