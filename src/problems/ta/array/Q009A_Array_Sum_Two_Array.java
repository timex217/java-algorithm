package problems.ta.array;

/**
 * 两个数组代表数字
 * 
 * a 求他们的和
 * b 求他们的乘积
 */

import java.util.*;

public class Q009A_Array_Sum_Two_Array {
	
	public static void main(String[] args) {
		int[] A = {7, 8, 9};
		int[] B = {9, 5, 9, 6};
		int[] C = sum(A, B);
		System.out.println(Arrays.toString(C));
		// output: {1, 0, 3, 8, 5}
	}
	
	public static int[] sum(int[] A, int[] B) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		
		int carry = 0;
		int i = A.length - 1;
		int j = B.length - 1;
		
		// sum A and B
		while (i >= 0 && j >= 0) {
			int s = A[i--] + B[j--] + carry;
			int mod = s % 10;
			carry = s / 10;
			
			list.add(mod);
		}
		
		// for any left over in A
		while (i >= 0) {
			int s = A[i--] + carry;
			int mod = s % 10;
			carry = s / 10;
			
			list.add(mod);
		}
		
		// for any left over in B
		while (j >= 0) {
			int s = B[j--] + carry;
			int mod = s % 10;
			carry = s / 10;
			
			list.add(mod);
		}
		
		// for any left over carry
		if (carry > 0) {
			list.add(carry);
		}
		
		// return result
		int n = list.size();
		int[] C = new int[n];
		for (i = n - 1; i >= 0; i--) {
			C[n - 1 - i] = list.get(i);
		}
		return C;
	}

}
