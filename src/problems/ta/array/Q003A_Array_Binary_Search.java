package problems.ta.array;

/**
 * 一个排好序的数组, 寻找元素
 * 
 * a 用binary search找到其中一个元素的位置
 * b 如果数组元素可以重复, 返回它的序号range
 */

public class Q003A_Array_Binary_Search {
	
	public static void main(String[] args) {
		// index:  0  1  2  3  4  5  6  7  8   9   10  11
		int[] A = {0, 1, 3, 4, 5, 7, 8, 9, 10, 17, 20, 21};
		int index = search(A, 17);
		System.out.println(index == -1 ? "Not found" : index);
		// output: 9
	}
	
	public static int search(int[] A, int target) {
		if (A == null) {
			return -1;
		}
		
		int lo = 0;
		int hi = A.length - 1;
		
		while (lo <= hi) {
			int mid = lo + (hi - lo) / 2;
			
			if (A[mid] == target) {
				return mid;
			}
			else if (A[mid] < target) {
				lo = mid + 1;
			}
			else {
				hi = mid - 1;
			}
		}
		
		return -1;
	}

}
