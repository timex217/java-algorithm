package problems.linkedlist;
/**
 * Q30. Assume that in a Facebook server, 90% of the users revisited the webpage urls 
 * that they surfed yesterday three times on average for each url. 
 * Then how can we speed up the url parsing process?
 *
 * Design a LRU(Least Recently Used) Cache
 *
 * Principle of LRU: Purge the least recently used element out of the cache. 
 * We always update the priority of the most recently used element 
 * (when a cache is hit, update the element hit to the highest priority)
 * 
 */

import java.util.*;

public class LRU {
	
	/**
	 * Define the structure of each list node
	 * doubly linked list
	 */
	private class ListNode {
		String url;
		ListNode prev;
		ListNode next;
		ListNode(String s) {
			url = s;
			prev = null;
			next = null;
		}
	}
	
	private int size;
	private ListNode head;
	private ListNode tail;
	private static final int MAX = 100;
	private HashMap<String, ListNode> map;
	/**
	 * Constructor
	 */
	public LRU() {
		size = 0;
		head = null;
		tail = null;
		map = new HashMap<String, ListNode>();
	}
	
	/**
	 * Insert a new node to the front
	 */
	public void insert(ListNode node) {
		if (node == null) return;
		if (map.containsKey(node.url)) return;
		
		//
		if (size >= MAX) remove();
		
		// 
		if (size == 0) {
			head = node;
			tail = node;
		} 
		//
		else {
			head.prev = node;
			node.next = head;
			head = node;
		}
		
		//
		map.put(node.url, node);
		
		size++;
	}
	
	/**
	 * Purge the last element
	 */
	public void remove() {
		if (size == 0) {
			return;
		}
		
		ListNode node = tail;
		tail = node.prev;
		
		// remove from the map
		if (map.containsKey(node.url)) {
			map.remove(node.url);
		}
		
		node = null;
		size--;
		
		if (tail != null) {
			tail.next = null;
		} 
		else {
			head = null;
		}
	}
	
	/**
	 * Get node by url
	 */
	public ListNode get(String url) {
		if (size == 0) return null;
		if (!map.containsKey(url)) return null;
		
		ListNode node = map.get(url);
		
		if (node != head) {
			ListNode prev = node.prev;
			ListNode next = node.next;
			
			head.prev = node;
			node.next = head;
			head = node;
			
			if (prev != null) {
				prev.next = next;
			}
			
			if (next != null) {
				next.prev = prev;
			}
		}
		
		return node;
	}

}
