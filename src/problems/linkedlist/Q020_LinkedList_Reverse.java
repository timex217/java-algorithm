package problems.linkedlist;

public class Q020_LinkedList_Reverse {

	
	public static class ListNode {
		 int val;
		 ListNode next;
		 ListNode(int x) {
			 val = x;
			 next = null;
		 }
	}
	
	public static void main(String[] args) {
		
	}
	
	public static ListNode reverse(ListNode head) {
		if (head == null || head.next == null) {
			return head;
		}
		
		ListNode prev = null;
		ListNode curr = head;
		ListNode next = null;
		
		while (curr != null) {
			next = curr.next;
			curr.next = prev;
			prev = curr;
			curr = next;
		}
		
		return prev;
	}
	
	public static ListNode reverseBetween(ListNode head, int m, int n) {
        if (head == null || m == n) return head;
        
        int total = n - m + 1;
        ListNode left = null, prev = null, next = null, tail = null, curr = head;
        
        // go to position m
        while (m > 1) {
            left = curr;
            curr = curr.next;
            m--;
        }
        
        // mark the tail
        tail = curr;
        
        // reverse from m to n
        while (total > 0) {
            next = curr.next;
            curr.next = prev;
            prev = curr;
            curr = next;
            total--;
        }
        
        if (left == null) {
            head = prev;
        } else {
            left.next = prev;
        }
        
        tail.next = curr;
        
        return head;
    }

}
