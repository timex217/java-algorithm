package problems.arithmetic;

public class Q002_Greatest_Common_Divisor {

	public static void main(String[] args) {
		
	}
	
	// 欧几里德算法又称辗转相除法
	static int greatestCommonDivisor(int a, int b) {
		if (b == 0) {
			return a;
		}
		
		return greatestCommonDivisor(b, a % b);
	}

}
