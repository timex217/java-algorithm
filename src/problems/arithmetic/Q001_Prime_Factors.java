package problems.arithmetic;

import java.util.*;

public class Q001_Prime_Factors {

	public static void main(String[] args) {
		List<Integer> res = primeFactors(11786);
		System.out.println(res.toString());
	}
	
	static List<Integer> primeFactors(int n) {
		List<Integer> res = new ArrayList<Integer>();
		
		// Print the number of 2s that divide n
		while (n % 2 == 0) {
	        res.add(2);
	        n = n / 2;
	    }
		
		// n must be odd at this point.  So we can skip one element (Note i = i +2)
	    for (int i = 3; i <= Math.sqrt(n); i += 2) {
	        // While i divides n, print i and divide n
	        while (n % i == 0) {
	            res.add(i);
	            n = n / i;
	        }
	    }
	    
	    // This condition is to handle the case whien n is a prime number
	    // greater than 2
	    if (n > 2) {
	        res.add(n);
	    }
		
		return res;
	}

}
