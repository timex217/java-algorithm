/**
 * Compute 2^n
 */

package problems.arithmetic;

public class Q005_Compute_Power {
	
	public static void main(String[] args) {
		int result = power(2, 10);
		System.out.println(result);
	}
	
	public static int power(int base, int exponent) {
		// base case
		if (exponent == 0) {
			return 1;
		}
		
		int half = power(base, exponent/2);
		
		if (exponent % 2 == 0) {
			return half * half;
		}
		else {
			return half * half * base;
		}
	}

}
