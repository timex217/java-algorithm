package problems.arithmetic;

public class Q003_Swap_Number_Without_Temp {

	public static void main(String[] args) {
		int a = 10;
		int b = 15;
		swapNumb(a, b);
	}
	
	static void swapNum(int a, int b) {
		b = b - a;
		a = a + b;
		b = a - b;
		System.out.format("a: %d, b: %d\n", a, b);
	}
	
	static void swapNumb(int a, int b) {
		b = a ^ b;
		a = a ^ b;
		b = a ^ b;
		System.out.format("a: %d, b: %d\n", a, b);
	}

}
