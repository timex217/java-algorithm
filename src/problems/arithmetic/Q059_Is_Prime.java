package problems.arithmetic;

public class Q059_Is_Prime {
	
	public static void main(String[] args) {
		boolean res = isPrime(18345679);
		System.out.println(res);
	}
	
	public static boolean isPrime(int n) {
		if (n < 2) {
			return false;
		}
		
		if (n == 2) {
			return true;
		}
		
		if (n % 2 == 0) {
			return false;
		}
		
		for (int i = 3; i * i <= n; i += 2) {
			if (n % i == 0) {
				return false;
			}
		}
		
		return true;
	}

}
