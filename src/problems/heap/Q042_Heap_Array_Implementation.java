package problems.heap;

public class Q042_Heap_Array_Implementation {

	private static class Heap {
		
		// heap is an array that implies the rules of complete binary tree
		private int[] arr;
		
		// the size of data
		private int total;
		
		// initialize
		public Heap() {
			total = 0;
			// by default it's empty and arr[0] is not used
			arr = new int[2];
		}
		
		// heapify bottom up from kth element
		private void swim(int k) {
			// loop until hits the top or parent is greater
			while (k > 1 && arr[k/2] > arr[k]) {
				swap(k, k/2);
				k = k / 2;
			}
		}
		
		// heapify top down from kth element
		private void sink(int k) {
			// loop until hits the bottom
			while (2 * k <= total) {
				int i = 2 * k;
				// choose a smaller child to compare
				if (i < total && arr[i+1] < arr[i]) {
					i++;
				}
				// already smaller than the child
				if (arr[i] >= arr[k]) {
					break;
				}
				// otherwise, exchange parent and child
				swap(k, i);
				k = i;
			}
		}
		
		public void offer(int v) {
			if (total == arr.length - 1) {
				resize(total * 2 + 1);
			}
			
			arr[++total] = v;
			
			// heapify from bottom up
			swim(total);
		}
		
		/**
		 * 返回第一个，将最后一个放到第一个的位置，然后向下sink
		 */
		public int poll() {
			if (total == 0) {
				return -1;
			}
			
			int v = arr[1];
			
			swap(1, total--);
			
			// heapify top down
			sink(1);
			
			// resize
			if (total == (arr.length - 1) / 4) {
				resize(total * 2 + 1);
			}
			
			return v;
		}
		
		private void resize(int cap) {
			int[] tmp = new int[cap];
			for (int i = 1; i <= total; i++) {
				tmp[i] = arr[i];
			}
			arr = tmp;
		}
		
		private void swap(int i, int j) {
			int tmp = arr[i];
			arr[i] = arr[j];
			arr[j] = tmp;
		}
		
	}
	
	public static void main(String[] args) {
		Heap heap = new Heap();
		heap.offer(10);
		heap.offer(5);
		heap.offer(3);
		heap.offer(1);
		
		System.out.println(heap.poll());
		System.out.println(heap.poll());
		System.out.println(heap.poll());
		System.out.println(heap.poll());
	}

}
