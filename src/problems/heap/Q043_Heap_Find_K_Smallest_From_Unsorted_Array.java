package problems.heap;

import java.util.*;

public class Q043_Heap_Find_K_Smallest_From_Unsorted_Array {

	private static class Heap {
		
		// heap is an array that implies the rules of complete binary tree
		private int[] arr;
		
		// the size of data
		private int total;
		
		// initialize
		public Heap() {
			total = 0;
			// by default it's empty and arr[0] is not used
			arr = new int[2];
		}
		
		// heapify bottom up from kth element
		private void swim(int k) {
			// loop until hits the top or parent is greater
			while (k > 1 && arr[k/2] > arr[k]) {
				swap(k, k/2);
				k = k / 2;
			}
		}
		
		// heapify top down from kth element
		private void sink(int k) {
			// loop until hits the bottom
			while (2 * k <= total) {
				int i = 2 * k;
				// choose a smaller child to compare
				if (i < total && arr[i+1] < arr[i]) {
					i++;
				}
				// already smaller than the child
				if (arr[i] >= arr[k]) {
					break;
				}
				// otherwise, exchange parent and child
				swap(k, i);
				k = i;
			}
		}
		
		public void offer(int v) {
			if (total == arr.length - 1) {
				resize(total * 2 + 1);
			}
			
			arr[++total] = v;
			
			// heapify from bottom up
			swim(total);
		}
		
		public int poll() {
			if (total == 0) {
				return -1;
			}
			
			swap(1, total);
			int v = arr[total--];
			
			if (total == (arr.length - 1) / 4) {
				resize(total * 2 + 1);
			}
			
			// heapify top down
			sink(1);
			
			return v;
		}
		
		private void resize(int cap) {
			int[] tmp = new int[cap];
			for (int i = 1; i <= total; i++) {
				tmp[i] = arr[i];
			}
			arr = tmp;
		}
		
		private void swap(int i, int j) {
			int tmp = arr[i];
			arr[i] = arr[j];
			arr[j] = tmp;
		}
		
	}

	public static void main(String[] args) {
		int[] A = {8, 4, 6, 3, 2, 9, 0, 1, 5, 7};
		ArrayList<Integer> result = findKthSmallest(A, 9);
		
		for (int i = 0; i < result.size(); i++) {
			System.out.println(result.get(i));
		}
	}
	
	public static ArrayList<Integer> findKthSmallest(int[] A, int k) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		
		Heap heap = new Heap();
		for (int i = 0; i < A.length; i++) {
			heap.offer(A[i]);
		}
		
		for (int i = 0; i < k; i++) {
			result.add(heap.poll());
		}
		
		return result;
	}

}
