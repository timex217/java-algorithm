package problems.best_first_search;

/**
 * Q27. Given three arrays with numbers in ascending order. Pull one number from each array
 * to form a coordinates<x, y, z> in a 3D space. (1) How to find the coordinates of the points
 * that is k-th closest to <0, 0, 0>
 *
 * Best-First Search
 */

import java.util.*;

public class FindKth3DPointClosestToOrigin {
	
	private static int[] x = {1, 3, 5, 7, 9};
	private static int[] y = {2, 3, 4, 5, 8};
	private static int[] z = {1, 3, 4, 5, 6};
	
	/**
	 * Custom point class
	 */
	private static class Point {
		int i, j, k;  // indecies of x[], y[], z[]
		int distance;
		
		Point(int i, int j, int k) {
			this.i = i;
			this.j = j;
			this.k = k;
			this.distance = x[i] * x[i] + y[j] * y[j] + z[k] * z[k];
		}
	}
	
	public static void main(String[] args) {
		Point p = find(10);
	}
	
	public static Point find(int k) {
		if (k < 1) return null;
		
		int count = 0;
		
		// step 1. create a priority queue (min heap)
		PriorityQueue<Point> heap = new PriorityQueue<Point>(5, new Comparator<Point>(){
			@Override
			public int compare(Point p1, Point p2) {
				return p1.distance - p2.distance;
			}
		});
		
		// step 2. create a hash map to avoid duplication
		Set<String> map = new HashSet<String>();
		
		// step 3. initial state
		Point p = new Point(0, 0, 0);
		heap.offer(p);
		
		// step 4. check and expand
		while (!heap.isEmpty()) {
			p = heap.poll();
			
			if (p != null) {
				System.out.format("x: %d, y: %d, z: %d\n", x[p.i], y[p.j], z[p.k]);
			}
			
			if (++count == k) return p;
			
			// generate new states
			Point p1 = new Point(p.i + 1, p.j, p.k);
			Point p2 = new Point(p.i, p.j + 1, p.k);
			Point p3 = new Point(p.i, p.j, p.k + 1);
			
			heap.offer(p1);
			heap.offer(p2);
			heap.offer(p3);
		}
		
		return null;
	}
	
}
