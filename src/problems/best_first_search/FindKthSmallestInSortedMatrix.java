package problems.best_first_search;
/**
 * Given a matrix of size NxN, and for each row the elements are sorted in an ascending order. 
 * And for each column the elements are also sorted in an ascending order. 
 * How to find the k-th smallest element in it? 
 */

import java.util.*;

public class FindKthSmallestInSortedMatrix {
	
	public static int[][] M = {
		{1, 2, 3, 4, 5},
		{2, 3, 4, 5, 6},
		{3, 4, 5, 6, 7},
		{4, 5, 6, 7, 8},
		{5, 6, 7, 8, 9}
	};
	
	private static class Item {
		int i;
		int j;
		int val;
		Item(int i, int j) {
			this.i = i;
			this.j = j;
			this.val = M[i][j];
		}
	}
	
	public static void main(String[] args) {
		int k = 13;
		int val = find(k);
		if (val != -1) {
			System.out.format("The %d smallest number is: %d\n", k, val);
		}
		else {
			System.out.println("Not found");
		}
	}
	
	public static int find(int k) {
		// Step 1. Create a min heap with size k and a hash map to avoid duplication
		PriorityQueue<Item> heap = new PriorityQueue<Item>(k, new Comparator<Item>() {
			@Override
			public int compare(Item n1, Item n2) {
				return n1.val - n2.val;
			}
		});
		
		Set<String> map = new HashSet<String>();
		
		// Step 2. Initial state
		int count = 0;
		heap.offer(new Item(0, 0));
		
		// Step 3. Expand the generate new states
		while (!heap.isEmpty()) {
			Item n = heap.poll();
			
			if (++count == k) {
				return n.val;
			}
			
			// right direction
			int i = n.i;
			int j = n.j + 1;
			
			if (j < M.length && !map.contains(getKey(i, j))) {
				heap.offer(new Item(i, j));
				map.add(getKey(i, j));
			}
			
			// down direction
			i = n.i + 1;
			j = n.j;
			
			if (i < M.length && !map.contains(getKey(i, j))) {
				heap.offer(new Item(i, j));
				map.add(getKey(i, j));
			}
		}
		
		return -1;
	}
	
	public static String getKey(int i, int j) {
		return String.format("%d%d", i, j);
	}

}
