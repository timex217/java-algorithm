package problems.string;

public class Q058_String_Binary_Hex_To_Integer {
	
	public static void main(String[] args) {
		String binary = "0x11111111";
		String hex = "0xFF";
		
		int b = binary2Integer(binary);
		int h = hex2Integer(hex);
		
		System.out.format("%s => %d\n", binary, b);
		System.out.format("%s => %d\n", hex, h);
	}
	
	public static int binary2Integer(String s) {
		int sum = 0;
		
		for (int i = 2; i < s.length(); i++) {
			int d = s.charAt(i) - '0';
			sum = sum * 2 + d;
		}
		
		return sum;
	}
	
	public static int hex2Integer(String s) {
		int sum = 0;
		
		for (int i = 2; i < s.length(); i++) {
			char ch = s.charAt(i);
			int d = digit(ch);
			sum = sum * 16 + d;
		}
		
		return sum;
	}
	
	public static int digit(char ch) {
		if (ch >= '0' && ch <= '9') {
			return ch - '0';
		}
		else if (ch >= 'a' && ch <= 'f') {
			return ch - 'a' + 10;
		}
		else {
			return ch - 'A' + 10;
		}
	}

}
