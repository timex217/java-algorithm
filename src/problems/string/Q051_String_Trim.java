package problems.string;

public class Q051_String_Trim {
	
	public static void main(String[] args) {
		String str = "  google apple yahoo  microsoft   amazon    ";
		char[] s = str.toCharArray();
		System.out.println(s.length);
		trim(s);
		System.out.println(s.length);
		System.out.println(s);
	}
	
	public static void trim(char[] s) {
		if (s == null) {
			return;
		}
		
		int n = s.length, i = 0, j = 0;
		
		while (j < n) {
			char ch = s[j];
			
			if (ch != ' ') {
				s[i++] = s[j++];
			}
			else {
				while (j < n && s[j] == ' ') {
					j++;
				}
				
				if (i != 0 && j != n) {
					s[i++] = ' ';
				}
			}
		}
	}

}
