package problems.string;

public class Q052_String_I_Love_Google {
	
	public static void main(String[] args) {
		String str = "I love Google";
		char[] s = str.toCharArray();
		reverse(s);
		System.out.println(s);
	}
	
	public static void reverse(char[] s) {
		if (s == null || s.length == 0) {
			return;
		}
		
		int n = s.length;
		
		// step 1. reverse the whole string
		reverse(s, 0, n - 1);
		
		// step 2. reverse each word
		int i = 0, j = 0;
		boolean inWord = false;
		
		for (j = 0; j < n; j++) {
			char ch = s[j];
			
			if (ch != ' ') {
				if (!inWord) i = j;
				
				inWord = true;
				
				if (j == n - 1) reverse(s, i, j);
			}
			else {
				if (inWord) reverse(s, i, j - 1);
				
				inWord = false;
			}
		}
	}
	
	public static void reverse(char[] s, int start, int end) {
		if (s == null || s.length == 0) {
			return;
		}
		
		int i = start, j = end;
		
		while (i < j) {
			swap(s, i++, j--);
		}
	}
	
	public static void swap(char[] s, int i, int j) {
		char tmp = s[i];
		s[i] = s[j];
		s[j] = tmp;
	}

}
