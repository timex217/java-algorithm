package problems.string;

import java.util.*;

public class Q056_String_Permutation {

	public static void main(String[] args) {
		char[] chars = {'1', '2', '3', '1'};
		
		Set<String> set = new HashSet<String>();
		
		perm(chars, 0, set);
	}
	
	public static void perm(char[] chars, int k, Set<String> set) {
		int n = chars.length;
		
		if (k == n - 1) {
			String s = new String(chars);
			
			if (!set.contains(s)) {
				System.out.format("%s ", new String(chars));
				set.add(s);
			}
			
			return;
		}
		
		for (int i = k; i < n; i++) {
			swap(chars, k, i);
			perm(chars, k + 1, set);
			swap(chars, k, i);
		}
	}
	
	private static void swap(char[] chars, int i, int j) {
		char tmp = chars[i];
		chars[i] = chars[j];
		chars[j] = tmp;
	}
	
	/**
	 * Method 2
	 */
	public static ArrayList<String> perm(String s) {
		ArrayList<String> res = new ArrayList<String>();
		helper("", s, res);
		return res;
	}
	
	public static void helper(String prefix, String remain, ArrayList<String> res) {
		if (remain.length() == 0) {
			res.add(prefix);
		}
		
		for (int i = 0; i < remain.length(); i++) {
			helper(prefix + remain.charAt(i), remain.substring(0, i) + remain.substring(i), res);
		}
	}

}
