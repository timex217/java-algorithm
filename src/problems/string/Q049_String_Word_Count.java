package problems.string;

public class Q049_String_Word_Count {
	
	public static void main(String[] args) {
		String s = "  google apple yahoo  microsoft   amazon    ";
		int count = wordCount(s);
		System.out.format("%d words\n", count);
	}
	
	public static int wordCount(String s) {
		// sanity check
		if (s == null || s.length() == 0) {
			return 0;
		}
		
		int count = 0;
		boolean inWord = false;
		
		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			
			if (ch == ' ') {
				inWord = false;
			}
			else if (!inWord) {
				inWord = true;
				count++;
			}
		}
		
		return count;
	}

}
