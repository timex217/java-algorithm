package problems.string;
/**
 * Q32. Convert String "ABCD1234" to "A1B2C3D4"
 * 
 * Time complexity: O(nlogn)
 * Space complexity: O(logn)
 */

import java.util.*;

public class StringConversion1 {

	public static void main(String[] args) {
		char[] chars = {'A', 'B', 'C', 'D', 'E', '1', '2', '3', '4', '5'};
		
		//convert(chars, 0, chars.length - 1);
		convertHalf(chars, 0, chars.length - 1);
		
		System.out.println(chars);
	}
	/*
	public static void convert(char[] chars, int lo, int hi) {
		if (lo >= hi) return;
		int n = hi - lo + 1 - 2;
		
		// keep the head the tail, rotate the middle
		rotateLeft(chars, lo + 1, hi - 1, n / 2);
		
		// recursively convert the left
		convert(chars, lo + 1, hi - 1);
	}
	*/
	
	public static void convertHalf(char[] chars, int lo, int hi) {
		// base case
		if (lo + 1 >= hi) return;
		
		// find the left and right middle point
		int mid = lo + (hi - lo) / 2;
		
		int left_mid = lo + (mid - lo) / 2;
		int right_mid = mid + 1 + (hi - mid - 1) / 2;
		
		// rotate from left_mid + 1 to right _mid
		rotateLeft(chars, left_mid + 1, right_mid, mid - left_mid);
		
		mid = left_mid + right_mid - mid;
		
		convertHalf(chars, lo, mid);
		convertHalf(chars, mid + 1, hi);
	}
	
	/**
	 * Left rotate string from lo to hi with k moves
	 */
	private static void rotateLeft(char[] chars, int lo, int hi, int k) {
		// step 1. reverse from lo to hi
		reverse(chars, lo, hi);
		
		// step 2. reverse [lo, lo + (n - k)) and [lo + (n - k), hi] respectively
		reverse(chars, lo, hi - k);
		reverse(chars, hi - k + 1, hi);
	}
	
	private static void reverse(char[] chars, int lo, int hi) {
		int i = lo, j = hi;
		
		while (i < j) {
			char tmp = chars[i];
			chars[i] = chars[j];
			chars[j] = tmp;
			i++; 
			j--;
		}
	}

}
