/**
 * "345" -> 345
 */

package problems.string;

public class Q054_String_To_Integer {
	
	public static void main(String[] args) {
		String s = "345";
		int num = toInteger(s);
		System.out.format("%d", num);
	}
	
	public static int toInteger(String s) {
		int sum = 0;
		
		for (int i = 0; i < s.length(); i++) {
			char ch = s.charAt(i);
			int d = ch - '0';
			sum = sum * 10 + d;
		}
		
		return sum;
	}

}
