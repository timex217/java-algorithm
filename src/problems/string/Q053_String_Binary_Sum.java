package problems.string;

import java.util.*;

public class Q053_String_Binary_Sum {
	
	public static void main(String[] args) {
		String a = "0x1001";
		String b = "0x101";
		String c = sum(a, b);
		System.out.println(c);
	}
	
	public static String sum(String a, String b) {
		StringBuffer sb = new StringBuffer();
		
		int i = a.length() - 1;
		int j = b.length() - 1;
		
		int carry = 0;
		
		while (a.charAt(i) != 'x' || b.charAt(j) != 'x' || carry > 0) {
			int d_a = a.charAt(i) == 'x' ? 0 : a.charAt(i) - '0';
			int d_b = b.charAt(j) == 'x' ? 0 : b.charAt(j) - '0';
			
			int d = (d_a + d_b + carry) % 2;
			carry = (d_a + d_b + carry) / 2;
			
			sb.insert(0, d);
			
			if (a.charAt(i) != 'x') i--;
			if (b.charAt(j) != 'x') j--;
		}
		
		sb.insert(0, "0x");
		
		return sb.toString();
	}

}
