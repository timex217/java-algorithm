/**
 * 9 -> "0x1001"
 */

package problems.string;

public class Q057_String_Integer_To_Binary_Hex {
	
	public static void main(String[] args) {
		int n = 255;
		String binary = toBinary(n);
		String hex = toHex(n);
		System.out.println(binary);
		System.out.println(hex);
	}
	
	public static String toBinary(int n) {
		StringBuffer sb = new StringBuffer();
		
		while (n > 0) {
			int mod = n % 2;
			sb.insert(0, mod);
			n = n / 2;
		}
		
		sb.insert(0, "0x");
		
		return sb.toString();
	}
	
	public static String toHex(int n) {
		StringBuffer sb = new StringBuffer();
		
		char[] table = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
		
		while (n > 0) {
			int mod = n % 16;
			sb.insert(0, table[mod]);
			n = n / 16;
		}
		
		sb.insert(0, "0x");
		
		return sb.toString();
	}

}
