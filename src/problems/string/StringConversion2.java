package problems.string;
/**
 * Q33. Convert String "A1B2C3D4" to "ABCD1234"
 */

public class StringConversion2 {
	
	public static void main(String[] args) {
		char[] chars = {'A', '1', 'B', '2', 'C', '3', 'D', '4', 'E', '5'};
		
		//convert(chars, 0, chars.length - 1);
		convertHalf(chars, 0, chars.length - 1);
		
		System.out.println(chars);
	}
	/*
	public static void convert(char[] chars, int lo, int hi) {
		if (lo >= hi) return;
		int n = hi - lo + 1;
		
		// recursively convert
		convert(chars, lo + 1, hi - 1);
		
		// rotate left
		rotateLeft(chars, lo + 1, hi - 1, n / 2);
	}
	*/
	public static void convertHalf(char[] chars, int lo, int hi) {
		// base case 
		if (lo + 1 >= hi) return;
		
		int mid = lo + (hi - lo) / 2;
		
		int left_mid = lo + (mid - lo) / 2;
		int right_mid = mid + 1 + (hi - mid - 1) / 2;
		
		mid = left_mid + right_mid - mid;
		
		convertHalf(chars, lo, mid);
		convertHalf(chars, mid + 1, hi);
		
		// rotate left
		rotateLeft(chars, left_mid + 1, right_mid, mid - left_mid);
	}
	
	/**
	 * Left rotate string from lo to hi with k moves
	 */
	private static void rotateLeft(char[] chars, int lo, int hi, int k) {
		// step 1. reverse from lo to hi
		reverse(chars, lo, hi);
		
		// step 2. reverse [lo, hi - k) and [hi - k + 1, hi] respectively
		reverse(chars, lo, hi - k);
		reverse(chars, hi - k + 1, hi);
	}
	
	private static void reverse(char[] chars, int lo, int hi) {
		int i = lo, j = hi;
		
		while (i < j) {
			char tmp = chars[i];
			chars[i] = chars[j];
			chars[j] = tmp;
			i++; j--;
		}
	}
}
