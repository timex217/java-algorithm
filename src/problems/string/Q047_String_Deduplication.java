/**
 * "aaaabbbcc" -> "abc" 
 */

package problems.string;

import java.util.*;

public class Q047_String_Deduplication {
	
	public static void main(String[] args) {
		String s = "aaaabbb_ccaa";
		s = deduplication(s);
		System.out.println(s);
	}
	
	public static String deduplication(String s) {
		if (s == null || s.length() == 0) {
			return s;
		}
		
		StringBuffer sb = new StringBuffer();
		sb.append(s.charAt(0));
		
		int i = 0, j = 1;
		
		for (j = 1; j < s.length(); j++) {
			char ch = s.charAt(j);
			if (ch != s.charAt(i)) {
				sb.append(ch);
				i = j;
			}
		}
		
		return sb.toString();
	}

}
