package problems.string;

public class Q048_String_Replacement {
	
	public static void main(String[] args) {
		String s = "20%google20%apple20%yahoo20%microsoft20%amazon20%";
		s = replace(s, "20%", "-");
		System.out.println(s);
		
		s = "-google-apple-yahoo-microsoft-amazon-";
		s = replace(s, "-", "20%");
		
		System.out.println(s);
	}
	
	public static String replace(String s, String old, String rep) {
		// step 1. count how many old strings
		int count = 0;
		int n = s.length();
		int n_old = old.length();
		int n_rep = rep.length();
		
		for (int i = 0; i + n_old <= n; i++) {
			if (s.substring(i, i + n_old).equals(old)) {
				count++;
			}
		}
		
		// step 2. build new chars
		int n_new = n + (n_rep - n_old) * count;
		char[] chars = new char[n_new];
		
		// copy from end to front
		if (n_new > n) {
			int i = n - 1;
			int j = n_new - 1;
			
			while (i >= 0) {
				if (s.substring(i + 1 - n_old, i + 1).equals(old)) {
					for (int k = n_rep - 1; k >= 0; k--) chars[j--] = rep.charAt(k);
					i -= n_old;
				}
				else {
					chars[j--] = s.charAt(i--);
				}
			}
		}
		// copy from front to end
		else {
			int i = 0;
			int j = 0;
			
			while (i < n) {
				if (i + n_old <= n && s.substring(i, i + n_old).equals(old)) {
					for (int k = 0; k < n_rep; k++) chars[j++] = rep.charAt(k);
					i += n_old;
				}
				else {
					chars[j++] = s.charAt(i++);
				}
			}
		}
		
		return new String(chars);
	}

}
