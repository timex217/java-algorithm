package problems.string;

public class Q050_String_Reverse {
	
	public static void main(String[] args) {
		String str = "Yong Su & Jing Jin";
		char[] s = str.toCharArray();
		reverse(s);
		System.out.println(s);
	}
	
	public static void reverse(char[] s) {
		if (s == null) {
			return;
		}
		
		int i = 0, j = s.length - 1;
		
		while (i < j) {
			swap(s, i++, j--);
		}
	}
	
	public static void swap(char[] s, int i, int j) {
		char tmp = s[i];
		s[i] = s[j];
		s[j] = tmp;
	}

}
