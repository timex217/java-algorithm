package problems.leetcode;

public class Q005_Valid_Palindrome {
	
	public static void main(String[] args) {
		String s = "a.";
		boolean res = isPalindrome(s);
		System.out.println(res);
	}
	
	static boolean isPalindrome(String s) {
        if (s == null) {
            return false;
        }
        
        s = s.toLowerCase();
        
        int i = 0, j = s.length() - 1;
        
        while (i < j) {
            while (i < j && !isValid(s.charAt(i))) {
                i++;
            }
            
            while (i < j && !isValid(s.charAt(j))) {
                j--;
            }
            
            if (i < j && s.charAt(i++) != s.charAt(j--)) {
                return false;
            }
        }
        
        return true;
    }
    
    static boolean isValid(char ch) {
        return (ch >= 'a' && ch <= 'z') || (ch >= '0' && ch <= '9');
    }
	
}
