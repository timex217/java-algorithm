package problems.leetcode;

/**
 * Combinations
 */

import java.util.*;

public class Q008A_Combinations {

	public static void main(String[] args) {
		int[] S = {1, 1, 2, 3, 4};
		List<List<Integer>> res = combinations(S, 2);
		
		for (int i = 0; i < res.size(); i++) {
			System.out.println(res.get(i).toString());
		}
	}
	
	// -------------------------------
	//  Recursion
	// -------------------------------
	static List<List<Integer>> combinations(int[] S, int k) {
		Arrays.sort(S);
		
		List<List<Integer>> res = new ArrayList<List<Integer>>();
		List<Integer> sol = new ArrayList<Integer>();
		
		helper(S, k, 0, sol, res);
		return res;
	}

	static void helper(int[] S, int k, int index, List<Integer> sol, List<List<Integer>> res) {
		if (sol.size() == k) {
			res.add(new ArrayList<Integer>(sol));
			return;
		}
		
		for (int i = index; i < S.length; i++) {
			// 我们只对于第一次得到这个数进行递归，接下来就跳过这个元素了，
			// 因为接下来的情况会在上一层的递归函数被考虑到，这样就可以避免重复元素的出现
			if (i > index && S[i] == S[i - 1]) {
				continue;
			}
			
			sol.add(S[i]);
			helper(S, k, i + 1, sol, res);
			sol.remove(sol.size() - 1);
		}
	}
	
}
