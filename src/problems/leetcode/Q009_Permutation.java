package problems.leetcode;

/**
 * Permutation
 */

import java.util.*;

public class Q009_Permutation {

	public static void main(String[] args) {
		int[] S = {1, 2, 3, 4};
		List<List<Integer>> res = permutation3(S);
		
		for (int i = 0; i < res.size(); i++) {
			System.out.println(res.get(i).toString());
		}
	}
	
	// -----------------------------
	//  Method 1: 精挑细选法
	// -----------------------------
	static List<List<Integer>> permutation(int[] S) {
		Arrays.sort(S);
		
		List<List<Integer>> res = new ArrayList<List<Integer>>();
		List<Integer> sol = new ArrayList<Integer>();
		boolean[] visited = new boolean[S.length];
		
		helper(S, visited, sol, res);
		
		return res;
	}
	
	static void helper(int[] S, boolean[] visited, List<Integer> sol, List<List<Integer>> res) {
		if (sol.size() == S.length) {
			res.add(new ArrayList<Integer>(sol));
			return;
		}
		
		for (int i = 0; i < S.length; i++) {
			if (!visited[i]) {
				visited[i] = true;
				sol.add(S[i]);
				
				helper(S, visited, sol, res);
				
				sol.remove(sol.size() - 1);
				visited[i] = false;
			}
		}
	}
	
	// -----------------------------
	//  Method 2: 两两交换法
	// -----------------------------
	static List<List<Integer>> permutation2(int[] S) {
		List<List<Integer>> res = new ArrayList<List<Integer>>();
		
		helper(S, 0, res);
		
		return res;
	}
	
	static void helper(int[] S, int index, List<List<Integer>> res) {
		if (index == S.length - 1) {
			res.add(getList(S));
			return;
		}
		
		for (int i = index; i < S.length; i++) {
			swap(S, index, i);
			helper(S, index + 1, res);
			swap(S, index, i);
		}
	}
	
	static void swap(int[] S, int i, int j) {
		int tmp = S[i];
		S[i] = S[j];
		S[j] = tmp;
	}
	
	static List<Integer> getList(int[] S) {
		List<Integer> res = new ArrayList<Integer>();
		for (int i = 0; i < S.length; i++) {
			res.add(S[i]);
		}
		return res;
	}
	
	// -----------------------------
	//  Method 3: 插入法 (Recursion)
	// -----------------------------
	static List<List<Integer>> permutation3(int[] S) {
		List<List<Integer>> res = new ArrayList<List<Integer>>();
		
		res = perm(S, S.length - 1, res);
		
		return res;
	}
	
	static List<List<Integer>> perm(int[] S, int index, List<List<Integer>> res) {
		if (index == 0) {
			List<Integer> sol = new ArrayList<Integer>();
			sol.add(S[0]);
			res.add(sol);
			return res;
		}
		
		res = perm(S, index - 1, res);
		
		List<List<Integer>> newRes = new ArrayList<List<Integer>>();
		
		for (int i = 0; i < res.size(); i++) {
			List<Integer> curr = res.get(i);
			
			for (int j = 0; j <= curr.size(); j++) {
				List<Integer> item = new ArrayList<Integer>(curr);
				item.add(j, S[index]);
				newRes.add(item);
			}
		}
		
		return newRes;
	}
	
	// -----------------------------
	//  Method 4: 插入法 (Iterative)
	// -----------------------------
	static List<List<Integer>> permutation4(int[] S) {
		List<List<Integer>> res = new ArrayList<List<Integer>>();
		
		// Add the first element
		List<Integer> first = new ArrayList<Integer>();
		first.add(S[0]);
		res.add(first);
		
		for (int i = 1; i < S.length; i++) {
			List<List<Integer>> newRes = new ArrayList<List<Integer>>();
			
			for (int j = 0; j < res.size(); j++) {
				List<Integer> curr = res.get(j);
				
				for (int k = 0; k <= curr.size(); k++) {
					List<Integer> item = new ArrayList<Integer>(curr);
					item.add(k, S[i]);
					newRes.add(item);
				}
			}
			
			res = new ArrayList<List<Integer>>(newRes);
		}
		
		return res;
	}
}
