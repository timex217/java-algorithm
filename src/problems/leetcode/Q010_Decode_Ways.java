package problems.leetcode;

/**
 * Decode ways
 * 
 * A message containing letters from A-Z is being encoded to numbers using the following mapping:
 * 
 * 'A' -> 1
 * 'B' -> 2
 * ...
 * 'Z' -> 26
 * Given an encoded message containing digits, determine the total number of ways to decode it.
 * 
 * For example,
 * Given encoded message "12", it could be decoded as "AB" (1 2) or "L" (12).
 * 
 * The number of ways decoding "12" is 2.
 */

import java.util.*;

public class Q010_Decode_Ways {

	public static void main(String[] args) {
		//String s = "12034";
		String s = "1787897759966261825913315262377298132516969578441236833255596967132573482281598412163216914566534565";
		int res = numDecodings(s);
		System.out.println(res);
	}
	
	// -----------------------------------
	//  Recursion
	// -----------------------------------
	static int numDecodingsR(String s) {
		if (s == null || s.length() == 0 || s.charAt(0) == '0') {
            return 0;
        }
		
        return helper(s, s.length());
    }
    
    static int helper(String s, int n) {
        if (n <= 1) {
            return 1;
        }
        
        int count = 0;
        
        char curr = s.charAt(n - 1);
        char prev = s.charAt(n - 2);
        
        if (curr > '0') {
            count = helper(s, n - 1);
        }
        
        if (prev == '1' || (prev == '2' && curr <= '6')) {
            count += helper(s, n - 2);
        }
        
        return count;
    }
    
    // -----------------------------------
 	//  DP
 	// -----------------------------------
    static int numDecodings(String s) {
    	if (s == null || s.length() == 0 || s.charAt(0) == '0') {
            return 0;
        }
        
        int n = s.length();
    	int[] dp = new int[n + 1];
    	dp[0] = 1;
    	dp[1] = 1;
    	
    	for (int i = 2; i <= n; i++) {
    		char curr = s.charAt(i - 1);
    		char prev = s.charAt(i - 2);
    		
    		int count = 0;
    		
    		if (curr > '0') {
    			count = dp[i - 1];
    		}
    		
    		if (prev == '1' || (prev == '2' && curr <= '6')) {
    			count += dp[i - 2];
    		}
    		
    		dp[i] = count;
    	}
    	
    	return dp[n];
    }
	
}
