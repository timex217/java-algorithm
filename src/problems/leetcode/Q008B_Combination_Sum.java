package problems.leetcode;

/**
 * Combination Sum
 */

import java.util.*;

public class Q008B_Combination_Sum {

	public static void main(String[] args) {
		int[] S = {21,46,35,20,20,44,31,29,23,45,37,33,34,39,42,24,40,41,26,22,38,36,27,25,49,48,43};
		//int[] S = {1, 1};
		List<List<Integer>> res = combinationSum(S, 176);
		
		for (int i = 0; i < res.size(); i++) {
			System.out.println(res.get(i).toString());
		}
	}
	
	// -------------------------------
	//  Recursion
	// -------------------------------
	static List<List<Integer>> combinationSum(int[] S, int sum) {
		Arrays.sort(S);
		
		List<List<Integer>> res = new ArrayList<List<Integer>>();
		List<Integer> sol = new ArrayList<Integer>();
		
		helper(S, sum, 0, sol, res);
		
		return res;
	}
	
	static void helper(int[] S, int sum, int index, List<Integer> sol, List<List<Integer>> res) {
		if (sum < 0) {
			return;
		}
		
		if (sum == 0) {
			res.add(new ArrayList<Integer>(sol));
			return;
		}
		
		for (int i = index; i < S.length; i++) {
			// 我们只对于第一次得到这个数进行递归，接下来就跳过这个元素了，
			// 因为接下来的情况会在上一层的递归函数被考虑到，这样就可以避免重复元素的出现
			// 这里，i > index 而不是 i > 0，是因为保证：在下一层递归中，如果那个元素和前一个相同，我们也允许将其添加到solution中
			// 如果用 i > 0, 那么这个元素就会被忽略！
			if (i > index && S[i] == S[i - 1]) {
				continue;
			}
			
			sol.add(S[i]);
			helper(S, sum - S[i], i + 1, sol, res);
			sol.remove(sol.size() - 1);
		}
	}
	
}
