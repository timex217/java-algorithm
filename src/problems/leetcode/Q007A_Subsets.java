package problems.leetcode;

/**
 * Subsets
 */

import java.util.*;

public class Q007A_Subsets {

	public static void main(String[] args) {
		int[] S = {1, 2, 3};
		List<List<Integer>> res = subsets3(S);
		
		for (int i = 0; i < res.size(); i++) {
			System.out.println(res.get(i).toString());
		}
	}
	
	// ----------------------------
	//  Recursion 1
	// ----------------------------
	static List<List<Integer>> subsets(int[] S) {
		Arrays.sort(S);
		
		List<List<Integer>> res = new ArrayList<List<Integer>>();
		List<Integer> sol = new ArrayList<Integer>();
		
		helper(S, 0, sol, res);
		
		return res;
	}
	
	static void helper(int[] S, int index, List<Integer> sol, List<List<Integer>> res) {
		if (index == S.length) {
			res.add(new ArrayList<Integer>(sol));
			return;
		}
		
		// do not add current number
		helper(S, index + 1, sol, res);
		
		// add current number
		sol.add(S[index]);
		helper(S, index + 1, sol, res);
		sol.remove(sol.size() - 1);
	}
	
	// ----------------------------
	//  Recursion 2
	// ----------------------------
	static List<List<Integer>> subsets2(int[] S) {
		Arrays.sort(S);
		
		List<List<Integer>> res = new ArrayList<List<Integer>>();
		
		helper(S, S.length - 1, res);
		
		return res;
	}
	
	static void helper(int[] S, int index, List<List<Integer>> res) {
		if (index == -1) {
			res.add(new ArrayList<Integer>());
			return;
		}
		
		helper(S, index - 1, res);
		
		int size = res.size();
		for (int i = 0; i < size; i++) {
			List<Integer> sol = new ArrayList<Integer>(res.get(i));
			sol.add(S[index]);
			res.add(sol);
		}
	}
	
	// -----------------------------
	//  Iterative
	// -----------------------------
	static List<List<Integer>> subsets3(int[] S) {
		Arrays.sort(S);
		
		List<List<Integer>> res = new ArrayList<List<Integer>>();
		res.add(new ArrayList<Integer>());
		
		for (int i = 0; i < S.length; i++) {
			int size = res.size();
			
			for (int j = 0; j < size; j++) {
				List<Integer> sol = new ArrayList<Integer>(res.get(j));
				sol.add(S[i]);
				res.add(sol);
			}
		}
		
		return res;
	}
	
	
}
