package problems.leetcode;

/**
 * Word Ladder
 * 
 * https://oj.leetcode.com/problems/word-ladder/
 * 
 * Given two words (start and end), and a dictionary, find the length of shortest transformation sequence from start to end, such that:
 * 
 * Only one letter can be changed at a time
 * Each intermediate word must exist in the dictionary
 * For example,
 * 
 * Given:
 * start = "hit"
 * end = "cog"
 * dict = ["hot","dot","dog","lot","log"]
 * As one shortest transformation is "hit" -> "hot" -> "dot" -> "dog" -> "cog",
 * return its length 5.
 * 
 * Note:
 * 		Return 0 if there is no such transformation sequence.
 * 		All words have the same length.
 * 		All words contain only lowercase alphabetic characters.
 */

import java.util.*;

public class Q003A_Word_Ladder {
	
	public static void main(String[] args) {
		String start = "hit";
		String end = "cog";
		
		Set<String> dict = new HashSet<String>();
		dict.add("hot");
		dict.add("dot");
		dict.add("dog");
		dict.add("lot");
		dict.add("log");
		
		int res = ladderLengthBFS(start, end, dict);
		
		System.out.println(res);
	}
	
	// ------------------------------
	//  BFS
	// ------------------------------
	
	static int ladderLengthBFS(String start, String end, Set<String> dict) {
		// base case
        if (start == null || end == null || start.length() == 0 || end.length() == 0 || start.length() != end.length()) {
            return 0;
        }
        
        int level = 1;
        
        Queue<String> queue = new LinkedList<String>();
        Set<String> visited = new HashSet<String>();
        
        queue.offer(start);
        queue.offer(null);
        
        visited.add(start);
        
        while (!queue.isEmpty()) {
            String curr = queue.poll();
            
            if (curr != null) {
                if (diff(curr, end) == 1) {
                    return level + 1;
                }
                
                for (String word : dict) {
    				if (diff(curr, word) == 1 && !visited.contains(word)) {
    				    queue.offer(word);
    				    visited.add(word);
    				}
                }
            } else {
                if (!queue.isEmpty()) {
                    queue.add(null);
                }
                
                level++;
            }
        }
        
        return 0;
	}
	
	
	// ------------------------------
	//  DFS
	// ------------------------------
	
	static void ladderLength(String start, String end, Set<String> dict) {
		// use a set to mark visited flag
		Set<String> set = new HashSet<String>();
		
		ArrayList<ArrayList<String>> res = new ArrayList<ArrayList<String>>();
		ArrayList<String> sol = new ArrayList<String>();
		
		dfs(start, end, dict, set, sol, res);
    }
	
	/**
	 * 
	 */
	static void dfs(String start, String end, Set<String> dict, Set<String> set, ArrayList<String> sol, ArrayList<ArrayList<String>> res) {
		// 处理当前解
		set.add(start);
		sol.add(start);
		
		// 检查是否需要终止
		if (diff(start, end) == 1) {
			res.add(new ArrayList<String>(sol));
			System.out.println(sol.toString());
		} else {
			// 处理下一层
			for (String word : dict) {
				if (diff(start, word) == 1 && !set.contains(word)) {
					dfs(word, end, dict, set, sol, res);
				}
			}
		}
		
		// 回溯, 恢复当前解
		sol.remove(sol.size() - 1);
		set.remove(start);
	}
	
	/**
	 * Return the difference between s1 and s2
	 */
	static int diff(String s1, String s2) {
		int d = 0, i = 0, j = 0;
		while (i < s1.length() && j < s2.length()) {
			if (s1.charAt(i++) != s2.charAt(j++)) {
				d++;
			}
		}
		return d;
	}
}
