package problems.leetcode;

/**
 * Surrounded Regions
 * 
 * https://oj.leetcode.com/problems/surrounded-regions/
 * 
 * Given a 2D board containing 'X' and 'O', capture all regions surrounded by 'X'.
 * 
 * A region is captured by flipping all 'O's into 'X's in that surrounded region.
 * 
 * For example,
 * X X X X
 * X O O X
 * X X O X
 * X O X X
 * After running your function, the board should be:
 * 
 * X X X X
 * X X X X
 * X X X X
 * X O X X
 */

import java.util.*;

public class Q002B_Surrounded_Regions {
	
	public static void main(String[] args) {
		char[][] board = {
			{'X','O','X','O','X','O','O','O','X','O'},
			{'X','O','O','X','X','X','O','O','O','X'},
			{'O','O','O','O','O','O','O','O','X','X'},
			{'O','O','O','O','O','O','X','O','O','X'},
			{'O','O','X','X','O','X','X','O','O','O'},
			{'X','O','O','X','X','X','O','X','X','O'},
			{'X','O','X','O','O','X','X','O','X','O'},
			{'X','X','O','X','X','O','X','O','O','X'},
			{'O','O','O','O','X','O','X','O','X','O'},
			{'X','X','O','X','X','X','X','O','O','O'}
		};
		
		solve(board);
		
		print(board);
	}
	
	static void solve(char[][] board) {
		if (board == null || board.length == 0 || board[0].length == 0) {
            return;
        }
		
		int n = board.length;
        int m = board[0].length;
        
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (board[i][j] == 'O' && !dfs(board, n, m, i, j)) {
                    fill(board, n, m, i, j);
                }
            }
        }
    }
    
    // whether we can reach the boarder from (i,j)
    static boolean dfs(char[][] board, int n, int m, int i, int j) {
    	int pos = i * m + j;
    	
    	// use a set to mark visited nodes
    	Set<Integer> set = new HashSet<Integer>();
    	
    	// use a stack to do dfs
    	Stack<Integer> stack = new Stack<Integer>();
    	stack.push(pos);
    	
    	while (!stack.isEmpty()) {
    		// 处理当前解
    		pos = stack.pop();
    		// 标记访问过
            set.add(pos);
            
            i = pos / m;
    		j = pos % m;
            
            // 查看是否已经到了边缘
            if (i == 0 || i == n - 1 || j == 0 || j == m - 1) {
            	return true;
            }
            
            // 否则处理下一层
            // up
            pos = (i - 1) * m + j;
            if (i > 0 && board[i - 1][j] == 'O' && !set.contains(pos)) {
            	stack.push(pos);
            }
            
            // down
            pos = (i + 1) * m + j;
            if (i < n - 1 && board[i + 1][j] == 'O' && !set.contains(pos)) {
            	stack.push(pos);
            }
            
            // left
            pos = i * m + j - 1;
            if (j > 0 && board[i][j - 1] == 'O' && !set.contains(pos)) {
            	stack.push(pos);
            }
            
            // right
            pos = i * m + j + 1;
            if (j < m - 1 && board[i][j + 1] == 'O' && !set.contains(pos)) {
            	stack.push(pos);
            }
    	}
    	
    	return false;
    }
    
    static void fill(char[][] board, int n, int m, int i, int j) {
    	int pos = i * m + j;
    	
    	Stack<Integer> stack = new Stack<Integer>();
    	stack.push(pos);
    	
    	while (!stack.isEmpty()) {
    		// 处理当前解
    		pos = stack.pop();
    		
    		i = pos / m;
    		j = pos % m;
    		
    		board[i][j] = 'X';
    		
    		// 处理下一层
    		if (i > 0 && board[i - 1][j] == 'O') {
    			stack.push((i - 1) * m + j);
    		}
    		
    		if (i < n - 1 && board[i + 1][j] == 'O') {
    			stack.push((i + 1) * m + j);
    		}
    		
    		if (j > 0 && board[i][j - 1] == 'O') {
    			stack.push(i * m + j - 1);
    		}
    		
    		if (j < m - 1 && board[i][j + 1] == 'O') {
    			stack.push(i * m + j + 1);
    		}
    	}
    }
    
    static void print(char[][] board) {
    	if (board == null || board.length == 0) {
            return;
        }
    	
    	int n = board.length;
        
        for (int i = 0; i < n; i++) {
        	System.out.println(Arrays.toString(board[i]));
        }
    }
	
}
