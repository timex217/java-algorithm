package problems.leetcode;

/**
 * Print all the paths 
 */

import java.util.*;

public class Q004A_DFS_Print_All_Paths {
	
	public static void main(String[] args) {
		int[][] board = {
			{1, 0, 1, 1, 1},
			{1, 1, 1, 0, 0},
			{1, 0, 1, 1, 1},
			{1, 1, 1, 0, 1},
			{1, 1, 1, 1, 1},
		};
		
		printAllPaths(board, 5, 5);
	}
	
	static void printAllPaths(int[][] board, int n, int m) {
		Set<Integer> set = new HashSet<Integer>();
		ArrayList<ArrayList<Integer>> res = new ArrayList<ArrayList<Integer>>();
		ArrayList<Integer> sol = new ArrayList<Integer>();
		
		dfs(board, n, m, 0, 0, set, sol, res);
	}
	
	static void dfs(int[][] board, int n, int m, int i, int j, Set<Integer> set, ArrayList<Integer> sol, ArrayList<ArrayList<Integer>> res) {
		// 处理当前解
		set.add(i * m + j);
		sol.add(i * m + j);
		
		// 查看是否需要剪枝或者终止
		if (i == n - 1 && j == m - 1) {
			System.out.println(sol.toString());
			res.add(new ArrayList<Integer>(sol));
		} else {
			// 处理下一层
			// up
			if (i > 0 && board[i - 1][j] == 1 && !set.contains((i - 1) * m + j)) {
				dfs(board, n, m, i - 1, j, set, sol, res);
			}
			
			// down
			if (i < n - 1 && board[i + 1][j] == 1 && !set.contains((i + 1) * m + j)) {
				dfs(board, n, m, i + 1, j, set, sol, res);
			}
			
			// left
			if (j > 0 && board[i][j - 1] == 1 && !set.contains((i * m + j - 1))) {
				dfs(board, n, m, i, j - 1, set, sol, res);
			}
			
			// right
			if (j < m - 1 && board[i][j + 1] == 1 && !set.contains((i * m + j + 1))) {
				dfs(board, n, m, i, j + 1, set, sol, res);
			}
		}
		
		// 回溯, 恢复当前解
		set.remove(i * m + j);
		sol.remove(sol.size() - 1);
	}
	
	static void printStack(Stack<Integer> stack) {
		if (stack.isEmpty()) {
			return;
		}
		
		int element = stack.pop();
		System.out.format("%d ", element);
		printStack(stack);
		stack.push(element);
	}
	
}
