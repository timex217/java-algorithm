package problems.leetcode;

/**
 * Reverse Words
 */

public class Q011_Reverse_Words {

	public static void main(String[] args) {
		String s = "   I   love       Google !  ";
		s = reverseWords(s);
		System.out.println(s);
	}
	
	static String reverseWords(String s) {
        if (s == null) {
			return null;
		}
        
        s = s.trim();
        
        int n = s.length();
		char[] chars = s.toCharArray();
		
		// step 1. reverse the whole string
		reverse(chars, 0, n - 1);
		
		// step 2. reverse each word
		reverseWords(chars, n);
		
		// step 3. trim spaces
		n = trimSpaces(chars, n);
		
		// return the final string
		return new String(chars).substring(0, n);
    }
	
	static void reverseWords(char[] chars, int n) {
		int i = 0, j = 0;
		
		while (i < n) {
			i = j;
		    while (i < n && chars[i] == ' ') { i++; }
		    
		    if (i >= n) { break; }
		    
		    j = i;
		    while (j < n && chars[j] != ' ') { j++; }
		    
		    // found a word, reverse it
		    reverse(chars, i, j - 1);
		}
	}
	
	static int trimSpaces(char[] chars, int n) {
		int i = 0, j = 0;
		
		while (j < n) {
			while (j < n && chars[j] != ' ') { chars[i++] = chars[j++]; }
			
			if (j >= n) { break; }
			
			while (j < n && chars[j] == ' ') { j++; }
			
			chars[i++] = ' ';
		}
		
		return i;
	}
    
    static void reverse(char[] chars, int i, int j) {
		while (i < j) {
		    swap(chars, i++, j--);
		}
	}
	
	static void swap(char[] chars, int i, int j) {
	    char tmp = chars[i];
		chars[i] = chars[j];
		chars[j] = tmp;
	}
	
}
