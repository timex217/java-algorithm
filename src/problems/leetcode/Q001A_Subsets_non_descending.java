package problems.leetcode;

/**
 * Subsets
 * 
 * https://oj.leetcode.com/problems/subsets/
 * 
 * Given a set of distinct integers, S, return all possible subsets.
 * 
 * Note:
 * Elements in a subset must be in non-descending order.
 * The solution set must not contain duplicate subsets.
 * For example,
 * If S = [1,2,3], a solution is:
 * 
 * [
 *   [3],
 *   [1],
 *   [2],
 *   [1,2,3],
 *   [1,3],
 *   [2,3],
 *   [1,2],
 *   []
 * ]
 */

import java.util.*;

public class Q001A_Subsets_non_descending {

	public static void main(String[] args) {
		int[] S = {1, 2, 3};
		List<List<Integer>> res = subsets(S);
		
		for (int i = 0; i < res.size(); i++) {
			System.out.println(res.get(i).toString());
		}
	}
	
	static List<List<Integer>> subsets(int[] S) {
        // sanity check
        if (S == null) {
            return null;
        }
        
        // must sort the S first!
        Arrays.sort(S);
        
        List<List<Integer>> res = new ArrayList<List<Integer>>();
        
        // add an empty list to result
        res.add(new ArrayList<Integer>());
        
        for (int i = 0; i < S.length; i++) {
            int size = res.size();
            
            for (int j = 0; j < size; j++) {
                // make a copy of the existing subset
                ArrayList<Integer> set = new ArrayList<Integer>(res.get(j));
                // add current element
                set.add(S[i]);
                // add new subset to the result
                res.add(set);
            }
        }
        
        return res;
    }
	
}
