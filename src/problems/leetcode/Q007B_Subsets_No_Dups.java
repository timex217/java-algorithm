package problems.leetcode;

/**
 * Subsets without duplication
 */

import java.util.*;

public class Q007B_Subsets_No_Dups {

	public static void main(String[] args) {
		int[] S = {1, 2, 2};
		List<List<Integer>> res = subsetsR(S);
		
		for (int i = 0; i < res.size(); i++) {
			System.out.println(res.get(i).toString());
		}
	}
	
	// ---------------------------
	//  Iterative
	// ---------------------------
	static List<List<Integer>> subsets(int[] S) {
		Arrays.sort(S);
		
		List<List<Integer>> res = new ArrayList<List<Integer>>();
		res.add(new ArrayList<Integer>());
		
		int start = 0;
		
		for (int i = 0; i < S.length; i++) {
			int size = res.size();
			
			for (int j = start; j < size; j++) {
				List<Integer> sol = new ArrayList<Integer>(res.get(j));
				sol.add(S[i]);
				res.add(sol);
			}
			
			// avoid duplication, only add to the 2nd half
			if (i < S.length - 1 && S[i] == S[i + 1]) {
				start = size;
			} else {
				start = 0;
			}
		}
		
		return res;
	}
	
	// ----------------------------
	//  Recursion
	// ----------------------------
	static int lastSize = 0;
	
	static List<List<Integer>> subsetsR(int[] S) {
		Arrays.sort(S);
		
		List<List<Integer>> res = new ArrayList<List<Integer>>();
		
		lastSize = 0;
		
		helper(S, S.length - 1, res);
		
		return res;
	}
	
	static void helper(int[] S, int index, List<List<Integer>> res) {
		if (index == -1) {
			res.add(new ArrayList<Integer>());
			return;
		}
		
		helper(S, index - 1, res);
		
		int start = 0;
		
		if (index > 0 && S[index] == S[index - 1]) {
			start = lastSize;
		}
		
		int size = res.size();
		
		for (int i = start; i < size; i++) {
			List<Integer> sol = new ArrayList<Integer>(res.get(i));
			sol.add(S[index]);
			res.add(sol);
		}
		
		lastSize = size;
	}
	
}
