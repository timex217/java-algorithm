package problems.leetcode;

/**
 * Surrounded Regions
 * 
 * https://oj.leetcode.com/problems/surrounded-regions/
 * 
 * Given a 2D board containing 'X' and 'O', capture all regions surrounded by 'X'.
 * 
 * A region is captured by flipping all 'O's into 'X's in that surrounded region.
 * 
 * For example,
 * X X X X
 * X O O X
 * X X O X
 * X O X X
 * After running your function, the board should be:
 * 
 * X X X X
 * X X X X
 * X X X X
 * X O X X
 */

import java.util.*;

public class Q002A_Surrounded_Regions {
	
	public static void main(String[] args) {
		char[][] board = {
			{'X','O','X','O','X','O','O','O','X','O'},
			{'X','O','O','X','X','X','O','O','O','X'},
			{'O','O','O','O','O','O','O','O','X','X'},
			{'O','O','O','O','O','O','X','O','O','X'},
			{'O','O','X','X','O','X','X','O','O','O'},
			{'X','O','O','X','X','X','O','X','X','O'},
			{'X','O','X','O','O','X','X','O','X','O'},
			{'X','X','O','X','X','O','X','O','O','X'},
			{'O','O','O','O','X','O','X','O','X','O'},
			{'X','X','O','X','X','X','X','O','O','O'}
		};
		
		solve(board);
		
		print(board);
	}
	
	static void solve(char[][] board) {
		if (board == null || board.length == 0 || board[0].length == 0) {
            return;
        }
		
		int n = board.length;
        int m = board[0].length;
        
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
            	// 如果当前点是O，并且不能从这个点到达边缘，那么就从这个点开始把所有的O变为X
                if (board[i][j] == 'O' && !dfs(board, n, m, i, j)) {
                    fill(board, n, m, i, j);
                }
            }
        }
    }
    
	// whether we can reach the boarder from (i,j)
    static boolean dfs(char[][] board, int n, int m, int i, int j) {
    	// 处理当前解, 标记visited
    	board[i][j] = '#';
    	
    	boolean res = false;
    	
    	// 检查是否需要终止
    	if (i == 0 || i == n - 1 || j == 0 || j == m - 1) {
    		res = true;
    	} else {
	    	// 处理下一层
	    	boolean up    = i > 0     && board[i - 1][j] == 'O' && dfs(board, n, m, i - 1, j);
	    	boolean down  = i < n - 1 && board[i + 1][j] == 'O' && dfs(board, n, m, i + 1, j);
	    	boolean left  = j > 0     && board[i][j - 1] == 'O' && dfs(board, n, m, i, j - 1);
	    	boolean right = j < m - 1 && board[i][j + 1] == 'O' && dfs(board, n, m, i, j + 1);
	    	
	    	res = up || down || left || right;
    	}
    	
    	// 回溯, 恢复当前解
    	board[i][j] = 'O';
    	
    	return res;
    }
    
    static void fill(char[][] board, int n, int m, int i, int j) {
    	// 处理当前解
    	board[i][j] = 'X';
    	
    	// 检查是否需要终止
    	if (i < 0 || i >= n || j < 0 || j >= m) {
    		return;
    	}
        
    	// 处理下一层
    	// up
    	if (i > 0 && board[i - 1][j] == 'O') fill(board, n, m, i - 1, j);
        // down
    	if (i < n - 1 && board[i + 1][j] == 'O') fill(board, n, m, i + 1, j);
    	// left
    	if (j > 0 && board[i][j - 1] == 'O') fill(board, n, m, i, j - 1);
    	// right
    	if (j < m - 1 && board[i][j + 1] == 'O') fill(board, n, m, i, j + 1);
    }
    
    static void print(char[][] board) {
    	if (board == null || board.length == 0) {
            return;
        }
    	
    	int n = board.length;
        
        for (int i = 0; i < n; i++) {
        	System.out.println(Arrays.toString(board[i]));
        }
    }
	
}
