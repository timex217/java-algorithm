package problems.leetcode;

/**
 * Add Binary
 */

public class Q006_Add_Binary {

	public static void main(String[] args) {
		String a =      "10100000100100110110010000010101111011011001101110111111111101000000101111001110001111100001101";
		String b =  "110101001011101110001111100110001010100001101011101010000011011011001011101111001100000011011110011";
		// expected: 110111101100010011000101110110100000011101000101011001000011011000001100011110011010010011000000000
		// actual:   110111101100010011000101110110100000011101000101011001000011011000001100011110011010010011000000000
		String res = addBinary(a, b);
		System.out.println(res);
	}
	
	static String addBinary(String a, String b) {
        if (a == null || b == null) {
            return null;
        }
        
        StringBuilder sb = new StringBuilder();
        
        int i = a.length() - 1;
        int j = b.length() - 1;
        int d1 = 0, d2 = 0, c = 0;
        
        while (i >= 0 && j >= 0) {
            d1 = getNum(a.charAt(i--));
            d2 = getNum(b.charAt(j--));
            
            int s = d1 + d2 + c;
            int m = s % 2;
            c = s / 2;
            
            sb.insert(0, m);
        }
        
        while (i >= 0) {
            d1 = getNum(a.charAt(i--));
            
            int s = d1 + c;
            int m = s % 2;
            c = s / 2;
            
            sb.insert(0, m);
        }
        
        while (j >= 0) {
            d2 = getNum(b.charAt(j--));
            
            int s = d2 + c;
            int m = s % 2;
            c = s / 2;
            
            sb.insert(0, m);
        }
        
        if (c > 0) {
            sb.insert(0, c);
        }
        
        return sb.toString();
        
    }
    
    static int getNum(char ch) {
        return (int)(ch - '0');
    }
}
