/**
 * Q2.If there is only one missing number from 1 to n in an unsorted array. 
 * How to find it in O(n) time? size of the array is n - 1.
 */

package problems.hashmap;

import java.util.*;

public class Q044_Hashmap_Find_Missing_Number_From_Unsorted_Array {
	
	public static void main(String[] args) {
		int[] A = {3, 2, 4, 1, 5, 7};
		int missing = findMissing(A, 7);
		System.out.println(missing == -1 ? "All good" : missing);
	}
	
	public static int findMissing(int[] A, int n) {
		HashSet<Integer> map = new HashSet<Integer>();
		for (int i = 1; i <= n; i++) {
			map.add(i);
		}
		
		for (int i = 0; i < A.length; i++) {
			if (map.contains(A[i])) {
				map.remove(A[i]);
			}
		}
		
		for (int key : map) {
			return key;
		}
		
		return -1;
	}

}
