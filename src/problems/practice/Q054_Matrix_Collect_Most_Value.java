/**
 * Given a 2D array that contains all positive numbers, one can walk right and down from each
 * element. Then when one person is walking down from the top left to the bottom right element,
 * how one can collect the most of the values.
 * 
 * Time complexity: O(2^n)
 * 
 * Use dynamic programming: O(n^2)
 */

package problems.practice;

import java.util.*;

public class Q054_Matrix_Collect_Most_Value {
	
	public static void main(String[] args) {
		int[][] M = {
			{1, 2, 3, 4},
			{2, 3, 4, 5},
			{3, 4, 5, 6},
			{4, 5, 6, 7}
		};
		
		//int max = collect(M, 0, 0);
		int max = collect_dp(M);
		
		System.out.format("\n%d\n", max);
	}
	
	public static int collect(int[][] M, int i, int j) {
		int m = M.length;
		int n = M[0].length;
		
		if (i < 0 || j < 0 || i >= m || j >= n) {
			return 0;
		}
		
		int right = collect(M, i, j + 1);
		int down = collect(M, i + 1, j);
		
		return M[i][j] + Math.max(right, down);
	}
	
	public static int collect_dp(int[][] M) {
		int m = M.length;
		int n = M[0].length;
		
		// dp(i, j) represents the maximum value that can be collected if we start from M(i, j)
		// base case: dp(m, n) = M[m][n]
		// dp(i, j) = M(i, j) + max{dp(i, j + 1), dp(i + 1, j)}
		// dp(0, 0) is our result
		int[][] dp = new int[m + 1][n + 1];
		
		for (int i = m; i >= 0; i--) {
			for (int j = n; j >= 0; j--) {
				if (i == m || j == n) {
					dp[i][j] = 0;
				}
				else if (i == m - 1 && j == n - 1) {
					dp[i][j] = M[i][j];
				}
				else {
					dp[i][j] = M[i][j] + Math.max(dp[i][j + 1], dp[i + 1][j]);
				}
			}
		}
		
		// print out
		print(dp);
		
		// find the path, start from (0, 0)
		int i = 0, j = 0;
		while (dp[i][j] > 0) {
			System.out.format("%d ", M[i][j]);
			if (dp[i + 1][j] >= dp[i][j + 1]) {
				i++;
			}
			else {
				j++;
			}
		}
		
		return dp[0][0];
	}
	
	public static void print(int[][] M) {
		int m = M.length;
		int n = M[0].length;
		
		for (int i = 0; i < m; i++) {
			StringBuffer sb = new StringBuffer();
			
			for (int j = 0; j < n; j++) {
				sb.append(M[i][j]).append("\t");
			}
			
			System.out.println(sb.toString());
		}
	}

}
