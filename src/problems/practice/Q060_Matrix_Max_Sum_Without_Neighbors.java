package problems.practice;

import java.util.*;

public class Q060_Matrix_Max_Sum_Without_Neighbors {
	
	public static void main(String[] args) {
		int[][] M = {
		    {1, 2, 3, 4, 5, 6, 7, 8},
		    {1, 2, 3, 4, 5, 6, 7, 8},
		    {1, 2, 3, 4, 5, 6, 7, 8},
		    {1, 2, 3, 4, 5, 6, 7, 8},
		    {1, 2, 3, 4, 5, 6, 7, 8},
		    {1, 2, 3, 4, 5, 6, 7, 8},
		    {1, 2, 3, 4, 5, 6, 7, 8},
		    {1, 2, 3, 4, 5, 6, 7, 8}
		};
		
		int res = getMaxSumWithoutNeighbors(M, 8);
		System.out.format("Max sum is %d.", res);
	}
	
	static int getMaxSumWithoutNeighbors(int[][] M, int n) {
	    int max = Integer.MIN_VALUE;
	    
	    // step 1. get all valid configurations
	    ArrayList<ArrayList<Integer>> configs = getValidConfigs(n);
	    
	    int k = configs.size();
	    int[][] sum = new int[n][k];
	    
	    // step 2. go through each config row
	    // compare with the last row, find out the max val from non-conflict configs
	    // and add up to its sum
	    for (int i = 0; i < n; i++) {
	        for (int j = 0; j < k; j++) {
	            // current config
	            ArrayList<Integer> config = configs.get(j);
	            // config value
	            int val = getConfigValue(config, M[i]);
	            
	            sum[i][j] = val;
	            
	            if (i > 0) {
	                sum[i][j] += getMaxNonConflictVal(configs, config, sum[i - 1]);
	            }
	            
	            max = Math.max(max, sum[i][j]);
	        }
	    }
	    
	    return max;
	}
	
	// ------------------------
	//  Get config value
	// ------------------------
	static int getConfigValue(ArrayList<Integer> config, int[] A) {
	    int sum = 0;
	    for (int i = 0; i < config.size(); i++) {
	        if (config.get(i) == 1) {
	            sum += A[i];
	        }
	    }
	    return sum;
	}
	
	// ------------------------------------------
	//  Get the max val from non-conflict confgs
	// ------------------------------------------
	static int getMaxNonConflictVal(ArrayList<ArrayList<Integer>> configs, ArrayList<Integer> config, int[] val) {
	    int max = 0;
	    
	    for (int i = 0; i < configs.size(); i++) {
	        if (!isConflict(configs.get(i), config)) {
	            max = Math.max(max, val[i]);
	        }
	    }
	    
	    return max;
	}
	
	// -------------------------------------
	//  Check whether two configs conflict
	// -------------------------------------
	static boolean isConflict(ArrayList<Integer> c1, ArrayList<Integer> c2) {
	    int n = c1.size();
	    
	    for (int i = 0; i < n; i++) {
	        if (c1.get(i) == 1) {
	            if (i > 0 && c2.get(i - 1) == 1) return true;
	            if (c2.get(i) == 1) return true;
	            if (i < n - 1 && c2.get(i + 1) == 1) return true;
	        }
	    }
	    
	    return false;
	}
	
	// ------------------------
	//  Get the valid configs
	// ------------------------
	static ArrayList<ArrayList<Integer>> getValidConfigs(int n) {
	    ArrayList<ArrayList<Integer>> res = new ArrayList<ArrayList<Integer>>();
	    
	    ArrayList<Integer> sol = new ArrayList<Integer>();
	    for (int i = 0; i < n; i++) sol.add(0);
	    
	    helper(n, 0, sol, res);
	    return res;
	}
	
	static void helper(int n, int index, ArrayList<Integer> sol, ArrayList<ArrayList<Integer>> res) {
	    if (index == n) {
	        res.add(new ArrayList<Integer>(sol));
	        return;
	    }
	    
	    // 1. 不置成 1
	    helper(n, index + 1, sol, res);
	    
	    // 2. 置成 1
	    // 类似 8 皇后，检查是否可以将当前位置置1
	    if (check(n, index, sol)) {
	        sol.set(index, 1);
	        helper(n, index + 1, sol, res);
	        sol.set(index, 0);
	    }
	}
	
    // -----------------------------------------
    //  Check whether it's ok to set 1 at index
    // -----------------------------------------
	static boolean check(int n, int index, ArrayList<Integer> sol) {
	    int left = index - 1;
	    int right = index + 1;
	    
	    if (left >= 0 && sol.get(left) == 1)  return false;
	    if (right < n && sol.get(right) == 1) return false;
	    
	    return true;
	}

}
