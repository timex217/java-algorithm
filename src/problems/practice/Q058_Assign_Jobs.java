package problems.practice;

import java.util.*;

public class Q058_Assign_Jobs {
	
	private static class Job {
		int index;
		
		Job(int x) {
			this.index = x;
		}
	}
	
	private static class Server {
		Set<Job> jobs;
		
		Server() {
			this.jobs = new HashSet<Job>();
		}
		
		void addJob(Job job) {
			this.jobs.add(job);
		}
		
		void removeJob(Job job) {
			if (this.jobs.contains(job)) {
				this.jobs.remove(job);
			}
		}
	}
	
	public static void main(String[] args) {
		int n_jobs = 4;
		int n_servers = 4;
		
		Job[] jobs = new Job[n_jobs];
		for (int i = 0; i < n_jobs; i++) {
			jobs[i] = new Job(i);
		}
		
		Server[] servers = new Server[n_servers];
		for (int i = 0; i < n_servers; i++) {
			servers[i] = new Server();
		}
		
		assign(jobs, 0, servers);
	}
	
	public static void assign(Job[] jobs, int index, Server[] servers) {
		if (index == jobs.length) {
			print(servers);
			return;
		}
		
		Job job = jobs[index];
		
		for (int i = 0; i < servers.length; i++) {
			Server server = servers[i];
			
			if (check(job, server)) {
				server.addJob(job);
				assign(jobs, index + 1, servers);
				server.removeJob(job);
			}
		}
	}
	
	public static boolean check(Job job, Server server) {
		int index = job.index;
		if (server.jobs.contains(index + 1) && server.jobs.contains(index + 2)) return false;
		if (server.jobs.contains(index - 1) && server.jobs.contains(index - 2)) return false;
		if (server.jobs.contains(index - 1) && server.jobs.contains(index + 1)) return false;
		return true;
	}
	
	public static void print(Server[] servers) {
		System.out.println("----------");
		for (int i = 0; i < servers.length; i++) {
			StringBuffer sb = new StringBuffer();
			
			for (Job job : servers[i].jobs) {
				sb.append(job.index).append(" ");
			}
			
			System.out.println(sb.toString());
		}
	}

}
