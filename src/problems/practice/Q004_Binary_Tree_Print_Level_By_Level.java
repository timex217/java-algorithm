package problems.practice;

import java.util.*;

public class Q004_Binary_Tree_Print_Level_By_Level {
	
	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) {
			this.val = x;
			this.left = null;
			this.right = null;
		}
	}
	
	// method 1: use a single queue and null pointer
	public static void levelOrderPrint1(TreeNode root) {
		if (root == null) {
			return;
		}
		
		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		queue.add(root);
		queue.add(null);
		
		while (!queue.isEmpty()) {
			TreeNode node = queue.poll();
			
			if (node != null) {
				System.out.format("%d ", node.val);
				
				if (node.left != null) {
					queue.add(node.left);
				}
				if (node.right != null) {
					queue.add(node.right);
				}
			}
			else {
				System.out.println();
				
				if(!queue.isEmpty()) {
					queue.add(null);
				}
			}
		}
	}
	
	// method 2: use a single queue and count level size
	public static void levelOrderPrint2(TreeNode root) {
		if (root == null) {
			return;
		}
		
		Queue<TreeNode> queue = new LinkedList<TreeNode>();
		queue.add(root);
		
		int curr_size = 1;
		int next_size = 0;
		
		while (!queue.isEmpty()) {
			TreeNode node = queue.poll();
			System.out.format("%d ", node.val);
			
			curr_size--;
			
			if (node.left != null) {
				queue.add(node.left);
				next_size++;
			}
			
			if (node.right != null) {
				queue.add(node.right);
				next_size++;
			}
			
			if (curr_size == 0) {
				System.out.println();
				
				curr_size = next_size;
				next_size = 0;
			}
		}
	}
	
	
	public static void main(String[] args) {
		// TODO...
	}

}
