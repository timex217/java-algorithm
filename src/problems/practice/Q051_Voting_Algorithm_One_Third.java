/**
 * Voting algorithm, what if one number has exceeded 50%, then how could we find it?
 * What if one number has exceeded 1/3?, then how could we find it?
 * 
 * ___1/3___ | ___1/3___ | ___1/3___
 * 
 * Step 1, divide the array into 3 part evenly, and there must exist a part that exceed 1/3 of that part,
 * that is to say, the number must be either top 1 or top 2 frequent number of that part.
 * 
 * Step 2, for each part we run voting algorithm, and we can get 3 results from each part.
 * 
 * Step 3, we use three counters for the three numbers returned, and iterate through the whole
 * array to find the final result.
 */

package problems.practice;

import java.util.*;

public class Q051_Voting_Algorithm_One_Third {
	
	public static void main(String[] args) {
		Integer[] A = {1, 2, 0, 1, 1, 1, 4, 5, 1, 7, 0, 0};
		Integer[] B = new Integer[3];
		
		for (int i = 0; i < 3; i++) {
			int num = (int)Math.ceil(A.length / 3.0);
			int start = i * num;
			int end = (i + 1) * num - 1;
			
			if (end >= A.length - 1) {
				end = A.length - 1;
			}
			
			System.out.format("%d, %d\n", start, end);
			
			B[i] = find(A, start, end);
		}
		
		System.out.println(Arrays.toString(B));
		
		Integer number = find(B, 0, B.length - 1);
		System.out.println(number != null ? number : "Can not find!");
	}
	
	public static Integer find(Integer[] A, int lo, int hi) {
		Integer number = null;
		int count = 0;
		
		for (int i = lo; i <= hi; i++) {
			if (A[i] == null) {
				continue;
			}
			
			if (number == null) {
				number = A[i];
				count = 1;
			}
			else if (number == A[i]) {
				count++;
			}
			else if (--count == 0) {
				number = null;
			}
		}
		
		return number;
	}

}
