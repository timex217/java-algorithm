/**
 * How to reconstruct a BST with postorder and inorder sequences of all nodes.
 * (all numbers are unique)
 * 
 *          10
 *      5        15
 *   2    7    12  20
 */

package problems.practice;

import java.util.*;

public class Q052_Binary_Tree_Reconstruction_Post_In {
	
	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		
		TreeNode(int x) {
			this.val = x;
			this.left = null;
			this.right = null;
		}
	}
	
	public static void main(String[] args) {
		int[] post = {2, 7, 5, 12, 20, 15, 10};
		int[] in   = {2, 5, 7, 10, 12, 15, 20};
		
		// use a hashmap to store the position info of in order array
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (int i = 0; i < in.length; i++) {
			map.put(in[i], i);
		}
		
		TreeNode tree = reconstruct(post, 0, post.length - 1, in, 0, in.length - 1, map);
	}
	
	public static TreeNode reconstruct(int[] post, int post_left, int post_right, int[] in, int in_left, int in_right, HashMap<Integer, Integer> map) {
		// base case
		if (post_left > post_right || in_left > in_right) {
			return null;
		}
		
		// create the root node
		int root_val = post[post_right];
		TreeNode root = new TreeNode(root_val);
		
		// find the index of the root val in in order
		int index = map.get(root_val);
		
		// recursion
		// (index - in_left): the number of left nodes
		root.left = reconstruct(post, post_left, post_left + (index - in_left) - 1, in, in_left, index - 1, map);
		root.right = reconstruct(post, post_left + (index - in_left), post_right - 1, in, index + 1, in_right, map);
		
		return root;
	}
}
