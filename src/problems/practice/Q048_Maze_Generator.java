package problems.practice;

import java.util.*;

public class Q048_Maze_Generator {
	
	private static class Position {
		int x;
		int y;
		Position(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}
	
	public static void main(String[] args) {
		// define number of rows and columns
		int m = 10;
		int n = 10;
		
		generateMaze(m, n);
	}
	
	public static void generateMaze(int m, int n) {
		// maze matrix
		int[][] M = new int[m][n];
		
		// initialize maze
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				M[i][j] = 1;
			}
		}
		
		// start from a random position on the edge
		Position pos = randomPosition(m, n);
		M[pos.x][pos.y] = 2;
		
		// generate maze, each time move two steps
		walk(M, pos.x, pos.y);
		
		print(M);
	}
	
	public static void walk(int[][] M, int i, int j) {
		int m = M.length;
		int n = M[0].length;
		
		ArrayList<Integer> direction = new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3));
		Collections.shuffle(direction);
		
		for (int k = 0; k < 4; k++) {
			switch (direction.get(k)) {
				// right
				case 0:
					if (j < n - 2 && M[i][j + 1] == 1 && M[i][j + 2] == 1) {
						M[i][j + 1] = 0;
						M[i][j + 2] = 0;
						
						walk(M, i, j + 2);
					}
					break;
					
				// left	
				case 1:
					if (j > 1 && M[i][j - 1] == 1 && M[i][j - 2] == 1) {
						M[i][j - 1] = 0;
						M[i][j - 2] = 0;
						
						walk(M, i, j - 2);
					}
					break;
					
				// down
				case 2:
					if (i < m - 2 && M[i + 1][j] == 1 && M[i + 2][j] == 1) {
						M[i + 1][j] = 0;
						M[i + 2][j] = 0;
						
						walk(M, i + 2, j);
					}
					break;
					
				// up
				case 3:
					if (i > 1 && M[i - 1][j] == 1 && M[i - 2][j] == 1) {
						M[i - 1][j] = 0;
						M[i - 2][j] = 0;
						
						walk(M, i - 2, j );
					}
					break;
			}
		}
	}
	
	public static Position randomPosition(int m, int n) {
		int x = 0, y = 0;
		
		// row or column
		Random rand = new Random();
		if (rand.nextInt() % 2 == 0) {
			x = rand.nextInt() % 2 == 0 ? 0 : m - 1;
			y = randRange(0, n);
		}
		else {
			y = rand.nextInt() % 2 == 0 ? 0 : n - 1;
			x = randRange(0, m);
		}
		
		return new Position(x, y);
	}
	
	private static int randRange(int lo, int hi) {
		Random rand = new Random();
		return rand.nextInt(hi - lo) + lo;
	}
	
	public static void print(int[][] M) {
		int m = M.length;
		int n = M[0].length;
		
		for (int i = 0; i < m; i++) {
			StringBuffer row = new StringBuffer();
			for (int j = 0; j < n; j++) {
				row.append(M[i][j]).append("  ");
			}
			System.out.println(row.toString());
		}
	}

}
