package problems.practice;

public class Q045_3_Sum_Square {
	
	public static void main(String[] args) {
		int n = 98;
		find(n);
	}
	
	public static void find(int n) {
		int range = (int)Math.sqrt(n);
		
		for (int i = 1; i <= range - 2; i++) {
			int j = i + 1;
			int k = range;
			
			while (j < k) {
				int sum = i * i + j * j + k * k;
				if (sum == n) {
					System.out.format("a = %d, b = %d, c = %d", i, j, k);
					return;
				}
				else if (sum < n) {
					j++;
				}
				else {
					k++;
				}
			}
		}
		
		System.out.println("Can not find!");
	}

}
