/**
 * How to find the k-th smallest number in the f(x,y,z) = 3^x * 5^y * 7^z (int x > 0, y > 0, z > 0).
 */

package problems.practice;

import java.util.*;

public class Q046_BFS_Find_Kth_Smallest_Result {
	
	private static class Point {
		int x;
		int y;
		int z;
		
		Point(int x, int y, int z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}
		
		int getValue() {
			return (int)(Math.pow(3, this.x) + Math.pow(5, this.y) + Math.pow(7, this.z));
		}
	}
	
	public static void main(String[] args) {
		find(10);
	}
	
	public static void find(int k) {
		// create a min heap with size k
		PriorityQueue<Point> heap = new PriorityQueue<Point>(k, new Comparator<Point>(){
			@Override
			public int compare(Point p1, Point p2) {
				return p1.getValue() - p2.getValue();
			}
		});
		
		// use a set to avoid duplication
		Set<String> map = new HashSet<String>();
		
		// initial state
		heap.offer(new Point(1, 1, 1));
		map.add(getKey(1, 1, 1));
		
		int count = 0;
		
		while (count++ < k) {
			Point p = heap.poll();
			
			if (count == k) {
				System.out.format("%d. x = %d, y = %d, z = %d, value = %d\n", count, p.x, p.y, p.z, p.getValue());
				break;
			}
			
			// generate new states
			String key = getKey(p.x + 1, p.y, p.z);
			if (!map.contains(key)) {
				map.add(key);
				heap.offer(new Point(p.x + 1, p.y, p.z));
			}
			
			key = getKey(p.x, p.y + 1, p.z);
			if (!map.contains(key)) {
				map.add(key);
				heap.offer(new Point(p.x, p.y + 1, p.z));
			}
			
			key = getKey(p.x, p.y, p.z + 1);
			if (!map.contains(key)) {
				map.add(key);
				heap.offer(new Point(p.x, p.y, p.z + 1));
			}
		}
	}
	
	public static String getKey(int x, int y, int z) {
		StringBuffer sb = new StringBuffer();
		sb.append(x).append(y).append(z);
		return sb.toString();
	}

}
