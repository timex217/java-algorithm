package problems.practice;
import java.util.*;

public class Q022_String_Most_Common_Words_Pair {
	/**
	 * Data structure to store sentence
	 */
	private static class Sentence implements Comparable<Sentence> {
		// sentence index
		public int index;
		
		// word map of sentence
		public HashMap<String, Integer> wordMap;
		
		// constructor
		public Sentence(int index, String s) {
			this.index = index;
			this.wordMap = this.buildWordMap(s);
		}
		
		public int getWordCount() {
			return this.wordMap.size();
		}
		
		public int compareTo(Sentence s) {
			return this.wordMap.size() - s.wordMap.size();
		}
		
		private HashMap<String, Integer> buildWordMap(String s) {
			HashMap<String, Integer> map = new HashMap<String, Integer>();
			
			if (s == null || s.length() == 0) {
				return map;
			}
			
			// count word
			int n = s.length();
			int i = 0, j = 0;
			
			while (j < n) {
				while (j < n && s.charAt(j) == ' ') j++;
				
				i = j;
				
				while (j < n && s.charAt(j) != ' ') j++;
				
				if (i < n) {
					String word = s.substring(i, j);
					
					if (!map.containsKey(word)) {
						map.put(word, 1);
					} else {
						map.put(word, map.get(word) + 1);
					}
				}
			}
			
			return map;
		}
		
		public int countCommonWords(Sentence s) {
			int count = 0;
			
			if (this.getWordCount() < s.getWordCount()) {
				for (String word : this.wordMap.keySet()) {
					if (s.wordMap.containsKey(word)) count++;
				}
			} 
			else {
				for (String word : s.wordMap.keySet()) {
					 if (this.wordMap.containsKey(word)) count++;
				}
			}
			
			return count;
		}
	}
	
	private static class SentenceComparator implements Comparator<Sentence> {
		public int compare(Sentence s1, Sentence s2) {
			return s1.compareTo(s2);
		}
	}
	
	public static void main(String[] args) {
		String[] list = {
			"This is a good day", 
			"This is a bad day", 
			"That was a day", 
			"a day"
		};
		
		ArrayList<String> result = mcwp(list);
		
		if (result == null || result.size() == 0) {
			System.out.println("No pair is found.");
			return;
		}
		
		// print the result
		System.out.println("The pair is:");
		for (int i = 0; i < result.size(); i++) {
			System.out.format("%d. %s\n", i + 1, result.get(i));
		}
	}
	
	public static ArrayList<String> mcwp(String[] list) {
		if (list == null || list.length == 0) {
			return null;
		}
		
		int n = list.length;
		if (n <= 2) {
			return null;
		}
		
		// step 1. build hash map for each sentence and put it to an array
		ArrayList<Sentence> sentences = new ArrayList<Sentence>();
		
		for (int i = 0; i < n; i++) {
			String s = list[i];
			Sentence sentence = new Sentence(i, s);
			sentences.add(sentence);
		}
		
		// step 2. sort the sentences array by word count
		Collections.sort(sentences, new SentenceComparator());
		
		// step 3. compare the sentences that have most words
		ArrayList<String> pair = new ArrayList<String>();
		
		int max = 0;
		
		for (int i = n - 2; i >= 0; i--) {
			Sentence s1 = sentences.get(i);
			
			for (int j = n - 1; j > i; j--) {
				Sentence s2 = sentences.get(j);
				
				int count = s1.countCommonWords(s2);
				
				if (count > max) {
					max = count;
					
					pair = new ArrayList<String>();
					pair.add(list[s1.index]);
					pair.add(list[s2.index]);
				}
				
				// early terminate
				if (i > 0 && max >= sentences.get(i).getWordCount()) {
					return pair;
				}
				
			}
		}
		
		return pair;
	}
	
}
