package problems.practice;

import java.util.*;

public class Q049_Matrix_Count_Disjoint_Area {
	
	private static class Node {
		int i;
		int j;
		
		Node(int i, int j) {
			this.i = i;
			this.j = j;
		}
	}
	
	private static int[][] M = {
		{0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
		{1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1},
		{1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1},
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
		{1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1},
		{1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1},
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
		{1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1},
		{1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1},
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
		{1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1},
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
		{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0},
	};
	
	public static void main(String[] args) {
		int count = countDisjointArea();
		System.out.format("%d disjoint areas", count);
	}
	
	public static int countDisjointArea() {
		if (M == null || M.length == 0 || M[0].length == 0) {
			return 0;
		}
		
		int m = M.length;
		int n = M[0].length;
		int count = 0;
		
		// scan each row and find a 0 then perform bfs or dfs
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				if (M[i][j] == 0) {
					//bfs(i, j, ++count);
					//dfs(i, j, ++count);
					dfs(new Node(i, j), ++count);
				}
			}
		}
		
		print();
		
		return count;
	}
	
	/**
	 * Breadth-first search
	 */
	public static void bfs(int i, int j, int val) {
		int m = M.length;
		int n = M[0].length;
		
		// use queue in bfs
		Queue<Node> queue = new LinkedList<Node>();
		
		// queue it
		Node node = new Node(i, j);
		queue.add(node);
		
		while (!queue.isEmpty()) {
			node = queue.poll();
			
			// mark as visited
			M[node.i][node.j] = val;
			
			ArrayList<Node> children = getUnvisitedChildren(node);
			
			for (Node child : children) {
				queue.add(child);
			}
		}
	}
	
	/**
	 * Depth-first search
	 */
	public static void dfs(int i, int j, int val) {
		int m = M.length;
		int n = M[0].length;
		
		// use stack in dfs
		Stack<Node> stack = new Stack<Node>();
		
		// push it
		Node node = new Node(i, j);
		stack.push(node);
		
		while (!stack.isEmpty()) {
			node = stack.peek();
			
			// mark as visited
			M[node.i][node.j] = val;
			
			ArrayList<Node> children = getUnvisitedChildren(node);
			
			if (children.size() > 0) {
				stack.push(children.get(0));
			} else {
				stack.pop();
			}
		}
	}
	
	/**
	 * Depth-first search recursion
	 */
	public static void dfs(Node node, int val) {
		if (node == null) {
			return;
		}
		
		// mark as visited
		M[node.i][node.j] = val;
		
		ArrayList<Node> children = getUnvisitedChildren(node);
		
		for (Node child : children) {
			dfs(child, val);
		}
	}
	
	/**
	 * Get next unvisited node
	 */
	public static ArrayList<Node> getUnvisitedChildren(Node node) {
		int m = M.length;
		int n = M[0].length;
		
		int i = node.i;
		int j = node.j;
		
		ArrayList<Node> list = new ArrayList<Node>();
		
		// up
		if (i > 0   && M[i-1][j] == 0) list.add(new Node(i-1, j));
		// down
		if (i < m-1 && M[i+1][j] == 0) list.add(new Node(i+1, j));
		// left
		if (j > 0   && M[i][j-1] == 0) list.add(new Node(i, j-1));
		// right
		if (j < n-1 && M[i][j+1] == 0) list.add(new Node(i, j+1));
		
		return list;
	}
	
	public static void print() {
		int m = M.length;
		int n = M[0].length;
		
		for (int i = 0; i < m; i++) {
			String row = "";
			for (int j = 0; j < n; j++) {
				row += String.valueOf(M[i][j]) + ", ";
			}
			System.out.println(row);
		}
	}

}
