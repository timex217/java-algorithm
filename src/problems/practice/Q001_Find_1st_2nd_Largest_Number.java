/**
 * Q001. How to use the least number of comparisons to find the largest and second largest number?
 * 
 * Use tunorment comparison algorithm.
 */

package problems.practice;

import java.util.*;

public class Q001_Find_1st_2nd_Largest_Number {
	
	private static class Node {
		int val;
		ArrayList<Integer> list;
		Node(int x) {
			this.val = x;
			this.list = new ArrayList<Integer>();
		}
	}
	
	public static void main(String[] args) {
		int[] A = {3, 1, 2, 5, 4, 7, 9, 6, 8, 0};
		int[] result = find(A);
		System.out.println(Arrays.toString(result));
	}
	
	public static int[] find(int[] A) {
		if (A == null) {
			return A;
		}
		
		// initialize the tunorment
		ArrayList<Node> list = new ArrayList<Node>();
		for (int i = 0; i < A.length; i++) {
			list.add(new Node(A[i]));
		}
		
		// tunorment comparison
		while (list.size() > 1) {
			ArrayList<Node> tmp = new ArrayList<Node>();
			
			for (int i = 0; i < list.size() / 2; i++) {
				Node a = list.get(2 * i);
				Node b = list.get(2 * i + 1);
				
				if (a.val > b.val) {
					a.list.add(b.val);
					tmp.add(a);
				}
				else {
					b.list.add(a.val);
					tmp.add(b);
				}
			}
			
			// for odd list, the last one enters to the next round directly
			if (list.size() % 2 == 1) {
				tmp.add(list.get(list.size() - 1));
			}
			
			list = tmp;
		}
		
		Node node = list.get(0);
		
		int first = node.val;
		int second = Integer.MIN_VALUE;
		
		for (int i = 0; i < node.list.size(); i++) {
			second = Math.max(second, node.list.get(i));
		}
		
		return new int[]{first, second};
	}

}
