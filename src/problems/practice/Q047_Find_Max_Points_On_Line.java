/**
 * Given an array of coordinates of points, how to find largest number of points that can be crossed by a same line in 2D space?
 */

package problems.practice;

import java.util.*;

public class Q047_Find_Max_Points_On_Line {
	
	private static class Point {
		int x;
		int y;
		
		Point(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}
	
	public static void main(String[] args) {
		Point[] points = {
				new Point(560,248),new Point(0,16),new Point(30,250),new Point(950,187),new Point(630,277),new Point(950,187),new Point(-212,-268),new Point(-287,-222),new Point(53,37),new Point(-280,-100),new Point(-1,-14),new Point(-5,4),new Point(-35,-387),new Point(-95,11),new Point(-70,-13),new Point(-700,-274),new Point(-95,11),new Point(-2,-33),new Point(3,62),new Point(-4,-47),new Point(106,98),new Point(-7,-65),new Point(-8,-71),new Point(-8,-147),new Point(5,5),new Point(-5,-90),new Point(-420,-158),new Point(-420,-158),new Point(-350,-129),new Point(-475,-53),new Point(-4,-47),new Point(-380,-37),new Point(0,-24),new Point(35,299),new Point(-8,-71),new Point(-2,-6),new Point(8,25),new Point(6,13),new Point(-106,-146),new Point(53,37),new Point(-7,-128),new Point(-5,-1),new Point(-318,-390),new Point(-15,-191),new Point(-665,-85),new Point(318,342),new Point(7,138),new Point(-570,-69),new Point(-9,-4),new Point(0,-9),new Point(1,-7),new Point(-51,23),new Point(4,1),new Point(-7,5),new Point(-280,-100),new Point(700,306),new Point(0,-23),new Point(-7,-4),new Point(-246,-184),new Point(350,161),new Point(-424,-512),new Point(35,299),new Point(0,-24),new Point(-140,-42),new Point(-760,-101),new Point(-9,-9),new Point(140,74),new Point(-285,-21),new Point(-350,-129),new Point(-6,9),new Point(-630,-245),new Point(700,306),new Point(1,-17),new Point(0,16),new Point(-70,-13),new Point(1,24),new Point(-328,-260),new Point(-34,26),new Point(7,-5),new Point(-371,-451),new Point(-570,-69),new Point(0,27),new Point(-7,-65),new Point(-9,-166),new Point(-475,-53),new Point(-68,20),new Point(210,103),new Point(700,306),new Point(7,-6),new Point(-3,-52),new Point(-106,-146),new Point(560,248),new Point(10,6),new Point(6,119),new Point(0,2),new Point(-41,6),new Point(7,19),new Point(30,250)
		};
		
		int res = maxPoints(points);
		System.out.println(res);
	}
	
	public static int maxPoints(Point[] points) {
		// base case
        if (points == null || points.length == 0) return 0;
		if (points.length == 1) return 1;
		
		int n = points.length;
		int res = 0;
		String maxKey = null;
		
		HashMap<String, Set<Integer>> map = new HashMap<String, Set<Integer>>();
		
		for (int i = 0; i < n - 1; i++) {
			for (int j = i + 1; j < n; j++) {
				Point p1 = points[i];
				Point p2 = points[j];
				
				// y = ax + b, calculate the slope and intercept
				double a = 0.0, b = 0.0;
				String key;
				Set<Integer> set;
				
				if (i == 1 && j == n - 7) {
					System.out.println();
				}
				
				if (p1.x == p2.x) {
					key = "x" + String.valueOf(p1.x);
				} else {
					if (p1.x > p2.x) {
						a = (double)(p1.y - p2.y) / (p1.x - p2.x);
						b = p1.y - a * p1.x;
					} else {
						a = (double)(p2.y - p1.y) / (p2.x - p1.x);
						b = p2.y - a * p2.x;
					}
					key = "a" + String.valueOf(a) + "b" + String.valueOf(b);
				}
				
				if (!map.containsKey(key)) {
					set = new HashSet<Integer>();
				} else {
					set = map.get(key);
				}
				
				if (!set.contains(i)) {
					set.add(i);
				}
				
				if (!set.contains(j)) {
					set.add(j);
				}
				
				map.put(key, set);
				
				if (set.size() > res) {
					res = set.size();
					maxKey = key;
				}
			}
		}
		
		Set<Integer> maxSet = map.get(maxKey);
		for (int i : maxSet) {
			Point p = points[i];
			System.out.format("%d: (%d, %d)\n", i, p.x, p.y);
		}
		
		return res;
	}

}
