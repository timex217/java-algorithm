package problems.practice;
import java.util.*;

public class Q023_String_Max_Multiply_Uncommon_Strings {
	
	private static class Pair {
		public int index1;
		public int index2;
		public int production;
		
		public Pair(int index1, int index2, int production) {
			this.index1 = index1;
			this.index2 = index2;
			this.production = production;
		}
	}
	
	public static void main(String[] args) {
		String[] list = {
			"adzz",
			"abd",
			"abcde",
			"fgz"
		};
		
		Pair pair = mmucs(list);
		
		if (pair == null) {
			System.out.println("No pair is found.");
		} else {
			System.out.println(list[pair.index1]);
			System.out.println(list[pair.index2]);
		}
	}
	
	public static Pair mmucs(String[] list) {
		if (list == null || list.length == 0) {
			return null;
		}
		
		int n = list.length;
		if (n <= 2) {
			return null;
		}
		
		// step 1. sort the list by string length
		Collections.sort(Arrays.asList(list), new StringLengthComparator());
		
		// step 2. best-first search
		// create a max heap
        PriorityQueue<Pair> heap = new PriorityQueue<Pair>(list.length, new Comparator<Pair>(){
            @Override
            public int compare(Pair p1, Pair p2) {
                return p2.production - p1.production;
            }
        });
        
        // have a hashmap to avoid duplication
        Set<String> map = new HashSet<String>();
        
        // initial state, first pair
        int index1 = 0, index2 = 1;
        int production = getProduction(list, index1, index2);
        
        Pair p = new Pair(index1, index2, production);
        heap.offer(p);
        
        while (!heap.isEmpty()) {
        	p = heap.poll();
        	
        	index1 = p.index1;
    		index2 = p.index2;
    		
    		if (!isChecked(map, index1, index2)) {
    			updateMap(map, index1, index2);
    		}
    		
    		if (!checkCommonLetters(list, index1, index2)) {
				return p;
			} 
    		else {
        		// expand and generate two states
        		// state 1
        		index1 = p.index1;
        		index2 = p.index2 + 1;
        		
        		if (index1 < n && index2 < n) {
        			production = getProduction(list, index1, index2);
        			p = new Pair(index1, index2, production);
        			heap.offer(p);
        		}
        		
        		// state 2
        		index1 = p.index1 + 1;
        		index2 = p.index2;
        		
        		if (index1 < n && index2 < n) {
        			production = getProduction(list, index1, index2);
        			p = new Pair(index1, index2, production);
        			heap.offer(p);
        		}
        	}
        }
        
        // no pair is found
		return null;
	}
	
	//
	// helper functions
	//
	
	private static int getProduction(String[] list, int index1, int index2) {
		String s1 = list[index1];
		String s2 = list[index2];
		
		return s1.length() * s2.length();
	}
	
	// return true if s1 and s2 have common a letter
	private static boolean checkCommonLetters(String[] list, int index1, int index2) {
		String s1 = list[index1]; 
		String s2 = list[index2];
		
		// suppose characters are only [a-z]
		int i = 0, bits1 = 0, bits2 = 0;
		
		for (i = 0; i < s1.length(); i++) {
			bits1 |= 1 << (s1.charAt(i) - 97);
		}
		
		for (i = 0; i < s2.length(); i++) {
			bits2 |= 1 << (s2.charAt(i) - 97);
		}
		
		return (bits1 & bits2) != 0;
	}
	
	private static class StringLengthComparator implements Comparator<String> {
		public int compare(String s1, String s2) {
			return s2.length() - s1.length();
		}
	}
	
	private static void updateMap(Set<String> map, int index1, int index2) {
		String strIndex1 = String.valueOf(index1);
		String strIndex2 = String.valueOf(index2);
		
		map.add(strIndex1 + strIndex2);
		map.add(strIndex2 + strIndex1);
	}
	
	private static boolean isChecked(Set<String> map, int index1, int index2) {
		String strIndex1 = String.valueOf(index1);
		String strIndex2 = String.valueOf(index2);
		
		return map.contains(strIndex1 + strIndex2) || map.contains(strIndex2 + strIndex1);
	}
	
}
