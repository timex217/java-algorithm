package problems.practice;

import java.util.*;

public class Q009_LinkedList_Merge_Sort {
	
	private static class ListNode {
		int val;
		ListNode next;
		ListNode(int x) {
			this.val = x;
			this.next = null;
		}
	}
	
	public static ListNode sortList(ListNode head) {
		if (head == null || head.next == null) {
			return head;
		}
		
		// cut the list into two halves
		ListNode p1 = head, p2 = head, p = null;
		
		while (p2 != null) {
			p = p1;
			p1 = p1.next;
			p2 = (p2.next == null) ? null : p2.next.next;
		}
		
		p2 = p1;
		p1 = head;
		p.next = null;
		
		ListNode head1 = sortList(p1);
		ListNode head2 = sortList(p2);
		
		return mergeList(head1, head2);
		
	}
	
	public static ListNode mergeList(ListNode head1, ListNode head2) {
		// sanity check
		if (head1 == null && head2 == null) {
			return null;
		}
		
		if (head1 == null) {
			return head2;
		}
		
		if (head2 == null) {
			return head1;
		}
		
		ListNode p1, p2, head;
		
		if (head1.val < head2.val) {
			p1 = head1;
			p2 = head2;
		}
		else {
			p1 = head2;
			p2 = head1;
		}
		
		head = p1;
		
		while (p1.next != null && p2 != null) {
			if (p2.val < p1.next.val) {
				ListNode tmp = p2.next;
				
				// insert p2 between p1 and p1.next
				p2.next = p1.next;
				p1.next = p2;
				
				p2 = tmp;
			}
			
			p1 = p1.next;
		}
		
		if (p1.next == null) {
			p1.next = p2;
		}
		
		return head;
	}

}
