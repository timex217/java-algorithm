package problems.practice;

import java.util.*;

public class Q057_Dynamic_Programming_Longest_Increasing_Sub_Sequence {
	
	public static void main(String[] args) {
		int[] A = {0, 12, 2, 6, 50, 9, 10};
		int[] B = lis(A);
		
		System.out.println(Arrays.toString(B));
	}
	
	public static int[] lis(int[] A) {
		if (A == null || A.length == 0) {
			return A;
		}
		
		int n = A.length;
		int max = 0;
		
		// lis(i) represents the length of the longest increasing sub sequence at i-th place
		// lis(i) = max{1, lis(j) + 1}, j < i && A[j] <= A[i]
		int[] dp = new int[n];
		
		for (int i = 0; i < n; i++) {
			dp[i] = 1;
			for (int j = 0; j < i; j++) {
				if (dp[j] + 1 > dp[i] && A[j] <= A[i]) {
					dp[i] = dp[j] + 1;
					max = Math.max(max, dp[i]);
				}
			}
		}
		
		// print the dp
		System.out.println(Arrays.toString(dp));
		
		// find the sequence out
		int[] result = new int[max];
		int j = 0;
		for (int i = 1; i <= max; i++) {
			while (j < n && dp[j] <= i) j++;
			result[i - 1] = A[j - 1];
		}
		
		return result;
		
	}

}
