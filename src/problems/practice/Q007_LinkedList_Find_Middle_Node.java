package problems.practice;

public class Q007_LinkedList_Find_Middle_Node {
	
	private static class ListNode {
		int val;
		ListNode next;
		ListNode(int x) {
			this.val = x;
			this.next = null;
		}
	}
	
	public static ListNode findMiddleNode(ListNode head) {
		ListNode p = null, slow = head, fast = head;
		
		while (fast != null) {
			p = slow;
			slow = slow.next;
			fast = (fast.next == null) ? null : fast.next.next;
		}
		
		return p;
	}

}
