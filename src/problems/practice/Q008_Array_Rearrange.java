/**
 * Given an unsorted array A[N] with positive, negative numbers and zeros.
 * How to move all negative numbers to the left side of the array, move zeros in the middle of the array, 
 * and move positive numbers to the right side of the array?
 * 
 * Requirements: O(n) time complexity.
 */

package problems.practice;

import java.util.*;

public class Q008_Array_Rearrange {
	
	public static void main(String[] args) {
		int[] A = {3, -1, 5, 0, -2, -4, 8, 0, 9};
		rearrange(A);
		System.out.println(Arrays.toString(A));
	}
	
	// method 1. basic
	public static void rearrange(int[] A) {
		if (A == null || A.length == 1) {
			return;
		}
		
		int i = 0, k = 0, j = A.length - 1;
		
		while (k <= j) {
			if (A[k] > 0) {
				swap(A, k, j--);
			}
			else if (A[k] < 0) {
				swap(A, i++, k++);
			}
			else {
				k++;
			}
		}
	}
	
	// method 2. keep appearance order
	public static void rearrangeAndKeepOrder(int[] A) {
		if (A == null || A.length == 1) {
			return;
		}
	}
	
	public static void swap(int[] A, int i, int j) {
		int tmp = A[i];
		A[i] = A[j];
		A[j] = tmp;
	}

}
