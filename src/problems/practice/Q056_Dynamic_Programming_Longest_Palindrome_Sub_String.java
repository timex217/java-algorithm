package problems.practice;

import java.util.*;

public class Q056_Dynamic_Programming_Longest_Palindrome_Sub_String {
	
	public static void main(String[] args) {
		String s = "xxabcbade";
		System.out.println(lps(s));
	}
	
	public static int lps(String s) {
		if (s == null) {
			return 0;
		}
		
		int n = s.length();
		int max = 0;
		int start = 0;
		
		// lp(i, j) represents from s[i] to s[j] can form a palindrome
		// case 1: lp(i, j) = true, (i == j)
		// case 2: two characters, lp(i, j) = true, (s[i] == s[j])
		// case 3: lp(i, j) = true, (s[i] == s[j] and lp(i+1, j-1) = true)
		// case 4: lp(i, j) = false, otherwise
		boolean[][] lp = new boolean[n][n];
		
		// case 1
		for (int i = 0; i < n; i++) {
			lp[i][i] = true;
		}
		
		// case 2
		for (int i = 0; i < n - 1; i++) {
			if (s.charAt(i) == s.charAt(i + 1)) {
				lp[i][i + 1] = true;
				start = i;
				max = 2;
			}
		}
		
		// case 3
		for (int k = 3; k <= n; k++) {
			for (int i = 0; i < n - k + 1; i++) {
				int j = i + k - 1;
				if (s.charAt(i) == s.charAt(j) && lp[i + 1][j - 1]) {
					lp[i][j] = true;
					
					if (k > max) {
						start = i;
						max = k;
					}
				}
			}
		}
		
		// print out the longest palindromic substring
		StringBuilder sb = new StringBuilder();
		
		for (int i = start; i < start + max; i++) {
			sb.append(s.charAt(i));
		}
		
		System.out.println(sb.toString());
		
		return max;
	}

}
