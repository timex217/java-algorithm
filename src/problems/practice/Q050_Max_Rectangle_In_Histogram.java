package problems.practice;

import java.util.*;

public class Q050_Max_Rectangle_In_Histogram {
	
	public static void main(String[] args) {
		int[] A = {2, 1, 3, 4, 5, 2, 6};
		int max = findMaxRectangleInHistogram1(A);
		System.out.format("max rectangle is: %d\n", max);
	}
	
	// method 2: O(n)
	public static int findMaxRectangleInHistogram(int[] A) {
		if (A == null) {
			return 0;
		}
		
		Stack<Integer> stack = new Stack<Integer>();
		
		int n = A.length, max = 0;
		
		for (int i = 0; i < n; i++) {
			
			while (!stack.isEmpty() && A[i] <= A[stack.peek()]) {
				int index = stack.pop();
				int left = stack.isEmpty() ? 0 : stack.peek() + 1;
				
				max = Math.max(max, A[index] * (i - left));
			}
			
			stack.push(i);
		}
		
		while (!stack.isEmpty()) {
			int index = stack.pop();
			int left = stack.isEmpty() ? 0 : stack.peek() + 1;
			
			max = Math.max(max, A[index] * (n - left));
		}
		
		return max;
	}
	
	
	// method 1: O(n^2)
	public static int findMaxRectangleInHistogram1(int[] A) {
		// sanity check
		if (A == null) {
			return 0;
		}
		
		int max = 0, max_left = 0, max_right = 0, height = 0;
		
		for (int i = 0; i < A.length; i++) {
			// find the left border
			int left = i;
			while (left > 0 && A[i] <= A[left - 1]) {
				left--;
			}
			
			// find the right border
			int right = i;
			while (right < A.length - 1 && A[i] <= A[right + 1]) {
				right++;
			}
			
			int curr = A[i] * (right - left + 1);
			if (curr > max) {
				max = curr;
				max_left = left;
				max_right = right;
				height = A[i];
			}
		}
		
		System.out.format("%d - %d, height is %d\n", A[max_left], A[max_right], height);
		
		return max;
	}

}
