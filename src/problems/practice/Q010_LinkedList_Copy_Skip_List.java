package problems.practice;

import java.util.*;

public class Q010_LinkedList_Copy_Skip_List {
	
	private static class ListNode {
		int val;
		ListNode next;
		ListNode forward;
		
		ListNode(int x) {
			this.val = x;
			this.next = null;
			this.forward = null;
		}
	}
	
	public static ListNode copySkipList(ListNode head) {
		ListNode head2 = null, p1 = head, p2 = null;
		
		// use a map to store the 1 - 1 relationship
		HashMap<ListNode, ListNode> map = new HashMap<ListNode, ListNode>();
		
		// step 1. copy the skip list with next pointer only
		while (p1 != null) {
			ListNode p = new ListNode(p1.val);
			
			map.put(p1, p);
			
			// create new head
			if (head2 == null) {
				head2 = p;
			}
			else {
				p2.next = p;
			}
			
			// move on
			p2 = p;
		}
		
		// step 2. assign the forward pointer
		p1 = head;
		while (p1 != null) {
			p2 = map.get(p1);
			p2.forward = map.get(p1.forward);
		}
		
		return head2;
	}
	
	public static void main(String[] args) {
		
	}

}
