package problems.practice;

public class Q015_String_Transform {
	
	public static void main(String[] args) {
		String s = "d3a0b1c2f4";
	    s = transform(s);
	    System.out.println(s);
	}
	
	public static String transform(String s) {
		if (s == null || s.length() == 0) return s;
		
		int i, j, k;
		int n = s.length();
		
		// count expand and collapse #
		int plus = 0, minus = 0;
		for (i = 0; i < n; i++) {
			char ch = s.charAt(i);
			if (ch == '0')                  minus += 2;
			else if (ch == '1')             minus += 1;
			else if (ch > '1' && ch <= '9') plus += ch - '0' - 1;
		}
		
		// step 1. expand
		// for in place operation, we use one char array
		for (k = 0; k < plus - minus; k++) s += " ";
		
		char[] chars = s.toCharArray();
		int len = chars.length;
		
		// copy from behind
		i = n - 1; j = len - 1;
		
		while (i >= 0) {
			char ch = chars[i];
			if (ch > '1' && ch <= '9') {
				k = ch - '0';
				while (k > 1) {
					chars[j--] = chars[i - 1];
					k--;
				}
			} else {
				chars[j--] = chars[i];
			}
			i--;
		}
		
		//System.out.println(new String(chars));
		
		// step 2. collapse
		i = 0; j = 0;
		
		while (i < len) {
			if (i < len - 1) {
				char next = chars[i + 1];
				
				if (next == '0') {
					i += 2;
					continue;
				} else if (next == '1') {
					chars[j++] = chars[i];
					i += 2;
					continue;
				}
			}
			
			chars[j++] = chars[i++];
		}
		
		s = new String(chars);
		
		return s.substring(0, j);
	}

}
