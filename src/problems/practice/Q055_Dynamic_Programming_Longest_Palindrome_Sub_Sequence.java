package problems.practice;

import java.util.*;

public class Q055_Dynamic_Programming_Longest_Palindrome_Sub_Sequence {
	
	public static void main(String[] args) {
		String s = "xaelgcbaoooogleab";
		System.out.println(lps(s));
	}
	
	public static String lps(String s) {
		if (s == null || s.length() == 0) {
			return s;
		}
		
		int n = s.length();
		
		// lp(i, j) represents the length of longest palindrome sub sequence between s[i] and s[j]
		// lp(i, j) = 1, (i == j)
		// lp(i, j) = 2 + lp(i+1, j-1), (s[i] == s[j])
		// lp(i, j) = max{lp(i+1, j), lp(i, j-1)}, (s[i] != s[j])
		int[][] lp = new int[n][n];
		
		for (int i = n - 1; i >= 0; i--) {
			for (int j = i; j < n; j++) {
				if (i == j) {
					lp[i][j] = 1;
				}
				else if (s.charAt(i) == s.charAt(j)) {
					lp[i][j] = 2 + lp[i + 1][j - 1];
				}
				else {
					lp[i][j] = Math.max(lp[i + 1][j], lp[i][j - 1]);
				}
			}
		}
		
		print(lp);
		
		// find the longest sub sequence
		StringBuffer sb = new StringBuffer();
		
		int i = 0, j = n - 1;
		while (lp[i][j] != 0) {
			if (lp[i][j] > lp[i][j - 1] && lp[i][j] > lp[i + 1][j]) {
				sb.append(s.charAt(j));
				i++;
				j--;
			}
			else if (lp[i][j] == lp[i][j - 1]) {
				j--;
			}
			else {
				i++;
			}
		}
	
		return sb.toString() + sb.reverse().toString();
	}
	
	public static void print(int[][] M) {
		int m = M.length;
		int n = M[0].length;
		
		for (int i = 0; i < m; i++) {
			StringBuffer sb = new StringBuffer();
			
			for (int j = 0; j < n; j++) {
				sb.append(M[i][j]).append("\t");
			}
			
			System.out.println(sb.toString());
		}
	}

}
