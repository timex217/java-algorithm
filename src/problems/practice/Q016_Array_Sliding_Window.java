/**
 * Sliding window of size k, always return the max element in the window size.
 */

package problems.practice;

import java.util.*;

public class Q016_Array_Sliding_Window {
	
	public static void main(String[] args) {
		int[] A = {1, 3, 2, 5, 8, 9, 4, 7, 3};
		int[] B = slideWindow(A, 3);
		
		System.out.println(Arrays.toString(B));
	}
	
	public static int[] slideWindow(int[] A, int w) {
		if (A == null || A.length == 0 || w < 1) {
			return null;
		}
		
		// use a deque to control the window
		Deque<Integer> q = new ArrayDeque<Integer>();
		
		int n = A.length;
		int[] B = new int[n - w + 1];
		
		int i = 0;
		
		// initialize the window
		for (i = 0; i < w; i++) {
			while (!q.isEmpty() && A[i] >= A[q.peekLast()]) {
				q.removeLast();
			}
			q.add(i);
		}
		
		for (i = w; i < n; i++) {
			B[i - w] = A[q.peekFirst()];
			
			// pop those smaller ones from behind
			while (!q.isEmpty() && A[i] >= A[q.peekLast()]) {
				q.removeLast();
			}
			
			// exceeds window size
			while (!q.isEmpty() && (i >= q.peekFirst() + w)) {
				q.removeFirst();
			}
			
			q.add(i);
		}
		
		B[i - w] = A[q.peekFirst()];
		
		return B;
	}
	
}
