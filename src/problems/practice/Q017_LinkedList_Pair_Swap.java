package problems.practice;

public class Q017_LinkedList_Pair_Swap {

	private static class ListNode {
		int val;
		ListNode next;
		
		ListNode(int x) {
			val = x;
			next = null;
		}
	}
	
	public static void main(String[] args) {
		ListNode head = createList(4);
		print(head);
		
		head = swapPair(head);
		print(head);
	}
	
	// Recursion
	public static ListNode swapPair(ListNode head) {
		if (head == null || head.next == null) {
			return head;
		}
		
		ListNode new_head = head.next;
		ListNode next_head = new_head.next;
		
		new_head.next = head;
		head.next = swapPair(next_head);
		
		return new_head;
	}
	
	// Non-recursion
	public static ListNode swapPair2(ListNode head) {
		if (head == null || head.next == null) {
			return head;
		}
		
		// get the new head
		ListNode new_head = head.next;
		
		ListNode curr = head;
		ListNode next = null;
		ListNode tmp = null;
		
		while (curr != null && curr.next != null) {
			// cache
			next = curr.next;
			tmp = next.next;
			
			next.next = curr;
			curr.next = (tmp != null && tmp.next != null) ? tmp.next : tmp;
			curr = tmp;
		}
		
		return new_head;
	}
	
	// helper functions
	public static ListNode createList(int n) {
		ListNode head = null;
		ListNode tail = null;
		
		int i = 0;
		while (++i <= n) {
			ListNode node = new ListNode(i);
			
			if (head == null) {
				head = node;
			}
			else {
				tail.next = node;
			}
			
			tail = node;
		}
		
		return head;
	}
	
	public static void print(ListNode head) {
		ListNode p = head;
		while (p != null) {
			System.out.format("%d ", p.val);
			p = p.next;
		}
		System.out.println();
	}

}
