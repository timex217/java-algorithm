/**
 * How to reconstruct a BST with preorder and inorder sequences of all nodes.
 * (all numbers are unique)
 * 
 *          10
 *      5        15
 *   2    7    12  20
 */

package problems.practice;

import java.util.*;

public class Q053_Binary_Tree_Reconstruction_Pre_In {
	
	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		
		TreeNode(int x) {
			this.val = x;
			this.left = null;
			this.right = null;
		}
	}
	
	public static void main(String[] args) {
		int[] pre = {10, 5, 2, 7,  15, 12, 20};
		int[] in  = {2,  5, 7, 10, 12, 15, 20};
		
		// use a hashmap to store the position info of in order array
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (int i = 0; i < in.length; i++) {
			map.put(in[i], i);
		}
		
		TreeNode tree = reconstruct(pre, 0, pre.length - 1, in, 0, in.length - 1, map);
	}
	
	public static TreeNode reconstruct(int[] pre, int pre_left, int pre_right, int[] in, int in_left, int in_right, HashMap<Integer, Integer> map) {
		// base case
		if (pre_left > pre_right || in_left > in_right) {
			return null;
		}
		
		// create the root node
		int root_val = pre[pre_left];
		TreeNode root = new TreeNode(root_val);
		
		// find the index of the root val in in order
		int index = map.get(root_val);
		
		// recursion
		// (index - in_left): the number of left nodes
		root.left = reconstruct(pre, pre_left + 1, pre_left + (index - in_left), in, in_left, index - 1, map);
		root.right = reconstruct(pre, pre_left + (index - in_left) + 1, pre_right, in, index + 1, in_right, map);
		
		return root;
	}

}
