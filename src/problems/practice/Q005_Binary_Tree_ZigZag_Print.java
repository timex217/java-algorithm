package problems.practice;

import java.util.*;

public class Q005_Binary_Tree_ZigZag_Print {

	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) {
			this.val = x;
			this.left = null;
			this.right = null;
		}
	}
	
	public static void zigzagPrint(TreeNode root) {
		if (root == null) {
			return;
		}
		
		Stack<TreeNode> stack_curr = new Stack<TreeNode>();
		Stack<TreeNode> stack_next = new Stack<TreeNode>();
		
		stack_curr.push(root);
		
		boolean leftToRight = true;
		
		while (!stack_curr.isEmpty()) {
			TreeNode node = stack_curr.pop();
			
			System.out.format("%d ", node.val);
			
			if (leftToRight) {
				if (node.left != null) stack_next.push(node.left);
				if (node.right != null) stack_next.push(node.right);
			}
			else {
				if (node.right != null) stack_next.push(node.right);
				if (node.left != null) stack_next.push(node.left);
			}
			
			if (stack_curr.isEmpty()) {
				System.out.println();
				
				leftToRight = !leftToRight;
				
				stack_curr = stack_next;
				stack_next = new Stack<TreeNode>();
			}
		}
	}
	
	public static void main(String[] args) {
		
	}

}
