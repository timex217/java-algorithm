package problems.practice;

import java.util.*;

public class Q003_Matrix_Rotation {
	
	public static void main(String[] args) {
		int[][] M = {
			{ 1,  2,  3,  4, 5},
			{16, 17, 18, 19, 6},
			{15, 24, 25, 20, 7},
			{14, 23, 22, 21, 8},
			{13, 12, 11, 10, 9}
		};
			
		rotate(M, 0);
		
		for (int i = 0; i < M.length; i++) {
			System.out.println(Arrays.toString(M[i]));
		}
	}
	
	public static void rotate(int[][] M, int layer) {
		if (M == null || M.length == 0 || M[0].length == 0) {
			return;
		}
		
		int i, j;
		int n = M.length;
		int total_layers = n / 2;
		
		if (layer > total_layers) {
			return;
		}
		
		for (i = 0; i < n - 2 * layer - 1; i++) {
			int tmp = M[layer][layer + i];
			
			// left to top
			M[layer][layer + i] = M[n - 1 - layer - i][layer];
			
			// bottom to left
			M[n - 1 - layer - i][layer] = M[n - 1 - layer][n - 1 - layer - i];
			
			// right to bottom
			M[n - 1 - layer][n - 1 - layer - i] = M[layer + i][n - 1 - layer];
			
			// top to right
			M[layer + i][n - 1 - layer] = tmp;
		}
		
		rotate(M, ++layer);
	}

}
