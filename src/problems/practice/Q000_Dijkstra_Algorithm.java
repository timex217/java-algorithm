/**
 * http://blog.csdn.net/guixunlong/article/details/8470671
 * 
 *     /
 *    /  
 * A0
 *    
 * 
 * 每次从当前的最短路径中选出一个, 并利用该节点优化图中起始点至图中其它各未访问到的节点的距离
 */

package problems.practice;

import java.util.*;

public class Q000_Dijkstra_Algorithm {
	
	// define the max number of vertex in the graph
	public static final int MAX_VERTEX = 100;
	
	public static final int INF = Integer.MAX_VALUE;
	
	// use matrix to represent graph
	public static int[][] graph = new int[MAX_VERTEX][MAX_VERTEX];
	
	// record the distance from vertex 0 to other vertex
	// 用数组D来存储A至其它各节点当前路短的距离, 例如: D[1]为A-B的当前最短距离, D[2]为A-C当前最短距离
	public static int[] d = new int[MAX_VERTEX];
	
	public static int[] prefix = new int[MAX_VERTEX];
	
	public static boolean[] visit = new boolean[MAX_VERTEX];
	
	public static int vertex_number = 6;
	
	// initialize the graph
	public static void buildGraph() {
		for (int i = 0; i < MAX_VERTEX; i++) {
			for (int j = 0; j < MAX_VERTEX; j++) {
				graph[i][j] = INF;
			}
		}
		
		graph[0][1] = 1;
		graph[0][2] = 6;
		graph[0][5] = 10;
		graph[1][3] = 2;
		graph[1][4] = 1;
		graph[2][3] = 4;
		graph[2][4] = 5;
		graph[3][5] = 3;
		graph[4][5] = 1;
	}
	
	// run the dijstra's algorithm
	public static void dijkstra() {
		// default node 0's distance is 0, others are infinite
		for (int i = 0; i < vertex_number; i++) {
			d[i] = (i == 0) ? 0 : INF;
		}
		
		for (int i = 0; i < vertex_number; i++) {
			int min_vertex = 0, min_value = INF;
			
			// 从未处理过的节点中找出d值最小的节点
			for (int j = 0; j < vertex_number; j++) {
				if (d[j] < min_value && !visit[j]) {
					min_value = d[j];
					min_vertex = j;
				}
			}
			
			// mark it as visited
			visit[min_vertex] = true;
			
			for (int j = 0; j < vertex_number; j++) {
				//
				if (graph[min_vertex][j] != INF) {
					if (d[min_vertex] + graph[min_vertex][j] < d[j]) {
						d[j] = d[min_vertex] + graph[min_vertex][j];
						prefix[j] = min_vertex;
					}
				}
			}
		}
	}
	
	public static void print() {
		for (int i = 0; i < vertex_number; i++) {
			System.out.format("Shortest path from 0 to %d is %d:\n", i, d[i]);
			for (int s = i; s != 0; s = prefix[s]) {
				System.out.format("%d->%d\n", prefix[s], s);
			}
		}
	}
	
	public static void main(String[] args) {
		buildGraph();
		dijkstra();
		print();
	}

}
