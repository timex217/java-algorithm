package problems.practice;

public class Q044_Array_Find_Unique_Number {
	
	public static void main(String[] args) {
		int[] A = {1, 1, 2, 3, 3, 4, 4, 5, 5};
		int num = find(A);
		System.out.format("The number is: %d", num);
	}
	
	public static int find(int[] A) {
		int sum = A[0];
		
		for (int i = 1; i < A.length; i++) {
			sum ^= A[i];
		}
		
		return sum;
	}

}
