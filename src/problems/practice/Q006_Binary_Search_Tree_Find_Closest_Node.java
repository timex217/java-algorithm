/**
 * Given a binary search tree, how to find the node with its value closest to a target value x.
 */

package problems.practice;

import java.util.*;

public class Q006_Binary_Search_Tree_Find_Closest_Node {
	
	private static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;
		TreeNode(int x) {
			this.val = x;
			this.left = null;
			this.right = null;
		}
	}
	
	public static TreeNode findClosestNode(TreeNode root, int target) {
		// sanity check
		if (root == null) {
			return null;
		}
		
		TreeNode closest_node = null;
		int closest_val = Integer.MAX_VALUE;
		
		TreeNode node = root;
		
		while (node != null) {
			if (node.val == target) {
				return node;
			}
			
			int dist = Math.abs(node.val - target);
			
			if (dist < closest_val) {
				closest_val = dist;
				closest_node = node;
			}
			
			if (node.val < target) {
				node = node.right;
			}
			else {
				node = node.left;
			}
		}
		
		return closest_node;
	}

}
