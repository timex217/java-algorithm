package problems.practice;

import java.util.*;

public class Q014_Sorting_K_Way_Merge_Array {
	
	public static void main(String[] args) {
		
	}
	
	private static class Node {
		int val;
		int index;
		int array_index;
		
		Node(int x, int idx, int array_idx) {
			this.val = x;
			this.index = idx;
			this.array_index = array_idx;
		}
	}
	
	// Method 1 - min heap
	public List<Integer> mergeKArraysWithMinHeap(List<ArrayList<Integer>> arrays) {
		if (arrays == null) {
			return null;
		}
		
		List<Integer> result = new ArrayList<Integer>();
		
		int k = arrays.size();
		
		// create a min heap
		PriorityQueue<Node> heap = new PriorityQueue<Node>(k, new Comparator<Node>() {
			@Override
			public int compare(Node node1, Node node2) {
				return node1.val - node2.val;
			}
		});
		
		// insert the k arrays head to the heap
		for (int i = 0; i < k; i++) {
			ArrayList<Integer> array = arrays.get(i);
			
			int val = array.get(0);
			Node node = new Node(val, 0, i);
		}
		
		while (!heap.isEmpty()) {
			Node node = heap.poll();
			result.add(node.val);
			
			int index = node.index;
			int array_index = node.array_index;
			ArrayList<Integer> array = arrays.get(array_index);
			
			if (index < array.size() - 1) {
				int val = array.get(index + 1);
				heap.offer(new Node(val, index + 1, array_index));
			}
		}
		
		return result;
	}
	

	// Method 2 - normal merge
	public List<Integer> mergeKArrays(List<ArrayList<Integer>> arrays) {
		if (arrays == null || arrays.size() == 0) {
			return null;
		}
		
		return mergeKArrays(arrays, 0, arrays.size() - 1);
	}
	
	public List<Integer> mergeKArrays(List<ArrayList<Integer>> arrays, int lo, int hi) {
		if (lo < hi) {
			int mid = lo + (hi - lo) / 2;
			
			List left = mergeKArrays(arrays, lo, mid);
			List right = mergeKArrays(arrays, mid + 1, hi);
			
			return mergeArrays(left, right);
		}
		else {
			return arrays.get(lo);
		}
	}
	
	public List<Integer> mergeArrays(List<Integer> array1, List<Integer> array2) {
		if (array1 == null && array2 == null) return null;
		if (array1 == null) return array2;
		if (array2 == null) return array1;
		
		List<Integer> array = new ArrayList<Integer>();
		
		int i = 0, j = 0, v1, v2;
		
		while (i < array1.size() && j < array2.size()) {
			v1 = array1.get(i);
			v2 = array2.get(j);
			
			if (v1 < v2) {
				array.add(v1);
				i++;
			}
			else {
				array.add(v2);
				j++;
			}
		}
		
		while (i < array1.size()) {
			v1 = array1.get(i);
			array.add(v1);
		}
		
		while (j < array2.size()) {
			v2 = array2.get(j);
			array.add(v2);
		}
		
		return array;
	}
}
