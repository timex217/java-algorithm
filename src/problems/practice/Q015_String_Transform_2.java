package problems.practice;

public class Q015_String_Transform_2 {
	
	public static void main(String[] args) {
		String s = "abbbcdfffffffbba";
	    s = transform(s);
	    System.out.println(s);
	}
	
	public static String transform(String s) {
		if (s == null || s.length() == 0) {
			return s;
		}
		
		StringBuffer sb = new StringBuffer();
		
		int i = 0, n = s.length();
		
		while (i < n) {
			char ch = s.charAt(i);
			
			int j = i + 1, count = 1;
			
			while (j < n && s.charAt(j++) == ch) {
				count++;
			}
			
			sb.append(ch).append(count);
			
			i += count;
		}
		
		return sb.toString();
	}

}
