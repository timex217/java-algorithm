/**
 * Clone an undirected graph. Each node in the graph contains a label and a list of its neighbors.
 */

package problems.practice;

import java.util.*;

public class Q011_Graph_Copy {
	
	private static class GraphNode {
		int val;
		List<GraphNode> neighbors;
		
		GraphNode(int x) { 
			val = x; 
			neighbors = new ArrayList<GraphNode>();
		}
	}
	
	public GraphNode cloneGraph(GraphNode node) {
        if (node == null) {
			return null;
		}
		
		// make a copy of the node
		GraphNode node_copy = new GraphNode(node.val);
		
		// use a hash map to store the relationship between the original and copied nodes
		HashMap<GraphNode, GraphNode> map = new HashMap<GraphNode, GraphNode>();
		
		map.put(node, node_copy);
		
		// use bfs to traverse the graph
		Queue<GraphNode> queue = new LinkedList<GraphNode>();
		queue.add(node);
		
		// bfs
		while (!queue.isEmpty()) {
			GraphNode p = queue.poll();
			GraphNode p_copy = map.get(p);
			
		    // loop through p's neighbors
		    for (int i = 0; i < p.neighbors.size(); i++) {
		        GraphNode p_neighbor = p.neighbors.get(i);
		        
		        // for each neighbor, if it's not visited
		        // make a copy of it and map it, put it to the queue
		        if (!map.containsKey(p_neighbor)) {
		            GraphNode p_neighbor_copy = new GraphNode(p_neighbor.val);
			        map.put(p_neighbor, p_neighbor_copy);
			        queue.add(p_neighbor);
		        }
		        
		        // link the neighbors
		        p_copy.neighbors.add(map.get(p_neighbor));
		    }
		}
		
		// done
		return node_copy;
    }
	
	public static void main(String[] args) {
		
	}

}
