package problems.practice;

public class Q039_Voting_Algorithm_Half {
	
	public static void main(String[] args) {
		int[] A = {1, 2, 2, 1, 5, 1, 4, 5, 1, 7, 8, 1, 1, 9, 3, 1, 0, 1, 1, 1};
		int number = find(A);
		System.out.format("The number is: %d\n", number);
	}
	
	public static int find(int[] A) {
		Integer number = null;
		int count = 0;
		
		for (int i = 0; i < A.length; i++) {
			if (number == null) {
				number = A[i];
				count = 1;
			}
			else if (number == A[i]) {
				count++;
			}
			else {
				if (--count == 0) {
					number = null;
				}
			}
		}
		
		return number;
	}

}
