/**
 * How to merge k sorted array into one big sorted array?
 * 
 * Hint: Use a min heap
 */

package problems.practice;

import java.util.*;

public class Q013_Sorting_K_Way_Merge_LinkedList {
	
	public static class ListNode {
		int val;
		ListNode next;
		
		ListNode(int x) {
			val = x;
			next = null;
		}
	}
	
	/**
	 * Method 1 - min heap
	 */
	public ListNode mergeKListsWithMinHeap(List<ListNode> lists) {
        if (lists == null) {
        	return null;
        }
        
        int k = lists.size();
        
        // create a min heap
        PriorityQueue<ListNode> heap = new PriorityQueue<ListNode>(k, new Comparator<ListNode>(){
            @Override
            public int compare(ListNode n1, ListNode n2) {
                return n1.val - n2.val;
            }
        });
        
        // insert the k lists head to the heap
        for (int i = 0; i < k; i++) {
            ListNode node = lists.get(i);
            if (node != null) {
                heap.offer(node);
            }
        }
        
        ListNode head = null;
        ListNode tail = head;
        
        while (heap.size() > 0) {
            ListNode curr = heap.poll();
            
            if (head == null) {
                head = curr;
                tail = head;
            } 
            else {
                tail.next = curr;
            }
            
            tail = curr;
            
            if (curr.next != null) {
                heap.offer(curr.next);
            }
        }
        
        return head;
    }
	
	
	/**
	 * Method 2 - normal merge
	 */
	public ListNode mergeKLists(List<ListNode> lists) {
        if (lists == null || lists.size() == 0) {
        	return null;
        }
        
        return mergeKLists(lists, 0, lists.size() - 1);
    }
    
    // merge lists lo ~ hi
    private ListNode mergeKLists(List<ListNode> lists, int lo, int hi) {
        if (lo < hi) {
            int mid = lo + (hi - lo) / 2;
            ListNode left = mergeKLists(lists, lo, mid);
            ListNode right = mergeKLists(lists, mid + 1, hi);
            return mergeLists(left, right);
        } 
        else {
            return lists.get(lo);
        }
    }
    
    private ListNode mergeLists(ListNode l1, ListNode l2) {
        if (l1 == null && l2 == null) return null;
        if (l1 == null) return l2;
        if (l2 == null) return l1;
        
        ListNode head, p1, p2;
        
        if (l1.val < l2.val) {
            head = l1;
            p1 = l1;
            p2 = l2;
        } 
        else {
            head = l2;
            p1 = l2;
            p2 = l1;
        }
        
        while (p1.next != null && p2 != null) {
            if (p1.next.val > p2.val) {
                ListNode tmp = p2.next;
                p2.next = p1.next;
                p1.next = p2;
                p2 = tmp;
            }
            p1 = p1.next;
        }
        
        if (p1.next == null) {
            p1.next = p2;
        }
        
        return head;
    }
    
    public static void main(String[] args) {
		
	}

}
