package problems.practice;

public class Q002_Matrix_Spiral_Print {
	
	public static void main(String[] args) {
		int[][] M = {
			{ 1,  2,  3,  4, 5},
			{16, 17, 18, 19, 6},
			{15, 24, 25, 20, 7},
			{14, 23, 22, 21, 8},
			{13, 12, 11, 10, 9}
		};
		
		spiralPrint(M, 0);
	}
	
	public static void spiralPrint(int[][] M, int layer) {
		if (M == null || M.length == 0 || M[0].length == 0) {
			return;
		}
		
		int i, j;
		int m = M.length, n = M[0].length;
		int total_layers = (int)Math.ceil(Math.min(m, n) / 2.0);
		
		if (layer >= total_layers) {
			return;
		}
		
		// only one row is left
		if (layer == m - layer - 1) {
			for (j = layer; j < n - layer; j++) {
				System.out.format("%d ", M[layer][j]);
			}
			return;
		}
		
		// only one column is left
		if (layer == n - layer - 1) {
			for (i = layer; i < m - layer; i++) {
				System.out.format("%d ", M[i][layer]);
			}
			return;
		}
		
		// top
		for (j = layer; j < n - layer - 1; j++) {
			System.out.format("%d ", M[layer][j]);
		}
		
		// right
		for (i = layer; i < m - layer - 1; i++) {
			System.out.format("%d ", M[i][n - layer - 1]);
		}
		
		// bottom
		for (j = n - layer - 1; j > layer; j--) {
			System.out.format("%d ", M[m - layer - 1][j]);
		}
		
		// left
		for (i = m - layer - 1; i > layer; i--) {
			System.out.format("%d ", M[i][layer]);
		}
		
		// recursion
		spiralPrint(M, ++layer);
	}

}
